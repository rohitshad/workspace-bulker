/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.ArticleItemsBean;
import com.shadowinfosystem.cashwrap.bean.BrandBean;
import com.shadowinfosystem.cashwrap.bean.CustomerBean;
import com.shadowinfosystem.cashwrap.bean.InventoryTransactionBean;
import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.MasterParameterBean;
import com.shadowinfosystem.cashwrap.bean.MessagePackBean;
import com.shadowinfosystem.cashwrap.bean.ModemBean;
import com.shadowinfosystem.cashwrap.bean.ReportManagerBean;
import com.shadowinfosystem.cashwrap.bean.SectionBean;
import com.shadowinfosystem.cashwrap.bean.StockBean;
import com.shadowinfosystem.cashwrap.bean.SubSectionBean;
import com.shadowinfosystem.cashwrap.bean.TaxBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.Invoice;
import com.shadowinfosystem.cashwrap.entities.InvoiceCreditReceiptDet;
import com.shadowinfosystem.cashwrap.entities.InvoiceLines;
import com.shadowinfosystem.cashwrap.entities.InvoicePayDet;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "invoice")
@SessionScoped
public class InvoiceBean {
	Invoice item;
	List<Invoice> list;
	@ManagedProperty("#{stock}")
	StockBean stock;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	@ManagedProperty("#{section}")
	SectionBean section;
	@ManagedProperty("#{subSection}")
	SubSectionBean subSection;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{masterParameter}")
	MasterParameterBean mastParam;
	@ManagedProperty("#{modem}")
	ModemBean modem;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{cust}")
	CustomerBean cust;
	@ManagedProperty("#{brand}")
	BrandBean brand;
	@ManagedProperty("#{messagePack}")
	MessagePackBean messagePack;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public InvoiceBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new Invoice();
		}

	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Invoice getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Invoice");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
			this.item.setCancelAllowed(temp.isCancelAllowed());
		}

		return this.item;
	}

	public CustomerBean getCust() {
		return this.cust;
	}

	public void setCust(CustomerBean cust) {
		this.cust = cust;
	}

	public void setItem(Invoice item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public ModemBean getModem() {
		return this.modem;
	}

	public void setModem(ModemBean modem) {
		this.modem = modem;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public SectionBean getSection() {
		return this.section;
	}

	public void setSection(SectionBean section) {
		this.section = section;
	}

	public SubSectionBean getSubSection() {
		return this.subSection;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public void setSubSection(SubSectionBean subSection) {
		this.subSection = subSection;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public MasterParameterBean getMastParam() {
		return this.mastParam;
	}

	public void setMastParam(MasterParameterBean mastParam) {
		this.mastParam = mastParam;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public List<Invoice> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<Invoice> list) {
		this.list = list;
	}

	public String setAndSearchItems(String itm_Code) {
		this.item.getInvoiceLineSelected().setItemCode(itm_Code);
		this.searchItems();
		return "success";
	}

	public String setItemCode(String itmcode) {
		this.articleItems.list.clear();
		this.item.getInvoiceLineSelected().setItemCode(itmcode);
		this.selectItem();
		return "Invoice";
	}

	public BrandBean getBrand() {
		return this.brand;
	}

	public void setBrand(BrandBean brand) {
		this.brand = brand;
	}

	public MessagePackBean getMessagePack() {
		return this.messagePack;
	}

	public void setMessagePack(MessagePackBean messagePack) {
		this.messagePack = messagePack;
	}

	public String searchItems() {
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getInvoiceLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getInvoiceLineSelected().getItemCode());
		this.articleItems.getFilter().setArticle_no(this.item.getInvoiceLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getInvoiceLineSelected().getItemDesc());
		this.articleItems.getFilter().setBar_code(this.item.getInvoiceLineSelected().getBarCode());
		this.articleItems.getList();
		if (this.articleItems.getList().size() == 1) {
			this.item.getInvoiceLineSelected()
					.setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.selectItem();
		}

		return this.articleItems.getList().size() > 1 ? "Invoice" : "Invoice";
	}

	public String selectItem() {
		FacesMessage fm;
		try {
			InvoiceLines ex = this.item.getInvoiceLineSelected();
			this.articleItems.clearFilter();
			this.articleItems.getFilter().setItem_code(ex.getItemCode());
			this.articleItems.getList();
			if (this.articleItems.getList().size() == 1) {
				this.articleItems.setItem((ArticleItems) this.articleItems.getList().get(0));
			}

			this.stock.addNew();
			this.stock.load(ex.getItemCode());
			if (this.mastParam.item.getIsBillOnZeroBal() != null
					&& this.mastParam.item.getIsBillOnZeroBal().equals("No")) {
				if (this.stock.getItem().getQtyOnHand() <= ex.getQty()) {
					this.facesContext = FacesContext.getCurrentInstance();
					fm = new FacesMessage("Sufficient Stock not available.");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					return "Invoice";
				}

				ex.setBatch(this.stock.getItem().getBatch());
				ex.setExpDate(this.stock.getItem().getExpDate());
			}

			double fm1 = 0.0D;
			double subSecTax = 0.0D;
			double secDisc = 0.0D;
			double subSecPerc = 0.0D;
			double tax = 0.0D;
			double disc = 0.0D;
			if (this.articleItems.item.getArticle_group() != null) {
				this.section.load(this.articleItems.item.getArticle_group());
				fm1 = this.section.item.getTaxPerc();
				secDisc = this.section.item.getDiscPerc();
			}

			if (this.articleItems.item.getArticle_sub_group() != null
					&& this.articleItems.item.getArticle_group() != null) {
				this.subSection.load(this.articleItems.item.getArticle_group(),
						this.articleItems.item.getArticle_group());
				subSecTax = this.subSection.item.getTaxPerc();
				subSecPerc = this.subSection.item.getDiscPerc();
			}

			if (this.articleItems.item.getTax_per() > 0.0D) {
				tax = this.articleItems.item.getTax_per();
			} else if (subSecTax > 0.0D) {
				tax = subSecTax;
			} else {
				tax = fm1;
			}

			if (subSecPerc > 0.0D) {
				disc = subSecPerc;
			} else {
				disc = secDisc;
			}

			double brandDisc = 0.0D;
			if (this.articleItems.item.getManufacturer() != null
					&& this.articleItems.item.getManufacturer().length() > 0) {
				this.brand.load(this.articleItems.item.getManufacturer());
				brandDisc = this.brand.getItem().getDiscPerc();
			}

			if (disc < brandDisc) {
				disc = brandDisc;
			}

			ex.setMrp(this.articleItems.item.getMrp());
			if (this.cust.item.getWsDiscAllowed() != null && this.cust.item.getWsDiscAllowed().equals("Y")) {
				ex.setSalesPrice(this.articleItems.item.getWsp());
			} else {
				ex.setSalesPrice(this.articleItems.item.getSales_price());
			}

			ex.setSalesPriceTemp(ex.getSalesPrice());
			ex.setTaxPer(tax);
			ex.setTaxAmt(ex.getSalesPrice() * tax / 100.0D);
			ex.setDiscPer(disc);
			ex.setDiscAmt(ex.getSalesPrice() * disc / 100.0D);
			ex.setQty(1.0D);
			ex.setUom(this.articleItems.item.getInventory_uom());
			ex.setNetAmt(ex.getQty() * ex.getSalesPrice() - ex.getDiscAmt() + ex.getTaxAmt());
			ex.setSubSection(this.articleItems.item.getArticle_sub_group());
			ex.setSection(this.articleItems.item.getArticle_group());
			ex.setColor(this.articleItems.item.getColor());
			ex.setBarCode(this.articleItems.item.getBar_code());
			ex.setArticleNo(this.articleItems.item.getArticle_no());
			ex.setItemDesc(this.articleItems.item.getArticle_desc());
			if (this.stock.item.getItemCode() != null && this.articleItems.getItem().getItem_code() != null
					&& this.stock.item.getItemCode().equals(this.articleItems.getItem().getItem_code())) {
				ex.setQtyOnHand(this.stock.item.getQtyOnHand());
			}

			this.setIsSelected(true);
			this.reProcessSelected();
		} catch (SQLException arg15) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg15.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "success";
	}

	public String setCustomer() throws SQLException {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		CustomerBean cust = (CustomerBean) exctx.getSessionMap().get("cust");
		cust.load(this.item.getCustomerId());
		this.item.setCustomerName(cust.item.getCustName());
		this.item.setCustAddress(cust.item.getAddress());
		this.item.setDiscPer(cust.item.getDiscPer());
		this.item.setCustMob(cust.item.getMob());
		return "success";
	}

	public String taxCodeSelected() throws SQLException {
		InvoiceLines ils = this.item.getInvoiceLineSelected();
		if (this.item.getInvoiceLineSelected().getTaxCode() != null
				&& this.item.getInvoiceLineSelected().getTaxCode().length() > 0) {
			ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
			TaxBean tax = (TaxBean) exctx.getSessionMap().get("tax");
			tax.load(this.item.getInvoiceLineSelected().getTaxCode());
			ils.setTaxPer(tax.getItem().getTaxPer());
		} else {
			ils.setTaxPer(0.0D);
		}

		return this.reProcessSelected();
	}

	public String reProcessSelected() {
		InvoiceLines ils = this.item.getInvoiceLineSelected();
		if (ils.getDiscPer() > this.getMastParam().item.getMaxDiscOnItem()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(
					"Max Disc Allowed is " + this.getMastParam().item.getMaxDiscOnItem() + " %.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		ils.process(this.mastParam.item.getIsTaxInclusive(), this.mastParam.item.getExtraChargeAmt());
		return "success";
	}

	public String resetSelected() {
		this.item.setInvoiceLineSelected(new InvoiceLines());
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.isSelected = false;
		return "Invoice";
	}

	public String addItem() {
		InvoiceLines pl = this.item.getInvoiceLineSelected();
		FacesMessage fm;
		if (pl.getDiscPer() > this.getMastParam().item.getMaxDiscOnItem()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Max Disc Allowed is " + this.getMastParam().item.getMaxDiscOnItem() + " %.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "Invoice";
		} else if (this.mastParam.item.getIsBillOnZeroBal() != null
				&& this.mastParam.item.getIsBillOnZeroBal().equals("No")
				&& this.stock.getItem().getQtyOnHand() < pl.getQty()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Qty available is less than Invoice Qty.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "Invoice";
		} else if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Please select Item.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "Invoice";
		} else {
			pl.process(this.mastParam.item.getIsTaxInclusive(), this.mastParam.item.getExtraChargeAmt());
			pl.setLineNo(this.getNextLineNo());
			pl.setInvoiceNo(this.item.getInvoiceNo());
			this.item.getInvoiceLines().add(0, pl);
			this.item.setInvoiceLineSelected(new InvoiceLines());
			this.item.process(this.mastParam.item.getIsTaxInclusive(), this.mastParam.item.getExtraChargeAmt());
			this.setIsSelected(false);
			return "Invoice";
		}
	}

	public String addNewPayLine() {
		InvoicePayDet pd = new InvoicePayDet();
		pd.setInvoiceNo(this.item.getInvoiceNo());
		double amtInPay = 0.0D;

		InvoicePayDet pl;
		for (Iterator i$ = this.item.getInvPayLines().iterator(); i$.hasNext(); amtInPay += pl.getPayAmt()) {
			pl = (InvoicePayDet) i$.next();
		}

		pd.setPayAmt(this.item.getNetValue() - amtInPay);
		this.item.getInvPayLines().add(pd);
		return "Invoice";
	}

	public String addNewCreditRecDetLine() {
		InvoiceCreditReceiptDet creRecDet = new InvoiceCreditReceiptDet();
		creRecDet.setInvoiceNo(this.item.getInvoiceNo());
		double amtInPay = 0.0D;

		InvoiceCreditReceiptDet pl;
		for (Iterator i$ = this.item.getInvCreditRecDet().iterator(); i$.hasNext(); amtInPay += pl.getPayAmt()) {
			pl = (InvoiceCreditReceiptDet) i$.next();
		}

		creRecDet.setPayAmt(this.item.getCreditAmount() - amtInPay);
		this.item.getInvCreditRecDet().add(creRecDet);
		return "InvoiceCreditReceipt";
	}

	public String removeCreditRecDetLine(InvoiceCreditReceiptDet creRecDet) {
		Connection con = Data.getConnection();
		if (creRecDet.getObjectId() == 0) {
			this.item.getInvCreditRecDet().remove(creRecDet);
			return "InvoiceCreditReceipt";
		} else {
			try {
				creRecDet.delete(con);
				this.item.getInvCreditRecDet().remove(creRecDet);
			} catch (SQLException arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}

			return "InvoiceCreditReceipt";
		}
	}

	public String creditRecPage() {
		Connection con = Data.getConnection();

		try {
			this.item.setInvCreditRecDet(InvoiceCreditReceiptDet.load(con, this.item.getInvoiceNo()));
			con.close();
		} catch (SQLException arg5) {
			Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg5);
		}

		double x = 0.0D;
		Iterator i$ = this.item.getInvPayLines().iterator();

		while (i$.hasNext()) {
			InvoicePayDet pd = (InvoicePayDet) i$.next();
			if (pd.getPayModeId() != null && pd.getPayModeId().equals("CREDIT")) {
				x += pd.getPayAmt();
			}
		}

		this.item.setCreditAmount(x);
		return "InvoiceCreditReceipt";
	}

	public String commitInvCreditRecLines() {
		Connection con = Data.getConnection();

		try {
			con.setAutoCommit(false);
			Iterator e = this.item.getInvCreditRecDet().iterator();

			while (e.hasNext()) {
				InvoiceCreditReceiptDet ex = (InvoiceCreditReceiptDet) e.next();
				ex.update(con);
			}

			con.setAutoCommit(true);
			con.commit();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e1 = new FacesMessage("Updated.");
			e1.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e1);
		} catch (Exception arg17) {
			FacesMessage fm;
			try {
				con.rollback();
			} catch (SQLException arg16) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg16.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}

			try {
				con.setAutoCommit(true);
			} catch (SQLException arg14) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg14.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			} finally {
				try {
					if (con != null && !con.isClosed()) {
						con.close();
					}
				} catch (SQLException arg13) {
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg13);
				}

			}
		}

		return "InvoiceCreditReceipt";
	}

	public String updateItem() {
		InvoiceLines il = this.item.getInvoiceLineSelected();
		this.item.setInvoiceLineSelected(new InvoiceLines());
		this.item.process(this.mastParam.item.getIsTaxInclusive(), this.mastParam.item.getExtraChargeAmt());
		return "Invoice";
	}

	public String getInvoiceList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "InvoiceList";
	}

	public String setSlectedDetail(InvoiceLines line) {
		this.setIsSelected(true);
		this.item.setInvoiceLineSelected(line);
		return "Invoice";
	}

	public String deletDetail(InvoiceLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getObjectId() > 0) {
			try {
				line.delete(con);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		con.close();
		this.item.getInvoiceLines().remove(line);
		return "Invoice";
	}

	public String deletePayDetail(InvoicePayDet line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getObjectId() == 0) {
			this.item.getInvPayLines().remove(line);
			return "Invoice";
		} else {
			try {
				line.delete(con);
				this.item.getInvPayLines().remove(line);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}

			return "Invoice";
		}
	}

	public String newItem() {
		this.item = new Invoice();
		return "Invoice";
	}

	public String load(String inv_no) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setInvoiceNo(inv_no);
		this.item.load(con);
		return "Invoice";
	}

	public String printPdfReport() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("ThermalInvoice");
			this.rep.getParameters().put("invoice_no", this.item.getInvoiceNo());
			this.rep.exportToPdfFile();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Invoice";
	}

	public String printToPrinter() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("ThermalInvoice");
			this.rep.getParameters().put("invoice_no", this.item.getInvoiceNo());
			if (this.login.getUser().getDefaultPrinter() == null
					|| this.login.getUser().getDefaultPrinter().length() <= 0) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage ex = new FacesMessage("Please select your default printer.");
				ex.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, ex);
				throw new Exception("Default Printer is not set.");
			}

			this.rep.exportToPrinter(this.login.getUser().getDefaultPrinter());
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (Exception arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Invoice";
	}

	public String printThermalReport() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("ThermalInvoice");
			this.rep.getParameters().put("invoice_no", this.item.getInvoiceNo());
			this.rep.exportToPdf();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Report";
	}

	public String printA4() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("InvoiceA4");
			this.rep.getParameters().put("invoice_no", this.item.getInvoiceNo());
			this.rep.exportToPdf();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Report";
	}

	public String printA5() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("InvoiceA5");
			this.rep.getParameters().put("invoice_no", this.item.getInvoiceNo());
			this.rep.exportToPdf();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Report";
	}

	public short getNextLineNo() {
		boolean i = false;
		short arg3 = 0;
		Iterator i$ = this.getItem().getInvoiceLines().iterator();

		while (i$.hasNext()) {
			InvoiceLines pl = (InvoiceLines) i$.next();
			if (pl.getLineNo() > arg3) {
				arg3 = pl.getLineNo();
			}
		}

		++arg3;
		return arg3;
	}

	public String processForDiscAmount() {
		double temodiscperc = 0.0D;
		if (this.item.getSubTotal() > 0.0D) {
			temodiscperc = this.item.getDiscAmt() * 100.0D / this.item.getSubTotal();
		} else {
			temodiscperc = 0.0D;
		}

		this.item.setDiscPer(temodiscperc);
		this.process();
		return "Invoice";
	}

	public String process() {
		if (this.getItem().getDiscPer() > this.getMastParam().item.getMaxDiscOnInv()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(
					"Max Disc Allowed " + this.getMastParam().item.getMaxDiscOnItem() + " %.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "Invoice";
		} else {
			this.item.process(this.mastParam.item.getIsTaxInclusive(), this.mastParam.item.getExtraChargeAmt());
			this.item.setTaxInclusiveExclusive(this.mastParam.item.getIsTaxInclusive());
			return "Invoice";
		}
	}

	public String commit() {
		if (this.getItem().getDiscPer() > this.getMastParam().item.getMaxDiscOnInv()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage con1 = new FacesMessage(
					"Max Disc Allowed " + this.getMastParam().item.getMaxDiscOnItem() + " %.");
			con1.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, con1);
			return "Invoice";
		} else {
			this.process();
			Connection con = Data.getConnection();

			FacesMessage fm;
			try {
				if (this.item.getInvoiceNo() == null || this.item.getInvoiceNo().length() == 0) {
					if (this.item.getInvoicePrefix() == null) {
						throw new Exception("Please select Invoice Prefix.");
					}

					this.trans.addNew();
					this.trans.item.setPrefix(this.item.getInvoicePrefix());
					this.trans.item.setTransType("INV");
					this.item.setInvoiceNo(this.trans.getNextOrderNo());
				}

				String e = this.item.update(con);
				this.item.load(con);
				this.articleItems.clearFilter();
				this.articleItems.list.clear();
				con.close();
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(e);
				fm.setSeverity(FacesMessage.SEVERITY_INFO);
				this.facesContext.addMessage((String) null, fm);
			} catch (Exception arg5) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg5.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
				if (con != null) {
					try {
						con.close();
					} catch (SQLException arg4) {
						Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
					}
				}
			}

			return "Invoice";
		}
	}

	public String addNew() {
		this.setItem(new Invoice());
		this.stock.list.clear();
		this.item.setInvoicePrefix(this.trans.getDefaultPrefix("INV"));
		this.articleItems.clearFilter();
		this.articleItems.list.clear();
		return "Invoice";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		return "InvoiceList";
	}

	public String search() {
		this.list.clear();
		return "InvoiceList";
	}

	public String deliver() {
		this.item.process(this.mastParam.item.getIsTaxInclusive(), this.mastParam.item.getExtraChargeAmt());
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				this.item.update(con);
				con.setAutoCommit(false);
				CallableStatement ex = null;
				ex = con.prepareCall("{call invoice_deliver(?)}");
				ex.setString(1, this.getItem().getInvoiceNo());
				ex.executeUpdate();
				ex.close();
				this.item.load(con);
				con.commit();
				con.setAutoCommit(true);
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage("Delivered");
				fm.setSeverity(FacesMessage.SEVERITY_INFO);
				this.facesContext.addMessage((String) null, fm);
				this.printToPrinter();
				if (this.getMastParam().getItem().getUseSmsService() != null
						&& this.getMastParam().getItem().getUseSmsService().equals("Yes")) {
					this.getModem().getSms().setMobNo(this.item.getCustMob());
					this.getModem().getSms()
							.setMsgText(this.getMastParam().getItem().getSmsPrefix() + "Invoice :"
									+ this.getItem().getInvoiceNo() + "\nAmount (Rs.) :" + this.getItem().getNetValue()
									+ "\n" + this.getMastParam().getItem().getSmsSuffix());
					this.getModem().sendSms();
				}

				if (this.getMastParam().getItem().getUserMessagePackService() != null
						&& this.getMastParam().getItem().getUserMessagePackService().equals("Yes")) {
					this.messagePack.getItem().setMobiles(this.item.getCustMob());
					this.messagePack.getItem()
							.setSms(this.getMastParam().getItem().getSmsPrefix() + "Invoice :"
									+ this.getItem().getInvoiceNo() + "\nAmount (Rs.) :" + this.getItem().getNetValue()
									+ "\n" + this.getMastParam().getItem().getSmsSuffix());
					this.messagePack.sendDirectMessage();
				}
			} catch (SQLException arg22) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg22.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);

				try {
					con.rollback();
				} catch (SQLException arg21) {
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg21);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg20) {
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg20);
				}
			} catch (Exception arg23) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg23.getMessage() + " ");
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);

				try {
					con.rollback();
				} catch (SQLException arg19) {
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg23);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg18) {
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg23);
				}
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg17) {
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg17);
				}
			}

		}

		return "Invoice";
	}

	public String prepareInvoice() {
		this.addNew();
		this.stock.addNew();
		this.stock.list.clear();
		this.item.setInvoicePrefix(this.trans.getDefaultPrefix("INV"));
		return "Invoice";
	}

	public String cancelDelivery() {
		FacesMessage con;
		if (this.getItem().getInvoiceNo() != null && this.getItem().getInvoiceNo().length() > 0) {
			if (this.getItem().getStatus() != null && this.getItem().getStatus().equals("Delivered")) {
				Connection con1 = Data.getConnection();
				CallableStatement cs = null;

				try {
					cs = con1.prepareCall("{call cancel_invoice(?)}");
					cs.setString(1, this.getItem().getInvoiceNo());
					cs.executeUpdate();
					cs.close();
					this.item.load(con1);
					con1.close();
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage ex = new FacesMessage("Invoice Delivery Canceled.");
					ex.setSeverity(FacesMessage.SEVERITY_INFO);
					this.facesContext.addMessage((String) null, ex);
				} catch (SQLException arg4) {
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(arg4.getMessage() + " ");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}

				return "Invoice";
			} else {
				this.facesContext = FacesContext.getCurrentInstance();
				con = new FacesMessage("Please select Delivered invoice to Cancel.");
				con.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, con);
				return "Invoice";
			}
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			con = new FacesMessage("Please select invoice to Cancel.");
			con.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, con);
			return "Invoice";
		}
	}
}