/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "custReport")
@SessionScoped
public class CustomerReportsBean extends EntitiesImpl {
	private String custId;
	private Date fromDate;
	private Date toDate;
	private String outputFormat;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	FacesContext facesContext = FacesContext.getCurrentInstance();
	@ManagedProperty("#{login}")
	LoginBean login;

	@PostConstruct
	public void setScreenRights() {
		if (getScreen() == null) {
			setScreen("CustomerReports");
			ScreenRights temp = this.login.getRightsForScreen(getScreen());
			setUserId(this.login.getUserid());
			setAddAddowed(temp.isAddAddowed());
			setViewAddowed(temp.isViewAddowed());
			setModifyAddowed(temp.isModifyAddowed());
			setDeleteAddowed(temp.isDeleteAddowed());
			setPostAddowed(temp.isPostAddowed());
			setPrintAddowed(temp.isPrintAddowed());
		}
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public String prepareReportPage() {
		setCustId(null);
		setFromDate(new Date());
		setToDate(new Date());
		return "CustomerReports";
	}

	public String getCustId() {
		return this.custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public String getOutputFormat() {
		if (this.outputFormat == null) {
			return "pdf";
		}
		return this.outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public String printCustomerLedger() {
		try {
			Connection con = Data.getConnection();

			Integer ResultKey = Integer.valueOf(0);
			try {
				CallableStatement cs = con.prepareCall("{call rep_cust_ledger_rpi(?,?)}");
				String parameter_ = "from_date|" + Utils.getStrDate(getFromDate());
				parameter_ = parameter_ + "^" + "to_date|" + Utils.getStrDate(getToDate());
				if ((getCustId() != null) && (getCustId().length() > 0)) {
					parameter_ = parameter_ + "^" + "cust_id|" + getCustId();
				} else {
					throw new JRException("Please select customer.");
				}
				cs.setString(2, parameter_);
				cs.registerOutParameter(1, 8);
				cs.executeUpdate();
				ResultKey = Integer.valueOf((int) cs.getDouble(1));
			} catch (SQLException ex) {
				Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, null, ex);
			}
			this.rep.getParameters().clear();
			this.rep.getParameters().put("Result_key", ResultKey + "");
			this.rep.setJasperFile("CustomerLedger");
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "CustomerReports";
	}

	public String printCustomerWiseLedger() {
		try {
			Connection con = Data.getConnection();

			Integer ResultKey = Integer.valueOf(0);
			try {
				CallableStatement cs = con.prepareCall("{call rep_cust_wise_ledger_rpi(?,?)}");
				String parameter_ = "from_date|" + Utils.getStrDate(getFromDate());
				parameter_ = parameter_ + "^" + "to_date|" + Utils.getStrDate(getToDate());
				if ((getCustId() != null) && (getCustId().length() > 0)) {
					parameter_ = parameter_ + "^" + "cust_id|" + getCustId();
				}
				cs.setString(2, parameter_);
				cs.registerOutParameter(1, 8);
				cs.executeUpdate();
				ResultKey = Integer.valueOf((int) cs.getDouble(1));
			} catch (SQLException ex) {
				Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, null, ex);
			}
			this.rep.getParameters().clear();
			this.rep.getParameters().put("Result_key", ResultKey + "");
			this.rep.setJasperFile("CustomerWiseLedger");
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "CustomerReports";
	}

	public String printCustomerInvoiceWiseSales() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("CustomerInvoiceWiseSaleReport");
			if ((getCustId() != null) && (getCustId().length() > 0)) {
				this.rep.getParameters().put("cust_id", getCustId());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("from_date", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("to_date", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "CustomerReports";
	}

	public String printCustomerWiseSales() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("CustomerWiseInvoice");
			if ((getCustId() != null) && (getCustId().length() > 0)) {
				this.rep.getParameters().put("cust_id", getCustId());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("from_date", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("to_date", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "CustomerReports";
	}
}
