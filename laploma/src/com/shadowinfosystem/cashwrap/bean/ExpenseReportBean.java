/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name="expenseReport")
@SessionScoped
public class ExpenseReportBean
  extends EntitiesImpl
{
  private String voucherNo;
  private String expenseHead;
  private String empId;
  private String accountId;
  private Date fromDate;
  private Date toDate;
  private String outputFormat;
  @ManagedProperty("#{reportManager}")
  ReportManagerBean rep;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  @PostConstruct
  public void setScreenRights()
  {
    if (getScreen() == null)
    {
      setScreen("ExpenseReport");
      ScreenRights temp = this.login.getRightsForScreen(getScreen());
      setUserId(this.login.getUserid());
      setAddAddowed(temp.isAddAddowed());
      setViewAddowed(temp.isViewAddowed());
      setModifyAddowed(temp.isModifyAddowed());
      setDeleteAddowed(temp.isDeleteAddowed());
      setPostAddowed(temp.isPostAddowed());
      setPrintAddowed(temp.isPrintAddowed());
    }
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public String prepareReportPage()
  {
    setVoucherNo(null);
    setExpenseHead(null);
    setEmpId(null);
    setAccountId(null);
    setFromDate(new Date());
    setToDate(new Date());
    return "ExpenseReport";
  }
  
  public String getVoucherNo()
  {
    return this.voucherNo;
  }
  
  public void setVoucherNo(String voucherNo)
  {
    this.voucherNo = voucherNo;
  }
  
  public String getExpenseHead()
  {
    return this.expenseHead;
  }
  
  public void setExpenseHead(String expenseHead)
  {
    this.expenseHead = expenseHead;
  }
  
  public String getEmpId()
  {
    return this.empId;
  }
  
  public void setEmpId(String empId)
  {
    this.empId = empId;
  }
  
  public String getAccountId()
  {
    return this.accountId;
  }
  
  public void setAccountId(String accountId)
  {
    this.accountId = accountId;
  }
  
  public Date getFromDate()
  {
    return this.fromDate;
  }
  
  public void setFromDate(Date fromDate)
  {
    this.fromDate = fromDate;
  }
  
  public Date getToDate()
  {
    return this.toDate;
  }
  
  public void setToDate(Date toDate)
  {
    this.toDate = toDate;
  }
  
  public ReportManagerBean getRep()
  {
    return this.rep;
  }
  
  public void setRep(ReportManagerBean rep)
  {
    this.rep = rep;
  }
  
  public FacesContext getFacesContext()
  {
    return this.facesContext;
  }
  
  public void setFacesContext(FacesContext facesContext)
  {
    this.facesContext = facesContext;
  }
  
  public String getOutputFormat()
  {
    if (this.outputFormat == null) {
      return "pdf";
    }
    return this.outputFormat;
  }
  
  public void setOutputFormat(String outputFormat)
  {
    this.outputFormat = outputFormat;
  }
  
  public String printExpenseRegister()
  {
    try
    {
      this.rep.getParameters().clear();
      this.rep.setJasperFile("ExpenseRegister");
      if ((getVoucherNo() != null) && (getVoucherNo().length() > 0)) {
        this.rep.getParameters().put("voucher_no", getVoucherNo());
      }
      if ((getEmpId() != null) && (getEmpId().length() > 0)) {
        this.rep.getParameters().put("emp_id", getEmpId());
      }
      if ((getExpenseHead() != null) && (getExpenseHead().length() > 0)) {
        this.rep.getParameters().put("expense_head", getExpenseHead());
      }
      if ((getAccountId() != null) && (getAccountId().length() > 0)) {
        this.rep.getParameters().put("account_id", getAccountId());
      }
      if (getFromDate() != null) {
        this.rep.getParameters().put("from_date", Utils.getStrDate(getFromDate()));
      }
      if (getToDate() != null) {
        this.rep.getParameters().put("to_date", Utils.getStrDate(getToDate()));
      }
      if (getOutputFormat().equalsIgnoreCase("PDF")) {
        this.rep.exportToPdf();
      }
      if (getOutputFormat().equalsIgnoreCase("XLSX")) {
        this.rep.exportToXlsx();
      }
      if (getOutputFormat().equalsIgnoreCase("DOCX")) {
        this.rep.exportToDocx();
      }
      if (getOutputFormat().equalsIgnoreCase("XML")) {
        this.rep.exportToXml();
      }
    }
    catch (JRException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
    }
    catch (IOException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
    }
    return "ExpenseReport";
  }
}
