/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/

package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseOrderBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.AccountTransactions;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "accTrans")
@SessionScoped
public class AccountTransactionBean {
	AccountTransactions item;
	List<AccountTransactions> list;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public AccountTransactionBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new AccountTransactions();
		}

	}

	public AccountTransactions getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("AccountTransactions");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public void setItem(AccountTransactions item) {
		this.item = item;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<AccountTransactions> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.getAccountTransactionsList();
			} catch (SQLException arg2) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg2.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		return this.list;
	}

	public void setList(List<AccountTransactions> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				if (this.item.getTransId() == null || this.item.getTransId().longValue() <= 0L) {
					if (this.item.getTransCode() == null || !this.item.getTransCode().equals("DirectPosting")) {
						throw new Exception("Please Select Trans Code as Direct posting");
					}

					this.item.setTransId(this.item.getNextTransactionId());
				}

				if (this.item.getAmount() <= 0.0D) {
					throw new Exception("Please enter a valid amount.");
				}

				String ex = this.item.update(con);
				this.item.load(con);
				con.close();
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(ex);
				fm.setSeverity(FacesMessage.SEVERITY_INFO);
				this.facesContext.addMessage((String) null, fm);
			} catch (SQLException arg13) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg13.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
				Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg13);
			} catch (Exception arg14) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg14.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
				Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg14);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg12) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg12);
				}
			}

		}

		return "AccountTransaction";
	}

	public String addNew() {
		this.item = new AccountTransactions();
		return "AccountTransaction";
	}

	public String load(Long trans_id) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setTransId(trans_id);
		this.item.load(con);
		return "AccountTransaction";
	}

	public String getAccountTransactionsList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "AccountTransactionsList";
	}

	public String refreshList() throws SQLException {
		this.setItem(new AccountTransactions());
		this.list.clear();
		this.getItem().setFromDate(new Date());
		this.getItem().setToDate(new Date());
		return "AccountTransactionsList";
	}

	public String search() throws SQLException {
		this.getList().clear();
		return "AccountTransactionsList";
	}

	public String prepareList() {
		this.setItem(new AccountTransactions());
		this.list.clear();
		return "AccountTransactionsList";
	}

	public String prepareAdd() {
		this.setItem(new AccountTransactions());
		this.list.clear();
		return "AccountTransaction";
	}
}