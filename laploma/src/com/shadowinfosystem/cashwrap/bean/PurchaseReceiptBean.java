/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.ArticleItemsBean;
import com.shadowinfosystem.cashwrap.bean.InventoryTransactionBean;
import com.shadowinfosystem.cashwrap.bean.InvoiceBean;
import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.ReportManagerBean;
import com.shadowinfosystem.cashwrap.bean.StockBean;
import com.shadowinfosystem.cashwrap.bean.SubSectionBean;
import com.shadowinfosystem.cashwrap.bean.SupplierBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.PurchaseReceipt;
import com.shadowinfosystem.cashwrap.entities.PurchaseReceiptLines;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "pr")
@SessionScoped
public class PurchaseReceiptBean {
	PurchaseReceipt item;
	List<PurchaseReceipt> list;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	@ManagedProperty("#{stock}")
	StockBean stock;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public PurchaseReceiptBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new PurchaseReceipt();
		}

	}

	public PurchaseReceipt getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("PurchaseReceipt");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
			this.item.setCancelAllowed(temp.isCancelAllowed());
		}

		return this.item;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public void setItem(PurchaseReceipt item) {
		this.item = item;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public List<PurchaseReceipt> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<PurchaseReceipt> list) {
		this.list = list;
	}

	public String addItem() {
		PurchaseReceiptLines pl = this.item.getPorLineSelected();
		FacesMessage fm;
		if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Please select Item.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "PurchaseReceipt";
		} else if (pl.getQty().doubleValue() <= 0.0D) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Qty must be > 0.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "PurchaseReceipt";
		} else {
			pl.setLineNo(this.getNextLineNo());
			pl.setPurchaseReceipt(this.item.getPorNo());
			this.item.getPorLines().add(pl);
			this.item.setPorLineSelected(new PurchaseReceiptLines());
			this.item.process();
			this.setIsSelected(false);
			return "PurchaseReceipt";
		}
	}

	public String callBarCodePorc(String itmCode) {
		Connection con = Data.getConnection();
		CallableStatement cs = null;

		try {
			cs = con.prepareCall("{call generate_bar_code(?,?)}");
			cs.setString(1, this.item.getPorNo());
			cs.setString(2, itmCode);
			cs.executeUpdate();
			cs.close();
			con.commit();
		} catch (Exception arg13) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg13.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg12) {
					;
				}
			}

		}

		return "PurchaseReceipt";
	}

	public String printPurchaseBarCode(String itmCode) {
		FacesMessage fm;
		try {
			this.callBarCodePorc(itmCode);
			this.rep.setJasperFile("BarCode5025");
			this.rep.setParameters(new HashMap());
			this.rep.exportToPdf();
		} catch (JRException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseReceipt";
	}

	public String printPurchaseBarCode3825(String itmCode) {
		FacesMessage fm;
		try {
			this.callBarCodePorc(itmCode);
			this.rep.setJasperFile("BarCode3825");
			this.rep.setParameters(new HashMap());
			this.rep.exportToPdf();
		} catch (JRException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseReceipt";
	}

	public String printPurchaseReport(String itmCode) {
		FacesMessage fm;
		try {
			this.rep.setJasperFile("PurchaseReceipt");
			HashMap ex = new HashMap();
			ex.put("POR_NO", this.item.getPorNo());
			this.rep.setParameters(ex);
			this.rep.exportToPdf();
		} catch (JRException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseReceipt";
	}

	public String reProcessSelected() {
		this.item.getPorLineSelected().processLine();
		return "PurchaseReceipt";
	}

	public String updateItem() {
		PurchaseReceiptLines pl = this.item.getPorLineSelected();
		this.item.setPorLineSelected(this.item.getPorLineSelected().getDoulicate());
		return "PurchaseReceipt";
	}

	public String getPoList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "PurchaseReceiptList";
	}

	public String setSlectedDetail(PurchaseReceiptLines line) {
		this.item.setPorLineSelected(line);
		return "PurchaseReceipt";
	}

	public String deletDetail(PurchaseReceiptLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getItemCode() != null && line.getItemCode().length() > 0) {
			line.delete(con);
		}

		con.close();
		this.item.getPorLines().remove(line);
		return "PurchaseReceipt";
	}

	public String setSupplier() throws SQLException {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		SupplierBean sup = (SupplierBean) exctx.getSessionMap().get("supp");
		sup.load(this.item.getSupplierId());
		this.item.setSupplierName(sup.item.getSuppName());
		this.item.setSupplierAddress(sup.item.getAddress());
		return "success";
	}

	public String getSubSecList() {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		SubSectionBean ss = (SubSectionBean) exctx.getSessionMap().get("subSection");
		ss.getItem().setSectionNo(this.item.getPorLineSelected().getSection());
		ss.setListSelectOne(ss.getSubSecList());
		return "success";
	}

	public String newItem() {
		this.item = new PurchaseReceipt();
		return "PurchaseReceipt";
	}

	public String load(String porno) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setPorNo(porno);
		this.item.load(con);
		return "PurchaseReceipt";
	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String setItemCode(String itmcode) {
		this.articleItems.getList().clear();
		this.item.getPorLineSelected().setItemCode(itmcode);
		this.searchItemWithItemCode();
		return "PurchaseReceipt";
	}

	public String searchItems() {
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getPorLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getPorLineSelected().getItemCode());
		this.articleItems.getFilter().setArticle_no(this.item.getPorLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getPorLineSelected().getItemDesc());
		this.articleItems.getFilter().setBar_code(this.item.getPorLineSelected().getBarCode());
		if (this.articleItems.getList().size() == 1) {
			this.item.getPorLineSelected()
					.setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.searchItemWithItemCode();
		}

		return this.articleItems.getList().size() > 1 ? "PurchaseReceipt" : "PurchaseReceipt";
	}

	public String searchItemWithItemCode() {
		PurchaseReceiptLines pol = this.item.getPorLineSelected();
		this.articleItems.clearFilter();
		this.articleItems.getFilter().setItem_code(this.getItem().getPorLineSelected().getItemCode());
		this.articleItems.getList();
		if (this.articleItems.list.size() > 0) {
			pol.setQty(Double.valueOf(1.0D));
			pol.setMrp(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getMrp()));
			pol.setSection(((ArticleItems) this.articleItems.list.get(0)).getArticle_group());
			pol.setSubSection(((ArticleItems) this.articleItems.list.get(0)).getArticle_sub_group());
			pol.setColor(((ArticleItems) this.articleItems.list.get(0)).getColor());
			pol.setItemSize(((ArticleItems) this.articleItems.list.get(0)).getArticle_size());
			pol.setUom(((ArticleItems) this.articleItems.list.get(0)).getInventory_uom());
			pol.setGrossPp(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getPur_price()));
			pol.setTaxPerc(((ArticleItems) this.articleItems.list.get(0)).getTax_per());
			pol.setBarCode(((ArticleItems) this.articleItems.list.get(0)).getBar_code());
			pol.setItemDesc(((ArticleItems) this.articleItems.list.get(0)).getArticle_desc());
			pol.setWsp(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getSales_price()));
			pol.setBrand(((ArticleItems) this.articleItems.list.get(0)).getManufacturer());
			pol.setArticleNo(((ArticleItems) this.articleItems.list.get(0)).getArticle_no());
			this.setIsSelected(true);
			pol.processLine();
		} else {
			String tempItemCode = pol.getItemCode();
			this.item.setPorLineSelected(new PurchaseReceiptLines());
			this.item.getPorLineSelected().setItemCode(tempItemCode);
		}

		return "PurchaseReceipt";
	}

	public String receive() throws SQLException, IOException {
		this.stock.list.clear();
		Connection con = null;

		try {
			FacesMessage fm;
			try {
				con = Data.getConnection();
				this.item.update(con);
				con.commit();
				con.setAutoCommit(false);
				CallableStatement ex = null;
				ex = con.prepareCall("{call grn_receive(?)}");
				ex.setString(1, this.getItem().getPorNo());
				ex.executeUpdate();
				ex.close();
				this.item.load(con);
				con.setAutoCommit(true);
				con.commit();
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage("Received.");
				fm.setSeverity(FacesMessage.SEVERITY_INFO);
				this.facesContext.addMessage((String) null, fm);
			} catch (Exception arg6) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg6.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
				con.rollback();
				con.setAutoCommit(true);
			}
		} finally {
			if (con != null && !con.isClosed()) {
				con.close();
			}

		}

		return "PurchaseReceipt";
	}

	public short getNextLineNo() {
		short i = 0;
		Iterator i$ = this.getItem().getPorLines().iterator();

		while (i$.hasNext()) {
			PurchaseReceiptLines pl = (PurchaseReceiptLines) i$.next();
			if (pl.getLineNo() > i) {
				i = pl.getLineNo();
			}
		}

		++i;
		return i;
	}

	public String process() {
		this.item.process();
		return "PurchaseReceipt";
	}

	public String commit() {
		this.process();
		Connection con = Data.getConnection();

		try {
			if (this.item.getPorNo() == null || this.item.getPorNo().trim().length() == 0) {
				this.trans.item.setPrefix((String) null);
				this.trans.item.setTransType("POR");
				this.item.setPorNo(this.trans.getNextOrderNo());
			}

			this.item.update(con);
			this.item.load(con);
			this.articleItems.clearFilter();
			this.articleItems.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseReceiptBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "PurchaseReceipt";
	}

	public String addNew() {
		this.setItem(new PurchaseReceipt());
		this.articleItems.clearFilter();
		this.articleItems.list.clear();
		return "PurchaseReceipt";
	}

	public String search() {
		this.list.clear();
		return "PurchaseReceiptList";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		return "PurchaseReceiptList";
	}

	public String createPurchaseOrderReturn() {
		return "PurchaseOrderReturn";
	}

	public String resetSelected() {
		PurchaseReceiptLines pl = new PurchaseReceiptLines();
		this.item.setPorLineSelected(pl);
		return "PurchaseReceipt";
	}

	public String preparePurchaseReceipt() {
		this.addNew();
		this.list.clear();
		this.trans.addNew();
		this.trans.list.clear();
		return "PurchaseReceipt";
	}

	public String cancelGrn() {
		FacesMessage con;
		if (this.getItem().getPorNo() != null && this.getItem().getPorNo().length() > 0) {
			if (this.getItem().getStatus() != null && this.getItem().getStatus().equals("Received")) {
				Connection con1 = Data.getConnection();
				CallableStatement cs = null;

				try {
					cs = con1.prepareCall("{call cancel_grn(?)}");
					cs.setString(1, this.getItem().getPorNo());
					cs.executeUpdate();
					cs.close();
					this.item.load(con1);
					con1.close();
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage ex = new FacesMessage("Goode Receipt Canceled.");
					ex.setSeverity(FacesMessage.SEVERITY_INFO);
					this.facesContext.addMessage((String) null, ex);
				} catch (SQLException arg4) {
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(arg4.getMessage() + " ");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}

				return "PurchaseReceipt";
			} else {
				this.facesContext = FacesContext.getCurrentInstance();
				con = new FacesMessage("Please select Received PR to Cancel.");
				con.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, con);
				return "PurchaseReceipt";
			}
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			con = new FacesMessage("Please select PR to Cancel.");
			con.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, con);
			return "PurchaseReceipt";
		}
	}
}