/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.ArticleItemsBean;
import com.shadowinfosystem.cashwrap.bean.InventoryTransactionBean;
import com.shadowinfosystem.cashwrap.bean.InvoiceBean;
import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseReceiptBean;
import com.shadowinfosystem.cashwrap.bean.ReportManagerBean;
import com.shadowinfosystem.cashwrap.bean.StockBean;
import com.shadowinfosystem.cashwrap.bean.SubSectionBean;
import com.shadowinfosystem.cashwrap.bean.SupplierBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.PurchaseOrder;
import com.shadowinfosystem.cashwrap.entities.PurchaseOrderLines;
import com.shadowinfosystem.cashwrap.entities.PurchaseReceiptLines;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "po")
@SessionScoped
public class PurchaseOrderBean {
	PurchaseOrder item;
	List<PurchaseOrder> list;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	@ManagedProperty("#{stock}")
	StockBean stock;
	@ManagedProperty("#{pr}")
	PurchaseReceiptBean pr;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public PurchaseOrderBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new PurchaseOrder();
		}

	}

	public PurchaseOrder getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("PurchaseOrder");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
			this.item.setCancelAllowed(temp.isCancelAllowed());
		}

		return this.item;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public PurchaseReceiptBean getPr() {
		return this.pr;
	}

	public void setPr(PurchaseReceiptBean pr) {
		this.pr = pr;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public void setItem(PurchaseOrder item) {
		this.item = item;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public List<PurchaseOrder> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<PurchaseOrder> list) {
		this.list = list;
	}

	public String addItem() {
		PurchaseOrderLines pl = this.item.getPoLineSelected();
		FacesMessage fm;
		if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Please select Item.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "PurchaseOrder";
		} else if (pl.getQty().doubleValue() <= 0.0D) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Qty must be > 0.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "PurchaseOrder";
		} else if (pl.getUom() != null && pl.getUom().length() > 0) {
			pl.setLineNo(this.getNextLineNo());
			pl.setPurchaseOrder(this.item.getPoNo());
			this.item.getPoLines().add(pl);
			this.item.setPoLineSelected(new PurchaseOrderLines());
			this.item.process();
			this.setIsSelected(false);
			return "PurchaseOrder";
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Please select Unit Of Measurement.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "PurchaseOrder";
		}
	}

	public String callBarCodePorc(String itmCode) {
		Connection con = Data.getConnection();
		CallableStatement cs = null;

		try {
			cs = con.prepareCall("{call generate_bar_code_from_po(?,?)}");
			cs.setString(1, this.item.getPoNo());
			cs.setString(2, itmCode);
			cs.executeUpdate();
			cs.close();
			con.commit();
		} catch (Exception arg13) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg13.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg12) {
					;
				}
			}

		}

		return "PurchaseOrder";
	}

	public String printPurchaseBarCode(String itmCode) {
		FacesMessage fm;
		try {
			this.callBarCodePorc(itmCode);
			this.rep.setJasperFile("BarCode5025");
			this.rep.setParameters(new HashMap());
			this.rep.exportToPdf();
		} catch (JRException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseOrder";
	}

	public String printPurchaseBarCode3825(String itmCode) {
		FacesMessage fm;
		try {
			this.callBarCodePorc(itmCode);
			this.rep.setJasperFile("BarCode3825");
			this.rep.setParameters(new HashMap());
			this.rep.exportToPdf();
		} catch (JRException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseOrder";
	}

	public String printPurchaseReport(String itmCode) {
		FacesMessage fm;
		try {
			this.rep.setJasperFile("PurchaseOrder");
			HashMap ex = new HashMap();
			ex.put("PO_NO", this.item.getPoNo());
			this.rep.setParameters(ex);
			this.rep.exportToPdf();
		} catch (JRException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseOrder";
	}

	public String reProcessSelected() {
		this.item.getPoLineSelected().processLine();
		return "PurchaseOrder";
	}

	public String updateItem() {
		PurchaseOrderLines pl = this.item.getPoLineSelected();
		this.item.setPoLineSelected(this.item.getPoLineSelected().getDoulicate());
		return "PurchaseOrder";
	}

	public String getPoList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "PurchaseOrderList";
	}

	public String setSlectedDetail(PurchaseOrderLines line) {
		this.item.setPoLineSelected(line);
		return "PurchaseOrder";
	}

	public String DeletDetail(PurchaseOrderLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getItemCode() != null && line.getItemCode().length() > 0) {
			line.delete(con);
		}

		con.close();
		this.item.getPoLines().remove(line);
		return "PurchaseOrder";
	}

	public String setSupplier() throws SQLException {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		SupplierBean sup = (SupplierBean) exctx.getSessionMap().get("supp");
		sup.load(this.item.getSupplierId());
		this.item.setSupplierName(sup.item.getSuppName());
		this.item.setSupplierAddress(sup.item.getAddress());
		return "success";
	}

	public String getSubSecList() {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		SubSectionBean ss = (SubSectionBean) exctx.getSessionMap().get("subSection");
		ss.getItem().setSectionNo(this.item.getPoLineSelected().getSection());
		ss.setListSelectOne(ss.getSubSecList());
		return "success";
	}

	public String newItem() {
		this.item = new PurchaseOrder();
		return "PurchaseOrder";
	}

	public String load(String pono) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setPoNo(pono);
		this.item.load(con);
		return "PurchaseOrder";
	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public String setItemCode(String itmcode) {
		this.articleItems.getList().clear();
		this.item.getPoLineSelected().setItemCode(itmcode);
		this.searchItemWithItemCode();
		return "PurchaseOrder";
	}

	public String searchItems() {
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getPoLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getPoLineSelected().getItemCode());
		this.articleItems.getFilter().setArticle_no(this.item.getPoLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getPoLineSelected().getItemDesc());
		this.articleItems.getFilter().setBar_code(this.item.getPoLineSelected().getBarCode());
		if (this.articleItems.getList().size() == 1) {
			this.item.getPoLineSelected()
					.setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.searchItemWithItemCode();
		}

		return this.articleItems.getList().size() > 1 ? "PurchaseOrder" : "PurchaseOrder";
	}

	public String searchItemWithItemCode() {
		PurchaseOrderLines pol = this.item.getPoLineSelected();
		this.articleItems.clearFilter();
		this.articleItems.getFilter().setItem_code(this.getItem().getPoLineSelected().getItemCode());
		this.articleItems.getList();
		if (this.articleItems.list.size() > 0) {
			pol.setQty(Double.valueOf(1.0D));
			pol.setMrp(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getMrp()));
			pol.setSection(((ArticleItems) this.articleItems.list.get(0)).getArticle_group());
			pol.setSubSection(((ArticleItems) this.articleItems.list.get(0)).getArticle_sub_group());
			pol.setColor(((ArticleItems) this.articleItems.list.get(0)).getColor());
			pol.setItemSize(((ArticleItems) this.articleItems.list.get(0)).getArticle_size());
			pol.setUom(((ArticleItems) this.articleItems.list.get(0)).getInventory_uom());
			pol.setGrossPp(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getPur_price()));
			pol.setDiscPerc(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getDisc_per()));
			pol.setTaxPerc(((ArticleItems) this.articleItems.list.get(0)).getTax_per());
			pol.setBarCode(((ArticleItems) this.articleItems.list.get(0)).getBar_code());
			pol.setItemDesc(((ArticleItems) this.articleItems.list.get(0)).getArticle_desc());
			pol.setWsp(Double.valueOf(((ArticleItems) this.articleItems.list.get(0)).getWsp()));
			pol.setBrand(((ArticleItems) this.articleItems.list.get(0)).getManufacturer());
			pol.setArticleNo(((ArticleItems) this.articleItems.list.get(0)).getArticle_no());
			this.setIsSelected(true);
			pol.processLine();
		} else {
			String tempItemCode = pol.getItemCode();
			this.item.setPoLineSelected(new PurchaseOrderLines());
			this.item.getPoLineSelected().setItemCode(tempItemCode);
		}

		return "PurchaseOrder";
	}

	public String createRecipt() throws SQLException, IOException {
		this.pr.preparePurchaseReceipt();
		this.pr.item.setPoNo(this.getItem().getPoNo());
		this.pr.item.setSupplierId(this.getItem().getSupplierId());
		this.pr.item.setSupplierName(this.getItem().getSupplierName());
		this.pr.item.setSupplierAddress(this.getItem().getSupplierAddress());
		PurchaseReceiptLines prl = null;
		Iterator i$ = this.getItem().getPoLines().iterator();

		while (i$.hasNext()) {
			PurchaseOrderLines pl = (PurchaseOrderLines) i$.next();
			prl = new PurchaseReceiptLines();
			prl.setLineNo(pl.getLineNo());
			prl.setItemCode(pl.getItemCode());
			prl.setItemDesc(pl.getItemDesc());
			prl.setQty(pl.getQty());
			prl.setAmount(pl.getAmount());
			prl.setArticleNo(pl.getArticleNo());
			prl.setAttr6(pl.getAttr6());
			prl.setBarCode(pl.getBarCode());
			prl.setBrand(pl.getBrand());
			prl.setColor(pl.getColor());
			prl.setDiscAmt(pl.getDiscAmt());
			prl.setDiscPerc(pl.getDiscPerc());
			prl.setFabric(pl.getFabric());
			prl.setGrossPp(pl.getGrossPp());
			prl.setItemSize(pl.getItemSize());
			prl.setMpPer(pl.getMpPer());
			prl.setMrp(pl.getMrp());
			prl.setNoteText(pl.getNoteText());
			prl.setPurPrice(pl.getPurPrice());
			prl.setRemark(pl.getRemark());
			prl.setSection(pl.getSection());
			prl.setSel(pl.getSel());
			prl.setStyle(pl.getStyle());
			prl.setSubSection(pl.getSubSection());
			prl.setTaxAmt(pl.getTaxAmt());
			prl.setUom(pl.getUom());
			prl.setWsp(pl.getWsp());
			prl.setWspPer(pl.getWspPer());
			this.pr.getItem().getPorLines().add(prl);
		}

		return "PurchaseReceipt";
	}

	public String receive() throws SQLException, IOException {
		this.stock.list.clear();
		Connection con = null;

		try {
			con = Data.getConnection();
			con.setAutoCommit(false);
			Iterator ex = this.item.getPoLines().iterator();

			while (ex.hasNext()) {
				PurchaseOrderLines fm1 = (PurchaseOrderLines) ex.next();
				this.stock.addNew();
				this.stock.item.setItemCode(fm1.getItemCode());
				String result = this.stock.item.load(con);
				if (result != null && result.equals("success")) {
					this.stock.item.setQtyOnHand(this.stock.item.getQtyOnHand() + fm1.getQty().doubleValue());
					this.stock.item.setQty(this.stock.item.getQty() + fm1.getQty().doubleValue());
					this.stock.item.setRemark("GRN DT: " + Utils.getStrDate(new Date()));
					this.stock.item.setPurchaseOrder(fm1.getPurchaseOrder());
					this.stock.item.update(con);
				} else {
					if (fm1.getItemDesc() == null) {
						this.stock.item.setItemDesc(fm1.getArticleNo());
					} else {
						this.stock.item.setItemDesc(fm1.getItemDesc());
					}

					this.stock.item.setItemCode(fm1.getItemCode());
					this.stock.item.setArticleNo(fm1.getArticleNo());
					this.stock.item.setSection(fm1.getSection());
					this.stock.item.setSubSection(fm1.getSubSection());
					this.stock.item.setUom(fm1.getUom());
					this.stock.item.setFabric(fm1.getFabric());
					this.stock.item.setRemark(fm1.getRemark());
					this.stock.item.setColor(fm1.getColor());
					this.stock.item.setStyle(fm1.getStyle());
					this.stock.item.setItemSize(fm1.getItemSize());
					this.stock.item.setBrand(fm1.getBrand());
					this.stock.item.setBarCode(fm1.getBarCode());
					this.stock.item.setMrp(fm1.getMrp().doubleValue());
					this.stock.item.setWsp(fm1.getWsp().doubleValue());
					this.stock.item.setWspPer(fm1.getWspPer().doubleValue());
					this.stock.item.setMpPer(fm1.getMpPer().doubleValue());
					this.stock.item.setGrossPp(fm1.getGrossPp().doubleValue());
					this.stock.item.setPurPrice(fm1.getPurPrice().doubleValue());
					this.stock.item.setTaxPerc(fm1.getTaxPerc());
					this.stock.item.setTaxAmt(fm1.getTaxAmt());
					this.stock.item.setDiscPerc(fm1.getDiscPerc().doubleValue());
					this.stock.item.setDiscAmt(fm1.getDiscAmt().doubleValue());
					this.stock.item.setPurchaseOrder(fm1.getPurchaseOrder());
					this.stock.item.setLineNo(Short.valueOf(fm1.getLineNo()));
					this.stock.item.setQty(fm1.getQty().doubleValue());
					this.stock.item.setQtyOnHand(fm1.getQty().doubleValue());
					this.stock.item.setReceiptDate(new Date());
					this.stock.item.setUpdatedOn(fm1.getUpdatedOn());
					this.stock.item.setCreatedOn(fm1.getCreatedOn());
					this.stock.item.update(con);
					this.stock.list.add(this.stock.item);
				}

				this.invTrans.addNew();
				this.invTrans.item.setTransactionId(this.invTrans.getItem().getNextTransactionId());
				this.invTrans.item.setTransactionCode("POREC");
				this.invTrans.item.setItemCode(fm1.getItemCode());
				this.invTrans.item.setItemDesc(fm1.getItemDesc());
				this.invTrans.item.setQty(fm1.getQty().doubleValue());
				this.invTrans.item.setUom(fm1.getUom());
				this.invTrans.item.setDirection("+");
				this.invTrans.item.setPricePerUnit(fm1.getPurPrice().doubleValue());
				this.invTrans.item.setPrice(fm1.getAmount().doubleValue());
				this.invTrans.item.setOrderNo(fm1.getPurchaseOrder());
				this.invTrans.item.setLineNo(fm1.getLineNo());
				this.invTrans.item.setCreatedBy(this.getItem().getUserId());
				this.invTrans.item.setArticleNo(fm1.getArticleNo());
				this.invTrans.item.update(con);
			}

			this.item.setStatus("Received");
			this.item.update(con);
			con.setAutoCommit(true);
			con.commit();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage ex1 = new FacesMessage("Received.");
			ex1.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, ex1);
		} catch (Exception arg7) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg7.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			con.rollback();
			con.setAutoCommit(true);
		} finally {
			if (con != null && !con.isClosed()) {
				con.close();
			}

		}

		return "PurchaseOrder";
	}

	public short getNextLineNo() {
		short i = 0;
		Iterator i$ = this.getItem().getPoLines().iterator();

		while (i$.hasNext()) {
			PurchaseOrderLines pl = (PurchaseOrderLines) i$.next();
			if (pl.getLineNo() > i) {
				i = pl.getLineNo();
			}
		}

		++i;
		return i;
	}

	public String process() {
		this.item.process();
		return "PurchaseOrder";
	}

	public String commit() {
		this.process();
		Connection con = Data.getConnection();

		try {
			if (this.item.getPoNo() == null || this.item.getPoNo().trim().length() == 0) {
				this.trans.item.setPrefix((String) null);
				this.trans.item.setTransType("PO");
				this.item.setPoNo(this.trans.getNextOrderNo());
			}

			this.item.update(con);
			this.item.load(con);
			this.articleItems.clearFilter();
			this.articleItems.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "PurchaseOrder";
	}

	public String delete() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.delete(con);
			this.setItem(new PurchaseOrder());
			this.articleItems.clearFilter();
			this.articleItems.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "PurchaseOrder";
	}

	public String addNew() {
		this.setItem(new PurchaseOrder());
		this.articleItems.clearFilter();
		this.articleItems.list.clear();
		return "PurchaseOrder";
	}

	public String search() {
		this.list.clear();
		return "PurchaseOrderList";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		return "PurchaseOrderList";
	}

	public String createPurchaseOrderReturn() {
		return "PurchaseOrderReturn";
	}

	public String resetSelected() {
		PurchaseOrderLines pl = new PurchaseOrderLines();
		this.item.setPoLineSelected(pl);
		return "PurchaseOrder";
	}

	public String preparePurchaseOrder() {
		this.addNew();
		this.list.clear();
		this.trans.addNew();
		this.trans.list.clear();
		return "PurchaseOrder";
	}

	public String cancelGrn() {
		FacesMessage con;
		if (this.getItem().getPoNo() != null && this.getItem().getPoNo().length() > 0) {
			if (this.getItem().getStatus() != null && this.getItem().getStatus().equals("Received")) {
				Connection con1 = Data.getConnection();
				CallableStatement cs = null;

				try {
					cs = con1.prepareCall("{call cancel_grn(?)}");
					cs.setString(1, this.getItem().getPoNo());
					cs.executeUpdate();
					cs.close();
					this.item.load(con1);
					con1.close();
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage ex = new FacesMessage("Goode Receipt Canceled.");
					ex.setSeverity(FacesMessage.SEVERITY_INFO);
					this.facesContext.addMessage((String) null, ex);
				} catch (SQLException arg4) {
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(arg4.getMessage() + " ");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}

				return "PurchaseOrder";
			} else {
				this.facesContext = FacesContext.getCurrentInstance();
				con = new FacesMessage("Please select Received GRN to Cancel.");
				con.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, con);
				return "PurchaseOrder";
			}
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			con = new FacesMessage("Please select PO to Cancel.");
			con.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, con);
			return "PurchaseOrder";
		}
	}

	public String duplicate() {
		this.setItem(this.getItem().duplicate());
		return "PurchaseOrder";
	}
}