/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseOrderBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.InventoryTransaction;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "trans")
@SessionScoped
public class InventoryTransactionBean {
	InventoryTransaction item;
	List<InventoryTransaction> list;
	FacesContext facesContext = FacesContext.getCurrentInstance();
	@ManagedProperty("#{login}")
	LoginBean login;

	public InventoryTransactionBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new InventoryTransaction();
		}

	}

	public InventoryTransaction getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("InventoryTransaction");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public void setItem(InventoryTransaction item) {
		this.item = item;
	}

	public List<InventoryTransaction> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.getInventoryTransactionList();
			} catch (SQLException arg2) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg2.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		return this.list;
	}

	public void setList(List<InventoryTransaction> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				String ex = this.item.update(con);
				this.item.load(con);
				con.close();
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(ex);
				fm.setSeverity(FacesMessage.SEVERITY_INFO);
				this.facesContext.addMessage((String) null, fm);
			} catch (SQLException arg13) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg13.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			} catch (Exception arg14) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg14.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg12) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg12);
				}
			}

		}

		return "InventoryTransaction";
	}

	public String addNew() {
		this.item = new InventoryTransaction();
		return "InventoryTransaction";
	}

	public String load(Long trans_id) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setTransactionId(trans_id);
		this.item.load(con);
		return "InventoryTransaction";
	}

	public String getInventoryTransactionList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "InventoryTransactionList";
	}

	public String refreshList() throws SQLException {
		this.setList((List) null);
		this.setItem(new InventoryTransaction());
		return "InventoryTransactionList";
	}

	public String search() throws SQLException {
		this.getList().clear();
		return "InventoryTransactionList";
	}
}