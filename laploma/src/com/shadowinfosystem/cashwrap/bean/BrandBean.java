/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.Brand;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="brand")
@SessionScoped
public class BrandBean
{
  Brand item;
  List<Brand> list;
  List<Brand> listSelectOne;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public BrandBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.listSelectOne == null) {
      this.listSelectOne = new ArrayList();
    }
    if (this.item == null) {
      this.item = new Brand();
    }
  }
  
  public Brand getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("Brand");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(Brand item)
  {
    this.item = item;
  }
  
  public List<Brand> getListSelectOne()
  {
    if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
      try
      {
        Connection con = Data.getConnection();
        addNew();
        this.item.setStatus("Active");
        setListSelectOne(this.item.getList(con));
        con.close();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.listSelectOne;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public void setListSelectOne(List<Brand> listSelectOne)
  {
    this.listSelectOne = listSelectOne;
  }
  
  public List<Brand> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        getBrandList();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(BrandBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<Brand> list)
  {
    this.list = list;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.update(con);
      this.item.load(con);
      con.close();
      this.listSelectOne.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
      con.close();
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Brand";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.delete(con);
      addNew();
      this.list.clear();
      con.close();
      this.listSelectOne.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
      con.close();
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Brand";
  }
  
  public String addNew()
  {
    this.item = new Brand();
    getItem();
    return "Brand";
  }
  
  public String load(String brand)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setBrand(brand);
    this.item.load(con);
    return "Brand";
  }
  
  public String getBrandList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.getList(con));
    return "BrandList";
  }
  
  public String refreshBrandList()
    throws SQLException
  {
    setItem(new Brand());
    this.list.clear();
    return "BrandList";
  }
  
  public String refreshBrand()
    throws SQLException
  {
    refreshBrandList();
    return "Brand";
  }
  
  public String search()
    throws SQLException
  {
    this.list.clear();
    return "BrandList";
  }
}
