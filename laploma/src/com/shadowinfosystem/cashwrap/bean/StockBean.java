/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Xls;
import com.shadowinfosystem.cashwrap.entities.ItemInStock;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "stock")
@SessionScoped
public class StockBean {
	ItemInStock item;
	List<ItemInStock> list;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public StockBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.item == null) {
			this.item = new ItemInStock();
		}
	}

	public ItemInStock getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("ItemInStock");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
			this.item.setCancelAllowed(temp.isCancelAllowed());
		}
		return this.item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public void setItem(ItemInStock item) {
		this.item = item;
	}

	public List<ItemInStock> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getStockList();
			} catch (SQLException ex) {
				Logger.getLogger(StockBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public void setList(List<ItemInStock> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			this.item.update(con);
			this.item.load(con);
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Stock";
	}

	public String addNew() {
		this.item = new ItemInStock();

		return "Stock";
	}

	public String load(String item_code) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setItemCode(item_code);
		this.item.load(con);
		con.close();
		return "Stock";
	}

	public String getStockList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "Stock";
	}

	public String refreshList() {
		addNew();
		this.list.clear();
		return "Stock";
	}

	public String search() {
		this.list.clear();
		return "Stock";
	}

	public String generateXls() {
		Xls xls = new Xls();
		try {
			xls.getXls(getList());
		} catch (IOException ex) {
			Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalArgumentException ex) {
			Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IllegalAccessException ex) {
			Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "Stock";
	}
}
