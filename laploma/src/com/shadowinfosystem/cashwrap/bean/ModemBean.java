/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import org.smslib.AGateway;
import org.smslib.IOutboundMessageNotification;
import org.smslib.Library;
import org.smslib.OutboundMessage;
import org.smslib.Service;
import org.smslib.modem.SerialModemGateway;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Sms;
import com.shadowinfosystem.cashwrap.entities.Modem;

@ManagedBean(name="modem")
@ApplicationScoped
public class ModemBean
{
  private Sms sms;
  private Modem modem;
  private List<Modem> list;
  SerialModemGateway gateway;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public ModemBean()
  {
    if (this.sms == null) {
      this.sms = new Sms();
    }
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.modem == null)
    {
      this.modem = new Modem();
      try
      {
        this.modem.getDefaultModem();
      }
      catch (Exception ex)
      {
        Logger.getLogger(ModemBean.class.getName()).log(Level.SEVERE, null, ex);
        this.facesContext = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(ex.getMessage());
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        this.facesContext.addMessage(null, fm);
      }
    }
    OutboundNotification outboundNotification = new OutboundNotification();
    
    this.gateway = new SerialModemGateway(getModem().getModemId(), getModem().getComPort(), getModem().getBaudRate(), getModem().getModemManufacturer(), getModem().getModemModel());
    
    this.gateway.setInbound(true);
    this.gateway.setOutbound(true);
    this.gateway.setSimPin("0000");
    
    Service.getInstance().setOutboundMessageNotification(outboundNotification);
    try
    {
      this.gateway.setSmscNumber(getModem().getSmscNumber());
      
      Service.getInstance().addGateway(this.gateway);
      
      Service.getInstance().startService();
    }
    catch (Exception ex)
    {
      Logger.getLogger(ModemBean.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  public Sms getSms()
  {
    return this.sms;
  }
  
  public void setSms(Sms sms)
  {
    this.sms = sms;
  }
  
  public Modem getModem()
  {
    if (this.modem == null)
    {
      this.modem = new Modem();
      try
      {
        this.modem.getDefaultModem();
      }
      catch (Exception ex)
      {
        Logger.getLogger(ModemBean.class.getName()).log(Level.SEVERE, null, ex);
        this.facesContext = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(ex.getMessage());
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        this.facesContext.addMessage(null, fm);
      }
    }
    return this.modem;
  }
  
  public SerialModemGateway getGateway()
  {
    return this.gateway;
  }
  
  public void setGateway(SerialModemGateway gateway)
  {
    this.gateway = gateway;
  }
  
  public List<Modem> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        Connection con = Data.getConnection();
        setList(this.modem.getList(con));
        con.close();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ModemBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<Modem> list)
  {
    this.list = list;
  }
  
  public void setModem(Modem modem)
  {
    this.modem = modem;
  }
  
  public String sendSms()
  {
    OutboundMessage msg = new OutboundMessage(getSms().getMobNo(), getSms().getMsgText());
    try
    {
      Service.getInstance().sendMessage(msg);
    }
    catch (Exception ex)
    {
      Logger.getLogger(ModemBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "Modem";
  }
  
  public class OutboundNotification
    implements IOutboundMessageNotification
  {
    public OutboundNotification() {}
    
    public void process(AGateway gateway, OutboundMessage msg)
    {
    }
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.modem.update(con);
      this.modem.load(con);
      con.close();
      this.list.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
      con.close();
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(e.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Modem";
  }
  
  public String addNew()
  {
    this.modem = new Modem();
    
    return "Modem";
  }
  
  public String load(String modemID)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.modem.setModemId(modemID);
    this.modem.load(con);
    return "Modem";
  }
  
  public String getModemList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.modem.getList(con));
    return "Modem";
  }
  
  public String refreshModemsList()
    throws SQLException
  {
    setModem(new Modem());
    this.list.clear();
    return "ModemList";
  }
  
  public String modemsList()
    throws SQLException
  {
    setModem(new Modem());
    this.list.clear();
    return "ModemList";
  }
}
