/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.awt.print.PrinterJob;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.print.PrintService;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.UserInfo;

@ManagedBean(name="printManager")
@SessionScoped
public class PrinterManagerBean
{
  private List<String> prinetersList;
  private String defaultPrinter;
  private String tempPrinter;
  @ManagedProperty("#{login}")
  LoginBean login;
  @ManagedProperty("#{userInfo}")
  UserInfoBean userInfo;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public PrinterManagerBean()
  {
    if (this.prinetersList == null) {
      this.prinetersList = new ArrayList();
    }
  }
  
  public List<String> getPrinetersList()
  {
    return this.prinetersList;
  }
  
  public void setPrinetersList(List<String> prinetersList)
  {
    this.prinetersList = prinetersList;
  }
  
  public String getDefaultPrinter()
  {
    return this.defaultPrinter;
  }
  
  public void setDefaultPrinter(String defaultPrinter)
  {
    this.defaultPrinter = defaultPrinter;
  }
  
  public String getTempPrinter()
  {
    return this.tempPrinter;
  }
  
  public void setTempPrinter(String tempPrinter)
  {
    this.tempPrinter = tempPrinter;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public String preparePage()
  {
    return "PrinterManager";
  }
  
  public UserInfoBean getUserInfo()
  {
    return this.userInfo;
  }
  
  public void setUserInfo(UserInfoBean userInfo)
  {
    this.userInfo = userInfo;
  }
  
  public FacesContext getFacesContext()
  {
    return this.facesContext;
  }
  
  public void setFacesContext(FacesContext facesContext)
  {
    this.facesContext = facesContext;
  }
  
  public String searchInstalledPrinters()
  {
    PrintService[] service = PrinterJob.lookupPrintServices();
    int count = service.length;
    for (int i = 0; i < count; i++) {
      getPrinetersList().add(service[i].getName());
    }
    return "PrinterManager";
  }
  
  public String updateMyDefaultPrinter()
  {
    if ((getTempPrinter() == null) || (getTempPrinter().length() <= 0))
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Please select printer from list / Click Search Installed Printer.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "PrinterManager";
    }
    this.userInfo.addNew();
    this.userInfo.load(this.login.getUserid());
    this.userInfo.getItem().setDefaultPrinter(getTempPrinter());
    Connection con = Data.getConnection();
    try
    {
      this.userInfo.item.update(con);
      con.close();
      this.login.setUser(this.userInfo.getItem());
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Default Prineter is set.");
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException ex)
    {
      try
      {
        if ((con != null) && (!con.isClosed())) {
          con.close();
        }
      }
      catch (SQLException ex1)
      {
        Logger.getLogger(PrinterManagerBean.class.getName()).log(Level.SEVERE, null, ex1);
      }
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
    }
    return "PrinterManager";
  }
}
