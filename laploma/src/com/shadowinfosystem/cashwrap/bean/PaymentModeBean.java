/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.PaymentMode;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="payMode")
@SessionScoped
public class PaymentModeBean
{
  PaymentMode item;
  List<PaymentMode> list;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public PaymentModeBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.item == null) {
      this.item = new PaymentMode();
    }
  }
  
  public PaymentMode getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("PaymentMode");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(PaymentMode item)
  {
    this.item = item;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public List<PaymentMode> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        getPaymentModeList();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(PaymentModeBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<PaymentMode> list)
  {
    this.list = list;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      this.item.update(con);
      this.item.load(con);
      this.list.clear();
      con.close();
    }
    catch (Exception e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PaymentModeBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "PaymentMode";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      this.item.delete(con);
      addNew();
      this.list.clear();
      con.close();
    }
    catch (Exception e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PaymentModeBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "PaymentMode";
  }
  
  public String addNew()
  {
    this.item = new PaymentMode();
    
    return "PaymentMode";
  }
  
  public String load(String paymentModeId)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setPayModeId(paymentModeId);
    this.item.load(con);
    return "PaymentMode";
  }
  
  public String getPaymentModeList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.getList(con));
    return "PaymentMode";
  }
  
  public String refreshPaymentModeList()
    throws SQLException
  {
    addNew();
    this.list.clear();
    return "PaymentModeList";
  }
}
