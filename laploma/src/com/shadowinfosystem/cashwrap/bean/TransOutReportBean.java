/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.util.Date;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "transOutReport")
@SessionScoped
public class TransOutReportBean extends EntitiesImpl {
	private String tarnsId;
	private String branchId;
	private Date fromDate;
	private Date toDate;
	private String outputFormat;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	@PostConstruct
	public void initPageSecurity() {
		if (getScreen() == null) {
			setScreen("TransInReport");
			ScreenRights temp = this.login.getRightsForScreen(getScreen());
			setUserId(this.login.getUserid());
			setAddAddowed(temp.isAddAddowed());
			setViewAddowed(temp.isViewAddowed());
			setModifyAddowed(temp.isModifyAddowed());
			setDeleteAddowed(temp.isDeleteAddowed());
			setPostAddowed(temp.isPostAddowed());
			setPrintAddowed(temp.isPrintAddowed());
		}
	}

	public String prepareReportPage() {
		setTarnsId(null);
		setBranchId(null);
		setFromDate(new Date());
		setToDate(new Date());
		return "TransOutReport";
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public String getTarnsId() {
		return this.tarnsId;
	}

	public void setTarnsId(String tarnsId) {
		this.tarnsId = tarnsId;
	}

	public String getBranchId() {
		return this.branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getOutputFormat() {
		if (this.outputFormat == null) {
			return "pdf";
		}
		return this.outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public String printTransInRegister() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("TransOutRegister");
			if ((getTarnsId() != null) && (getTarnsId().length() > 0)) {
				this.rep.getParameters().put("trans_id", getTarnsId());
			}
			if ((getBranchId() != null) && (getBranchId().length() > 0)) {
				this.rep.getParameters().put("branch_id", getBranchId());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("from_date", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("to_date", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "TransOutReport";
	}

	public String printTransInItemWiseRegister() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("TransOutItemWiseRegister");
			if ((getTarnsId() != null) && (getTarnsId().length() > 0)) {
				this.rep.getParameters().put("trans_id", getTarnsId());
			}
			if ((getBranchId() != null) && (getBranchId().length() > 0)) {
				this.rep.getParameters().put("branch_id", getBranchId());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("from_date", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("to_date", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "TransOutReport";
	}
}
