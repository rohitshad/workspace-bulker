/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.TransIn;
import com.shadowinfosystem.cashwrap.entities.TransInLines;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "transIn")
@SessionScoped
public class TransInBean {
	TransIn item;
	List<TransIn> list;
	@ManagedProperty("#{stock}")
	StockBean stock;
	@ManagedProperty("#{section}")
	SectionBean section;
	@ManagedProperty("#{subSection}")
	SubSectionBean subSection;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{masterParameter}")
	MasterParameterBean mastParam;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{branch}")
	BranchBean branch;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public TransInBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new TransIn();
		}

	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public TransIn getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("TransIn");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public BranchBean getBranch() {
		return this.branch;
	}

	public void setBranch(BranchBean branch) {
		this.branch = branch;
	}

	public void setItem(TransIn item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public SectionBean getSection() {
		return this.section;
	}

	public void setSection(SectionBean section) {
		this.section = section;
	}

	public SubSectionBean getSubSection() {
		return this.subSection;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public void setSubSection(SubSectionBean subSection) {
		this.subSection = subSection;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public MasterParameterBean getMastParam() {
		return this.mastParam;
	}

	public void setMastParam(MasterParameterBean mastParam) {
		this.mastParam = mastParam;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public List<TransIn> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<TransIn> list) {
		this.list = list;
	}

	public String taxCodeSelected() throws SQLException {
		TransInLines ils = this.item.getLineSelected();
		if (this.item.getLineSelected().getTaxCode() != null && this.item.getLineSelected().getTaxCode().length() > 0) {
			ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
			TaxBean tax = (TaxBean) exctx.getSessionMap().get("tax");
			tax.load(this.item.getLineSelected().getTaxCode());
			ils.setTaxPer(tax.getItem().getTaxPer());
		} else {
			ils.setTaxPer(0.0D);
		}

		return this.reProcessSelected();
	}

	public String reProcessSelected() {
		TransInLines ils = this.getItem().getLineSelected();
		if (ils.getDiscPer() > this.getMastParam().getItem().getMaxDiscOnItem()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(
					"Max Disc Allowed is " + this.getMastParam().getItem().getMaxDiscOnItem() + " %.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		ils.process();
		return "success";
	}

	public String addItem() {
		TransInLines pl = this.getItem().getLineSelected();
		FacesMessage fm;
		if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Please select Item.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "TransIn";
		} else if (pl.getQty() <= 0.0D) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Qty must be > 0.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "TransIn";
		} else {
			pl.setLineNo(this.getNextLineNo());
			pl.setTransId(this.item.getTransId());
			this.getItem().getLines().add(pl);
			this.getItem().setLineSelected(new TransInLines());
			this.getItem().process(this.mastParam.getItem().getIsTaxInclusive());
			this.setIsSelected(false);
			return "TransIn";
		}
	}

	public String updateItem() {
		TransInLines il = this.getItem().getLineSelected();
		this.getItem().setLineSelected(new TransInLines());
		this.getItem().process(this.mastParam.getItem().getIsTaxInclusive());
		return "TransIn";
	}

	public String resetSelectedItem() {
		this.item.setLineSelected(new TransInLines());
		this.articleItems.clearFilter();
		this.articleItems.list.clear();
		return "TransIn";
	}

	public String getTransInList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "TransInList";
	}

	public String setSlectedDetail(TransInLines line) {
		this.setIsSelected(true);
		this.item.setLineSelected(line);
		return "TransIn";
	}

	public String deletDetail(TransInLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getObjectId() > 0) {
			try {
				line.delete(con);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		con.close();
		this.getItem().getLines().remove(line);
		return "TransIn";
	}

	public String getSubSecList() {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		SubSectionBean ss = (SubSectionBean) exctx.getSessionMap().get("subSection");
		ss.getItem().setSectionNo(this.item.getLineSelected().getSection());
		ss.getSubSecList();
		return "success";
	}

	public String newItem() {
		this.item = new TransIn();
		return "TransIn";
	}

	public String load(String trans_id) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setTransId(trans_id);
		this.item.load(con);
		return "TransIn";
	}

	public String printReport() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("TransIn");
			HashMap ex = new HashMap();
			ex.put("trans_id", this.getItem().getTransId());
			this.rep.setParameters(ex);
			this.rep.exportToPdf();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "TransIn";
	}

	public short getNextLineNo() {
		boolean i = false;
		short arg3 = 0;
		Iterator i$ = this.getItem().getLines().iterator();

		while (i$.hasNext()) {
			TransInLines pl = (TransInLines) i$.next();
			if (pl.getLineNo() > arg3) {
				arg3 = pl.getLineNo();
			}
		}

		++arg3;
		return arg3;
	}

	public String process() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		return "TransIn";
	}

	public String commit() {
		this.process();
		Connection con = Data.getConnection();

		try {
			if (this.item.getTransId() == null || this.item.getTransId().length() == 0) {
				this.trans.addNew();
				this.trans.item.setPrefix((String) null);
				this.trans.item.setTransType("TRANSIN");
				this.item.setTransId(this.trans.getNextOrderNo());
				this.item.setTransDate(Utils.getSqlDate(new Date()));
			}

			this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(TransInBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "TransIn";
	}

	public String addNew() {
		this.setItem(new TransIn());
		this.stock.list.clear();
		this.articleItems.clearFilter();
		this.articleItems.list.clear();
		return "TransIn";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		this.item.setFromDate(new Date());
		this.item.setToDate(new Date());
		return "TransInList";
	}

	public String search() {
		this.list.clear();
		return "TransInList";
	}

	public String approveReceipt() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				con.setAutoCommit(false);
				Iterator ex = this.item.getLines().iterator();

				while (true) {
					while (ex.hasNext()) {
						TransInLines fm1 = (TransInLines) ex.next();
						this.invTrans.addNew();
						this.invTrans.item.setTransactionId(this.invTrans.getItem().getNextTransactionId());
						this.invTrans.item.setTransactionCode("TRANSIN");
						this.invTrans.item.setItemCode(fm1.getItemCode());
						this.invTrans.item.setItemDesc(fm1.getItemDesc());
						this.invTrans.item.setQty(fm1.getQty());
						this.invTrans.item.setUom(fm1.getUom());
						this.invTrans.item.setDirection("+");
						this.invTrans.item.setPricePerUnit(fm1.getSalesPrice());
						this.invTrans.item.setPrice(fm1.getNetAmt());
						this.invTrans.item.setOrderNo(fm1.getTransId());
						this.invTrans.item.setLineNo(fm1.getLineNo());
						this.invTrans.item.setCreatedBy(this.invTrans.item.getUserId());
						this.invTrans.item.setArticleNo(fm1.getArticleNo());
						this.invTrans.item.update(con);
						this.stock.addNew();
						this.stock.item.setItemCode(fm1.getItemCode());
						String ex1 = this.stock.item.load(con);
						if (ex1 != null && ex1.equals("success")) {
							this.stock.item.setQtyOnHand(this.stock.item.getQtyOnHand() + fm1.getQty());
							this.stock.item.setQty(this.stock.item.getQty() + fm1.getQty());
							this.stock.item.setRemark("TRANS IN DT: " + Utils.getStrDate(new Date()));
							this.stock.item.setPurchaseOrder(fm1.getTransId());
							this.stock.item.setBatch(fm1.getBatch());
							this.stock.item.setExpDate(fm1.getExpDate());
							this.stock.item.update(con);
						} else {
							this.stock.addNew();
							this.stock.item.setItemCode(fm1.getItemCode());
							if (fm1.getItemDesc() == null) {
								this.stock.item.setItemDesc(fm1.getArticleNo());
							} else {
								this.stock.item.setItemDesc(fm1.getItemDesc());
							}

							this.stock.item.setArticleNo(fm1.getArticleNo());
							this.stock.item.setSection(fm1.getSection());
							this.stock.item.setSubSection(fm1.getSubSection());
							this.stock.item.setUom(fm1.getUom());
							this.stock.item.setFabric(fm1.getFabric());
							this.stock.item.setRemark(fm1.getRemark());
							this.stock.item.setColor(fm1.getColor());
							this.stock.item.setStyle(fm1.getStyle());
							this.stock.item.setItemSize(fm1.getItemSize());
							this.stock.item.setBrand(fm1.getBrand());
							this.stock.item.setBarCode(fm1.getBarCode());
							this.stock.item.setMrp(fm1.getMrp());
							this.stock.item.setWsp(fm1.getSalesPrice());
							this.stock.item.setGrossPp(fm1.getPurPrice());
							this.stock.item.setPurPrice(fm1.getPurPrice());
							this.stock.item.setTaxPerc(fm1.getTaxPer());
							this.stock.item.setTaxAmt(fm1.getTaxAmt());
							this.stock.item.setDiscPerc(fm1.getDiscPer());
							this.stock.item.setDiscAmt(fm1.getDiscAmt());
							this.stock.item.setPurchaseOrder(fm1.getTransId());
							this.stock.item.setLineNo(Short.valueOf(fm1.getLineNo()));
							this.stock.item.setQty(fm1.getQty());
							this.stock.item.setQtyOnHand(fm1.getQty());
							this.stock.item.setReceiptDate(new Date());
							this.stock.item.setUpdatedOn(fm1.getUpdatedOn());
							this.stock.item.setCreatedOn(fm1.getCreatedOn());
							this.stock.item.setBatch(fm1.getBatch());
							this.stock.item.setExpDate(fm1.getExpDate());
							this.stock.item.update(con);
						}
					}

					this.item.setStatus("Approved");
					this.item.update(con);
					con.commit();
					con.setAutoCommit(true);
					break;
				}
			} catch (SQLException arg22) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg22.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);

				try {
					con.rollback();
				} catch (SQLException arg21) {
					Logger.getLogger(TransInBean.class.getName()).log(Level.SEVERE, (String) null, arg21);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg20) {
					Logger.getLogger(TransInBean.class.getName()).log(Level.SEVERE, (String) null, arg20);
				}
			} catch (Exception arg23) {
				try {
					con.rollback();
				} catch (SQLException arg19) {
					Logger.getLogger(TransInBean.class.getName()).log(Level.SEVERE, (String) null, arg19);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg18) {
					Logger.getLogger(TransInBean.class.getName()).log(Level.SEVERE, (String) null, arg18);
				}

				arg23.printStackTrace();
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg23.getMessage() + "");
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg17) {
					Logger.getLogger(TransInBean.class.getName()).log(Level.SEVERE, (String) null, arg17);
				}
			}

		}

		return "TransIn";
	}

	public String setItemCode(String itmcode) {
		this.articleItems.getList().clear();
		this.item.getLineSelected().setItemCode(itmcode);
		this.searchItemWithItemCode();
		return "TransIn";
	}

	public String searchItems() {
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getLineSelected().getItemCode());
		this.articleItems.getFilter().setArticle_no(this.item.getLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getLineSelected().getItemDesc());
		this.articleItems.getFilter().setBar_code(this.item.getLineSelected().getBarCode());
		if (this.articleItems.getList().size() == 1) {
			this.item.getLineSelected().setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.searchItemWithItemCode();
		}

		return this.articleItems.getList().size() > 1 ? "TransIn" : "TransIn";
	}

	public String searchItemWithItemCode() {
		TransInLines pol = this.item.getLineSelected();
		this.articleItems.clearFilter();
		this.articleItems.getFilter().setItem_code(this.getItem().getLineSelected().getItemCode());
		this.articleItems.getList();
		if (this.articleItems.list.size() > 0) {
			pol.setQty(1.0D);
			pol.setMrp(((ArticleItems) this.articleItems.list.get(0)).getMrp());
			pol.setSection(((ArticleItems) this.articleItems.list.get(0)).getArticle_group());
			pol.setSubSection(((ArticleItems) this.articleItems.list.get(0)).getArticle_sub_group());
			pol.setColor(((ArticleItems) this.articleItems.list.get(0)).getColor());
			pol.setItemSize(((ArticleItems) this.articleItems.list.get(0)).getArticle_size());
			pol.setUom(((ArticleItems) this.articleItems.list.get(0)).getInventory_uom());
			pol.setPurPrice(((ArticleItems) this.articleItems.list.get(0)).getPur_price());
			pol.setDiscPer(((ArticleItems) this.articleItems.list.get(0)).getDisc_per());
			pol.setTaxPer(((ArticleItems) this.articleItems.list.get(0)).getTax_per());
			pol.setBarCode(((ArticleItems) this.articleItems.list.get(0)).getBar_code());
			pol.setItemDesc(((ArticleItems) this.articleItems.list.get(0)).getArticle_desc());
			pol.setSalesPrice(((ArticleItems) this.articleItems.list.get(0)).getWsp());
			pol.setBrand(((ArticleItems) this.articleItems.list.get(0)).getManufacturer());
			pol.setArticleNo(((ArticleItems) this.articleItems.list.get(0)).getArticle_no());
			this.setIsSelected(true);
			pol.process();
		} else {
			String tempItemCode = pol.getItemCode();
			this.item.setLineSelected(new TransInLines());
			this.item.getLineSelected().setItemCode(tempItemCode);
			this.setIsSelected(false);
		}

		return "TransIn";
	}

	public String cancelApproval() {
		FacesMessage con;
		if (this.getItem().getTransId() != null && this.getItem().getTransId().length() > 0) {
			if (this.getItem().getStatus() != null && this.getItem().getStatus().equals("Approved")) {
				Connection con1 = Data.getConnection();
				CallableStatement cs = null;

				try {
					cs = con1.prepareCall("{call cancel_trin(?)}");
					cs.setString(1, this.getItem().getTransId());
					cs.executeUpdate();
					cs.close();
					this.item.load(con1);
					con1.close();
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage ex = new FacesMessage("Trans In Approval Canceled.");
					ex.setSeverity(FacesMessage.SEVERITY_INFO);
					this.facesContext.addMessage((String) null, ex);
				} catch (SQLException arg4) {
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(arg4.getMessage() + " ");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}

				return "TransIn";
			} else {
				this.facesContext = FacesContext.getCurrentInstance();
				con = new FacesMessage("Please select Approved TransIn to Cancel.");
				con.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, con);
				return "TransIn";
			}
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			con = new FacesMessage("Please select Trans IN to Cancel.");
			con.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, con);
			return "TransIn";
		}
	}
}