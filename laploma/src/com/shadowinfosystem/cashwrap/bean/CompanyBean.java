/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/

package com.shadowinfosystem.cashwrap.bean;

import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

import com.shadowinfosystem.cashwrap.entities.Company;

@ManagedBean(name="company")
@ApplicationScoped
public class CompanyBean
{
  private Company company;
  
  public CompanyBean()
  {
    if (this.company == null) {
      this.company = new Company();
    }
  }
  
  public Company getCompany()
  {
    return this.company;
  }
  
  public void setCompany(Company company)
  {
    this.company = company;
  }
}
