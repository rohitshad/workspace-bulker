/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Tax extends EntitiesImpl implements Serializable {
	private String taxCode;
	private String taxDesc;
	private double taxPer;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public Tax() {
	}

	public Tax(String color) {
		this.taxCode = color;
	}

	public String getTaxCode() {
		return this.taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public String getTaxDesc() {
		return this.taxDesc;
	}

	public void setTaxDesc(String taxDesc) {
		this.taxDesc = taxDesc;
	}

	public double getTaxPer() {
		return this.taxPer;
	}

	public void setTaxPer(double taxPer) {
		this.taxPer = taxPer;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update tax p\nset tax_desc=?,tax_per=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere tax_code=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.taxDesc);
		ps.setDouble(2, this.taxPer);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(this.createdOn));
		ps.setString(5, this.createdBy);
		ps.setDate(6, Utils.getSqlDate(new Date()));
		ps.setString(7, getUserId());
		ps.setString(8, this.taxCode);
		ps.setShort(9, this.objectId);

		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		setCreatedOn(new Date());
		String sql = "insert into tax\n  (tax_code,\n   tax_desc,tax_per,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by)\nvalues\n  (?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.taxCode);
		ps.setString(2, this.taxDesc);
		ps.setDouble(3, this.taxPer);
		ps.setString(4, this.status);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setDate(7, Utils.getSqlDate(this.updatedOn));
		ps.setString(8, this.updatedBy);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<Tax> getList(Connection con) throws SQLException {
		String sql = "select tax_code,\n       tax_desc,tax_per,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from tax p\n where tax_code like nvl(?,'%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.taxCode);

		ResultSet rs = ps.executeQuery();
		List<Tax> list = new ArrayList();
		while (rs.next()) {
			Tax temp = new Tax();
			temp.setTaxCode(rs.getString("tax_code"));
			temp.setTaxDesc(rs.getString("tax_desc"));
			temp.setTaxPer(rs.getDouble("tax_per"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select tax_code,\n       tax_desc,tax_per,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from tax p\n where tax_code = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.taxCode);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setTaxCode(rs.getString("tax_code"));
			setTaxDesc(rs.getString("tax_desc"));
			setTaxPer(rs.getDouble("tax_per"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from tax p\nwhere tax_code=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.taxCode);
		ps.setShort(2, this.objectId);

		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.taxCode != null ? this.taxCode.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Tax)) {
			return false;
		}
		Tax other = (Tax) object;
		if (((this.taxCode == null) && (other.taxCode != null))
				|| ((this.taxCode != null) && (!this.taxCode.equals(other.taxCode)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "tax=" + this.taxCode + "";
	}
}
