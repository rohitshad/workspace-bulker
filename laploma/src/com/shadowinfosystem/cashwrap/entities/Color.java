/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Color extends EntitiesImpl implements Serializable {
	private String color;
	private String colorDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public Color() {
	}

	public Color(String color) {
		this.color = color;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getColorDesc() {
		return this.colorDesc;
	}

	public void setColorDesc(String colorDesc) {
		this.colorDesc = colorDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update color p\nset color_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere color=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.colorDesc);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setString(7, this.color);
		ps.setShort(8, this.objectId);
		int n = ps.executeUpdate();
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into color\n  (color,\n   color_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by)\nvalues\n  (?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.color);
		ps.setString(2, this.colorDesc);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		int n = ps.executeUpdate();
		return "Record Created.";
	}

	public List<Color> search(Connection con) throws SQLException {
		String sql = "select color,\n       color_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from color p\n where 1= 1 ";

		List<Color> list = new ArrayList();
		boolean doFetchData = false;
		if ((getColor() != null) && (getColor().length() > 0)) {
			sql = sql + " and upper(color) like upper('" + getColor() + "') ";
			doFetchData = true;
		}
		if ((getColorDesc() != null) && (getColorDesc().length() > 0)) {
			sql = sql + " and upper(color_desc) like upper('" + getColorDesc() + "') ";
			doFetchData = true;
		}
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Color temp = new Color();
			temp.setColor(rs.getString("color"));
			temp.setColorDesc(rs.getString("color_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select color,\n       color_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from color p\n where color = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.color);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setColor(rs.getString("color"));
			setColorDesc(rs.getString("color_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from color p\nwhere color=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.color);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			return "Record could not be deleted.";
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.color != null ? this.color.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Color)) {
			return false;
		}
		Color other = (Color) object;
		if (((this.color == null) && (other.color != null))
				|| ((this.color != null) && (!this.color.equals(other.color)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "color=" + this.color + "";
	}
}
