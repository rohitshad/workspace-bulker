/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class InvoiceRetPayDet implements Serializable {
	private String invRetNo;
	private String payModeId;
	private String branchId;
	private double payAmt;
	private double cashTAmt;
	private double refAmt;
	private String ccCode;
	private String ccNo;
	private String coupId;
	private String custId;
	private String bankName;
	private String chequeNo;
	private Date chequeDt;
	private String gvNo;
	private double gvAmt;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public String getInvRetNo() {
		return this.invRetNo;
	}

	public void setInvRetNo(String invRetNo) {
		this.invRetNo = invRetNo;
	}

	public String getPayModeId() {
		return this.payModeId;
	}

	public void setPayModeId(String payModeId) {
		this.payModeId = payModeId;
	}

	public String getBranchId() {
		return this.branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public double getPayAmt() {
		return this.payAmt;
	}

	public void setPayAmt(double payAmt) {
		this.payAmt = payAmt;
	}

	public double getCashTAmt() {
		return this.cashTAmt;
	}

	public void setCashTAmt(double cashTAmt) {
		this.cashTAmt = cashTAmt;
	}

	public double getRefAmt() {
		return this.refAmt;
	}

	public void setRefAmt(double refAmt) {
		this.refAmt = refAmt;
	}

	public String getCcCode() {
		return this.ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getCcNo() {
		return this.ccNo;
	}

	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	public String getCoupId() {
		return this.coupId;
	}

	public void setCoupId(String coupId) {
		this.coupId = coupId;
	}

	public String getCustId() {
		return this.custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getChequeDt() {
		return this.chequeDt;
	}

	public void setChequeDt(Date chequeDt) {
		this.chequeDt = chequeDt;
	}

	public String getGvNo() {
		return this.gvNo;
	}

	public void setGvNo(String gvNo) {
		this.gvNo = gvNo;
	}

	public double getGvAmt() {
		return this.gvAmt;
	}

	public void setGvAmt(double gvAmt) {
		this.gvAmt = gvAmt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1) {
			return create(con);
		}
		this.updatedOn = new Date();
		String sql = "update invoice_ret_pay_det\n   set branch_id  = ?,\n       pay_amt    = ?,\n       cash_t_amt = ?,\n       ref_amt    = ?,\n       cc_code    = ?,\n       cc_no      = ?,\n       coup_id    = ?,\n       cust_id    = ?,\n       bank_name  = ?,\n       cheque_no  = ?,\n       cheque_dt  = ?,\n       gv_no      = ?,\n       gv_amt     = ?,\n       status     = ?,\n       created_on = ?,\n       created_by = ?,\n       updated_on = ?,\n       updated_by = ?,\n       object_id  = nvl(object_id, 1) + 1\n where inv_ret_no = ?\n   and pay_mode_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, "N/A");
		ps.setDouble(2, this.payAmt);
		ps.setDouble(3, this.cashTAmt);
		ps.setDouble(4, this.refAmt);
		ps.setString(5, this.ccCode);
		ps.setString(6, this.ccNo);
		ps.setString(7, this.coupId);
		ps.setString(8, this.coupId);
		ps.setString(9, this.bankName);
		ps.setString(10, this.chequeNo);
		ps.setDate(11, Utils.getSqlDate(this.chequeDt));
		ps.setString(12, this.gvNo);
		ps.setDouble(13, this.gvAmt);
		ps.setString(14, this.status);
		ps.setDate(15, Utils.getSqlDate(this.createdOn));
		ps.setString(16, this.createdBy);
		ps.setDate(17, Utils.getSqlDate(this.updatedOn));
		ps.setString(18, this.updatedBy);
		ps.setString(19, this.invRetNo);
		ps.setString(20, this.payModeId);
		ps.setShort(21, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public String create(Connection con) throws SQLException, Exception {
		if ((this.invRetNo == null) || (this.invRetNo.trim().length() == 0)) {
			throw new Exception("Invoice Ret No can not be Blank.");
		}
		setObjectId((short) 1);
		this.createdOn = new Date();
		String sql = "insert into invoice_ret_pay_det\n  (inv_ret_no,\n   pay_mode_id,\n   branch_id,\n   pay_amt,\n   cash_t_amt,\n   ref_amt,\n   cc_code,\n   cc_no,\n   coup_id,\n   cust_id,\n   bank_name,\n   cheque_no,\n   cheque_dt,\n   gv_no,\n   gv_amt,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invRetNo);
		ps.setString(2, this.payModeId);
		ps.setString(3, "N/A");
		ps.setDouble(4, this.payAmt);
		ps.setDouble(5, this.cashTAmt);
		ps.setDouble(6, this.refAmt);
		ps.setString(7, this.ccCode);
		ps.setString(8, this.ccNo);
		ps.setString(9, this.coupId);
		ps.setString(10, this.coupId);
		ps.setString(11, this.bankName);
		ps.setString(12, this.chequeNo);
		ps.setDate(13, Utils.getSqlDate(this.chequeDt));
		ps.setString(14, this.gvNo);
		ps.setDouble(15, this.gvAmt);
		ps.setString(16, this.status);
		ps.setDate(17, Utils.getSqlDate(this.createdOn));
		ps.setString(18, this.createdBy);
		ps.setDate(19, Utils.getSqlDate(this.updatedOn));
		ps.setString(20, this.updatedBy);
		ps.setShort(21, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			setObjectId((short) 0);
			throw new Exception("Inv Ret Pay Detail record could not be created.");
		}
		ps.close();
		return "success";
	}

	public static List<InvoiceRetPayDet> load(Connection con, String inv) throws SQLException {
		List<InvoiceRetPayDet> list = new ArrayList();
		InvoiceRetPayDet inp = null;
		String sql = "select inv_ret_no,\n       pay_mode_id,\n       branch_id,\n       pay_amt,\n       cash_t_amt,\n       ref_amt,\n       cc_code,\n       cc_no,\n       coup_id,\n       cust_id,\n       bank_name,\n       cheque_no,\n       cheque_dt,\n       gv_no,\n       gv_amt,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from invoice_ret_pay_det  where inv_ret_no = ?\n order by pay_mode_id";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, inv);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			inp = new InvoiceRetPayDet();
			inp.setInvRetNo(rs.getString("inv_ret_no"));
			inp.setPayModeId(rs.getString("pay_mode_id"));
			inp.setBranchId(rs.getString("branch_id"));
			inp.setPayAmt(rs.getDouble("pay_amt"));
			inp.setCashTAmt(rs.getDouble("cash_t_amt"));
			inp.setRefAmt(rs.getDouble("ref_amt"));
			inp.setCcCode(rs.getString("cc_code"));
			inp.setCcNo(rs.getString("cc_no"));
			inp.setCoupId(rs.getString("coup_id"));
			inp.setCustId(rs.getString("cust_id"));
			inp.setBankName(rs.getString("bank_name"));
			inp.setChequeNo(rs.getString("cheque_no"));
			inp.setChequeDt(rs.getDate("cheque_dt"));
			inp.setGvNo(rs.getString("gv_no"));
			inp.setGvAmt(rs.getDouble("gv_amt"));
			inp.setStatus(rs.getString("status"));
			inp.setCreatedOn(rs.getDate("created_On"));
			inp.setCreatedBy(rs.getString("Created_By"));
			inp.setUpdatedOn(rs.getDate("updated_On"));
			inp.setUpdatedBy(rs.getString("updated_By"));
			inp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(inp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from invoice_ret_pay_det where inv_ret_no=? and pay_mode_id=? and object_Id=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invRetNo);
		ps.setString(2, this.payModeId);
		ps.setShort(3, this.objectId);
		int nrd = 0;
		nrd = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (((this.invRetNo != null ? 1 : 0) & (this.payModeId != null ? 1 : 0)) != 0
				? (this.invRetNo + this.payModeId).hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof InvoiceRetPayDet)) {
			return false;
		}
		InvoiceRetPayDet other = (InvoiceRetPayDet) object;
		if (this.invRetNo == null) {
			return false;
		}
		if (other.invRetNo == null) {
			return false;
		}
		if (this.payModeId == null) {
			return false;
		}
		if (other.payModeId == null) {
			return false;
		}
		return (this.invRetNo.equals(other.invRetNo)) && (this.payModeId.equals(other.payModeId));
	}

	public String toString() {
		return "Inv Ret No and Pay Mode ID =" + this.invRetNo + " + " + this.payModeId;
	}
}
