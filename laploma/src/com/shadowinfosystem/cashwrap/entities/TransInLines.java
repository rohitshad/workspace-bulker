/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class TransInLines implements Serializable {
	private String transId;
	private short lineNo;
	private String itemCode;
	private String itemDesc;
	private double qty;
	private String uom;
	private double mrp;
	private double discPer;
	private double discAmt;
	private String selsPers;
	private String taxCode;
	private double taxPer;
	private double taxAmt;
	private double netAmt;
	private String subSection;
	private String section;
	private String color;
	private String itemSize;
	private String brand;
	private String style;
	private String fabric;
	private String remark;
	private String attr6;
	private String sel;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double salesPrice;
	private double qtyOnHand;
	private String articleNo;
	private String barCode;
	private double purPrice;
	private String batch;
	private Date expDate;
	private String search_text;

	public TransInLines() {
	}

	public TransInLines(String transId, short lineNo) {
		this.transId = transId;
		this.lineNo = lineNo;
	}

	public void setSelsPers(String selsPers) {
		this.selsPers = selsPers;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public double getQty() {
		return this.qty;
	}

	public String getTransId() {
		return this.transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public short getLineNo() {
		return this.lineNo;
	}

	public void setLineNo(short lineNo) {
		this.lineNo = lineNo;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public double getMrp() {
		return this.mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getDiscPer() {
		return this.discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public double getDiscAmt() {
		return this.discAmt;
	}

	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}

	public String getSelsPers() {
		return this.selsPers;
	}

	public String getTaxCode() {
		return this.taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public double getTaxPer() {
		return this.taxPer;
	}

	public void setTaxPer(double taxPer) {
		this.taxPer = taxPer;
	}

	public double getTaxAmt() {
		return this.taxAmt;
	}

	public void setTaxAmt(double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public double getNetAmt() {
		return this.netAmt;
	}

	public void setNetAmt(double netAmt) {
		this.netAmt = netAmt;
	}

	public String getSubSection() {
		return this.subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getItemSize() {
		return this.itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getStyle() {
		return this.style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getFabric() {
		return this.fabric;
	}

	public void setFabric(String fabric) {
		this.fabric = fabric;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAttr6() {
		return this.attr6;
	}

	public void setAttr6(String attr6) {
		this.attr6 = attr6;
	}

	public String getSel() {
		return this.sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getSalesPrice() {
		return this.salesPrice;
	}

	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}

	public double getQtyOnHand() {
		return this.qtyOnHand;
	}

	public void setQtyOnHand(double qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getBarCode() {
		return this.barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public double getPurPrice() {
		return this.purPrice;
	}

	public void setPurPrice(double purPrice) {
		this.purPrice = purPrice;
	}

	public String getSearch_text() {
		return this.search_text;
	}

	public void setSearch_text(String search_text) {
		this.search_text = search_text;
	}

	public String getBatch() {
		return this.batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpDate() {
		return this.expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		this.updatedOn = new Date();
		String sql = "update trans_in_lines\n   set item_code   = ?,\n       item_desc   = ?,\n       qty         = ?,\n       uom         = ?,\n       mrp         = ?,\n       disc_per    = ?,\n       disc_amt    = ?,\n       sels_pers    = ?,\n       tax_code    = ?,\n       tax_per     = ?,\n       tax_amt     = ?,\n       net_amt     = ?,\n       sub_section = ?,\n       section     = ?,\n       color       = ?,\n       item_size   = ?,\n       brand       = ?,\n       style       = ?,\n       fabric      = ?,\n       remark      = ?,\n       attr6       = ?,\n       sel         = ?,\n       created_on  = ?,\n       created_by  = ?,\n       updated_on  = ?,\n       updated_by  = ?,sales_price=?,\n       object_id   = object_id + 1,\n       article_no  = ?,bar_code=?, pur_price=?, batch=?, exp_date=?  where trans_id = ?\n   and line_no = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.itemCode);
		ps.setString(2, this.itemDesc);
		ps.setDouble(3, this.qty);
		ps.setString(4, this.uom);
		ps.setDouble(5, this.mrp);
		ps.setDouble(6, this.discPer);
		ps.setDouble(7, this.discAmt);
		ps.setString(8, this.selsPers);
		ps.setString(9, this.taxCode);
		ps.setDouble(10, this.taxPer);
		ps.setDouble(11, this.taxAmt);
		ps.setDouble(12, this.netAmt);
		ps.setString(13, this.subSection);
		ps.setString(14, this.section);
		ps.setString(15, this.color);
		ps.setString(16, this.itemSize);
		ps.setString(17, this.brand);
		ps.setString(18, this.style);
		ps.setString(19, this.fabric);
		ps.setString(20, this.remark);
		ps.setString(21, this.attr6);
		ps.setString(22, this.sel);
		ps.setDate(23, Utils.getSqlDate(this.createdOn));
		ps.setString(24, this.createdBy);
		ps.setDate(25, Utils.getSqlDate(this.updatedOn));
		ps.setString(26, this.updatedBy);
		ps.setDouble(27, this.salesPrice);
		ps.setString(28, this.articleNo);
		ps.setString(29, this.barCode);
		ps.setDouble(30, this.purPrice);
		ps.setString(31, getBatch());
		ps.setDate(32, Utils.getSqlDate(getExpDate()));
		ps.setString(33, this.transId);
		ps.setShort(34, this.lineNo);
		ps.setShort(35, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		return "success";
	}

	private String getNextItemCode(Connection con) throws SQLException {
		String sql = "select nvl((select c.item_code_prefix from company_parameters c where c.parameter_id='COMPANY_PARAM'),'')||item_code_seq.nextval from dual";
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		int i = 0;
		if (rs.next()) {
			i = (int) rs.getDouble(1);
		}
		rs.close();
		ps.close();
		return i + "";
	}

	public String create(Connection con) throws SQLException {
		setObjectId((short) 1);
		this.createdOn = new Date();
		String sql = "insert into trans_in_lines\n  (trans_id,\n   line_no,\n   item_code,\n   item_desc,\n   qty,\n   uom,\n   mrp,\n   disc_per,\n   disc_amt,\n   sels_pers,\n   tax_code,\n   tax_per,\n   tax_amt,\n   net_amt,\n   sub_section,\n   section,\n   color,\n   item_size,\n   brand,\n   style,\n   fabric,\n   remark,\n   attr6,\n   sel,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,sales_price   ,article_no,bar_code,pur_price,batch, exp_date)  values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.transId);
		ps.setShort(2, this.lineNo);
		if ((getItemCode() == null) || (getItemCode().length() <= 0)) {
			ps.setString(3, getNextItemCode(con));
		} else {
			ps.setString(3, this.itemCode);
		}
		ps.setString(4, this.itemDesc);
		ps.setDouble(5, this.qty);
		ps.setString(6, this.uom);
		ps.setDouble(7, this.mrp);
		ps.setDouble(8, this.discPer);
		ps.setDouble(9, this.discAmt);
		ps.setString(10, this.selsPers);
		ps.setString(11, this.taxCode);
		ps.setDouble(12, this.taxPer);
		ps.setDouble(13, this.taxAmt);
		ps.setDouble(14, this.netAmt);
		ps.setString(15, this.subSection);
		ps.setString(16, this.section);
		ps.setString(17, this.color);
		ps.setString(18, this.itemSize);
		ps.setString(19, this.brand);
		ps.setString(20, this.style);
		ps.setString(21, this.fabric);
		ps.setString(22, this.remark);
		ps.setString(23, this.attr6);
		ps.setString(24, this.sel);
		ps.setDate(25, Utils.getSqlDate(this.createdOn));
		ps.setString(26, this.createdBy);
		ps.setDate(27, Utils.getSqlDate(this.updatedOn));
		ps.setString(28, this.updatedBy);
		ps.setShort(29, this.objectId);
		ps.setDouble(30, this.salesPrice);
		ps.setString(31, this.articleNo);
		ps.setString(32, this.barCode);
		ps.setDouble(33, this.purPrice);
		ps.setString(34, getBatch());
		ps.setDate(35, Utils.getSqlDate(getExpDate()));

		int n = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public static List<TransInLines> load(Connection con, String il) throws SQLException {
		List<TransInLines> list = new ArrayList();
		TransInLines inl = null;
		String sql = "select trans_id,\n       line_no,\n       item_code,\n       item_desc,\n       qty,\n       uom,\n       mrp,\n       disc_per,\n       disc_amt,\n       sels_pers,\n       tax_code,\n       tax_per,\n       tax_amt,\n       net_amt,\n       sub_section,\n       section,\n       color,\n       item_size,\n       brand,\n       style,\n       fabric,\n       remark,\n       attr6,\n       sel,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,sales_price,article_no,bar_code, pur_price,batch, exp_date\n  from trans_in_lines\n where trans_id = ?\n order by line_no";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, il);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			inl = new TransInLines();
			inl.setTransId(rs.getString("trans_id"));
			inl.setLineNo((short) (int) rs.getDouble("line_No"));
			inl.setItemDesc(rs.getString("item_desc"));
			inl.setItemCode(rs.getString("item_code"));
			inl.setBrand(rs.getString("brand"));
			inl.setColor(rs.getString("color"));
			inl.setDiscPer(rs.getDouble("disc_Per"));
			inl.setDiscAmt(rs.getDouble("disc_amt"));
			inl.setSelsPers(rs.getString("sels_pers"));
			inl.setTaxCode(rs.getString("tax_code"));
			inl.setTaxPer(rs.getDouble("tax_Per"));
			inl.setTaxAmt(rs.getDouble("tax_amt"));
			inl.setNetAmt(rs.getDouble("net_Amt"));
			inl.setFabric(rs.getString("fabric"));
			inl.setItemSize(rs.getString("item_Size"));
			inl.setMrp(rs.getDouble("mrp"));
			inl.setAttr6(rs.getString("attr6"));
			inl.setRemark(rs.getString("remark"));
			inl.setSection(rs.getString("section"));
			inl.setSubSection(rs.getString("sub_Section"));
			inl.setStyle(rs.getString("style"));
			inl.setUom(rs.getString("uom"));
			inl.setQty(rs.getDouble("qty"));
			inl.setCreatedOn(rs.getDate("created_On"));
			inl.setCreatedBy(rs.getString("Created_By"));
			inl.setUpdatedOn(rs.getDate("updated_On"));
			inl.setUpdatedBy(rs.getString("updated_By"));
			inl.setObjectId((short) (int) rs.getDouble("object_Id"));
			inl.setSalesPrice(rs.getDouble("sales_price"));
			inl.setArticleNo(rs.getString("article_no"));
			inl.setBarCode(rs.getString("bar_code"));
			inl.setPurPrice(rs.getDouble("pur_price"));
			inl.setBatch(rs.getString("batch"));
			inl.setExpDate(rs.getDate("exp_date"));
			list.add(inl);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String delete(Connection con) throws SQLException, Exception {
		String sql = "delete from trans_in_lines where trans_id=? and line_no=? and object_Id=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.transId);
		ps.setShort(2, this.lineNo);
		ps.setDouble(3, this.objectId);
		int nrd = 0;
		nrd = ps.executeUpdate();
		if (nrd <= 0) {
			throw new Exception("Trans In Record could not be deleted.");
		}
		ps.close();
		return "success";
	}

	public TransInLines getDoulicate() {
		TransInLines pod = new TransInLines();
		pod.setTransId(this.transId);
		pod.setArticleNo(this.articleNo);
		pod.setBrand(this.brand);
		pod.setColor(this.color);
		pod.setDiscPer(this.discPer);
		pod.setFabric(this.fabric);
		pod.setItemSize(this.itemSize);
		pod.setMrp(this.mrp);
		pod.setSalesPrice(this.salesPrice);
		pod.setAttr6(this.attr6);
		pod.setRemark(this.remark);
		pod.setPurPrice(this.purPrice);
		pod.setSection(this.section);
		pod.setSubSection(this.subSection);
		pod.setStyle(this.style);
		pod.setPurPrice(this.purPrice);
		pod.setUom(this.uom);
		pod.setQty(this.qty);
		pod.setItemDesc(this.itemDesc);
		pod.setDiscAmt(this.discAmt);
		pod.setTaxPer(this.taxPer);
		pod.setTaxAmt(this.taxAmt);
		pod.setNetAmt(this.netAmt);
		pod.setBatch(this.batch);
		pod.setExpDate(this.expDate);
		return pod;
	}

	public String process() {
		double tot_price = getQty() * getPurPrice();

		setDiscAmt(tot_price * getDiscPer() / 100.0D);

		tot_price -= getDiscAmt();

		setTaxAmt(tot_price * getTaxPer() / 100.0D);

		tot_price += getTaxAmt();
		if (getQty() > 0.0D) {
			setPurPrice(tot_price / getQty());
		} else {
			setPurPrice(tot_price);
		}
		setNetAmt(tot_price);

		return "success";
	}

	public boolean equals(Object object) {
		if (!(object instanceof TransInLines)) {
			return false;
		}
		TransInLines other = (TransInLines) object;
		if (((this.transId == null) && (other.transId != null))
				|| ((this.transId != null) && (!this.transId.equals(other.transId))) || (this.lineNo != other.lineNo)) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Trans Line=[" + this.transId + "," + this.lineNo + " ]";
	}
}
