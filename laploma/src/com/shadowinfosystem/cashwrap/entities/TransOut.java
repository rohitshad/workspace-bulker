/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class TransOut extends EntitiesImpl implements Serializable {
	private String transId;
	private Date transDate;
	private String branchId;
	private String branchName;
	private String branchTransId;
	private Date branchTransDate;
	private double purchAmt;
	private double salesAmt;
	private double noOfItems;
	private double totalQuantity;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private Date fromDate;
	private Date toDate;
	private String noteText;
	private List<TransOutLines> lines;
	private TransOutLines lineSelected;

	public TransOut() {
		if (this.lines == null) {
			this.lines = new ArrayList();
		}
		if (this.lineSelected == null) {
			this.lineSelected = new TransOutLines();
		}
	}

	public TransOut(String transId) {
		this.transId = transId;
	}

	public String getTransId() {
		return this.transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public Date getTransDate() {
		return this.transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getBranchTransId() {
		return this.branchTransId;
	}

	public void setBranchTransId(String branchTransId) {
		this.branchTransId = branchTransId;
	}

	public Date getBranchTransDate() {
		return this.branchTransDate;
	}

	public void setBranchTransDate(Date branchTransDate) {
		this.branchTransDate = branchTransDate;
	}

	public double getPurchAmt() {
		return this.purchAmt;
	}

	public void setPurchAmt(double purchAmt) {
		this.purchAmt = purchAmt;
	}

	public double getSalesAmt() {
		return this.salesAmt;
	}

	public void setSalesAmt(double salesAmt) {
		this.salesAmt = salesAmt;
	}

	public double getNoOfItems() {
		return this.noOfItems;
	}

	public void setNoOfItems(double noOfItems) {
		this.noOfItems = noOfItems;
	}

	public double getTotalQuantity() {
		return this.totalQuantity;
	}

	public void setTotalQuantity(double totalQuantity) {
		this.totalQuantity = totalQuantity;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public List<TransOutLines> getLines() {
		return this.lines;
	}

	public void setLines(List<TransOutLines> lines) {
		this.lines = lines;
	}

	public TransOutLines getLineSelected() {
		return this.lineSelected;
	}

	public void setLineSelected(TransOutLines lineSelected) {
		this.lineSelected = lineSelected;
	}

	public String getBranchId() {
		return this.branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update trans_out\n   set trans_date        = ?,\n       branch_id         = ?,\n       branch_name       = ?,\n       branch_trans_id   = ?,\n       branch_trans_date = ?,\n       purch_amt         = ?,\n       sales_amt         = ?,\n       no_of_items       = ?,\n       total_quantity    = ?,\n       status            = ?,\n       created_on        = ?,\n       created_by        = ?,\n       updated_on        = ?,\n       updated_by        = ?,\n       object_id         = nvl(object_id, 0) + 1, note_text=? \n where trans_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(getTransDate()));
		ps.setString(2, this.branchId);
		ps.setString(3, this.branchName);
		ps.setString(4, this.branchTransId);
		ps.setDate(5, Utils.getSqlDate(getBranchTransDate()));
		ps.setDouble(6, this.purchAmt);
		ps.setDouble(7, this.salesAmt);
		ps.setDouble(8, this.noOfItems);
		ps.setDouble(9, this.totalQuantity);
		ps.setString(10, this.status);
		ps.setDate(11, Utils.getSqlDate(this.createdOn));
		ps.setString(12, this.createdBy);
		ps.setDate(13, Utils.getSqlDate(new Date()));
		ps.setString(14, getUserId());
		ps.setString(15, getNoteText());
		ps.setString(16, this.transId);
		ps.setDouble(17, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Trans In Record could not be updated.");
		}
		for (TransOutLines il : this.lines) {
			il.setTransId(this.transId);
			il.update(con);
		}
		return "Trans IN Record Updated.";
	}

	public String create(Connection con) throws SQLException, Exception {
		String sql = "insert into trans_out\n  (trans_id,\n   trans_date,\n   branch_id,\n   branch_name,\n   branch_trans_id,\n   branch_trans_date,\n   purch_amt,\n   sales_amt,\n   no_of_items,\n   total_quantity,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id, note_text)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.transId);
		ps.setDate(2, Utils.getSqlDate(getTransDate()));
		ps.setString(3, this.branchId);
		ps.setString(4, this.branchName);
		ps.setString(5, this.branchTransId);
		ps.setDate(6, Utils.getSqlDate(getBranchTransDate()));
		ps.setDouble(7, this.purchAmt);
		ps.setDouble(8, this.salesAmt);
		ps.setDouble(9, this.noOfItems);
		ps.setDouble(10, this.totalQuantity);
		ps.setString(11, this.status);
		ps.setDate(12, Utils.getSqlDate(new Date()));
		ps.setString(13, getUserId());
		ps.setDate(14, Utils.getSqlDate(this.updatedOn));
		ps.setString(15, this.updatedBy);
		ps.setDouble(16, 1.0D);
		ps.setString(17, getNoteText());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Trans In Record could not be created.");
		}
		for (TransOutLines il : this.lines) {
			il.setTransId(this.transId);
			il.update(con);
		}
		return "Record Created.";
	}

	public List<TransOut> getList(Connection con) throws SQLException {
		String sql = "select trans_id,\n       trans_date,\n       branch_id,\n       branch_name,\n       branch_trans_id,\n       branch_trans_date,\n       purch_amt,\n       sales_amt,\n       no_of_items,\n       total_quantity,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,note_text\n  from trans_out\n where 1 = 1";

		boolean doFetchData = false;
		if ((getTransId() != null) && (getTransId().trim().length() > 0)) {
			sql = sql + " and upper(trans_id) like upper('" + getTransId().trim() + "') ";
			doFetchData = true;
		}
		if ((getBranchTransId() != null) && (getBranchTransId().trim().length() > 0)) {
			sql = sql + " and upper(branch_trans_id) like upper('" + getBranchTransId().trim() + "') ";
			doFetchData = true;
		}
		if ((getBranchId() != null) && (getBranchId().trim().length() > 0)) {
			sql = sql + " and upper(branch_id) like upper('" + getBranchId().trim() + "') ";
			doFetchData = true;
		}
		if ((getBranchName() != null) && (getBranchName().trim().length() > 0)) {
			sql = sql + " and upper(branch_name) like upper('" + getBranchName().trim() + "') ";
			doFetchData = true;
		}
		if ((getNoteText() != null) && (getNoteText().trim().length() > 0)) {
			sql = sql + " and upper(note_text) like upper('" + getNoteText().trim() + "') ";
			doFetchData = true;
		}
		if (this.fromDate != null) {
			sql = sql + " and trunc(trans_date)  >=  to_date('" + Utils.getStrDate(this.fromDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (this.toDate != null) {
			sql = sql + " and trunc(trans_date)  <=  to_date('" + Utils.getStrDate(this.toDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().trim().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus().trim() + "') ";
			doFetchData = true;
		}
		List<TransOut> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			TransOut temp = new TransOut();
			temp.setTransId(rs.getString("trans_id"));
			temp.setTransDate(rs.getDate("trans_date"));
			temp.setBranchId(rs.getString("branch_id"));
			temp.setBranchName(rs.getString("branch_name"));
			temp.setBranchTransId(rs.getString("branch_trans_id"));
			temp.setBranchTransDate(rs.getDate("branch_trans_date"));
			temp.setPurchAmt(rs.getDouble("purch_amt"));
			temp.setSalesAmt(rs.getDouble("sales_amt"));
			temp.setNoOfItems(rs.getDouble("no_of_items"));
			temp.setTotalQuantity(rs.getDouble("total_quantity"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			temp.setNoteText(rs.getString("note_text"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException, Exception {
		String sql = "select trans_id,\n       trans_date,\n       branch_id,\n       branch_name,\n       branch_trans_id,\n       branch_trans_date,\n       purch_amt,\n       sales_amt,\n       no_of_items,\n       total_quantity,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,note_text\n  from trans_out\n where trans_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getTransId());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setTransId(rs.getString("trans_id"));
			setTransDate(rs.getDate("trans_date"));
			setBranchId(rs.getString("branch_id"));
			setBranchName(rs.getString("branch_name"));
			setBranchTransId(rs.getString("branch_trans_id"));
			setBranchTransDate(rs.getDate("branch_trans_date"));
			setPurchAmt(rs.getDouble("purch_amt"));
			setSalesAmt(rs.getDouble("sales_amt"));
			setNoOfItems(rs.getDouble("no_of_items"));
			setTotalQuantity(rs.getDouble("total_quantity"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
			setNoteText(rs.getString("note_text"));
		} else {
			throw new Exception("Trans out Record Not found :" + getTransId());
		}
		setLines(TransOutLines.load(con, getTransId()));
		rs.close();
		ps.close();
		return "success";
	}

	public String process(String isTaxInclusive) {
		processDetails(isTaxInclusive);
		processHeader();
		return "success";
	}

	public String processHeader() {
		return "success";
	}

	public String processDetails(String isTaxInclusive) {
		double grosssValue = 0.0D;
		double nettValue = 0.0D;
		double saleAmt = 0.0D;
		double totQty = 0.0D;
		for (TransOutLines l : this.lines) {
			l.process(isTaxInclusive);
			grosssValue += l.getPurPrice() * l.getQty();
			nettValue += l.getNetAmt();
			totQty += l.getQty();
			saleAmt += l.getSalesPrice() * l.getQty();
		}
		setPurchAmt(grosssValue);
		setSalesAmt(saleAmt);
		setTotalQuantity(totQty);
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.transId != null ? this.transId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof TransOut)) {
			return false;
		}
		TransOut other = (TransOut) object;
		if (((this.transId == null) && (other.transId != null))
				|| ((this.transId != null) && (!this.transId.equals(other.transId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "TransIn[" + this.transId + " ]";
	}
}
