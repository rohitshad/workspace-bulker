/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class CustPaymentReceipt extends EntitiesImpl implements Serializable {
	private String voucherNo;
	private Date voucherDate;
	private String payerRefId;
	private Date payerRefDate;
	private String custId;
	private String custName;
	private String payModeId;
	private String branchId;
	private double amount;
	private String ccCode;
	private String ccNo;
	private String bankName;
	private String chequeNo;
	private Date chequeDt;
	private String status;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp updatedOn;
	private String updatedBy;
	private double objectId;
	private String prefix;
	private Date fromDate;
	private Date toDate;
	private String remark;
	private String accountId;

	public String getVoucherNo() {
		return this.voucherNo;
	}

	public void setVoucherNo(String voucherNo) {
		this.voucherNo = voucherNo;
	}

	public Date getVoucherDate() {
		return this.voucherDate;
	}

	public void setVoucherDate(Date voucherDate) {
		this.voucherDate = voucherDate;
	}

	public String getPayerRefId() {
		return this.payerRefId;
	}

	public void setPayerRefId(String payerRefId) {
		this.payerRefId = payerRefId;
	}

	public Date getPayerRefDate() {
		return this.payerRefDate;
	}

	public void setPayerRefDate(Date payerRefDate) {
		this.payerRefDate = payerRefDate;
	}

	public String getCustId() {
		return this.custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getBranchId() {
		return this.branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getCcCode() {
		return this.ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getCcNo() {
		return this.ccNo;
	}

	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getChequeDt() {
		return this.chequeDt;
	}

	public void setChequeDt(Date chequeDt) {
		this.chequeDt = chequeDt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public String getPayModeId() {
		return this.payModeId;
	}

	public void setPayModeId(String payModeId) {
		this.payModeId = payModeId;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAccountId() {
		return this.accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update cust_payment_receipt\n   set voucher_date   = ?,\n       payer_ref_id   = ?,\n       payer_ref_date = ?,\n       cust_id       = ?,\n       cust_name     = ?,\n       pay_mode_id    = ?,\n       branch_id      = ?,\n       amount         = ?,\n       cc_code        = ?,\n       cc_no          = ?,\n       bank_name      = ?,\n       cheque_no      = ?,\n       cheque_dt      = ?,\n       status         = ?,\n       created_on     = ?,\n       created_by     = ?,\n       updated_on     = ?,\n       updated_by     = ?,\n       object_id      = nvl(object_id, 0) + 1, remark=?, account_id=?\n where voucher_no = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(getVoucherDate()));
		ps.setString(2, getPayerRefId());
		ps.setDate(3, Utils.getSqlDate(getPayerRefDate()));
		ps.setString(4, getCustId());
		ps.setString(5, getCustName());
		ps.setString(6, getPayModeId());
		ps.setString(7, getBranchId());
		ps.setDouble(8, getAmount());
		ps.setString(9, getCcCode());
		ps.setString(10, getCcNo());
		ps.setString(11, getBankName());
		ps.setString(12, getChequeNo());
		ps.setDate(13, Utils.getSqlDate(getChequeDt()));
		ps.setString(14, getStatus());
		ps.setTimestamp(15, Utils.getTimestamp(getCreatedOn()));
		ps.setString(16, getCreatedBy());
		ps.setTimestamp(17, Utils.getTimestamp(new Date()));
		ps.setString(18, getUserId());
		ps.setString(19, getRemark());
		ps.setString(20, getAccountId());
		ps.setString(21, getVoucherNo());
		ps.setDouble(22, getObjectId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Cust Payment Receipt Voucher Record could not be updated.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException, Exception {
		String sql = "insert into cust_payment_receipt\n  (voucher_no,\n   voucher_date,\n   payer_ref_id,\n   payer_ref_date,\n   cust_id,\n   cust_name,\n   pay_mode_id,\n   branch_id,\n   amount,\n   cc_code,\n   cc_no,\n   bank_name,\n   cheque_no,\n   cheque_dt,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,remark, account_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getVoucherNo());
		ps.setDate(2, Utils.getSqlDate(new Date()));
		ps.setString(3, getPayerRefId());
		ps.setDate(4, Utils.getSqlDate(getPayerRefDate()));
		ps.setString(5, getCustId());
		ps.setString(6, getCustName());
		ps.setString(7, getPayModeId());
		ps.setString(8, getBranchId());
		ps.setDouble(9, getAmount());
		ps.setString(10, getCcCode());
		ps.setString(11, getCcNo());
		ps.setString(12, getBankName());
		ps.setString(13, getChequeNo());
		ps.setDate(14, Utils.getSqlDate(getChequeDt()));
		ps.setString(15, getStatus());
		ps.setTimestamp(16, Utils.getTimestamp(new Date()));
		ps.setString(17, getUserId());
		ps.setTimestamp(18, Utils.getTimestamp(getUpdatedOn()));
		ps.setString(19, getUpdatedBy());
		ps.setDouble(20, 1.0D);
		ps.setString(21, getRemark());
		ps.setString(22, getAccountId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Cust Payemet Receipt Record could not be created.");
		}
		return "Record Created.";
	}

	public List<CustPaymentReceipt> getList(Connection con) throws SQLException {
		String sql = "select voucher_no,\n       voucher_date,\n       payer_ref_id,\n       payer_ref_date,\n       cust_id,\n       cust_name,\n       pay_mode_id,\n       branch_id,\n       amount,\n       cc_code,\n       cc_no,\n       bank_name,\n       cheque_no,\n       cheque_dt,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,remark, account_id \n  from cust_payment_receipt\n where 1 = 1 ";

		boolean doFetchData = false;
		if ((getVoucherNo() != null) && (getVoucherNo().length() > 0)) {
			sql = sql + " and upper(voucher_no) like upper('" + getVoucherNo() + "') ";
			doFetchData = true;
		}
		if ((getPayerRefId() != null) && (getPayerRefId().length() > 0)) {
			sql = sql + " and upper(payer_ref_id) like upper('" + getPayerRefId() + "') ";
			doFetchData = true;
		}
		if ((getCustId() != null) && (getCustId().length() > 0)) {
			sql = sql + " and upper(cust_id) like upper('" + getCustId() + "') ";
			doFetchData = true;
		}
		if ((getCustName() != null) && (getCustName().length() > 0)) {
			sql = sql + " and upper(cust_name) like upper('" + getCustName() + "') ";
			doFetchData = true;
		}
		if ((getPayModeId() != null) && (getPayModeId().length() > 0)) {
			sql = sql + " and upper(pay_mode_id) like upper('" + getPayModeId() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus() + "') ";
			doFetchData = true;
		}
		if (this.fromDate != null) {
			sql = sql + " and trunc(voucher_date)  >=  to_date('" + Utils.getStrDate(this.fromDate)
					+ "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (this.toDate != null) {
			sql = sql + " and trunc(voucher_date)  <=  to_date('" + Utils.getStrDate(this.toDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		List<CustPaymentReceipt> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			CustPaymentReceipt temp = new CustPaymentReceipt();
			temp.setVoucherNo(rs.getString("voucher_no"));
			temp.setVoucherDate(rs.getDate("voucher_Date"));
			temp.setPayerRefId(rs.getString("payer_Ref_Id"));
			temp.setPayerRefDate(rs.getDate("payer_Ref_Date"));
			temp.setCustId(rs.getString("cust_Id"));
			temp.setCustName(rs.getString("cust_Name"));
			temp.setPayModeId(rs.getString("pay_Mode_Id"));
			temp.setBranchId(rs.getString("branch_id"));
			temp.setAmount(rs.getDouble("amount"));
			temp.setCcCode(rs.getString("cc_code"));
			temp.setCcNo(rs.getString("cc_no"));
			temp.setBankName(rs.getString("bank_name"));
			temp.setChequeNo(rs.getString("cheque_no"));
			temp.setChequeDt(rs.getDate("cheque_dt"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getTimestamp("created_on"));
			temp.setCreatedBy(rs.getString("created_by"));
			temp.setUpdatedOn(rs.getTimestamp("updated_on"));
			temp.setUpdatedBy(rs.getString("updated_by"));
			temp.setObjectId(rs.getDouble("object_id"));
			temp.setRemark(rs.getString("remark"));
			temp.setAccountId(rs.getString("account_id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select voucher_no,\n       voucher_date,\n       payer_ref_id,\n       payer_ref_date,\n       cust_id,\n       cust_name,\n       pay_mode_id,\n       branch_id,\n       amount,\n       cc_code,\n       cc_no,\n       bank_name,\n       cheque_no,\n       cheque_dt,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id, remark, account_id\n  from cust_payment_receipt\n where voucher_no = ? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getVoucherNo());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setVoucherNo(rs.getString("voucher_no"));
			setVoucherDate(rs.getDate("voucher_Date"));
			setPayerRefId(rs.getString("payer_Ref_Id"));
			setPayerRefDate(rs.getDate("payer_Ref_Date"));
			setCustId(rs.getString("cust_Id"));
			setCustName(rs.getString("cust_Name"));
			setPayModeId(rs.getString("pay_Mode_Id"));
			setBranchId(rs.getString("branch_id"));
			setAmount(rs.getDouble("amount"));
			setCcCode(rs.getString("cc_code"));
			setCcNo(rs.getString("cc_no"));
			setBankName(rs.getString("bank_name"));
			setChequeNo(rs.getString("cheque_no"));
			setChequeDt(rs.getDate("cheque_dt"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getTimestamp("created_on"));
			setCreatedBy(rs.getString("created_by"));
			setUpdatedOn(rs.getTimestamp("updated_on"));
			setUpdatedBy(rs.getString("updated_by"));
			setObjectId(rs.getDouble("object_id"));
			setRemark(rs.getString("remark"));
			setAccountId(rs.getString("account_id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.voucherNo != null ? this.voucherNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof CustPaymentReceipt)) {
			return false;
		}
		CustPaymentReceipt other = (CustPaymentReceipt) object;
		if (((this.voucherNo == null) && (other.voucherNo != null))
				|| ((this.voucherNo != null) && (!this.voucherNo.equals(other.voucherNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Receipt [ voucherNo=" + this.voucherNo + " ]";
	}
}
