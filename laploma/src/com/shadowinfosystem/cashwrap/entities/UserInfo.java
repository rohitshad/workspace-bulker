/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.ServletException;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;

public class UserInfo extends EntitiesImpl implements Serializable {
	private String loginId;
	private String userName;
	private String userPassword;
	private String status;
	private List<UserRoles> rolesList;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private String oldPassword;
	private String newPassword;
	private String confirmPassword;
	private boolean isAuthenticated;
	private String tempRole;
	private String defaultPrinter;

	public UserInfo() {
		this.isAuthenticated = false;
		if (this.rolesList == null) {
			this.rolesList = new ArrayList();
		}
	}

	public UserInfo(String loginId) {
		this.loginId = loginId;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserPassword() {
		return this.userPassword;
	}

	public void setUserPassword(String userPassword) {
		this.userPassword = userPassword;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public boolean isIsAuthenticated() throws ServletException, IOException {
		return this.isAuthenticated;
	}

	public void setIsAuthenticated(boolean isAuthenticated) {
		this.isAuthenticated = isAuthenticated;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String getOldPassword() {
		return this.oldPassword;
	}

	public void setOldPassword(String oldPassword) {
		this.oldPassword = oldPassword;
	}

	public String getNewPassword() {
		return this.newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmPassword() {
		return this.confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public List<UserRoles> getRolesList() {
		return this.rolesList;
	}

	public void setRolesList(List<UserRoles> rolesList) {
		this.rolesList = rolesList;
	}

	public String getTempRole() {
		return this.tempRole;
	}

	public void setTempRole(String tempRole) {
		this.tempRole = tempRole;
	}

	public String getDefaultPrinter() {
		return this.defaultPrinter;
	}

	public void setDefaultPrinter(String defaultPrinter) {
		this.defaultPrinter = defaultPrinter;
	}

	public String changePassword(Connection con) throws Exception {
		String sql = "UPDATE user_info u\n   SET u.user_password = ?,\n       u.object_id     = u.object_id + 1,\n       u.updated_on    = sysdate,\n       u.updated_by    = ?\n where u.user_id = ?\n   and u.object_id = ?\n   and u.user_password = ?";

		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, getNewPassword());
			ps.setString(2, getUpdatedBy());
			ps.setString(3, getLoginId());
			ps.setDouble(4, getObjectId());
			ps.setString(5, getOldPassword());

			int nru = 0;
			nru = ps.executeUpdate();
			ps.close();
			if (nru == 1) {
				return "Password Changanged Successfully.";
			}
			throw new Exception("Password Could not be changed.");
		} catch (SQLException ex) {
			Logger.getLogger(UserInfo.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "Password Could not be changed.";
	}

	public String changeUserPassword(Connection con) throws Exception {
		String sql = "UPDATE user_info u\n   SET u.user_password = ?,\n       u.object_id     = u.object_id + 1,\n       u.updated_on    = sysdate,\n       u.updated_by    = ?\n where u.user_id = ?\n   and u.object_id = ?\n";

		PreparedStatement ps = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, getNewPassword());
			ps.setString(2, getUpdatedBy());
			ps.setString(3, getLoginId());
			ps.setDouble(4, getObjectId());

			int nru = 0;
			nru = ps.executeUpdate();
			ps.close();
			if (nru == 1) {
				return "Password Changanged Successfully.";
			}
			throw new Exception("Password Could not be changed.");
		} catch (SQLException ex) {
			Logger.getLogger(UserInfo.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "Password Could not be changed.";
	}

	public boolean isAuthentic(String user_id, String password) {
		if (user_id == null) {
			return false;
		}
		if (password == null) {
			return false;
		}
		Connection con = null;
		try {
			con = Data.getConnection();

			setLoginId(user_id);
			load(con);
			con.close();
			if ((getStatus() == null) && (getStatus().equals("InActive"))) {
				return false;
			}
			if (getUserPassword().equals(password)) {
				setIsAuthenticated(true);
				return true;
			}
		} catch (SQLException ex) {
			System.out.println(ex + "");
		} catch (Exception e) {
			System.out.println(e + "");
		}
		return false;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		setUpdatedOn(new Date());
		String sql = "update user_info i\n   set i.user_name     = ?,\n       i.status        = ?,\n       i.created_on    = ?,\n       i.created_by    = ?,\n       i.updated_on    = ?,\n       i.updated_by    = ?,\n       i.object_id     = i.object_id +1, default_printer=? \n where i.user_id = ?\n   and i.object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.userName);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(this.updatedOn));
		ps.setString(6, this.updatedBy);
		ps.setString(7, getDefaultPrinter());
		ps.setString(8, this.loginId);
		ps.setDouble(9, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified By Other User.");
		}
		for (UserRoles ur : this.rolesList) {
			ur.setUserId(getUserId());
			ur.update(con);
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		setObjectId(1.0D);
		setCreatedOn(new Date());
		String sql = "insert into user_info\n  (user_id,\n   user_name,\n   user_password,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.loginId);
		ps.setString(2, this.userName);
		ps.setString(3, this.loginId);
		ps.setString(4, this.status);
		ps.setDate(5, Utils.getSqlDate(this.createdOn));
		ps.setString(6, this.createdBy);
		ps.setDate(7, Utils.getSqlDate(this.updatedOn));
		ps.setString(8, this.updatedBy);
		ps.setDouble(9, this.objectId);

		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		for (UserRoles ur : this.rolesList) {
			ur.update(con);
		}
		return "Record Created.";
	}

	public List<UserInfo> getList(Connection con) throws SQLException {
		String sql = "select user_id,\n       user_name,\n       user_password,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,default_printer\n  from user_info i\n where i.user_id like nvl(?, user_id)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.loginId);

		ResultSet rs = ps.executeQuery();
		List<UserInfo> list = new ArrayList();
		while (rs.next()) {
			UserInfo temp = new UserInfo();
			temp.setLoginId(rs.getString("user_id"));
			temp.setUserName(rs.getString("user_name"));
			temp.setUserPassword(rs.getString("user_password"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			temp.setDefaultPrinter(rs.getString("default_printer"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public List<UserInfo> getAllList(Connection con) throws SQLException {
		String sql = "select user_id,\n       user_name,\n       user_password,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,default_printer\n  from user_info i\n";

		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		List<UserInfo> list = new ArrayList();
		while (rs.next()) {
			UserInfo temp = new UserInfo();
			temp.setLoginId(rs.getString("user_id"));
			temp.setUserName(rs.getString("user_name"));
			temp.setUserPassword(rs.getString("user_password"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			temp.setDefaultPrinter(rs.getString("default_printer"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select user_id,\n       user_name,\n       user_password,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,default_printer\n  from user_info i\n where i.user_id =?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.loginId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setLoginId(rs.getString("user_id"));
			setUserName(rs.getString("user_name"));
			setUserPassword(rs.getString("user_password"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
			setDefaultPrinter(rs.getString("default_printer"));
		}
		rs.close();
		ps.close();
		UserRoles t = new UserRoles();
		t.setLoginId(getLoginId());
		setRolesList(t.getList(con));
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.loginId != null ? this.loginId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof UserInfo)) {
			return false;
		}
		UserInfo other = (UserInfo) object;
		if (((this.loginId == null) && (other.loginId != null))
				|| ((this.loginId != null) && (!this.loginId.equals(other.loginId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "UuserId :" + this.loginId + "";
	}
}
