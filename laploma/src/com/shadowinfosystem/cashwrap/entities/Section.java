/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Section extends EntitiesImpl implements Serializable {
	private String sectionNo;
	private String sectionDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double taxPerc;
	private double discPerc;

	public Section() {
	}

	public Section(String sectionNo) {
		this.sectionNo = sectionNo;
	}

	public String getSectionNo() {
		return this.sectionNo;
	}

	public void setSectionNo(String sectionNo) {
		this.sectionNo = sectionNo;
	}

	public String getSectionDesc() {
		return this.sectionDesc;
	}

	public void setSectionDesc(String sectionDesc) {
		this.sectionDesc = sectionDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getTaxPerc() {
		return this.taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	public double getDiscPerc() {
		return this.discPerc;
	}

	public void setDiscPerc(double discPerc) {
		this.discPerc = discPerc;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update section p\nset section_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1, tax_perc=?, disc_perc=?\nwhere section_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.sectionDesc);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setDouble(7, this.taxPerc);
		ps.setDouble(8, this.discPerc);
		ps.setString(9, this.sectionNo);
		ps.setShort(10, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by Other User.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into section\n  (section_no,\n   section_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by, tax_perc, disc_perc   )\nvalues\n  (?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.sectionNo);
		ps.setString(2, this.sectionDesc);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		ps.setDouble(8, this.taxPerc);
		ps.setDouble(9, this.discPerc);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could Not Be created.");
		}
		return "Record Created";
	}

	public List<Section> getList(Connection con) throws SQLException {
		String sql = "select section_no,\n       section_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id, tax_perc, disc_perc\n  from section p\n where 1 = 1";

		boolean doFetchData = false;
		if ((getSectionNo() != null) && (getSectionNo().length() > 0)) {
			sql = sql + " and upper(section_no) like upper('" + getSectionNo() + "') ";
			doFetchData = true;
		}
		if ((getSectionDesc() != null) && (getSectionDesc().length() > 0)) {
			sql = sql + " and upper(section_desc) like upper('" + getSectionDesc() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus() + "') ";
			doFetchData = true;
		}
		List<Section> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Section temp = new Section();
			temp.setSectionNo(rs.getString("section_no"));
			temp.setSectionDesc(rs.getString("section_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTaxPerc(rs.getDouble("tax_perc"));
			temp.setDiscPerc(rs.getDouble("disc_perc"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select section_no,\n       section_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id, tax_perc, disc_perc\n  from section p\n where section_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.sectionNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setSectionNo(rs.getString("section_no"));
			setSectionDesc(rs.getString("section_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setTaxPerc(rs.getDouble("tax_perc"));
			setDiscPerc(rs.getDouble("disc_perc"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "delete from section p\nwhere section_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.sectionNo);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by Other User.");
		}
		return "Record Deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.sectionNo != null ? this.sectionNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Section)) {
			return false;
		}
		Section other = (Section) object;
		if (((this.sectionNo == null) && (other.sectionNo != null))
				|| ((this.sectionNo != null) && (!this.sectionNo.equals(other.sectionNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return " sectionNo=" + this.sectionNo + "";
	}
}
