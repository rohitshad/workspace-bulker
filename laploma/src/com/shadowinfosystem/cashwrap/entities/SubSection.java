/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class SubSection extends EntitiesImpl implements Serializable {
	private String subSection;
	private String sectionNo;
	private String subSectionDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double taxPerc;
	private double discPerc;

	public SubSection() {
	}

	public String getSectionNo() {
		return this.sectionNo;
	}

	public void setSectionNo(String sectionNo) {
		this.sectionNo = sectionNo;
	}

	public String getSubSection() {
		return this.subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public SubSection(String subSection, String sectionNo) {
		this.subSection = subSection;
		this.sectionNo = sectionNo;
	}

	public String getSubSectionDesc() {
		return this.subSectionDesc;
	}

	public void setSubSectionDesc(String subSectionDesc) {
		this.subSectionDesc = subSectionDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getTaxPerc() {
		return this.taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	public double getDiscPerc() {
		return this.discPerc;
	}

	public void setDiscPerc(double discPerc) {
		this.discPerc = discPerc;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update sub_section p\nset sub_section_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1, tax_perc=?, disc_perc=? \nwhere sub_section=? and section_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.subSectionDesc);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setDouble(7, this.taxPerc);
		ps.setDouble(8, this.discPerc);
		ps.setString(9, this.subSection);
		ps.setString(10, this.sectionNo);
		ps.setShort(11, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by Other User.");
		}
		return "Sub Sec record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into sub_section\n  (sub_section,section_no,\n   sub_section_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,tax_perc,disc_perc)\nvalues\n  (?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.subSection);
		ps.setString(2, this.sectionNo);
		ps.setString(3, this.subSectionDesc);
		ps.setString(4, this.status);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setDate(7, Utils.getSqlDate(this.updatedOn));
		ps.setString(8, this.updatedBy);
		ps.setDouble(9, this.taxPerc);
		ps.setDouble(10, this.discPerc);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could Not Be created.");
		}
		return "Sub Section Record Created.";
	}

	public List<SubSection> getList(Connection con) throws SQLException {
		String sql = "select sub_section,section_no,\n       sub_section_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id, tax_perc, disc_perc \n  from sub_section p\n where sub_section like nvl(?,'%') and section_no like nvl(?,'%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.subSection);
		ps.setString(2, this.sectionNo);

		ResultSet rs = ps.executeQuery();
		List<SubSection> list = new ArrayList();
		while (rs.next()) {
			SubSection temp = new SubSection();
			temp.setSubSection(rs.getString("sub_Section"));
			temp.setSectionNo(rs.getString("section_no"));
			temp.setSubSectionDesc(rs.getString("sub_section_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTaxPerc(rs.getDouble("tax_perc"));
			temp.setDiscPerc(rs.getDouble("disc_perc"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public List<SubSection> getSubSecList(Connection con, String sec) throws SQLException {
		String sql = "select sub_section,section_no,\n       sub_section_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,tax_perc,disc_perc \n  from sub_section p\n where section_no  =?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, sec);

		ResultSet rs = ps.executeQuery();
		List<SubSection> list = new ArrayList();
		while (rs.next()) {
			SubSection temp = new SubSection();
			temp.setSubSection(rs.getString("sub_Section"));
			temp.setSectionNo(rs.getString("section_no"));
			temp.setSubSectionDesc(rs.getString("sub_section_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTaxPerc(rs.getDouble("tax_perc"));
			temp.setDiscPerc(rs.getDouble("disc_perc"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select sub_section,section_no,\n       sub_section_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,tax_perc, disc_perc \n  from sub_section p\n where sub_section=? and section_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.subSection);
		ps.setString(2, this.sectionNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setSubSection(rs.getString("sub_Section"));
			setSectionNo(rs.getString("section_no"));
			setSubSectionDesc(rs.getString("sub_section_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setTaxPerc(rs.getDouble("tax_perc"));
			setDiscPerc(rs.getDouble("disc_perc"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "delete from sub_section p\nwhere sub_section=? and section_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.subSection);
		ps.setString(2, this.sectionNo);
		ps.setShort(3, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by Other User.");
		}
		return "Sub Section record Deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.subSection != null ? this.subSection.hashCode() : 0);
		hash += (this.sectionNo != null ? this.sectionNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof SubSection)) {
			return false;
		}
		SubSection other = (SubSection) object;
		if (((this.subSection == null) && (other.subSection != null))
				|| ((this.subSection != null) && (!this.subSection.equals(other.subSection)))) {
			return false;
		}
		if (((this.sectionNo == null) && (other.sectionNo != null))
				|| ((this.sectionNo != null) && (!this.sectionNo.equals(other.sectionNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "subSection=" + this.subSection + ", sectionNo=" + this.sectionNo + " ";
	}
}
