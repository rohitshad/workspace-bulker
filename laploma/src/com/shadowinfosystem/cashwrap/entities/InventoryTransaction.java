/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;

public class InventoryTransaction extends EntitiesImpl implements Serializable {
	private Long transactionId;
	private String transactionCode;
	private String itemCode;
	private String articleNo;
	private String itemDesc;
	private double qty;
	private String uom;
	private String direction;
	private double pricePerUnit;
	private double price;
	private String orderNo;
	private short lineNo;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp updatedOn;
	private String updatedBy;
	private short objectId;
	private Date fromDate;
	private Date toDate;

	public InventoryTransaction() {
	}

	public InventoryTransaction(Long transactionId) {
		this.transactionId = transactionId;
	}

	public Long getTransactionId() {
		return this.transactionId;
	}

	public void setTransactionId(Long transactionId) {
		this.transactionId = transactionId;
	}

	public String getTransactionCode() {
		return this.transactionCode;
	}

	public void setTransactionCode(String transactionCode) {
		this.transactionCode = transactionCode;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public double getQty() {
		return this.qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getDirection() {
		return this.direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public double getPricePerUnit() {
		return this.pricePerUnit;
	}

	public void setPricePerUnit(double pricePerUnit) {
		this.pricePerUnit = pricePerUnit;
	}

	public double getPrice() {
		return this.price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String getOrderNo() {
		return this.orderNo;
	}

	public void setOrderNo(String orderNo) {
		this.orderNo = orderNo;
	}

	public short getLineNo() {
		return this.lineNo;
	}

	public void setLineNo(short lineNo) {
		this.lineNo = lineNo;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update inventory_transaction\n   set transaction_code = ?,\n       item_code        = ?,\n       item_desc        = ?,\n       qty              = ?,\n       uom              = ?,\n       direction        = ?,\n       price_per_unit   = ?,\n       price            = ?,\n       order_no         = ?,\n       line_no          = ?,\n       updated_on       = sysdate,\n       updated_by       = ?,\n       object_id        = nvl(object_id, 1) + 1,        article_no       = ?  where transaction_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.transactionCode);
		ps.setString(2, this.itemCode);
		ps.setString(3, this.itemDesc);
		ps.setDouble(4, this.qty);
		ps.setString(5, this.uom);
		ps.setString(6, this.direction);
		ps.setDouble(7, this.pricePerUnit);
		ps.setDouble(8, this.price);
		ps.setString(9, this.orderNo);
		ps.setShort(10, this.lineNo);
		ps.setString(11, this.updatedBy);
		ps.setString(12, this.articleNo);
		ps.setLong(13, this.transactionId.longValue());
		ps.setDouble(14, this.objectId);

		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Record could not be updated.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException, Exception {
		String sql = "insert into inventory_transaction\n  (transaction_id,\n   transaction_code,\n   item_code,\n   item_desc,\n   qty,\n   uom,\n   direction,\n   price_per_unit,\n   price,\n   order_no,\n   line_no,\n   created_by,   article_no)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, this.transactionId.longValue());
		ps.setString(2, this.transactionCode);
		ps.setString(3, this.itemCode);
		ps.setString(4, this.itemDesc);
		ps.setDouble(5, this.qty);
		ps.setString(6, this.uom);
		ps.setString(7, this.direction);
		ps.setDouble(8, this.pricePerUnit);
		ps.setDouble(9, this.price);
		ps.setString(10, this.orderNo);
		ps.setShort(11, this.lineNo);
		ps.setString(12, getUserId());
		ps.setString(13, this.articleNo);

		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Could Not Update Created.");
		}
		return "Record Created.";
	}

	public String load(Connection con) throws SQLException {
		String sql = "select transaction_id,\n       transaction_code,\n       item_code,\n       item_desc,\n       qty,\n       uom,\n       direction,\n       price_per_unit,\n       price,\n       order_no,\n       line_no,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,       article_no   from inventory_transaction where transaction_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getItemCode());
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			setTransactionId(Long.valueOf(rs.getLong("transaction_id")));
			setTransactionCode(rs.getString("transaction_code"));
			setItemCode(rs.getString("item_Code"));
			setItemDesc(rs.getString("item_Desc"));
			setQty(rs.getDouble("qty"));
			setUom(rs.getString("uom"));
			setDirection(rs.getString("direction"));
			setPricePerUnit(rs.getDouble("price_Per_Unit"));
			setPrice(rs.getDouble("price"));
			setOrderNo(rs.getString("order_No"));
			setLineNo(rs.getShort("line_No"));
			setCreatedOn(rs.getTimestamp("created_On"));
			setCreatedBy(rs.getString("created_By"));
			setUpdatedOn(rs.getTimestamp("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getShort("object_Id"));
			setArticleNo(rs.getString("article_No"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public List<InventoryTransaction> getList(Connection con) throws SQLException {
		List<InventoryTransaction> list = new ArrayList();

		String sql = "select transaction_id,\n       transaction_code,\n       item_code,\n       item_desc,\n       qty,\n       uom,\n       direction,\n       price_per_unit,\n       price,\n       order_no,\n       line_no,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id, article_no \n  from inventory_transaction where 1 = 1 ";

		boolean doFetchData = false;
		if ((getTransactionCode() != null) && (getTransactionCode().length() > 0)) {
			sql = sql + " AND upper(transaction_code)  like upper('" + getTransactionCode() + "') ";
			doFetchData = true;
		}
		if ((getItemCode() != null) && (getItemCode().length() > 0)) {
			sql = sql + " AND upper(item_code)  like upper('" + getItemCode() + "') ";
			doFetchData = true;
		}
		if ((getItemDesc() != null) && (getItemDesc().trim().length() > 0)) {
			sql = sql + " AND upper(item_desc)  like upper('" + getItemDesc().trim() + "') ";
			doFetchData = true;
		}
		if ((getDirection() != null) && (getDirection().trim().length() > 0)) {
			sql = sql + " AND upper(direction)  like upper('" + getDirection().trim() + "') ";
			doFetchData = true;
		}
		if ((getDirection() != null) && (getDirection().trim().length() > 0)) {
			sql = sql + " AND upper(direction)  like upper('" + getDirection().trim() + "') ";
			doFetchData = true;
		}
		if ((getOrderNo() != null) && (getOrderNo().trim().length() > 0)) {
			sql = sql + " AND upper(order_no)  like upper('" + getOrderNo().trim() + "') ";
			doFetchData = true;
		}
		if ((getArticleNo() != null) && (getArticleNo().trim().length() > 0)) {
			sql = sql + " AND upper(article_no)  like upper('" + getArticleNo().trim() + "') ";
			doFetchData = true;
		}
		if (this.fromDate != null) {
			sql = sql + " and trunc(created_on)  >=  to_date('" + Utils.getStrDate(this.fromDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (this.toDate != null) {
			sql = sql + " and trunc(created_on)  >=  to_date('" + Utils.getStrDate(this.toDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			InventoryTransaction it = new InventoryTransaction();
			it.setTransactionId(Long.valueOf(rs.getLong("transaction_id")));
			it.setTransactionCode(rs.getString("transaction_code"));
			it.setItemCode(rs.getString("item_Code"));
			it.setItemDesc(rs.getString("item_Desc"));
			it.setQty(rs.getDouble("qty"));
			it.setUom(rs.getString("uom"));
			it.setDirection(rs.getString("direction"));
			it.setPricePerUnit(rs.getDouble("price_Per_Unit"));
			it.setPrice(rs.getDouble("price"));
			it.setOrderNo(rs.getString("order_No"));
			it.setLineNo(rs.getShort("line_No"));
			it.setCreatedOn(rs.getTimestamp("created_On"));
			it.setCreatedBy(rs.getString("created_By"));
			it.setUpdatedOn(rs.getTimestamp("updated_On"));
			it.setUpdatedBy(rs.getString("updated_By"));
			it.setObjectId(rs.getShort("object_id"));
			it.setArticleNo(rs.getString("article_No"));
			list.add(it);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String delete(Connection con) throws SQLException, Exception {
		String sql = "delete from inventory_transaction where transaction_id=? and object_Id=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, this.transactionId.longValue());
		ps.setDouble(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Could Not Delete.");
		}
		return "Record Deleted.";
	}

	public Long getNextTransactionId() throws SQLException {
		Connection con = Data.getConnection();
		String sql = "select inventory_trans_seq.nextval from dual";
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		Long i = Long.valueOf(0L);
		if (rs.next()) {
			i = Long.valueOf(rs.getLong(1));
		}
		rs.close();
		ps.close();
		con.close();
		return i;
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.transactionId != null ? this.transactionId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof InventoryTransaction)) {
			return false;
		}
		InventoryTransaction other = (InventoryTransaction) object;
		return ((this.transactionId != null) || (other.transactionId == null))
				&& ((this.transactionId == null) || (this.transactionId.equals(other.transactionId)));
	}

	public String toString() {
		return "Transaction Id :" + this.transactionId + " ";
	}
}
