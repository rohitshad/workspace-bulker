/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;

public class AccountTransactions extends EntitiesImpl implements Serializable {
	private Long transId;
	private Date transDate;
	private String transCode;
	private String accountId;
	private String transMode;
	private String drCr;
	private double amount;
	private String refId;
	private Date refDate;
	private String bankName;
	private String chequeNo;
	private Date chequeDt;
	private String status;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp updatedOn;
	private String updatedBy;
	private double objectId;
	private String remark;
	private Date fromDate;
	private Date toDate;

	public AccountTransactions() {
	}

	public AccountTransactions(Long transId) {
		this.transId = transId;
	}

	public Long getTransId() {
		return this.transId;
	}

	public void setTransId(Long transId) {
		this.transId = transId;
	}

	public Date getTransDate() {
		return this.transDate;
	}

	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}

	public String getTransCode() {
		return this.transCode;
	}

	public void setTransCode(String transCode) {
		this.transCode = transCode;
	}

	public String getAccountId() {
		return this.accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getDrCr() {
		return this.drCr;
	}

	public void setDrCr(String drCr) {
		this.drCr = drCr;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public String getRefId() {
		return this.refId;
	}

	public void setRefId(String refId) {
		this.refId = refId;
	}

	public Date getRefDate() {
		return this.refDate;
	}

	public void setRefDate(Date refDate) {
		this.refDate = refDate;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getChequeDt() {
		return this.chequeDt;
	}

	public void setChequeDt(Date chequeDt) {
		this.chequeDt = chequeDt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String getTransMode() {
		return this.transMode;
	}

	public void setTransMode(String transMode) {
		this.transMode = transMode;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update account_transactions\n   set trans_date=?,\n       trans_code=?,\n       account_id=?,\n       trans_mode=?,\n       dr_cr=?,\n       amount=?,\n       ref_id=?,\n       ref_date=?,\n       bank_name=?,\n       cheque_no=?,\n       cheque_dt=?,\n       status=?,\n       created_on=?,\n       created_by=?,\n       updated_on=?,\n       updated_by=?,remark=?,\n       object_id = nvl(object_id, 1) + 1\n where trans_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(getTransDate()));
		ps.setString(2, getTransCode());
		ps.setString(3, getAccountId());
		ps.setString(4, getTransMode());
		ps.setString(5, getDrCr());
		ps.setDouble(6, getAmount());
		ps.setString(7, getRefId());
		ps.setDate(8, Utils.getSqlDate(getRefDate()));
		ps.setString(9, getBankName());
		ps.setString(10, getChequeNo());
		ps.setDate(11, Utils.getSqlDate(getChequeDt()));
		ps.setString(12, getStatus());
		ps.setTimestamp(13, getCreatedOn());
		ps.setString(14, this.createdBy);
		ps.setTimestamp(15, Utils.getTimestamp(new Date()));
		ps.setString(16, getUserId());
		ps.setString(17, getRemark());
		ps.setLong(18, getTransId().longValue());
		ps.setDouble(19, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Account Trans record could not be updated.");
		}
		return "Account Account record updated";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into account_transactions\n  (trans_id,\n   trans_date,\n   trans_code,\n   account_id,\n   trans_mode,\n   dr_cr,\n   amount,\n   ref_id,\n   ref_date,\n   bank_name,\n   cheque_no,\n   cheque_dt,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,remark)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, getTransId().longValue());
		ps.setDate(2, Utils.getSqlDate(new Date()));
		ps.setString(3, getTransCode());
		ps.setString(4, getAccountId());
		ps.setString(5, getTransMode());
		ps.setString(6, getDrCr());
		ps.setDouble(7, getAmount());
		ps.setString(8, getRefId());
		ps.setDate(9, Utils.getSqlDate(getRefDate()));
		ps.setString(10, getBankName());
		ps.setString(11, getChequeNo());
		ps.setDate(12, Utils.getSqlDate(getChequeDt()));
		ps.setString(13, getStatus());
		ps.setTimestamp(14, Utils.getTimestamp(new Date()));
		ps.setString(15, getUserId());
		ps.setTimestamp(16, getUpdatedOn());
		ps.setString(17, this.updatedBy);
		ps.setDouble(18, 1.0D);
		ps.setString(19, getRemark());

		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Account Trans record could not be Created.");
		}
		return "Account Trans record created.";
	}

	public List<AccountTransactions> getList(Connection con) throws SQLException {
		String sql = "select trans_id,\n       trans_date,\n       trans_code,\n       account_id,\n       trans_mode,\n       dr_cr,\n       amount,\n       ref_id,\n       ref_date,\n       bank_name,\n       cheque_no,\n       cheque_dt,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,remark\n  from account_transactions\n where 1 = 1";

		boolean doFetchData = false;
		if (getTransId() != null) {
			sql = sql + " and trans_id=" + getTransId();
			doFetchData = true;
		}
		if ((getTransCode() != null) && (getTransCode().length() > 0)) {
			sql = sql + " and upper(trans_code) like upper('" + getTransCode() + "')";
			doFetchData = true;
		}
		if ((getDrCr() != null) && (getDrCr().length() > 0)) {
			sql = sql + " and upper(dr_cr) like upper('" + getDrCr() + "')";
			doFetchData = true;
		}
		if ((getAccountId() != null) && (getAccountId().length() > 0)) {
			sql = sql + " and upper(account_id) like upper('" + getAccountId() + "')";
			doFetchData = true;
		}
		if ((getBankName() != null) && (getBankName().length() > 0)) {
			sql = sql + " and upper(bank_name) like upper('" + getBankName() + "')";
			doFetchData = true;
		}
		if ((getChequeNo() != null) && (getChequeNo().length() > 0)) {
			sql = sql + " and upper(cheque_no) like upper('" + getChequeNo() + "')";
			doFetchData = true;
		}
		if (this.toDate != null) {
			sql = sql + " and trunc(trans_date)  <=  to_date('" + Utils.getStrDate(this.toDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (this.fromDate != null) {
			sql = sql + " and trunc(trans_date)  >=  to_date('" + Utils.getStrDate(this.fromDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().trim().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus().trim() + "') ";
			doFetchData = true;
		}
		List<AccountTransactions> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			AccountTransactions temp = new AccountTransactions();
			temp.setTransId(Long.valueOf(rs.getLong("trans_id")));
			temp.setTransDate(rs.getDate("trans_date"));
			temp.setTransCode(rs.getString("trans_code"));
			temp.setAccountId(rs.getString("account_id"));
			temp.setTransMode(rs.getString("trans_mode"));
			temp.setDrCr(rs.getString("dr_cr"));
			temp.setAmount(rs.getDouble("amount"));
			temp.setRefId(rs.getString("ref_id"));
			temp.setRefDate(rs.getDate("ref_date"));
			temp.setBankName(rs.getString("bank_name"));
			temp.setChequeNo(rs.getString("cheque_no"));
			temp.setChequeDt(rs.getDate("cheque_dt"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getTimestamp("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getTimestamp("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			temp.setRemark(rs.getString("remark"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select trans_id,\n       trans_date,\n       trans_code,\n       account_id,\n       trans_mode,\n       dr_cr,\n       amount,\n       ref_id,\n       ref_date,\n       bank_name,\n       cheque_no,\n       cheque_dt,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,remark\n  from account_transactions\n where trans_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setLong(1, getTransId().longValue());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setTransId(Long.valueOf(rs.getLong("trans_id")));
			setTransDate(rs.getDate("trans_date"));
			setTransCode(rs.getString("trans_code"));
			setAccountId(rs.getString("account_id"));
			setTransMode(rs.getString("trans_mode"));
			setDrCr(rs.getString("dr_cr"));
			setAmount(rs.getDouble("amount"));
			setRefId(rs.getString("ref_id"));
			setRefDate(rs.getDate("ref_date"));
			setBankName(rs.getString("bank_name"));
			setChequeNo(rs.getString("cheque_no"));
			setChequeDt(rs.getDate("cheque_dt"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getTimestamp("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getTimestamp("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
			setRemark(rs.getString("remark"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public Long getNextTransactionId() throws SQLException {
		Connection con = Data.getConnection();
		String sql = "select ACCOUNT_TRANS_SEQ.nextval from dual";
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		Long i = Long.valueOf(0L);
		if (rs.next()) {
			i = Long.valueOf(rs.getLong(1));
		}
		rs.close();
		ps.close();
		con.close();
		return i;
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.transId != null ? this.transId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof AccountTransactions)) {
			return false;
		}
		AccountTransactions other = (AccountTransactions) object;
		if (((this.transId == null) && (other.transId != null))
				|| ((this.transId != null) && (!this.transId.equals(other.transId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Account trans Id :" + this.transId + "";
	}
}
