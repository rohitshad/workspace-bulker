/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class MasterParameters extends EntitiesImpl implements Serializable {
	private static final long serialVersionUID = 1L;
	private String parameterId;
	private String isTaxInclusive;
	private String isBillOnZeroBal;
	private String isDamageOnZeroBal;
	private String editSalePrice;
	private String editDiscount;
	private String editDiscOnInv;
	private double maxDiscOnItem;
	private double maxDiscOnInv;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private String useSmsService;
	private String smsPrefix;
	private String smsSuffix;
	private String userMessagePackService;
	private double extraChargeAmt;

	public MasterParameters() {
		this.isDamageOnZeroBal = "Yes";
	}

	public MasterParameters(String parameterId) {
		this.parameterId = parameterId;
	}

	public String getParameterId() {
		return this.parameterId;
	}

	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}

	public String getIsTaxInclusive() {
		return this.isTaxInclusive;
	}

	public void setIsTaxInclusive(String isTaxInclusive) {
		this.isTaxInclusive = isTaxInclusive;
	}

	public String getIsBillOnZeroBal() {
		return this.isBillOnZeroBal;
	}

	public void setIsBillOnZeroBal(String isBillOnZeroBal) {
		this.isBillOnZeroBal = isBillOnZeroBal;
	}

	public String getEditSalePrice() {
		return this.editSalePrice;
	}

	public void setEditSalePrice(String editSalePrice) {
		this.editSalePrice = editSalePrice;
	}

	public String getEditDiscount() {
		return this.editDiscount;
	}

	public void setEditDiscount(String editDiscount) {
		this.editDiscount = editDiscount;
	}

	public String getEditDiscOnInv() {
		return this.editDiscOnInv;
	}

	public void setEditDiscOnInv(String editDiscOnInv) {
		this.editDiscOnInv = editDiscOnInv;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public double getMaxDiscOnItem() {
		return this.maxDiscOnItem;
	}

	public void setMaxDiscOnItem(double maxDiscOnItem) {
		this.maxDiscOnItem = maxDiscOnItem;
	}

	public double getMaxDiscOnInv() {
		return this.maxDiscOnInv;
	}

	public void setMaxDiscOnInv(double maxDiscOnInv) {
		this.maxDiscOnInv = maxDiscOnInv;
	}

	public String getIsDamageOnZeroBal() {
		return this.isDamageOnZeroBal;
	}

	public void setIsDamageOnZeroBal(String isDamageOnZeroBal) {
		this.isDamageOnZeroBal = isDamageOnZeroBal;
	}

	public String getUseSmsService() {
		return this.useSmsService;
	}

	public void setUseSmsService(String useSmsService) {
		this.useSmsService = useSmsService;
	}

	public String getSmsPrefix() {
		return this.smsPrefix;
	}

	public void setSmsPrefix(String smsPrefix) {
		this.smsPrefix = smsPrefix;
	}

	public String getSmsSuffix() {
		return this.smsSuffix;
	}

	public void setSmsSuffix(String smsSuffix) {
		this.smsSuffix = smsSuffix;
	}

	public String getUserMessagePackService() {
		return this.userMessagePackService;
	}

	public void setUserMessagePackService(String userMessagePackService) {
		this.userMessagePackService = userMessagePackService;
	}

	public double getExtraChargeAmt() {
		return this.extraChargeAmt;
	}

	public void setExtraChargeAmt(double extraChargeAmt) {
		this.extraChargeAmt = extraChargeAmt;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update master_parameters m\n   set IS_TAX_INCLUSIVE    = ?,\n       IS_BILL_ON_ZERO_BAL = ?,\n       EDIT_SALE_PRICE     = ?,\n       EDIT_DISCOUNT       = ?,\n       EDIT_DISC_ON_INV    = ?,\n       CREATED_ON          = ?,\n       CREATED_BY          = ?,\n       UPDATED_ON          = ?,\n       UPDATED_BY          = ?,\n       OBJECT_ID           = object_id + 1, MAX_DISC_ON_ITEM=?, MAX_DISC_ON_INV=?, use_sms_service=?,sms_prefix=?, sms_suffix=?,user_message_pack_service=?,extra_charge_amt=?\n where m.parameter_id = ?\n   and m.object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.isTaxInclusive);
		ps.setString(2, this.isBillOnZeroBal);
		ps.setString(3, this.editSalePrice);
		ps.setString(4, this.editDiscount);
		ps.setString(5, this.editDiscOnInv);
		ps.setDate(6, Utils.getSqlDate(this.createdOn));
		ps.setString(7, this.createdBy);
		ps.setDate(8, Utils.getSqlDate(new Date()));
		ps.setString(9, getUserId());
		ps.setDouble(10, this.maxDiscOnItem);
		ps.setDouble(11, this.maxDiscOnInv);
		ps.setString(12, getUseSmsService());
		ps.setString(13, getSmsPrefix());
		ps.setString(14, getSmsSuffix());
		ps.setString(15, getUserMessagePackService());
		ps.setDouble(16, getExtraChargeAmt());
		ps.setString(17, this.parameterId);
		ps.setDouble(18, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Mater Parameter record could not be Updated.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		setCreatedOn(new Date());
		String sql = "insert into master_parameters\n  (PARAMETER_ID,\n   IS_TAX_INCLUSIVE,\n   IS_BILL_ON_ZERO_BAL,\n   EDIT_SALE_PRICE,\n   EDIT_DISCOUNT,\n   EDIT_DISC_ON_INV,\n   CREATED_ON,\n   CREATED_BY,\n   UPDATED_ON,\n   UPDATED_BY,\n   OBJECT_ID, use_sms_service, sms_prefix, sms_suffix,user_message_pack_service,extra_charge_amt)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.parameterId);
		ps.setString(2, this.isTaxInclusive);
		ps.setString(3, this.isBillOnZeroBal);
		ps.setString(4, this.editSalePrice);
		ps.setString(5, this.editDiscount);
		ps.setString(6, this.editDiscOnInv);
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getUserId());
		ps.setDate(9, Utils.getSqlDate(this.updatedOn));
		ps.setString(10, this.updatedBy);
		ps.setDouble(11, 1.0D);
		ps.setString(12, getUseSmsService());
		ps.setString(13, getSmsPrefix());
		ps.setString(14, getSmsSuffix());
		ps.setString(15, getUserMessagePackService());
		ps.setDouble(16, getExtraChargeAmt());
		int n = ps.executeUpdate();
		return "Record Created.";
	}

	public List<MasterParameters> getList(Connection con) throws SQLException {
		String sql = "select PARAMETER_ID,\n       IS_TAX_INCLUSIVE,\n       IS_BILL_ON_ZERO_BAL,\n       EDIT_SALE_PRICE,\n       EDIT_DISCOUNT,\n       EDIT_DISC_ON_INV,\n       CREATED_ON,\n       CREATED_BY,\n       UPDATED_ON,\n       UPDATED_BY,\n       OBJECT_ID,MAX_DISC_ON_ITEM, MAX_DISC_ON_INV,use_sms_service, sms_prefix, sms_suffix,user_message_pack_service\n       ,extra_charge_amt  from master_parameters p\n where p.parameter_id like nvl( ?, '%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.parameterId);

		ResultSet rs = ps.executeQuery();
		List<MasterParameters> list = new ArrayList();
		while (rs.next()) {
			MasterParameters temp = new MasterParameters();
			temp.setParameterId(rs.getString("PARAMETER_ID"));
			temp.setIsTaxInclusive(rs.getString("IS_TAX_INCLUSIVE"));
			temp.setIsBillOnZeroBal(rs.getString("IS_BILL_ON_ZERO_BAL"));
			temp.setEditSalePrice(rs.getString("EDIT_SALE_PRICE"));
			temp.setEditDiscount(rs.getString("EDIT_DISCOUNT"));
			temp.setEditDiscOnInv(rs.getString("EDIT_DISC_ON_INV"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setMaxDiscOnItem(rs.getDouble("MAX_DISC_ON_ITEM"));
			temp.setMaxDiscOnInv(rs.getDouble("MAX_DISC_ON_INV"));
			temp.setUseSmsService(rs.getString("use_sms_service"));
			temp.setSmsPrefix(rs.getString("sms_prefix"));
			temp.setSmsSuffix(rs.getString("sms_suffix"));
			temp.setUserMessagePackService(rs.getString("user_message_pack_service"));
			temp.setExtraChargeAmt(rs.getDouble("extra_charge_amt"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select PARAMETER_ID,\n       IS_TAX_INCLUSIVE,\n       IS_BILL_ON_ZERO_BAL,\n       EDIT_SALE_PRICE,\n       EDIT_DISCOUNT,\n       EDIT_DISC_ON_INV,\n       CREATED_ON,\n       CREATED_BY,\n       UPDATED_ON,\n       UPDATED_BY,\n       OBJECT_ID,MAX_DISC_ON_ITEM, MAX_DISC_ON_INV,use_sms_service, sms_prefix, sms_suffix,user_message_pack_service,extra_charge_amt\n  from master_parameters p\n where p.parameter_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.parameterId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setParameterId(rs.getString("PARAMETER_ID"));
			setIsTaxInclusive(rs.getString("IS_TAX_INCLUSIVE"));
			setIsBillOnZeroBal(rs.getString("IS_BILL_ON_ZERO_BAL"));
			setEditSalePrice(rs.getString("EDIT_SALE_PRICE"));
			setEditDiscount(rs.getString("EDIT_DISCOUNT"));
			setEditDiscOnInv(rs.getString("EDIT_DISC_ON_INV"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setMaxDiscOnItem(rs.getDouble("MAX_DISC_ON_ITEM"));
			setMaxDiscOnInv(rs.getDouble("MAX_DISC_ON_INV"));
			setUseSmsService(rs.getString("use_sms_service"));
			setSmsPrefix(rs.getString("sms_prefix"));
			setSmsSuffix(rs.getString("sms_suffix"));
			setUserMessagePackService(rs.getString("user_message_pack_service"));
			setExtraChargeAmt(rs.getDouble("extra_charge_amt"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.parameterId != null ? this.parameterId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof MasterParameters)) {
			return false;
		}
		MasterParameters other = (MasterParameters) object;
		if (((this.parameterId == null) && (other.parameterId != null))
				|| ((this.parameterId != null) && (!this.parameterId.equals(other.parameterId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Master Parameter :" + this.parameterId + "";
	}
}
