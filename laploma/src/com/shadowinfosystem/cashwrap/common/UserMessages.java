/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.common;

public class UserMessages {
	public static String getMessage(String msg) {
		if (msg == null) {
			return msg;
		}
		if (msg.indexOf("integrity constraint") >= 0) {
			return "Child record found.";
		}
		if (msg.indexOf("unique constraint") >= 0) {
			return "A data record with the specified key already exists.";
		}
		return msg;
	}
}
