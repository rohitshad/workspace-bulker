/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.Roles;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "roles")
@SessionScoped
public class RolesBean {
	Roles item;
	List<Roles> list;
	List<Roles> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public RolesBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}
		if (this.item == null) {
			this.item = new Roles();
		}
	}

	public Roles getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Roles");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public void setItem(Roles item) {
		this.item = item;
	}

	public List<Roles> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getRolesList();
			} catch (SQLException ex) {
				Logger.getLogger(RolesBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public List<Roles> getListSelectOne() {
		if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
			try {
				Roles r = new Roles();
				Connection con = Data.getConnection();
				setListSelectOne(r.getList(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(RolesBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.listSelectOne;
	}

	public void setListSelectOne(List<Roles> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public void setList(List<Roles> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (Exception e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(e.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Roles";
	}

	public String addNew() {
		this.item = new Roles();

		return "Roles";
	}

	public String load(String role) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setRole(role);
		this.item.load(con);
		return "Roles";
	}

	public String getRolesList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "RolesList";
	}

	public String refreshList() {
		this.item = new Roles();
		this.list.clear();
		return "RolesList";
	}

	public String SelectLine(Roles r) {
		setItem(r);
		return "Roles";
	}
}
