/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.RolesScreenGrants;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Screens;

@ManagedBean(name = "roleScreen")
@SessionScoped
public class RoleScreensBean {
	RolesScreenGrants item;
	Screens selectedScreen;
	List<RolesScreenGrants> list;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public RoleScreensBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.item == null) {
			this.item = new RolesScreenGrants();
		}
		if (this.selectedScreen == null) {
			this.selectedScreen = new Screens();
		}
	}

	public Screens getSelectedScreen() {
		return this.selectedScreen;
	}

	public void setSelectedScreen(Screens selectedScreen) {
		this.selectedScreen = selectedScreen;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public RolesScreenGrants getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("RolesScreenGrants");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
			this.item.setCanCancel(temp.isCancelAllowed());
		}
		return this.item;
	}

	public void setItem(RolesScreenGrants item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<RolesScreenGrants> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getRolesScreenGrantsList();
			} catch (SQLException ex) {
				Logger.getLogger(RoleScreensBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public void setList(List<RolesScreenGrants> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (Exception e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(e.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "RoleScreens";
	}

	public String addNew() {
		this.item = new RolesScreenGrants();

		return "RoleScreens";
	}

	public String load(String role) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setRole(role);
		return "RoleScreens";
	}

	public String getRolesScreenGrantsList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "RoleScreensList";
	}

	public String refreshList() {
		this.item = new RolesScreenGrants();
		this.list.clear();
		return "RoleScreensList";
	}

	public String SelectLine(RolesScreenGrants r) {
		setItem(r);
		return "RoleScreens";
	}

	public String addNewScreen() {
		String tempRole = this.item.getRole();
		String tempScreen = this.selectedScreen.getUserScreen();

		addNew();
		this.item.setRole(tempRole);
		this.item.setScreenId(tempScreen);
		commit();
		try {
			this.list.clear();
			load(tempRole);
		} catch (SQLException ex) {
			Logger.getLogger(RoleScreensBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "RoleScreens";
	}

	public String showScreensForRole() {
		this.list.clear();
		return "RoleScreens";
	}

	public String saveAll() {
		Connection con = Data.getConnection();
		try {
			for (RolesScreenGrants rs : this.list) {
				rs.update(con);
			}
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Saved.");
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (Exception ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex1) {
				}
			}
		}
		return "RoleScreens";
	}
}
