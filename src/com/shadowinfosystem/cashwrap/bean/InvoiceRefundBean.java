/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.InvoiceRefund;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="invRefund")
@SessionScoped
public class InvoiceRefundBean
{
  InvoiceRefund item;
  List<InvoiceRefund> list;
  List<InvoiceRefund> listPending;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public InvoiceRefundBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.listPending == null) {
      this.listPending = new ArrayList();
    }
    if (this.item == null) {
      this.item = new InvoiceRefund();
    }
  }
  
  public InvoiceRefund getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("InvoiceRefund");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(InvoiceRefund item)
  {
    this.item = item;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public List<InvoiceRefund> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        getInvoiceRefundList();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(InvoiceRefundBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<InvoiceRefund> list)
  {
    this.list = list;
  }
  
  public List<InvoiceRefund> getListPending()
  {
    this.item.setOnlyPending(true);
    this.item.setRefundVoucherNo(null);
    Connection con = null;
    try
    {
      con = Data.getConnection();
      setListPending(this.item.getList(con));
      con.close();
    }
    catch (SQLException ex)
    {
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex1) {}
      }
    }
    return this.listPending;
  }
  
  public void setListPending(List<InvoiceRefund> listPending)
  {
    this.listPending = listPending;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.update(con);
      this.item.load(con);
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (Exception e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(e.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "InvoiceRefund";
  }
  
  public String addNew()
  {
    this.item = new InvoiceRefund();
    
    return "InvoiceRefund";
  }
  
  public String load(String refVNo)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setRefundVoucherNo(refVNo);
    this.item.load(con);
    return "InvoiceRefund";
  }
  
  public String getInvoiceRefundList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.getList(con));
    return "InvoiceRefundList";
  }
  
  public String refresh()
  {
    addNew();
    this.list.clear();
    return "InvoiceRefundList";
  }
}
