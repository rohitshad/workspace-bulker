/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.SubSection;

@ManagedBean(name = "subSection")
@SessionScoped
public class SubSectionBean {
	SubSection item;
	List<SubSection> list;
	List<SubSection> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public SubSectionBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}
		if (this.item == null) {
			this.item = new SubSection();
		}
	}

	public SubSection getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("SubSection");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(SubSection item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<SubSection> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getSubSectionList();
			} catch (SQLException ex) {
				Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public List<SubSection> getListSelectOne() {
		if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
			try {
				addNew();
				Connection con = Data.getConnection();
				setListSelectOne(getItem().getList(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.listSelectOne;
	}

	public void setListSelectOne(List<SubSection> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public List<SubSection> getSubSecList() {
		Connection con = Data.getConnection();
		try {
			setList(this.item.getSubSecList(con, this.item.getSectionNo()));
			con.close();
		} catch (SQLException ex) {
			Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return this.list;
	}

	public void setList(List<SubSection> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			this.listSelectOne.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "SubSection";
	}

	public String delete() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.delete(con);
			addNew();
			this.listSelectOne.clear();
			this.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "SubSection";
	}

	public String addNew() {
		this.item = new SubSection();

		return "SubSection";
	}

	public String load(String sub_section, String section_no) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setSectionNo(section_no);
		this.item.setSubSection(sub_section);
		this.item.load(con);
		return "SubSection";
	}

	public String getSubSectionList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		con.close();
		return "SubSectionList";
	}

	public String refreshList() {
		addNew();
		this.list.clear();
		return "SubSectionList";
	}
}
