/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.ArticleItemsBean;
import com.shadowinfosystem.cashwrap.bean.InventoryTransactionBean;
import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.MasterParameterBean;
import com.shadowinfosystem.cashwrap.bean.ReportManagerBean;
import com.shadowinfosystem.cashwrap.bean.SectionBean;
import com.shadowinfosystem.cashwrap.bean.StockBean;
import com.shadowinfosystem.cashwrap.bean.SubSectionBean;
import com.shadowinfosystem.cashwrap.bean.TaxBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.DamageLines;
import com.shadowinfosystem.cashwrap.entities.Damages;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "damage")
@SessionScoped
public class DamageBean {
	Damages item;
	List<Damages> list;
	@ManagedProperty("#{stock}")
	StockBean stock;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	@ManagedProperty("#{section}")
	SectionBean section;
	@ManagedProperty("#{subSection}")
	SubSectionBean subSection;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{masterParameter}")
	MasterParameterBean mastParam;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public DamageBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new Damages();
		}

	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public Damages getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Damages");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public void setItem(Damages item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public SectionBean getSection() {
		return this.section;
	}

	public void setSection(SectionBean section) {
		this.section = section;
	}

	public SubSectionBean getSubSection() {
		return this.subSection;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public void setSubSection(SubSectionBean subSection) {
		this.subSection = subSection;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public MasterParameterBean getMastParam() {
		return this.mastParam;
	}

	public void setMastParam(MasterParameterBean mastParam) {
		this.mastParam = mastParam;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public List<Damages> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<Damages> list) {
		this.list = list;
	}

	public String setAndSearchItems(String itm_Code) {
		this.item.getDamageLineSelected().setItemCode(itm_Code);
		this.searchItems();
		return "success";
	}

	public String setItemCode(String itmcode) {
		this.articleItems.list.clear();
		this.item.getDamageLineSelected().setItemCode(itmcode);
		this.selectItem();
		return "Damage";
	}

	public String resetSelected() {
		DamageLines pl = new DamageLines();
		this.item.setDamageLineSelected(pl);
		return "Damage";
	}

	public String searchItems() {
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getDamageLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getDamageLineSelected().getItemCode());
		this.articleItems.getFilter().setArticle_no(this.item.getDamageLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getDamageLineSelected().getItemDesc());
		this.articleItems.getFilter().setBar_code(this.item.getDamageLineSelected().getBarCode());
		this.articleItems.getList();
		if (this.articleItems.getList().size() == 1) {
			this.item.getDamageLineSelected()
					.setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.selectItem();
		}

		return this.articleItems.getList().size() > 1 ? "Damage" : "Damage";
	}

	public String selectItem() {
		FacesMessage fm;
		try {
			DamageLines ex = this.item.getDamageLineSelected();
			this.articleItems.clearFilter();
			this.articleItems.getFilter().setItem_code(ex.getItemCode());
			this.articleItems.getList();
			if (this.articleItems.getList().size() == 1) {
				this.articleItems.setItem((ArticleItems) this.articleItems.getList().get(0));
			}

			this.stock.addNew();
			this.stock.load(ex.getItemCode());
			if (this.mastParam.item.getIsBillOnZeroBal() != null
					&& this.mastParam.item.getIsBillOnZeroBal().equals("No")) {
				if (this.stock.getItem().getQtyOnHand() <= ex.getQty()) {
					this.facesContext = FacesContext.getCurrentInstance();
					fm = new FacesMessage("Sufficient Stock not available.");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					return "Damage";
				}

				ex.setBatch(this.stock.getItem().getBatch());
				ex.setExpDate(this.stock.getItem().getExpDate());
			}

			double fm1 = 0.0D;
			double subSecTax = 0.0D;
			double secDisc = 0.0D;
			double subSecPerc = 0.0D;
			double tax = 0.0D;
			double disc = 0.0D;
			if (this.articleItems.item.getArticle_group() != null) {
				this.section.load(this.articleItems.item.getArticle_group());
				fm1 = this.section.item.getTaxPerc();
				secDisc = this.section.item.getDiscPerc();
			}

			if (this.articleItems.item.getArticle_sub_group() != null
					&& this.articleItems.item.getArticle_group() != null) {
				this.subSection.load(this.articleItems.item.getArticle_group(),
						this.articleItems.item.getArticle_group());
				subSecTax = this.subSection.item.getTaxPerc();
				subSecPerc = this.subSection.item.getDiscPerc();
			}

			if (this.articleItems.item.getTax_per() > 0.0D) {
				tax = this.articleItems.item.getTax_per();
			} else if (subSecTax > 0.0D) {
				tax = subSecTax;
			} else {
				tax = fm1;
			}

			if (subSecPerc > 0.0D) {
				disc = subSecPerc;
			} else {
				disc = secDisc;
			}

			ex.setMrp(this.articleItems.item.getMrp());
			ex.setSalesPrice(this.articleItems.item.getSales_price());
			ex.setTaxPer(tax);
			ex.setTaxAmt(this.articleItems.item.getSales_price() * tax / 100.0D);
			ex.setDiscPer(disc);
			ex.setDiscAmt(this.articleItems.item.getSales_price() * disc / 100.0D);
			ex.setQtyOnHand(this.stock.getItem().getQtyOnHand());
			ex.setQty(1.0D);
			ex.setUom(this.articleItems.item.getInventory_uom());
			ex.setNetAmt(ex.getQty() * ex.getSalesPrice() - ex.getDiscAmt() + ex.getTaxAmt());
			ex.setSubSection(this.articleItems.item.getArticle_sub_group());
			ex.setSection(this.articleItems.item.getArticle_group());
			ex.setColor(this.articleItems.item.getColor());
			ex.setBarCode(this.articleItems.item.getBar_code());
			ex.setArticleNo(this.articleItems.item.getArticle_no());
			ex.setItemDesc(this.articleItems.item.getArticle_desc());
			this.setIsSelected(true);
			this.reProcessSelected();
		} catch (SQLException arg13) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg13.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "success";
	}

	public String taxCodeSelected() throws SQLException {
		DamageLines ils = this.item.getDamageLineSelected();
		if (this.item.getDamageLineSelected().getTaxCode() != null
				&& this.item.getDamageLineSelected().getTaxCode().length() > 0) {
			ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
			TaxBean tax = (TaxBean) exctx.getSessionMap().get("tax");
			tax.load(this.item.getDamageLineSelected().getTaxCode());
			ils.setTaxPer(tax.getItem().getTaxPer());
		} else {
			ils.setTaxPer(0.0D);
		}

		return this.reProcessSelected();
	}

	public String reProcessSelected() {
		DamageLines ils = this.item.getDamageLineSelected();
		if (ils.getDiscPer() > this.getMastParam().item.getMaxDiscOnItem()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(
					"Max Disc Allowed is " + this.getMastParam().item.getMaxDiscOnItem() + " %.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		ils.process(this.mastParam.item.getIsTaxInclusive());
		return "success";
	}

	public String addItem() {
		DamageLines pl = this.item.getDamageLineSelected();
		if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please select Item.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "Damage";
		} else {
			pl.process(this.mastParam.item.getIsTaxInclusive());
			pl.setLineNo(this.getNextLineNo());
			pl.setDamageNo(this.item.getDamageNo());
			this.item.getDamageLines().add(0, pl);
			this.item.setDamageLineSelected(new DamageLines());
			this.item.process(this.mastParam.item.getIsTaxInclusive());
			this.setIsSelected(false);
			return "Damage";
		}
	}

	public String updateItem() {
		DamageLines il = this.item.getDamageLineSelected();
		this.item.setDamageLineSelected(new DamageLines());
		this.item.process(this.mastParam.item.getIsTaxInclusive());
		return "Damage";
	}

	public String setSlectedDetail(DamageLines line) {
		this.setIsSelected(true);
		this.item.setDamageLineSelected(line);
		return "Damage";
	}

	public String deletDetail(DamageLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getObjectId() > 0) {
			try {
				line.delete(con);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		con.close();
		this.item.getDamageLines().remove(line);
		return "Damage";
	}

	public String newItem() {
		this.item = new Damages();
		return "Damage";
	}

	public String load(String inv_no) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setDamageNo(inv_no);
		this.item.load(con);
		return "Damage";
	}

	public String printThermalReport() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("DamageReport");
			this.rep.getParameters().put("damage_no", this.item.getDamageNo());
			this.rep.exportToPdf();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Damage";
	}

	public short getNextLineNo() {
		boolean i = false;
		short arg3 = 0;
		Iterator i$ = this.getItem().getDamageLines().iterator();

		while (i$.hasNext()) {
			DamageLines pl = (DamageLines) i$.next();
			if (pl.getLineNo() > arg3) {
				arg3 = pl.getLineNo();
			}
		}

		++arg3;
		return arg3;
	}

	public String process() {
		this.item.process(this.mastParam.item.getIsTaxInclusive());
		return "Damage";
	}

	public String commit() {
		this.process();
		Connection con = Data.getConnection();

		try {
			if (this.item.getDamageNo() == null || this.item.getDamageNo().length() == 0) {
				this.trans.addNew();
				this.trans.item.setPrefix((String) null);
				this.trans.item.setTransType("DAMAGE");
				this.item.setDamageNo(this.trans.getNextOrderNo());
				this.item.setDamageDate(Utils.getSqlDate(new Date()));
			}

			this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(DamageBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Damage";
	}

	public String addNew() {
		this.setItem(new Damages());
		this.stock.addNew();
		this.stock.list.clear();
		return "Damage";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		this.item.setFromDate(new Date());
		this.item.setToDate(new Date());
		return "DamageList";
	}

	public String search() {
		this.list.clear();
		return "DamageList";
	}

	public String approveDamage() {
		this.item.process(this.mastParam.item.getIsTaxInclusive());
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				con.setAutoCommit(false);
				Iterator ex = this.item.getDamageLines().iterator();

				while (ex.hasNext()) {
					DamageLines fm1 = (DamageLines) ex.next();
					this.invTrans.addNew();
					this.invTrans.item.setTransactionId(this.invTrans.getItem().getNextTransactionId());
					this.invTrans.item.setTransactionCode("DAMAGE");
					this.invTrans.item.setItemCode(fm1.getItemCode());
					this.invTrans.item.setItemDesc(fm1.getItemDesc());
					this.invTrans.item.setQty(fm1.getQty());
					this.invTrans.item.setUom(fm1.getUom());
					this.invTrans.item.setDirection("-");
					this.invTrans.item.setPricePerUnit(fm1.getSalesPrice());
					this.invTrans.item.setPrice(fm1.getNetAmt());
					this.invTrans.item.setOrderNo(fm1.getDamageNo());
					this.invTrans.item.setLineNo(fm1.getLineNo());
					this.invTrans.item.setCreatedBy("ADMIN");
					this.invTrans.item.setArticleNo(fm1.getArticleNo());
					this.invTrans.item.update(con);
					this.stock.addNew();
					this.stock.item.setItemCode(fm1.getItemCode());
					this.stock.item.load(con);
					this.stock.item.setQtyOnHand(this.stock.item.getQtyOnHand() - fm1.getQty());
					this.stock.item.setRemark("Damage against Damage no " + fm1.getDamageNo() + " on :" + new Date());
					this.stock.item.setUpdatedOn(new Date());
					this.stock.item.update(con);
				}

				this.item.setStatus("Approved");
				this.item.update(con);
				con.commit();
				con.setAutoCommit(true);
			} catch (SQLException arg18) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg18.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);

				try {
					con.rollback();
				} catch (SQLException arg17) {
					Logger.getLogger(DamageBean.class.getName()).log(Level.SEVERE, (String) null, arg17);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg16) {
					Logger.getLogger(DamageBean.class.getName()).log(Level.SEVERE, (String) null, arg16);
				}
			} catch (Exception arg19) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg19.getMessage() + "satyendra ");
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg15) {
					Logger.getLogger(DamageBean.class.getName()).log(Level.SEVERE, (String) null, arg15);
				}
			}

		}

		return "Damage";
	}
}