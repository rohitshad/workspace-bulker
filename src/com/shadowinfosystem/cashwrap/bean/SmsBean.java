/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.shadowinfosystem.cashwrap.common.Sms;

@ManagedBean(name = "sms")
@SessionScoped
public class SmsBean {
	private Sms sms;
	private String mobNo;
	private String msgText;

	public Sms getSms() {
		return this.sms;
	}

	public void setSms(Sms sms) {
		this.sms = sms;
	}

	public String getMobNo() {
		return this.mobNo;
	}

	public void setMobNo(String mobNo) {
		this.mobNo = mobNo;
	}

	public String getMsgText() {
		return this.msgText;
	}

	public void setMsgText(String msgText) {
		this.msgText = msgText;
	}
}
