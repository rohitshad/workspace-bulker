/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.common.Xls;
import com.shadowinfosystem.cashwrap.entities.Customer;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "cust")
@SessionScoped
public class CustomerBean {
	Customer item;
	List<Customer> list;
	List<Customer> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public CustomerBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}

		if (this.item == null) {
			this.item = new Customer();
		}

	}

	public Customer getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Customer");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public void setItem(Customer item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Customer> getListSelectOne() {
		if (this.listSelectOne.isEmpty()) {
			this.setItem(new Customer());
			this.item.setStatus("Active");
			Connection con = Data.getConnection();

			try {
				this.setListSelectOne(this.item.getList(con));
			} catch (SQLException arg10) {
				;
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException arg9) {
						;
					}
				}

			}
		}

		return this.listSelectOne;
	}

	public void setListSelectOne(List<Customer> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public List<Customer> getList() {
		if (this.list.isEmpty()) {
			try {
				this.getCustomerList();
			} catch (SQLException arg1) {
				;
			}
		}

		return this.list;
	}

	public void setList(List<Customer> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.update(con);
			this.listSelectOne.clear();
			this.item.load(con);
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
			con.close();
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Customer";
	}

	public String delete() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.delete(con);
			this.addNew();
			this.listSelectOne.clear();
			this.list.clear();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
			con.close();
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Customer";
	}

	public String addNew() {
		this.item = new Customer();
		return "Customer";
	}

	public String load(String cust_id) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setCustId(cust_id);
		this.item.load(con);
		return "Customer";
	}

	public String getCustomerList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		con.close();
		return "CustomerList";
	}

	public String search() {
		this.list.clear();
		return "CustomerList";
	}

	public String refresh() {
		this.setItem(new Customer());
		this.list.clear();
		this.getListSelectOne().clear();
		return "CustomerList";
	}

	public String generateXls() {
		Xls xls = new Xls();

		try {
			xls.getXls(this.getList());
		} catch (IOException arg2) {
			Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, (String) null, arg2);
		} catch (IllegalArgumentException arg3) {
			Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, (String) null, arg3);
		} catch (IllegalAccessException arg4) {
			Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
		}

		return "CustomerList";
	}
}