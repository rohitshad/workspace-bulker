/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Screens;

@ManagedBean(name = "screen")
@SessionScoped
public class ScreenBean {
	Screens item;
	List<Screens> list;
	List<Screens> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public ScreenBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}
		if (this.item == null) {
			this.item = new Screens();
		}
	}

	public Screens getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Screens");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(Screens item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Screens> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getScreensList();
			} catch (SQLException ex) {
				Logger.getLogger(ScreenBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public List<Screens> getListSelectOne() {
		if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
			try {
				Screens s = new Screens();
				Connection con = Data.getConnection();
				setListSelectOne(s.getList(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(ScreenBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.listSelectOne;
	}

	public void setListSelectOne(List<Screens> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public void setList(List<Screens> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			this.listSelectOne.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (Exception e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(e.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Screens";
	}

	public String addNew() {
		setItem(new Screens());

		return "Screens";
	}

	public String load(String screen) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setScreen(screen);
		this.item.load(con);
		return "Screens";
	}

	public String getScreensList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "ScreensList";
	}

	public String search() {
		this.list.clear();
		return "ScreensList";
	}

	public String SelectLine(Screens s) {
		setItem(s);
		return "Screens";
	}

	public String refresh() {
		addNew();
		this.list.clear();
		return "ScreensList";
	}

	public String prepareList() {
		addNew();
		this.list.clear();
		return "ScreensList";
	}
}
