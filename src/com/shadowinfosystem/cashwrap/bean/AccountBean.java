/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.common.Xls;
import com.shadowinfosystem.cashwrap.entities.Account;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="account")
@SessionScoped
public class AccountBean
{
  Account item;
  Account filter;
  List<Account> list;
  List<Account> listSelectOne;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public AccountBean()
  {
    if (this.listSelectOne == null) {
      this.listSelectOne = new ArrayList();
    }
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.item == null) {
      this.item = new Account();
    }
    if (this.filter == null) {
      this.filter = new Account();
    }
  }
  
  public Account getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("Account");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public Account getFilter()
  {
    return this.filter;
  }
  
  public void setFilter(Account filter)
  {
    this.filter = filter;
  }
  
  public void setItem(Account item)
  {
    this.item = item;
  }
  
  public List<Account> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      getAccountList();
    }
    return this.list;
  }
  
  public List<Account> getListSelectOne()
  {
    if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty()))
    {
      Connection con = Data.getConnection();
      try
      {
        clearFiler();
        getFilter().setAccountId("%");
        setListSelectOne(getFilter().search(con));
        con.close();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.listSelectOne;
  }
  
  public void setListSelectOne(List<Account> listSelectOne)
  {
    this.listSelectOne = listSelectOne;
  }
  
  public void setList(List<Account> list)
  {
    this.list = list;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public FacesContext getFacesContext()
  {
    return this.facesContext;
  }
  
  public void setFacesContext(FacesContext facesContext)
  {
    this.facesContext = facesContext;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = "";
      msg = this.item.update(con);
      this.list.clear();
      this.item.load(con);
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Account";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = "";
      msg = this.item.delete(con);
      this.list.clear();
      addNew();
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Account";
  }
  
  public String clearFiler()
  {
    setFilter(new Account());
    return "Account";
  }
  
  public String addNew()
  {
    setItem(new Account());
    return "Account";
  }
  
  public String load(String account_no)
  {
    try
    {
      Connection con = Data.getConnection();
      Logger.getLogger(ColorBean.class.getName()).log(Level.INFO," Parameter :" + account_no);
      this.item.setAccountId(account_no);
      this.item.load(con);
      con.close();
    }
    catch (SQLException ex)
    {
      Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "Account";
  }
  
  public String getAccountList()
  {
    Connection con = Data.getConnection();
    try
    {
      setList(getFilter().search(con));
      con.close();
    }
    catch (SQLException ex)
    {
      Logger.getLogger(AccountBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "AccountList";
  }
  
  public String refreshList()
  {
    this.list.clear();
    return "AccountList";
  }
  
  public String reset()
  {
    clearFiler();
    this.list.clear();
    return "AccountList";
  }
  
  public String generateXls()
  {
    Xls xls = new Xls();
    try
    {
      xls.getXls(getList());
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("File Created @ " + xls.getFile());
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (IOException ex)
    {
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
    }
    catch (IllegalArgumentException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "AccountList";
  }
}
