/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Uom;

@ManagedBean(name = "uom")
@SessionScoped
public class UomBean {
	Uom item;
	List<Uom> list;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public UomBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.item == null) {
			this.item = new Uom();
		}
	}

	public Uom getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Uom");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(Uom item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Uom> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getUomList();
			} catch (SQLException ex) {
				Logger.getLogger(UomBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public void setList(List<Uom> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			con.close();
			this.list.clear();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(UomBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Uom";
	}

	public String delete() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.delete(con);
			addNew();
			con.close();
			this.list.clear();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(UomBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Uom";
	}

	public String addNew() {
		this.item = new Uom();

		return "Uom";
	}

	public String load(String uom) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setUom(uom);
		this.item.load(con);
		return "Uom";
	}

	public String getUomList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "UomList";
	}

	public String refreshList() throws SQLException {
		setItem(new Uom());
		this.list.clear();
		return "UomList";
	}
}
