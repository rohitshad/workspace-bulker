/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Supplier;

@ManagedBean(name = "supp")
@SessionScoped
public class SupplierBean {
	Supplier item;
	List<Supplier> list;
	List<Supplier> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public SupplierBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}

		if (this.item == null) {
			this.item = new Supplier();
		}

	}

	public Supplier getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Supplier");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public void setItem(Supplier item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Supplier> getListSelectOne() {
		if (this.listSelectOne.isEmpty()) {
			this.setItem(new Supplier());
			this.item.setStatus("Active");
			Connection con = Data.getConnection();

			try {
				this.setListSelectOne(this.item.getList(con));
			} catch (SQLException arg10) {
				;
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException arg9) {
						;
					}
				}

			}
		}

		return this.listSelectOne;
	}

	public void setListSelectOne(List<Supplier> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public List<Supplier> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.getSupplierList();
			} catch (SQLException arg1) {
				Logger.getLogger(SupplierBean.class.getName()).log(Level.SEVERE, (String) null, arg1);
			}
		}

		return this.list;
	}

	public void setList(List<Supplier> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Supplier";
	}

	public String delete() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.delete(con);
			this.addNew();
			this.listSelectOne.clear();
			this.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Supplier";
	}

	public String addNew() {
		this.item = new Supplier();
		return "Supplier";
	}

	public String load(String supp_id) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setSuppId(supp_id);
		this.item.load(con);
		return "Supplier";
	}

	public String getSupplierList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "SupplierList";
	}

	public String search() {
		this.list.clear();
		return "SupplierList";
	}

	public String refresh() {
		this.getListSelectOne().clear();
		this.setItem(new Supplier());
		this.list.clear();
		return "SupplierList";
	}
}