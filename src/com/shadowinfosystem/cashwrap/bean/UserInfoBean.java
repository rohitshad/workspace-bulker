/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.UserInfo;
import com.shadowinfosystem.cashwrap.entities.UserRoles;

@ManagedBean(name = "userInfo")
@SessionScoped
public class UserInfoBean {
	UserInfo item;
	List<UserInfo> list;
	List<UserInfo> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public UserInfoBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}
		if (this.item == null) {
			this.item = new UserInfo();
		}
	}

	public UserInfo getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("UserInfo");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(UserInfo item) {
		this.item = item;
	}

	public List<UserInfo> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getUserInfoList();
			} catch (SQLException ex) {
				Logger.getLogger(UserInfoBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public List<UserInfo> getListSelectOne() {
		if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				setListSelectOne(getItem().getAllList(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(UserInfoBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.listSelectOne;
	}

	public void setListSelectOne(List<UserInfo> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public void setList(List<UserInfo> list) {
		this.list = list;
	}

	public String addNewRole() {
		UserRoles ur = new UserRoles();
		ur.setLoginId(getItem().getLoginId());
		ur.setRole(getItem().getTempRole());
		if ((ur.getRole() == null) || (ur.getRole().length() <= 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please select a Role to be asigned.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "UserInfo";
		}
		if (!getItem().getRolesList().contains(ur)) {
			getItem().getRolesList().add(ur);
			commit();
			getItem().setTempRole(null);
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Role already Assigned.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "UserInfo";
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.listSelectOne.clear();
			this.item.load(con);

			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(e.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "UserInfo";
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public String addNew() {
		this.item = new UserInfo();

		return "UserInfo";
	}

	public String removeRole(String rolep) {
		for (UserRoles r : this.item.getRolesList()) {
			if ((r.getRole() != null) && (rolep != null) && (r.getRole().equals(rolep))) {
				Connection con = Data.getConnection();
				try {
					String msg = r.delete(con);
					con.close();
					this.facesContext = FacesContext.getCurrentInstance();
					FacesMessage fm = new FacesMessage(msg);
					fm.setSeverity(FacesMessage.SEVERITY_INFO);
					this.facesContext.addMessage(null, fm);
				} catch (SQLException ex) {
					Logger.getLogger(UserInfoBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		load(this.item.getLoginId());
		return "UserInfo";
	}

	public String load(String usr) {
		try {
			Connection con = Data.getConnection();
			this.item.setLoginId(usr);
			this.item.load(con);
		} catch (SQLException ex) {
			Logger.getLogger(UserInfoBean.class.getName()).log(Level.SEVERE, null, ex);
		}
		return "UserInfo";
	}

	public String getUserInfoList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "UserInfo";
	}

	public String selectUser(UserInfo u) {
		setItem(u);
		return "UserInfo";
	}

	public String logout() {
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "login";
	}

	public String refreshUsersList() {
		addNew();
		this.list.clear();
		return "UsersList";
	}

	public String prapreChangePassowrd() {
		this.item.setLoginId(getLogin().getUserid());
		Connection con = Data.getConnection();
		try {
			this.item.load(con);
			con.close();
		} catch (SQLException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Error encountered while fetching Data for User " + ex);
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		if ((this.item.getUserId() == null) || (this.item.getUserId().length() <= 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please select User to change Password.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "UserManagement";
	}

	public String prapreChangeOthersPassowrd() {
		return "ChangePassword";
	}

	public String changePassword() {
		if ((getItem().getLoginId() == null) || (getItem().getLoginId().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please select user from list.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "UserManagement";
		}
		if ((getItem().getOldPassword() == null) || (getItem().getOldPassword().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please enter old password.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "UserManagement";
		}
		if ((getItem().getNewPassword() == null) || (getItem().getNewPassword().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please enter new password.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "UserManagement";
		}
		if ((getItem().getConfirmPassword() == null) || (getItem().getConfirmPassword().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please enter confirm password.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "UserManagement";
		}
		if (!getItem().getConfirmPassword().equals(getItem().getNewPassword())) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("New password and conform password is not matching.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "UserManagement";
		}
		Connection con = Data.getConnection();
		try {
			String msg = this.item.changePassword(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (Exception ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "UserManagement";
	}

	public String changeUserPassword() {
		if ((getItem().getLoginId() == null) || (getItem().getLoginId().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please select user from list.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "ChangePassword";
		}
		if ((getItem().getNewPassword() == null) || (getItem().getNewPassword().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please enter new password.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "ChangePassword";
		}
		if ((getItem().getConfirmPassword() == null) || (getItem().getConfirmPassword().length() == 0)) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("Please enter confirm password.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "ChangePassword";
		}
		if (!getItem().getConfirmPassword().equals(getItem().getNewPassword())) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage("New password and conform password is not matching.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			return "ChangePassword";
		}
		Connection con = Data.getConnection();
		try {
			this.item.load(con);
			String msg = this.item.changeUserPassword(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (Exception ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "ChangePassword";
	}
}
