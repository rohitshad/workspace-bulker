/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Tax;

@ManagedBean(name = "tax")
@SessionScoped
public class TaxBean {
	Tax item;
	List<Tax> list;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public TaxBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.item == null) {
			this.item = new Tax();
		}
	}

	public Tax getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Tax");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(Tax item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Tax> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getTaxList();
			} catch (SQLException ex) {
				Logger.getLogger(TaxBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public void setList(List<Tax> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			this.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(TaxBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Tax";
	}

	public String delete() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.delete(con);
			addNew();
			this.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(TaxBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Tax";
	}

	public String addNew() {
		this.item = new Tax();

		return "Tax";
	}

	public String load(String tax_code) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setTaxCode(tax_code);
		this.item.load(con);
		return "Tax";
	}

	public String getTaxList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "TaxList";
	}

	public String refreshList() throws SQLException {
		setItem(new Tax());
		this.list.clear();
		return "TaxList";
	}
}
