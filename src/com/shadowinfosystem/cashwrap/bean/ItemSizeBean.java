/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ItemSize;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="size")
@SessionScoped
public class ItemSizeBean
{
  ItemSize item;
  List<ItemSize> list;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public ItemSizeBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.item == null) {
      this.item = new ItemSize();
    }
  }
  
  public ItemSize getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("ItemSize");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(ItemSize item)
  {
    this.item = item;
  }
  
  public List<ItemSize> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        getItemSizeList();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ItemSizeBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public void setList(List<ItemSize> list)
  {
    this.list = list;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.update(con);
      this.item.load(con);
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "ItemSize";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.delete(con);
      addNew();
      this.list.clear();
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "ItemSize";
  }
  
  public String addNew()
  {
    this.item = new ItemSize();
    
    return "ItemSize";
  }
  
  public String load(String itemSize)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setItemSize(itemSize);
    this.item.load(con);
    return "ItemSize";
  }
  
  public String getItemSizeList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.getList(con));
    return "ItemSizeList";
  }
  
  public String prepareList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    addNew();
    setList(this.item.getList(con));
    return "ItemSizeList";
  }
}
