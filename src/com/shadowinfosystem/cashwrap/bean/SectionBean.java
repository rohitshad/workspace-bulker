/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Section;

@ManagedBean(name = "section")
@SessionScoped
public class SectionBean {
	Section item;
	List<Section> list;
	List<Section> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public SectionBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}

		if (this.item == null) {
			this.item = new Section();
		}

	}

	public Section getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Section");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public void setItem(Section item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Section> getListSelectOne() {
		if (this.listSelectOne.isEmpty()) {
			this.setItem(new Section());
			this.item.setStatus("Active");
			Connection con = Data.getConnection();

			try {
				this.setListSelectOne(this.item.getList(con));
			} catch (SQLException arg10) {
				;
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException arg9) {
						;
					}
				}

			}
		}

		return this.listSelectOne;
	}

	public void setListSelectOne(List<Section> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public List<Section> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.getSectionList();
			} catch (SQLException arg1) {
				Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, (String) null, arg1);
			}
		}

		return this.list;
	}

	public void setList(List<Section> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.update(con);
			this.item.load(con);
			this.listSelectOne.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Section";
	}

	public String delete() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.delete(con);
			this.addNew();
			this.list.clear();
			this.listSelectOne.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Section";
	}

	public String addNew() {
		this.item = new Section();
		return "Section";
	}

	public String load(String section_no) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setSectionNo(section_no);
		this.item.load(con);
		return "Section";
	}

	public String getSectionList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		con.close();
		return "SectionList";
	}

	public String search() {
		this.list.clear();
		return "SectionList";
	}

	public String refresh() {
		this.getListSelectOne().clear();
		this.setItem(new Section());
		this.list.clear();
		return "SectionList";
	}
}