/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class InvoiceRefund extends EntitiesImpl implements Serializable {
	private String refundVoucherNo;
	private Date refundDate;
	private double refundValue;
	private String invRetNo;
	private String invoiceNo;
	private double refundedValue;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private boolean onlyPending;

	public InvoiceRefund() {
	}

	public InvoiceRefund(String refundVoucherNo) {
		this.refundVoucherNo = refundVoucherNo;
	}

	public String getRefundVoucherNo() {
		return this.refundVoucherNo;
	}

	public void setRefundVoucherNo(String refundVoucherNo) {
		this.refundVoucherNo = refundVoucherNo;
	}

	public String getInvRetNo() {
		return this.invRetNo;
	}

	public void setInvRetNo(String invRetNo) {
		this.invRetNo = invRetNo;
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getRefundDate() {
		return this.refundDate;
	}

	public void setRefundDate(Date refundDate) {
		this.refundDate = refundDate;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getRefundValue() {
		return this.refundValue;
	}

	public void setRefundValue(double refundValue) {
		this.refundValue = refundValue;
	}

	public double getRefundedValue() {
		return this.refundedValue;
	}

	public void setRefundedValue(double refundedValue) {
		this.refundedValue = refundedValue;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public boolean isOnlyPending() {
		return this.onlyPending;
	}

	public void setOnlyPending(boolean onlyPending) {
		this.onlyPending = onlyPending;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update invoice_refund\n   set refund_date    = ?,\n       refund_value   = ?,\n       inv_ret_no     = ?,\n       invoice_no     = ?,\n       refunded_value = ?,\n       created_on     = ?,\n       created_by     = ?,\n       updated_on     = ?,\n       updated_by     = ?\n where refund_voucher_no = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(getRefundDate()));
		ps.setDouble(2, getRefundValue());
		ps.setString(3, getInvRetNo());
		ps.setString(4, getInvoiceNo());
		ps.setDouble(5, getRefundedValue());
		ps.setDate(6, Utils.getSqlDate(getCreatedOn()));
		ps.setString(7, getCreatedBy());
		ps.setDate(8, Utils.getSqlDate(new Date()));
		ps.setString(9, getUserId());
		ps.setString(10, getRefundVoucherNo());
		ps.setDouble(11, getObjectId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Invoice Refund Record could not be updated.");
		}
		return "success";
	}

	public String create(Connection con) throws SQLException, Exception {
		setCreatedOn(new Date());
		setRefundDate(new Date());
		setObjectId(1.0D);
		String sql = "insert into invoice_refund\n  (refund_voucher_no,\n   refund_date,\n   refund_value,\n   inv_ret_no,\n   invoice_no,\n   refunded_value,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getRefundVoucherNo());
		ps.setDate(2, Utils.getSqlDate(getRefundDate()));
		ps.setDouble(3, getRefundValue());
		ps.setString(4, getInvRetNo());
		ps.setString(5, getInvoiceNo());
		ps.setDouble(6, getRefundedValue());
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getUserId());
		ps.setDate(9, Utils.getSqlDate(getUpdatedOn()));
		ps.setString(10, getUpdatedBy());
		ps.setDouble(11, getObjectId());

		int n = ps.executeUpdate();
		if (n == 0) {
			setObjectId(0.0D);
			throw new Exception("Invoice Refund Record could not be created.");
		}
		return "Record Created.";
	}

	public List<InvoiceRefund> getList(Connection con) throws SQLException {
		String sql = "select refund_voucher_no,\n       refund_date,\n       refund_value,\n       inv_ret_no,\n       invoice_no,\n       refunded_value,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from invoice_refund x\n where 1 = 1 ";
		if ((getRefundVoucherNo() != null) && (getRefundVoucherNo().length() > 0)) {
			sql = sql + " and refund_voucher_no='" + getRefundVoucherNo() + "'";
		}
		if (this.onlyPending) {
			sql = sql
					+ " and not exists (select 1 from invoice_pay_det d where d.refund_voucher_no = x.refund_voucher_no) ";
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		List<InvoiceRefund> list = new ArrayList();
		while (rs.next()) {
			InvoiceRefund temp = new InvoiceRefund();
			temp.setRefundVoucherNo(rs.getString("refund_voucher_no"));
			temp.setRefundDate(rs.getDate("refund_date"));
			temp.setRefundValue(rs.getDouble("refund_value"));
			temp.setInvRetNo(rs.getString("inv_ret_no"));
			temp.setInvoiceNo(rs.getString("invoice_no"));
			temp.setRefundedValue(rs.getDouble("refunded_value"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select refund_voucher_no,\n       refund_date,\n       refund_value,\n       inv_ret_no,\n       invoice_no,\n       refunded_value,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from invoice_refund\n where refund_voucher_no = ? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getRefundVoucherNo());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setRefundVoucherNo(rs.getString("refund_voucher_no"));
			setRefundDate(rs.getDate("refund_date"));
			setRefundValue(rs.getDouble("refund_value"));
			setInvRetNo(rs.getString("inv_ret_no"));
			setInvoiceNo(rs.getString("invoice_no"));
			setRefundedValue(rs.getDouble("refunded_value"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.refundVoucherNo != null ? this.refundVoucherNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof InvoiceRefund)) {
			return false;
		}
		InvoiceRefund other = (InvoiceRefund) object;
		if (((this.refundVoucherNo == null) && (other.refundVoucherNo != null))
				|| ((this.refundVoucherNo != null) && (!this.refundVoucherNo.equals(other.refundVoucherNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "RefundVoucherNo=" + this.refundVoucherNo;
	}
}
