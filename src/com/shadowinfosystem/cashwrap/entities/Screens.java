/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Screens extends EntitiesImpl implements Serializable {
	private String userScreen;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;

	public String getUserScreen() {
		return this.userScreen;
	}

	public void setUserScreen(String userScreen) {
		this.userScreen = userScreen;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update screens t\n   set status     = ?,\n       created_on = ?,\n       created_by = ?,\n       updated_on = ?,\n       updated_by = ?,\n       object_id  = object_id + 1\n where screen = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.status);
		ps.setDate(2, Utils.getSqlDate(this.createdOn));
		ps.setString(3, this.createdBy);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setString(6, getUserScreen());
		ps.setDouble(7, this.objectId);
		int n = ps.executeUpdate();
		return "Record Updated.";
	}

	public String create(Connection con) throws Exception {
		int n = 0;
		String sql = "insert into screens\n  (screen,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, getUserScreen());
			ps.setString(2, this.status);
			ps.setDate(3, Utils.getSqlDate(new Date()));
			ps.setString(4, getUserId());
			ps.setDate(5, Utils.getSqlDate(this.updatedOn));
			ps.setString(6, this.updatedBy);
			ps.setDouble(7, 1.0D);
			n = ps.executeUpdate();
			ps.close();
			return "Record Created.";
		} catch (SQLException ex) {
			if (n == 0) {
				throw new Exception("Record could not be created." + ex.getMessage());
			}
		}
		return "Success";
	}

	public List<Screens> getList(Connection con) throws SQLException {
		String sql = "select screen,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from screens\n where screen like nvl(?,screen) ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getUserScreen());

		ResultSet rs = ps.executeQuery();
		List<Screens> list = new ArrayList();
		while (rs.next()) {
			Screens temp = new Screens();
			temp.setUserScreen(rs.getString("screen"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select screen,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from screens\n where screen =? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getUserScreen());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setUserScreen(rs.getString("screen"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.userScreen != null ? this.userScreen.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Screens)) {
			return false;
		}
		Screens other = (Screens) object;
		if (((this.userScreen == null) && (other.userScreen != null))
				|| ((this.userScreen != null) && (!this.userScreen.equals(other.userScreen)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Screen : " + this.userScreen + "";
	}
}
