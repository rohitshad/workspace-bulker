/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class SalesPerson extends EntitiesImpl implements Serializable {
	private String personCode;
	private String personName;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double commPerc;

	public SalesPerson() {
	}

	public SalesPerson(String personCode) {
		this.personCode = personCode;
	}

	public String getPersonCode() {
		return this.personCode;
	}

	public void setPersonCode(String personCode) {
		this.personCode = personCode;
	}

	public String getPersonName() {
		return this.personName;
	}

	public void setPersonName(String personName) {
		this.personName = personName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getCommPerc() {
		return this.commPerc;
	}

	public void setCommPerc(double commPerc) {
		this.commPerc = commPerc;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update sales_person p\nset person_name=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1,comm_perc=?\nwhere person_code=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.personName);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setDouble(7, getCommPerc());
		ps.setString(8, this.personCode);
		ps.setShort(9, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		setCreatedOn(new Date());
		String sql = "insert into sales_person\n  (person_code,\n   person_name,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,comm_perc)\nvalues\n  (?,?,?,?,?,?,?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.personCode);
		ps.setString(2, this.personName);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		ps.setDouble(8, getCommPerc());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<SalesPerson> getList(Connection con) throws SQLException {
		String sql = "select person_code,\n       person_name,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,comm_perc\n  from sales_person p\n where person_code like nvl(?,'%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.personCode);

		ResultSet rs = ps.executeQuery();
		List<SalesPerson> list = new ArrayList();
		while (rs.next()) {
			SalesPerson temp = new SalesPerson();
			temp.setPersonCode(rs.getString("person_code"));
			temp.setPersonName(rs.getString("person_name"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setCommPerc(rs.getDouble("comm_perc"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select person_code,\n       person_name,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,comm_perc\n  from sales_person p\n where person_code = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.personCode);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setPersonCode(rs.getString("person_code"));
			setPersonName(rs.getString("person_name"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setCommPerc(rs.getDouble("comm_perc"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from sales_person p\nwhere person_code=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.personCode);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.personCode != null ? this.personCode.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof SalesPerson)) {
			return false;
		}
		SalesPerson other = (SalesPerson) object;
		if (((this.personCode == null) && (other.personCode != null))
				|| ((this.personCode != null) && (!this.personCode.equals(other.personCode)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Person=" + this.personCode + "";
	}
}
