/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Account extends EntitiesImpl implements Serializable {
	private String accountId;
	private String accountDesc;
	private String accountNo;
	private String ifsCode;
	private String address;
	private String remarks;
	private String status;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp updatedOn;
	private String updatedBy;
	private double objectId;

	public Account() {
	}

	public Account(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountId() {
		return this.accountId;
	}

	public void setAccountId(String accountId) {
		this.accountId = accountId;
	}

	public String getAccountDesc() {
		return this.accountDesc;
	}

	public void setAccountDesc(String accountDesc) {
		this.accountDesc = accountDesc;
	}

	public String getAccountNo() {
		return this.accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}

	public String getIfsCode() {
		return this.ifsCode;
	}

	public void setIfsCode(String ifsCode) {
		this.ifsCode = ifsCode;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRemarks() {
		return this.remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update account\n   set account_desc = ?,\n       account_no   = ?,\n       ifs_code     = ?,\n       address      = ?,\n       remarks      = ?,\n       status       = ?,\n       created_on   = ?,\n       created_by   = ?,\n       updated_on   = ?,\n       updated_by   = ?,\n       object_id    = object_id + 1\n where account_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getAccountDesc());
		ps.setString(2, getAccountNo());
		ps.setString(3, getIfsCode());
		ps.setString(4, getAddress());
		ps.setString(5, getRemarks());
		ps.setString(6, this.status);
		ps.setTimestamp(7, getCreatedOn());
		ps.setString(8, this.createdBy);
		ps.setTimestamp(9, Utils.getTimestamp(new Date()));
		ps.setString(10, getUserId());
		ps.setString(11, getAccountId());
		ps.setDouble(12, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Account record could not be updated.");
		}
		return "Account record updated";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into account\n  (account_id,\n   account_desc,\n   account_no,\n   ifs_code,\n   address,\n   remarks,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getAccountId());
		ps.setString(2, getAccountDesc());
		ps.setString(3, getAccountNo());
		ps.setString(4, getIfsCode());
		ps.setString(5, getAddress());
		ps.setString(6, getRemarks());
		ps.setString(7, this.status);
		ps.setTimestamp(8, Utils.getTimestamp(new Date()));
		ps.setString(9, getUserId());
		ps.setTimestamp(10, getUpdatedOn());
		ps.setString(11, this.updatedBy);
		ps.setDouble(12, 1.0D);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Account record could not be Created.");
		}
		return "Account record created.";
	}

	public List<Account> search(Connection con) throws SQLException {
		String sql = "select account_id,\n       account_desc,\n       account_no,\n       ifs_code,\n       address,\n       remarks,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from account  where 1=1 ";

		boolean doFetchData = false;
		if ((getAccountId() != null) && (getAccountId().length() > 0)) {
			sql = sql + " and  account_id like '" + getAccountId() + "'";
			doFetchData = true;
		}
		if ((getAccountDesc() != null) && (getAccountDesc().length() > 0)) {
			sql = sql + " and  upper(account_id) like upper('%" + getAccountDesc() + "%')";
			doFetchData = true;
		}
		if ((getAccountNo() != null) && (getAccountNo().length() > 0)) {
			sql = sql + " and  account_no like '" + getAccountNo() + "'";
			doFetchData = true;
		}
		if ((getIfsCode() != null) && (getIfsCode().length() > 0)) {
			sql = sql + " and  ifs_code like '" + getIfsCode() + "'";
			doFetchData = true;
		}
		if ((getAddress() != null) && (getAddress().length() > 0)) {
			sql = sql + " and  address like '" + getAddress() + "'";
			doFetchData = true;
		}
		if ((getRemarks() != null) && (getRemarks().length() > 0)) {
			sql = sql + " and  upper(remarks) like upper('" + getRemarks() + "')";
			doFetchData = true;
		}
		List<Account> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Account temp = new Account();
			temp.setAccountId(rs.getString("account_id"));
			temp.setAccountDesc(rs.getString("account_desc"));
			temp.setAccountNo(rs.getString("account_no"));
			temp.setIfsCode(rs.getString("ifs_code"));
			temp.setAddress(rs.getString("address"));
			temp.setRemarks(rs.getString("remarks"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getTimestamp("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getTimestamp("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select account_id,\n       account_desc,\n       account_no,\n       ifs_code,\n       address,\n       remarks,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from account  where account_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getAccountId());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setAccountId(rs.getString("account_id"));
			setAccountDesc(rs.getString("account_desc"));
			setAccountNo(rs.getString("account_no"));
			setIfsCode(rs.getString("ifs_code"));
			setAddress(rs.getString("address"));
			setRemarks(rs.getString("remarks"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getTimestamp("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getTimestamp("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from account\n where account_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getAccountId());
		ps.setDouble(2, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Account record could not be deleted.");
		}
		return "Account record deleted";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.accountId != null ? this.accountId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Account)) {
			return false;
		}
		Account other = (Account) object;
		if (((this.accountId == null) && (other.accountId != null))
				|| ((this.accountId != null) && (!this.accountId.equals(other.accountId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Account :" + this.accountId + "";
	}
}
