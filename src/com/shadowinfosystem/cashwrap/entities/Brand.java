/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Brand extends EntitiesImpl implements Serializable {
	private String brand;
	private String brandDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double discPerc;
	private double taxPerc;

	public Brand() {
	}

	public Brand(String brand) {
		this.brand = brand;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getBrandDesc() {
		return this.brandDesc;
	}

	public void setBrandDesc(String brandDesc) {
		this.brandDesc = brandDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getDiscPerc() {
		return this.discPerc;
	}

	public void setDiscPerc(double discPerc) {
		this.discPerc = discPerc;
	}

	public double getTaxPerc() {
		return this.taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update brand p\nset brand_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1,disc_perc=?,tax_perc=?\nwhere brand=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.brandDesc);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setDouble(7, getDiscPerc());
		ps.setDouble(8, getTaxPerc());
		ps.setString(9, this.brand);
		ps.setShort(10, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into brand\n  (brand,\n   brand_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,disc_perc,tax_perc, object_id)\nvalues\n  (?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.brand);
		ps.setString(2, this.brandDesc);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		ps.setDouble(8, getDiscPerc());
		ps.setDouble(9, getTaxPerc());
		ps.setDouble(10, 1.0D);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<Brand> getList(Connection con) throws SQLException {
		String sql = "select brand,\n       brand_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,disc_perc,tax_perc\n  from brand p\n where 1 =1 ";

		List<Brand> list = new ArrayList();

		boolean doFetchData = false;
		if ((getBrand() != null) && (getBrand().trim().length() > 0)) {
			sql = sql + " and upper(brand) like upper('" + getBrand().trim() + "') ";
			doFetchData = true;
		}
		if ((getBrandDesc() != null) && (getBrandDesc().trim().length() > 0)) {
			sql = sql + " and upper(brand_desc) like upper('" + getBrandDesc().trim() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().trim().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus().trim() + "') ";
			doFetchData = true;
		}
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Brand temp = new Brand();
			temp.setBrand(rs.getString("brand"));
			temp.setBrandDesc(rs.getString("brand_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setDiscPerc(rs.getDouble("disc_perc"));
			temp.setTaxPerc(rs.getDouble("tax_perc"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select brand,\n       brand_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,disc_perc,tax_perc\n  from brand p\n where brand = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.brand);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setBrand(rs.getString("brand"));
			setBrandDesc(rs.getString("brand_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setDiscPerc(rs.getDouble("disc_perc"));
			setTaxPerc(rs.getDouble("tax_perc"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from  brand p\nwhere brand=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.brand);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.brand != null ? this.brand.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Brand)) {
			return false;
		}
		Brand other = (Brand) object;
		if (((this.brand == null) && (other.brand != null))
				|| ((this.brand != null) && (!this.brand.equals(other.brand)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "brand=" + this.brand + "";
	}
}
