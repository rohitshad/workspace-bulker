/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Employee extends EntitiesImpl implements Serializable {
	private String empId;
	private String empName;
	private String department;
	private String supervisor;
	private String address;
	private String mail;
	private String phone;
	private String mob;
	private Date dob;
	private Date marrAnniv;
	private double salary;
	private String noteText;
	private String status;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp updatedOn;
	private String updatedBy;
	private double objectId;

	public Employee() {
	}

	public Employee(String empId) {
		this.empId = empId;
	}

	public String getEmpId() {
		return this.empId;
	}

	public void setEmpId(String empId) {
		this.empId = empId;
	}

	public String getEmpName() {
		return this.empName;
	}

	public void setEmpName(String empName) {
		this.empName = empName;
	}

	public String getDepartment() {
		return this.department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getSupervisor() {
		return this.supervisor;
	}

	public void setSupervisor(String supervisor) {
		this.supervisor = supervisor;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMob() {
		return this.mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getMarrAnniv() {
		return this.marrAnniv;
	}

	public void setMarrAnniv(Date marrAnniv) {
		this.marrAnniv = marrAnniv;
	}

	public double getSalary() {
		return this.salary;
	}

	public void setSalary(double salary) {
		this.salary = salary;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update employee\n   set emp_name   = ?,\n       department = ?,\n       supervisor = ?,\n       address    = ?,\n       mail       = ?,\n       phone      = ?,\n       mob        = ?,\n       dob        = ?,\n       marr_anniv = ?,\n       salary     = ?,\n       note_text  = ?,\n       status     = ?,\n       created_on = ?,\n       created_by = ?,\n       updated_on = ?,\n       updated_by = ?,\n       object_id  = object_id +1\n where emp_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getEmpName());
		ps.setString(2, getDepartment());
		ps.setString(3, getSupervisor());
		ps.setString(4, getAddress());
		ps.setString(5, getMail());
		ps.setString(6, getPhone());
		ps.setString(7, getMob());
		ps.setDate(8, Utils.getSqlDate(getDob()));
		ps.setDate(9, Utils.getSqlDate(getMarrAnniv()));
		ps.setDouble(10, getSalary());
		ps.setString(11, this.noteText);
		ps.setString(12, this.status);
		ps.setTimestamp(13, getCreatedOn());
		ps.setString(14, this.createdBy);
		ps.setTimestamp(15, Utils.getTimestamp(new Date()));
		ps.setString(16, getUserId());
		ps.setString(17, getEmpId());
		ps.setDouble(18, getObjectId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into employee\n  (emp_id,\n   emp_name,\n   department,\n   supervisor,\n   address,\n   mail,\n   phone,\n   mob,\n   dob,\n   marr_anniv,\n   salary,\n   note_text,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getEmpId());
		ps.setString(2, getEmpName());
		ps.setString(3, getDepartment());
		ps.setString(4, getSupervisor());
		ps.setString(5, getAddress());
		ps.setString(6, getMail());
		ps.setString(7, getPhone());
		ps.setString(8, getMob());
		ps.setDate(9, Utils.getSqlDate(getDob()));
		ps.setDate(10, Utils.getSqlDate(getMarrAnniv()));
		ps.setDouble(11, getSalary());
		ps.setString(12, this.noteText);
		ps.setString(13, this.status);
		ps.setTimestamp(14, Utils.getTimestamp(new Date()));
		ps.setString(15, getUserId());
		ps.setTimestamp(16, getUpdatedOn());
		ps.setString(17, this.updatedBy);
		ps.setDouble(18, 1.0D);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public String load(Connection con) throws SQLException {
		String sql = "select emp_id,\n       emp_name,\n       department,\n       supervisor,\n       address,\n       mail,\n       phone,\n       mob,\n       dob,\n       marr_anniv,\n       salary,\n       note_text,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from employee  where emp_id =? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getEmpId());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setEmpId(rs.getString("emp_id"));
			setEmpName(rs.getString("emp_name"));
			setDepartment(rs.getString("department"));
			setSupervisor(rs.getString("supervisor"));
			setAddress(rs.getString("address"));
			setMail(rs.getString("mail"));
			setPhone(rs.getString("phone"));
			setMob(rs.getString("mob"));
			setDob(rs.getDate("dob"));
			setMarrAnniv(rs.getDate("marr_anniv"));
			setSalary(rs.getDouble("salary"));
			setNoteText(rs.getString("note_text"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getTimestamp("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getTimestamp("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public List<Employee> getList(Connection con) throws SQLException {
		String sql = "select emp_id,\n       emp_name,\n       department,\n       supervisor,\n       address,\n       mail,\n       phone,\n       mob,\n       dob,\n       marr_anniv,\n       salary,\n       note_text,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from employee  where 1=1 ";

		boolean doFetchData = false;
		if ((getEmpId() != null) && (getEmpId().length() > 0)) {
			sql = sql + " and emp_id like '" + getEmpId() + "' ";
			doFetchData = true;
		}
		if ((getEmpName() != null) && (getEmpName().length() > 0)) {
			sql = sql + " and upper(emp_name) like upper('" + getEmpName() + "') ";
			doFetchData = true;
		}
		if ((getAddress() != null) && (getAddress().length() > 0)) {
			sql = sql + " and upper(address) like upper('" + getAddress() + "') ";
			doFetchData = true;
		}
		if ((getPhone() != null) && (getPhone().length() > 0)) {
			sql = sql + " and upper(phone) like upper('" + getPhone() + "') ";
			doFetchData = true;
		}
		if ((getMob() != null) && (getMob().length() > 0)) {
			sql = sql + " and upper(mob) like upper('" + getMob() + "') ";
			doFetchData = true;
		}
		if ((getMail() != null) && (getMail().length() > 0)) {
			sql = sql + " and upper(mail) like upper('" + getMail() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().length() > 0)) {
			sql = sql + " and upper(nvl(status,'InActive')) like upper('" + getStatus() + "') ";
			doFetchData = true;
		}
		List<Employee> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Employee temp = new Employee();
			temp.setEmpId(rs.getString("emp_id"));
			temp.setEmpName(rs.getString("emp_name"));
			temp.setDepartment(rs.getString("department"));
			temp.setSupervisor(rs.getString("supervisor"));
			temp.setAddress(rs.getString("address"));
			temp.setMail(rs.getString("mail"));
			temp.setPhone(rs.getString("phone"));
			temp.setMob(rs.getString("mob"));
			temp.setDob(rs.getDate("dob"));
			temp.setMarrAnniv(rs.getDate("marr_anniv"));
			temp.setSalary(rs.getDouble("salary"));
			temp.setNoteText(rs.getString("note_text"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getTimestamp("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getTimestamp("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from employee\n where emp_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getEmpId());
		ps.setDouble(2, getObjectId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.empId != null ? this.empId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Employee)) {
			return false;
		}
		Employee other = (Employee) object;
		if (((this.empId == null) && (other.empId != null))
				|| ((this.empId != null) && (!this.empId.equals(other.empId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Emp Id :" + this.empId + " ";
	}
}
