/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.util.ResourceBundle;

public class Company {
	private final String brand;
	private final String companyName;
	private final String address1;
	private final String address2;
	private final String address3;

	public Company() {
		ResourceBundle bundle = ResourceBundle.getBundle("resources/resources");
		this.brand = bundle.getString("brand");
		this.companyName = bundle.getString("companyName");
		this.address1 = bundle.getString("address1");
		this.address2 = bundle.getString("address2");
		this.address3 = bundle.getString("address3");
	}

	public String getBrand() {
		return this.brand;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getAddress1() {
		return this.address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public String getAddress3() {
		return this.address3;
	}
}
