/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.common;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class Utils {
	public static SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

	public static java.sql.Date getSqlDate(java.util.Date dt) {
		java.sql.Date sdate = null;
		if (dt == null) {
			return sdate;
		}
		sdate = new java.sql.Date(dt.getTime());
		return sdate;
	}

	public static double getDouble(Double d) {
		if (d == null) {
			return 0.0D;
		}
		return d.doubleValue();
	}

	public static String getStrDate(java.util.Date d) {
		if (d == null) {
			return null;
		}
		return sdf.format(d);
	}

	public static Timestamp getTimestamp(java.util.Date dt) {
		Timestamp ts = null;
		if (dt == null) {
			return ts;
		}
		ts = new Timestamp(dt.getTime());
		return ts;
	}
}
