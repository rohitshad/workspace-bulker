/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class UserRoles extends EntitiesImpl implements Serializable {
	private String loginId;
	private String role;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;

	public UserRoles() {
	}

	public UserRoles(String loginId, String role) {
		this.loginId = loginId;
		this.role = role;
	}

	public String getLoginId() {
		return this.loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into user_roles\n  (user_id,\n   role,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getLoginId());
		ps.setString(2, getRole());
		ps.setDate(3, Utils.getSqlDate(new Date()));
		ps.setString(4, getUserId());
		ps.setDate(5, Utils.getSqlDate(this.updatedOn));
		ps.setString(6, getUserId());
		ps.setDouble(7, 1.0D);

		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from user_roles\n  where user_id=? and role=? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getLoginId());
		ps.setString(2, getRole());

		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be deleted.");
		}
		return "Record Deleted.";
	}

	public List<UserRoles> getList(Connection con) throws SQLException {
		String sql = "select user_id,\n       role,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from user_roles r\n where r.user_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getLoginId());

		ResultSet rs = ps.executeQuery();
		List<UserRoles> list = new ArrayList();
		while (rs.next()) {
			UserRoles temp = new UserRoles();
			temp.setLoginId(rs.getString("user_id"));
			temp.setRole(rs.getString("role"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.loginId != null ? this.loginId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof UserRoles)) {
			return false;
		}
		UserRoles other = (UserRoles) object;
		if (((this.loginId == null) && (other.loginId != null))
				|| ((this.loginId != null) && (!this.loginId.equals(other.loginId)))) {
			return false;
		}
		if (((this.role == null) && (other.role != null)) || ((this.role != null) && (!this.role.equals(other.role)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "User :" + this.loginId + " Roles :" + this.role;
	}
}
