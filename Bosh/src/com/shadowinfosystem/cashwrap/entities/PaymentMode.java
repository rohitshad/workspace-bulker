/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class PaymentMode extends EntitiesImpl implements Serializable {
	private String payModeId;
	private String payModeDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public PaymentMode() {
	}

	public PaymentMode(String payModeId) {
		this.payModeId = payModeId;
	}

	public String getPayModeId() {
		return this.payModeId;
	}

	public void setPayModeId(String payModeId) {
		this.payModeId = payModeId;
	}

	public String getPayModeDesc() {
		return this.payModeDesc;
	}

	public void setPayModeDesc(String payModeDesc) {
		this.payModeDesc = payModeDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update payment_mode p\nset pay_mode_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere pay_mode_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.payModeDesc);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setString(7, this.payModeId);
		ps.setShort(8, this.objectId);
		int n = ps.executeUpdate();
		return "success";
	}

	public String create(Connection con) throws SQLException, Exception {
		String sql = "insert into payment_mode\n  (pay_mode_id,\n   pay_mode_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.payModeId);
		ps.setString(2, this.payModeDesc);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		ps.setShort(8, (short) 1);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Pay Mode Record could not be created.");
		}
		return "success";
	}

	public List<PaymentMode> getList(Connection con) throws SQLException {
		String sql = "select pay_mode_id,\n       pay_mode_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from payment_mode p\n where pay_mode_id like nvl(?,'%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.payModeId);

		ResultSet rs = ps.executeQuery();
		List<PaymentMode> list = new ArrayList();
		while (rs.next()) {
			PaymentMode temp = new PaymentMode();
			temp.setPayModeId(rs.getString("pay_mode_id"));
			temp.setPayModeDesc(rs.getString("pay_mode_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select pay_mode_id,\n       pay_mode_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from payment_mode p\n where pay_mode_id  = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.payModeId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setPayModeId(rs.getString("pay_mode_id"));
			setPayModeDesc(rs.getString("pay_mode_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException, Exception {
		String sql = "delete from payment_mode p\nwhere pay_mode_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.payModeId);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Deleted";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.payModeId != null ? this.payModeId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof PaymentMode)) {
			return false;
		}
		PaymentMode other = (PaymentMode) object;
		if (((this.payModeId == null) && (other.payModeId != null))
				|| ((this.payModeId != null) && (!this.payModeId.equals(other.payModeId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Payment Mode=" + this.payModeId + " ";
	}
}
