/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class ArticleItems extends EntitiesImpl implements Serializable {
	private String item_code;
	private String article_no;
	private String bar_code;
	private double pur_price;
	private double mrp;
	private double sales_price;
	private String status;
	private Date created_on;
	private String created_by;
	private Date updated_on;
	private String updated_by;
	private double object_id;
	private double wsp;
	private String itemDesc;
	private double barCodeQty;
	private boolean isEditable;
	private String article_desc;
	private String article_short_desc;
	private String inventory_uom;
	private String article_type;
	private String article_group;
	private String article_sub_group;
	private String article_size;
	private String color;
	private String manufacturer;
	private double tax_per;
	private double disc_per;
	private String search_text;
	private double qty_on_hand;
	private String colorList = "";
	private String sizeList = "";
	private String transId;
	private short lineNo;
	private double qty;
	private String uom;
	private String selsPers;
	private double discPer;
	private double discAmt;
	private String taxCode;
	private double taxPer;
	private double taxAmt;
	private double netAmt;
	private String fabric;
	private String remark;
	private String attr6;
	private String sel;
	private String batch;
	private Date expDate;
	private String subSection;
	private String section;
	private String itemSize;
	private String brand;
	private String style;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double salesPrice;
	private double qtyOnHand;
	private String articleNo;
	private String barCode;
	private double purPrice;
	
	private String noteText;
	private String purchaseOrder;
	private Date receiptDate;
	private boolean zeroStcokAllowed;
	private double amount;
	private double mpPer;
	private double wspPer;
	private boolean isItemCodeInStock;
	private double grossPp;
	private double discPerc;
	private double taxPerc;
	
	public ArticleItems() {
		setIsEditable(false);
	}

	public boolean isIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getItem_code() {
		return this.item_code;
	}

	public void setItem_code(String item_code) {
		this.item_code = item_code;
	}

	public String getArticle_no() {
		return this.article_no;
	}

	public void setArticle_no(String article_no) {
		this.article_no = article_no;
	}

	public String getBar_code() {
		return this.bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	public double getPur_price() {
		return this.pur_price;
	}

	public void setPur_price(double pur_price) {
		this.pur_price = pur_price;
	}

	public double getMrp() {
		return this.mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getSales_price() {
		return this.sales_price;
	}

	public void setSales_price(double sales_price) {
		this.sales_price = sales_price;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreated_on() {
		return this.created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public String getCreated_by() {
		return this.created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_on() {
		return this.updated_on;
	}

	public void setUpdated_on(Date updated_on) {
		this.updated_on = updated_on;
	}

	public String getUpdated_by() {
		return this.updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public double getObject_id() {
		return this.object_id;
	}

	public void setObject_id(double object_id) {
		this.object_id = object_id;
	}

	public double getWsp() {
		return this.wsp;
	}

	public void setWsp(double wsp) {
		this.wsp = wsp;
	}

	public String getArticle_desc() {
		return this.article_desc;
	}

	public void setArticle_desc(String article_desc) {
		this.article_desc = article_desc;
	}

	public String getArticle_short_desc() {
		return this.article_short_desc;
	}

	public void setArticle_short_desc(String article_short_desc) {
		this.article_short_desc = article_short_desc;
	}

	public String getInventory_uom() {
		return this.inventory_uom;
	}

	public void setInventory_uom(String inventory_uom) {
		this.inventory_uom = inventory_uom;
	}

	public String getArticle_type() {
		return this.article_type;
	}

	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}

	public String getArticle_group() {
		return this.article_group;
	}

	public void setArticle_group(String article_group) {
		this.article_group = article_group;
	}

	public String getArticle_sub_group() {
		return this.article_sub_group;
	}

	public void setArticle_sub_group(String article_sub_group) {
		this.article_sub_group = article_sub_group;
	}

	public String getArticle_size() {
		return this.article_size;
	}

	public void setArticle_size(String article_size) {
		this.article_size = article_size;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getDisc_per() {
		return this.disc_per;
	}

	public void setDisc_per(double disc_per) {
		this.disc_per = disc_per;
	}

	public double getQty_on_hand() {
		return this.qty_on_hand;
	}

	public void setQty_on_hand(double qty_on_hand) {
		this.qty_on_hand = qty_on_hand;
	}

	public String getSearch_text() {
		return this.search_text;
	}

	public void setSearch_text(String search_text) {
		this.search_text = search_text;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public int hashCode() {
		int hash = 5;
		hash = 37 * hash + (this.item_code != null ? this.item_code.hashCode() : 0);
		return hash;
	}

	public double getTax_per() {
		return this.tax_per;
	}

	public void setTax_per(double tax_per) {
		this.tax_per = tax_per;
	}

	public double getBarCodeQty() {
		return this.barCodeQty;
	}

	public void setBarCodeQty(double barCodeQty) {
		this.barCodeQty = barCodeQty;
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArticleItems other = (ArticleItems) obj;
		if (this.item_code == null ? other.item_code != null : !this.item_code.equals(other.item_code)) {
			return false;
		}
		return true;
	}

	public String update(Connection con) throws SQLException {
		if (getObject_id() <= 0.0D) {
			return create(con);
		}
		String sql = "update article_items\n   set article_no  = ?,\n       bar_code    = ?,\n       pur_price   = ?,\n       mrp         = ?,\n       sales_price = ?,\n       status      = ?,\n       created_on  = ?,\n       created_by  = ?,\n       updated_on  = ?,\n       updated_by  = ?,\n       object_id   = nvl(object_id, 1) + 1,wsp=?,item_desc=?\n where item_code = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getArticle_no());
		ps.setString(2, getBar_code());
		ps.setDouble(3, getPur_price());
		ps.setDouble(4, getMrp());
		ps.setDouble(5, getSales_price());
		ps.setString(6, getStatus());
		ps.setDate(7, Utils.getSqlDate(getCreated_on()));
		ps.setString(8, getCreated_by());
		ps.setDate(9, Utils.getSqlDate(new Date()));
		ps.setString(10, getUpdated_by());
		ps.setDouble(11, getWsp());
		ps.setString(12, getItemDesc());
		ps.setString(13, getItem_code());
		ps.setDouble(14, getObject_id());
		double nr = 0.0D;
		nr = ps.executeUpdate();
		
		
		
		if (nr <= 0.0D) {
			throw new SQLException("Record could not be modified.");
		}
	
		
		return "Record updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into article_items\n  (article_no,\n   bar_code,\n   pur_price,\n   mrp,\n   sales_price,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   item_code,\n   object_id,wsp,item_desc,color,article_size)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getArticle_no());
		ps.setString(2, getBar_code());
		ps.setDouble(3, getPur_price());
		ps.setDouble(4, getMrp());
		ps.setDouble(5, getSales_price());
		ps.setString(6, getStatus());
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getCreated_by());
		ps.setDate(9, Utils.getSqlDate(getUpdated_on()));
		ps.setString(10, getUpdated_by());
		ps.setString(11, getItem_code());
		ps.setDouble(12, 1.0D);
		ps.setDouble(13, getWsp());
		ps.setString(14, getItemDesc());
		ps.setString(15, getColor());
		ps.setString(16, getArticle_size());
		
		double nr = 0.0D;
		nr = ps.executeUpdate();
		if (nr <= 0.0D) {
			throw new SQLException("Record could not be created.");
		}
		
	
		
		return "Record created.";
	}

	private Object getItemCode() {
		// TODO Auto-generated method stub
		return null;
	}

	private String getNextItemCode(Connection con) {
		// TODO Auto-generated method stub
		return null;
	}

	public List<ArticleItems> fetchItemsForArticle(Connection con) throws SQLException, Exception {
		String sql = "select item_code,\n       article_no,\n       bar_code,\n       pur_price,\n       mrp,\n       sales_price,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n      ,wsp,item_desc,color,ARTICLE_SIZE  from article_items i\n where i.article_no = ? ORDER BY I.item_code";
		if ((getArticle_no() == null) || (getArticle_no().length() <= 0)) {
			throw new Exception("Please enter Atricle Number.");
		}
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getArticle_no());

		ResultSet rs = ps.executeQuery();
		List<ArticleItems> list = new ArrayList();
		while (rs.next()) {
			ArticleItems temp = new ArticleItems();
			temp.setArticle_no(rs.getString("article_no"));
			temp.setItem_code(rs.getString("item_code"));
			temp.setBar_code(rs.getString("bar_code"));
			temp.setPur_price(rs.getDouble("pur_price"));
			temp.setMrp(rs.getDouble("mrp"));
			temp.setSales_price(rs.getDouble("sales_price"));
			temp.setStatus(rs.getString("status"));
			temp.setCreated_on(rs.getDate("created_on"));
			temp.setCreated_by(rs.getString("created_by"));
			temp.setUpdated_on(rs.getDate("updated_on"));
			temp.setUpdated_by(rs.getString("updated_by"));
			temp.setObject_id(rs.getDouble("object_id"));
			temp.setWsp(rs.getDouble("wsp"));
			temp.setItemDesc(rs.getString("item_desc"));
			temp.setColor(rs.getString("COLOR"));
			temp.setArticle_size(rs.getString("ARTICLE_SIZE"));
			
			list.add(temp);
		    colorList = colorList+"'"+rs.getString("COLOR")+"',";
		    sizeList = sizeList+"'"+rs.getString("ARTICLE_SIZE")+"',";
		    
		    
		}
		
		return list;
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete FROM article_items\n where item_code = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getItem_code());
		ps.setDouble(2, getObject_id());
		double nr = 0.0D;
		nr = ps.executeUpdate();
		if (nr <= 0.0D) {
			throw new SQLException("Record could not be delete.");
		}
		return "Record deleted.";
	}
	public List<ArticleItems> search(Connection con) throws SQLException, Exception {
		String sql = "select d.item_code,\n       d.article_no,\n       d.bar_code,\n       d.pur_price,\n       d.mrp,\n       d.sales_price,\n       d.status,\n       d.created_on,\n       d.created_by,\n       d.updated_on,\n       d.updated_by,\n       d.object_id,\n       d.wsp,\n       h.article_desc,\n       h.article_type,\n       h.article_group,\n       h.article_sub_group,\n       d.article_size,\n       d.color,\n       h.article_short_desc,\n       h.manufacturer,\n       h.inventory_uom,\n       h.style,\n       h.min_stock,\n       h.max_stock,\n       h.remark1,\n       h.remark2,\n       h.remark3,\n       h.tax_per,\n       h.disc_per,\n       h.open_bal,\n       h.rack,\n       h.wsp_disc_per, d.item_desc item_desc_line\n  from article_items d, article h\n where d.article_no = h.article_no ";

		boolean doFetchData = false;
		if ((getArticle_no() != null) && (getArticle_no().length() > 0)) {
			sql = sql + " and d.article_no='" + getArticle_no() + "' ";
			doFetchData = true;
		}
		if ((getBar_code() != null) && (getBar_code().length() > 0)) {
			sql = sql + " and d.bar_code='" + getBar_code() + "' ";
			doFetchData = true;
		}
		
		if ((getItem_code() != null) && (getItem_code().length() > 0)) {
			sql = sql + " and d.item_code='" + getItem_code() + "' ";
			doFetchData = true;
		}
		if ((getManufacturer() != null) && (getManufacturer().length() > 0)) {
			sql = sql + " and h.manufacturer='" + getManufacturer() + "' ";
			doFetchData = true;
		}
		
		if ((getItemDesc() != null) && (getItemDesc().length() > 0)) {
			sql = sql + " and d.item_desc='" + getItemDesc() + "' ";
			doFetchData = true;
		}
		
		if ((getArticle_desc() != null) && (getArticle_desc().length() > 0)) {
			sql = sql + " and upper(h.article_desc) like upper('%" + getArticle_desc() + "%') ";
			doFetchData = true;
		}
		if ((getSearch_text() != null) && (getSearch_text().length() > 0)) {
			sql = sql + " and upper(nvl(d.bar_code,'')||nvl(d.item_code,'')||nvl(h.manufacturer,'')||nvl(d.item_desc,'')||nvl(d.article_no,'')) like upper('%"
					+ getSearch_text().trim() + "%') ";
			doFetchData = true;
		}
		if ((getArticle_short_desc() != null) && (getArticle_short_desc().length() > 0)) {
			sql = sql + " and upper(h.article_short_desc) like upper('%" + getArticle_short_desc() + "%') ";
			doFetchData = true;
		}
		List<ArticleItems> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleItems temp = new ArticleItems();
			temp.setArticle_no(rs.getString("article_no"));
			temp.setItem_code(rs.getString("item_code"));
			temp.setBar_code(rs.getString("bar_code"));
			temp.setPur_price(rs.getDouble("pur_price"));
			temp.setMrp(rs.getDouble("mrp"));
			temp.setSales_price(rs.getDouble("sales_price"));
			temp.setStatus(rs.getString("status"));
			temp.setCreated_on(rs.getDate("created_on"));
			temp.setCreated_by(rs.getString("created_by"));
			temp.setUpdated_on(rs.getDate("updated_on"));
			temp.setUpdated_by(rs.getString("updated_by"));
			temp.setObject_id(rs.getDouble("object_id"));
			temp.setWsp(rs.getDouble("wsp"));

			temp.setArticle_desc(rs.getString("article_desc"));
			temp.setArticle_short_desc(rs.getString("article_short_desc"));
			temp.setInventory_uom(rs.getString("inventory_uom"));
			temp.setArticle_type(rs.getString("article_type"));
			temp.setArticle_group(rs.getString("article_group"));
			temp.setArticle_sub_group(rs.getString("article_sub_group"));
			temp.setArticle_size(rs.getString("article_size"));
			temp.setColor(rs.getString("color"));
			temp.setManufacturer(rs.getString("manufacturer"));
			temp.setTax_per(rs.getDouble("tax_per"));
			temp.setDisc_per(rs.getDouble("disc_per"));
			temp.setItemDesc(rs.getString("item_desc_line"));

			list.add(temp);
		}
		return list;
	}

	public boolean isItemAvailable(Connection con,String color, String size,String item_code) throws SQLException, Exception {
		String sql = "select count(*) noOfItem from article_items d, article h\n where d.article_no = h.article_no and d.color = '"+ color+"' and d.ARTICLE_SIZE='"+ size +"' and d.item_code like '%"+item_code+"%'";

	
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
	    int count = 0;
		while(rs.next()){
			count = rs.getInt("noOfItem");
		}
		
		if(count > 0){
			return true;
		}
		return false;
	}

	public int getTotalArticleItem(Connection con,String item_code) throws SQLException, Exception {
		String sql = "select count(*) noOfItem from article_items d, article h\n where d.article_no = h.article_no and  d.item_code like '%"+item_code+"%'";

	
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
	    int count = 0;
		while(rs.next()){
			 count = rs.getInt("noOfItem");
		}
		
		return count;
	}
	
	public String getColorList() {
		return colorList;
	}

	public void setColorList(String colorList) {
		this.colorList = colorList;
	}

	public String getSizeList() {
		return sizeList;
	}

	public void setSizeList(String sizeList) {
		this.sizeList = sizeList;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public short getLineNo() {
		return lineNo;
	}

	public void setLineNo(short lineNo) {
		this.lineNo = lineNo;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getSelsPers() {
		return selsPers;
	}

	public void setSelsPers(String selsPers) {
		this.selsPers = selsPers;
	}

	public double getDiscPer() {
		return discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public double getDiscAmt() {
		return discAmt;
	}

	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public double getTaxPer() {
		return taxPer;
	}

	public void setTaxPer(double taxPer) {
		this.taxPer = taxPer;
	}

	public double getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public double getNetAmt() {
		return netAmt;
	}

	public void setNetAmt(double netAmt) {
		this.netAmt = netAmt;
	}

	public String getFabric() {
		return fabric;
	}

	public void setFabric(String fabric) {
		this.fabric = fabric;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAttr6() {
		return attr6;
	}

	public void setAttr6(String attr6) {
		this.attr6 = attr6;
	}

	public String getSel() {
		return sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String getSubSection() {
		return subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getSalesPrice() {
		return salesPrice;
	}

	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}

	public double getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(double qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public String getArticleNo() {
		return articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public double getPurPrice() {
		return purPrice;
	}

	public void setPurPrice(double purPrice) {
		this.purPrice = purPrice;
	}

	public String getNoteText() {
		return noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public boolean isZeroStcokAllowed() {
		return zeroStcokAllowed;
	}

	public void setZeroStcokAllowed(boolean zeroStcokAllowed) {
		this.zeroStcokAllowed = zeroStcokAllowed;
	}

	public boolean isItemCodeInStock() {
		return isItemCodeInStock;
	}

	public void setItemCodeInStock(boolean isItemCodeInStock) {
		this.isItemCodeInStock = isItemCodeInStock;
	}

	

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getMpPer() {
		return mpPer;
	}

	public void setMpPer(double mpPer) {
		this.mpPer = mpPer;
	}

	public double getWspPer() {
		return wspPer;
	}

	public void setWspPer(double wspPer) {
		this.wspPer = wspPer;
	}

	public double getGrossPp() {
		return grossPp;
	}

	public void setGrossPp(double grossPp) {
		this.grossPp = grossPp;
	}

	public double getDiscPerc() {
		return discPerc;
	}

	public void setDiscPerc(double discPerc) {
		this.discPerc = discPerc;
	}

	public double getTaxPerc() {
		return taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	
	
	
	
}
