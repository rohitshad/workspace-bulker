/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Locale;
import java.util.ResourceBundle;

public class Company {
	private String brand;
	private String companyName;
	private String address1;
	private String address2;
	private String address3;

	public Company() {
		File file = new File("//root//tmp//bulker");
		try {
			URL[]  urls={file.toURI().toURL()};
			ClassLoader loader = new URLClassLoader(urls);
			ResourceBundle bundle = ResourceBundle.getBundle("resources", Locale.getDefault(), loader);
			this.brand = bundle.getString("brand");
			this.companyName = bundle.getString("companyName");
			this.address1 = bundle.getString("address1");
			this.address2 = bundle.getString("address2");
			this.address3 = bundle.getString("address3");
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		
	}

	public String getBrand() {
		return this.brand;
	}

	public String getCompanyName() {
		return this.companyName;
	}

	public String getAddress1() {
		return this.address1;
	}

	public String getAddress2() {
		return this.address2;
	}

	public String getAddress3() {
		return this.address3;
	}
}
