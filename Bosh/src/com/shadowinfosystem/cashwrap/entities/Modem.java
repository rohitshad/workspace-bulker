/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;

public class Modem extends EntitiesImpl implements Serializable {
	private String smscNumber;
	private String modemId;
	private String comPort;
	private String modemManufacturer;
	private String modemModel;
	private int baudRate;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public String getSmscNumber() {
		return this.smscNumber;
	}

	public void setSmscNumber(String smscNumber) {
		this.smscNumber = smscNumber;
	}

	public String getModemId() {
		return this.modemId;
	}

	public void setModemId(String modemId) {
		this.modemId = modemId;
	}

	public String getComPort() {
		return this.comPort;
	}

	public void setComPort(String comPort) {
		this.comPort = comPort;
	}

	public String getModemManufacturer() {
		return this.modemManufacturer;
	}

	public void setModemManufacturer(String modemManufacturer) {
		this.modemManufacturer = modemManufacturer;
	}

	public String getModemModel() {
		return this.modemModel;
	}

	public void setModemModel(String modemModel) {
		this.modemModel = modemModel;
	}

	public int getBaudRate() {
		return this.baudRate;
	}

	public void setBaudRate(int baudRate) {
		this.baudRate = baudRate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String getDefaultModem() throws Exception {
		String sql = "select smsc_number,\n       modem_id,\n       com_port,\n       modem_manufacturer,\n       modem_model,\n       baud_rate,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from modem_manager t\n where t.status = 'Active'";

		Connection con = Data.getConnection();
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			ps = con.prepareStatement(sql);
			rs = ps.executeQuery();
			if (rs.next()) {
				setModemId(rs.getString("modem_id"));
				setSmscNumber(rs.getString("smsc_number"));
				setComPort(rs.getString("com_port"));
				setModemManufacturer(rs.getString("modem_manufacturer"));
				setModemModel(rs.getString("modem_model"));
				setBaudRate((int) rs.getDouble("baud_rate"));
				setStatus(rs.getString("status"));
				setCreatedOn(rs.getDate("created_On"));
				setCreatedBy(rs.getString("Created_By"));
				setUpdatedOn(rs.getDate("updated_On"));
				setUpdatedBy(rs.getString("updated_By"));
				setObjectId((short) (int) rs.getDouble("object_Id"));
			}
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(Modem.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
			throw new Exception(e.getMessage());
		}
		return "Modem";
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		setUpdatedOn(new Date());
		String sql = "update modem_manager m\n   set smsc_number        = ?,\n       com_port           = ?,\n       modem_manufacturer = ?,\n       modem_model        = ?,\n       baud_rate          = ?,\n       status             = ?,\n       created_on         = ?,\n       created_by         = ?,\n       updated_on         = ?,\n       updated_by         = ?,\n       object_id          = nvl(object_id, 1) + 1\n where modem_id = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getSmscNumber());
		ps.setString(2, getComPort());
		ps.setString(3, getModemManufacturer());
		ps.setString(4, getModemModel());
		ps.setDouble(5, getBaudRate());
		ps.setString(6, getStatus());
		ps.setDate(7, Utils.getSqlDate(this.createdOn));
		ps.setString(8, this.createdBy);
		ps.setDate(9, Utils.getSqlDate(new Date()));
		ps.setString(10, this.updatedBy);
		ps.setString(11, getModemId());
		ps.setShort(12, this.objectId);
		int n = ps.executeUpdate();
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		setCreatedOn(new Date());
		String sql = "insert into modem_manager\n  (smsc_number,\n   modem_id,\n   com_port,\n   modem_manufacturer,\n   modem_model,\n   baud_rate,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getSmscNumber());
		ps.setString(2, getModemId());
		ps.setString(3, getComPort());
		ps.setString(4, getModemManufacturer());
		ps.setString(5, getModemModel());
		ps.setDouble(6, getBaudRate());
		ps.setString(7, getStatus());
		ps.setDate(8, Utils.getSqlDate(new Date()));
		ps.setString(9, this.createdBy);
		ps.setDate(10, Utils.getSqlDate(this.updatedOn));
		ps.setString(11, this.updatedBy);
		ps.setShort(12, (short) 1);
		int n = ps.executeUpdate();
		return "Record Created.";
	}

	public List<Modem> getList(Connection con) throws SQLException {
		String sql = "select smsc_number,\n       modem_id,\n       com_port,\n       modem_manufacturer,\n       modem_model,\n       baud_rate,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from modem_manager m\n where 1=1 ";

		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		List<Modem> list = new ArrayList();
		while (rs.next()) {
			Modem temp = new Modem();

			temp.setModemId(rs.getString("modem_id"));
			temp.setSmscNumber(rs.getString("smsc_number"));
			temp.setComPort(rs.getString("com_port"));
			temp.setModemManufacturer(rs.getString("modem_manufacturer"));
			temp.setModemModel(rs.getString("modem_model"));
			temp.setBaudRate((int) rs.getDouble("baud_rate"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select smsc_number,\n       modem_id,\n       com_port,\n       modem_manufacturer,\n       modem_model,\n       baud_rate,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from modem_manager m\n where modem_id=? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getModemId());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setModemId(rs.getString("modem_id"));
			setSmscNumber(rs.getString("smsc_number"));
			setComPort(rs.getString("com_port"));
			setModemManufacturer(rs.getString("modem_manufacturer"));
			setModemModel(rs.getString("modem_model"));
			setBaudRate((int) rs.getDouble("baud_rate"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}
}
