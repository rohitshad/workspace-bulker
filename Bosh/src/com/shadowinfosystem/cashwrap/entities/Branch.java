/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Branch extends EntitiesImpl implements Serializable {
	private String branchId;
	private String branchName;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;

	public String getBranchId() {
		return this.branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public String getBranchName() {
		return this.branchName;
	}

	public void setBranchName(String branchName) {
		this.branchName = branchName;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update branch p\nset branch_name=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere branch_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getBranchName());
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setString(7, getBranchId());
		ps.setDouble(8, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into branch\n  (branch_id,\n   branch_name,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by)\nvalues\n  (?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getBranchId());
		ps.setString(2, getBranchName());
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<Branch> getList(Connection con) throws SQLException {
		String sql = "select branch_id,\n       branch_name,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from branch p\n where 1 =1 ";

		List<Branch> list = new ArrayList();

		boolean doFetchData = false;
		if ((getBranchId() != null) && (getBranchId().trim().length() > 0)) {
			sql = sql + " and upper(branch_ID) like upper('" + getBranchId().trim() + "') ";
			doFetchData = true;
		}
		if ((getBranchName() != null) && (getBranchName().trim().length() > 0)) {
			sql = sql + " and upper(branch_name) like upper('" + getBranchName().trim() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().trim().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus().trim() + "') ";
			doFetchData = true;
		}
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Branch temp = new Branch();
			temp.setBranchId(rs.getString("branch_id"));
			temp.setBranchName(rs.getString("branch_name"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select branch_id,\n       branch_name,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from branch p\n where branch_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getBranchId());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setBranchId(rs.getString("branch_id"));
			setBranchName(rs.getString("branch_name"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from  branch p\nwhere branch_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getBranchId());
		ps.setDouble(2, getObjectId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.branchId != null ? this.branchId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Branch)) {
			return false;
		}
		Branch other = (Branch) object;
		if (((this.branchId == null) && (other.branchId != null))
				|| ((this.branchId != null) && (!this.branchId.equals(other.branchId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Branch Id=" + this.branchId + "";
	}
}
