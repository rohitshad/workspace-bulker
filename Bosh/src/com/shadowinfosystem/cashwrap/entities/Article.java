/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Article extends EntitiesImpl implements Serializable {
	private String articleNo;
	private String articleDesc;
	private String article_type;
	private String article_group;
	private String article_sub_group;
	private List<String> article_size;
	private List<String> color;
	private String article_short_desc;
	private String manufacturer;
	private String inventory_uom;
	private String style;
	private double min_stock;
	private double max_stock;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private String remark1;
	private String remark2;
	private String remark3;
	private double tax_per;
	private double disc_per;
	private double open_bal;
	private String rack;
	private double wsp_disc_per;
	private String bar_code;
    private double Mrp;
    private double wsp;
	private String search_text;
	private List<ArticleItems> itemsList;
	private List<Color> colorList = new ArrayList<Color>();
	List<Color> selectedColorList = new ArrayList<Color>();
	List<ItemSize> selectedSizeList = new ArrayList<ItemSize>();
	private String transId;
	private short lineNo;
	private double qty;
	private String uom;
	private String selsPers;
	private double discPer;
	private double discAmt;
	private String taxCode;
	private double taxPer;
	private double taxAmt;
	private double netAmt;
	private String fabric;
	private String remark;
	private String attr6;
	private String sel;
	private String batch;
	private Date expDate;
	private String subSection;
	private String section;
	private String itemSize;
	private String brand;
	private double mrp;
	private double salesPrice;
	private double qtyOnHand;
	
	private String barCode;
	private double purPrice;
	private String itemCode;
	private String noteText;
	private String purchaseOrder;
	private Date receiptDate;
	private boolean zeroStcokAllowed;
	private double amount;
	private double mpPer;
	private double wspPer;
	private boolean isItemCodeInStock;
	private double grossPp;
	private double discPerc;
	private double taxPerc;
	public Article() {
		if (this.itemsList == null) {
			this.itemsList = new ArrayList();
		}
	}

	public Article(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getArticleDesc() {
		return this.articleDesc;
	}

	public void setArticleDesc(String articleDesc) {
		this.articleDesc = articleDesc;
	}

	public String getArticle_type() {
		return this.article_type;
	}

	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}

	public String getArticle_group() {
		return this.article_group;
	}

	public void setArticle_group(String article_group) {
		this.article_group = article_group;
	}

	public String getArticle_sub_group() {
		return this.article_sub_group;
	}

	public void setArticle_sub_group(String article_sub_group) {
		this.article_sub_group = article_sub_group;
	}

	public List<String> getArticle_size() {
		return article_size;
	}

	public void setArticle_size(List<String> article_size) {
		this.article_size = article_size;
	}

	public List<String> getColor() {
		return color;
	}

	public void setColor(List<String> color) {
		this.color = color;
	}

	public String getArticle_short_desc() {
		return this.article_short_desc;
	}

	public void setArticle_short_desc(String article_short_desc) {
		this.article_short_desc = article_short_desc;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getInventory_uom() {
		return this.inventory_uom;
	}

	public void setInventory_uom(String inventory_uom) {
		this.inventory_uom = inventory_uom;
	}

	public String getStyle() {
		return this.style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public double getMin_stock() {
		return this.min_stock;
	}

	public void setMin_stock(double min_stock) {
		this.min_stock = min_stock;
	}

	public double getMax_stock() {
		return this.max_stock;
	}

	public void setMax_stock(double max_stock) {
		this.max_stock = max_stock;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String getRemark1() {
		return this.remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return this.remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return this.remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public double getTax_per() {
		return this.tax_per;
	}

	public void setTax_per(double tax_per) {
		this.tax_per = tax_per;
	}

	public double getDisc_per() {
		return this.disc_per;
	}

	public void setDisc_per(double disc_per) {
		this.disc_per = disc_per;
	}

	public double getOpen_bal() {
		return this.open_bal;
	}

	public void setOpen_bal(double open_bal) {
		this.open_bal = open_bal;
	}

	public String getRack() {
		return this.rack;
	}

	public void setRack(String rack) {
		this.rack = rack;
	}

	public List<ArticleItems> getItemsList() {
		return this.itemsList;
	}

	public void setItemsList(List<ArticleItems> itemsList) {
		this.itemsList = itemsList;
	}

	public double getWsp_disc_per() {
		return this.wsp_disc_per;
	}

	public void setWsp_disc_per(double wsp_disc_per) {
		this.wsp_disc_per = wsp_disc_per;
	}

	public String getBar_code() {
		return this.bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	public String getSearch_text() {
		return this.search_text;
	}

	public void setSearch_text(String search_text) {
		this.search_text = search_text;
	}

	public double getMrp() {
		return Mrp;
	}

	public void setMrp(double mrp) {
		Mrp = mrp;
	}

	public double getWsp() {
		return wsp;
	}

	public void setWsp(double wsp) {
		this.wsp = wsp;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update article p\nset article_desc=?,\narticle_type=?,\narticle_group=?,\narticle_sub_group=?,\narticle_short_desc=?,\nmanufacturer=?,\ninventory_uom=?,\nstyle=?,\nmin_stock=?,\nmax_stock=?,status=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\n,remark1 =? ,remark2 =? ,remark3 =? ,tax_per =? ,disc_per =? ,open_bal =? ,rack =? ,wsp_disc_per =? where article_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.articleDesc);
		ps.setString(2, getArticle_type());
		ps.setString(3, getArticle_group());
		ps.setString(4, getArticle_sub_group());
		ps.setString(5, getArticle_short_desc());
		ps.setString(6, getManufacturer());
		ps.setString(7, getInventory_uom());
		ps.setString(8, getStatus());
		ps.setDouble(9, getMin_stock());
		ps.setDouble(10, getMax_stock());
		ps.setString(11, this.status);
		ps.setDate(12, Utils.getSqlDate(getCreatedOn()));
		ps.setString(13, this.createdBy);
		ps.setDate(14, Utils.getSqlDate(new Date()));
		ps.setString(15, getUserId());
		ps.setString(16, getRemark1());
		ps.setString(17, getRemark2());
		ps.setString(18, getRemark3());
		ps.setDouble(19, getTax_per());
		ps.setDouble(20, getDisc_per());
		ps.setDouble(21, getOpen_bal());
		ps.setString(22, getRack());
		ps.setDouble(23, getWsp_disc_per());
		ps.setString(24, this.articleNo);
		ps.setDouble(25, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record could not be modified.");
		}
	
		
		int i = 0;
		
		for (ArticleItems ai : getItemsList()) {
			ai.setItemDesc(getArticleDesc());
			
			
				ai.update(con);
		}
		
		
		
		
		
            
		return "Record Updated";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into article\n  (article_no,\n   article_desc,\n   article_type,\n   article_group,\n   article_sub_group,\n    article_short_desc,\n   manufacturer,\n   inventory_uom,\n   style,\n   min_stock,\n   max_stock,   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,   object_id,remark1,remark2,remark3, tax_per,disc_per, open_bal,rack,wsp_disc_per    )\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.articleNo);
		ps.setString(2, this.articleDesc);
		ps.setString(3, getArticle_type());
		ps.setString(4, getArticle_group());
		ps.setString(5, getArticle_sub_group());

		ps.setString(6, getArticle_short_desc());
		ps.setString(7, getManufacturer());
		ps.setString(8, getInventory_uom());
		ps.setString(9, getStatus());
		ps.setDouble(10, getMin_stock());
		ps.setDouble(11, getMax_stock());
		ps.setString(12, this.status);
		ps.setDate(13, Utils.getSqlDate(new Date()));
		ps.setString(14, getUserId());
		ps.setDate(15, Utils.getSqlDate(this.updatedOn));
		ps.setString(16, getUpdatedBy());
		ps.setDouble(17, 1.0D);
		ps.setString(18, getRemark1());
		ps.setString(19, getRemark2());
		ps.setString(20, getRemark3());
		ps.setDouble(21, getTax_per());
		ps.setDouble(22, getDisc_per());
		ps.setDouble(23, getOpen_bal());
		ps.setString(24, getRack());
		ps.setDouble(25, getWsp_disc_per());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could Not Be created.");
		}
		
		int i = 0;
		
		
		
		for (String clr : getColor()) {
			for (String size : getArticle_size()) {
				DecimalFormat df1 = new DecimalFormat("#000");
				ArticleItems temp = new ArticleItems();
				temp.setIsEditable(true);
				temp.setArticle_no(this.articleNo);
				String item_code = this.articleNo;
				
				if(i>0){
					item_code = this.articleNo + "-"
							+ df1.format((long) (i));
				}
				temp.setItem_code(item_code);
				temp.setStatus("Active");
				temp.setColor(clr);
				temp.setMrp(Mrp);
				temp.setWsp(wsp);
				temp.setSales_price(salesPrice);
				temp.setArticle_size(size);
				temp.setItemDesc(getArticleDesc());
				temp.update(con);
				i++;
			}
		}
		
		return "Record Created";
	}

	public List<Article> search(Connection con) throws SQLException {
		String sql = "select article_no,\n       article_desc,\n       article_type,\n       article_group,\n       article_sub_group,\n       article_size,\n       color,\n       article_short_desc,\n       manufacturer,\n       inventory_uom,\n       style,\n       min_stock,\n       max_stock,       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       remark1,       remark2,       remark3,       tax_per,       disc_per,       open_bal,       rack       ,wsp_disc_per  from article p\n where 1=1 ";

		boolean doFetchData = false;
		if ((getArticleNo() != null) && (getArticleNo().length() > 0)) {
			sql = sql + " and upper(article_no) like upper('" + getArticleNo().trim() + "')";
			doFetchData = true;
		}
		if ((getArticleDesc() != null) && (getArticleDesc().length() > 0)) {
			sql = sql + " and upper(article_desc) like upper('%" + getArticleDesc().trim() + "%')";
			doFetchData = true;
		}
		if ((getArticle_type() != null) && (getArticle_type().length() > 0)) {
			sql = sql + " and upper(article_type) like upper('" + getArticle_type().trim() + "')";
			doFetchData = true;
		}
		if ((getArticle_group() != null) && (getArticle_group().length() > 0)) {
			sql = sql + " and upper(article_group) like upper('" + getArticle_group().trim() + "')";
			doFetchData = true;
		}
		if ((getArticle_sub_group() != null) && (getArticle_sub_group().length() > 0)) {
			sql = sql + " and upper(article_sub_group) like upper('" + getArticle_sub_group().trim() + "')";
			doFetchData = true;
		}
		if ((getManufacturer() != null) && (getManufacturer().length() > 0)) {
			sql = sql + " and upper(manufacturer) like upper('" + getManufacturer().trim() + "')";
			doFetchData = true;
		}
		if ((getInventory_uom() != null) && (getInventory_uom().length() > 0)) {
			sql = sql + " and upper(inventory_uom) like upper('" + getInventory_uom().trim() + "')";
			doFetchData = true;
		}
		if ((getRemark1() != null) && (getRemark1().length() > 0)) {
			sql = sql + " and upper(remark1) like upper('" + getRemark1().trim() + "')";
			doFetchData = true;
		}
		if ((getRemark2() != null) && (getRemark2().length() > 0)) {
			sql = sql + " and upper(remark2) like upper('" + getRemark2().trim() + "')";
			doFetchData = true;
		}
		if ((getRemark3() != null) && (getRemark3().length() > 0)) {
			sql = sql + " and upper(remark3) like upper('" + getRemark3().trim() + "')";
			doFetchData = true;
		}
		if ((getRack() != null) && (getRack().length() > 0)) {
			sql = sql + " and upper(rack) like upper('" + getRack().trim() + "')";
			doFetchData = true;
		}
		if ((getBar_code() != null) && (getBar_code().length() > 0)) {
			sql = sql
					+ " and exists(SELECT 1 FROM article_items A WHERE P.ARTICLE_NO= A.ARTICLE_NO AND upper(BAR_CODE) like upper('"
					+ getBar_code().trim() + "'))";
			doFetchData = true;
		}
		if ((getSearch_text() != null) && (getSearch_text().length() > 0)) {
			sql = sql
					+ " and exists(SELECT 1 FROM article_items A WHERE P.ARTICLE_NO= A.ARTICLE_NO AND upper(nvl(BAR_CODE,'')||nvl(article_no,'')||nvl(item_code,'')||nvl(item_desc,'')) like upper('%"
					+ getSearch_text().trim() + "%'))";
			doFetchData = true;
		}
		List<Article> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Article temp = new Article();

			temp.setArticleNo(rs.getString("article_no"));
			temp.setArticleDesc(rs.getString("article_desc"));
			temp.setArticle_type(rs.getString("article_type"));
			temp.setArticle_group(rs.getString("article_group"));
			temp.setArticle_sub_group(rs.getString("article_sub_group"));

			temp.setArticle_short_desc(rs.getString("article_short_desc"));
			temp.setManufacturer(rs.getString("manufacturer"));
			temp.setInventory_uom(rs.getString("inventory_uom"));
			temp.setStyle(rs.getString("style"));
			temp.setMin_stock(rs.getDouble("min_stock"));
			temp.setMax_stock(rs.getDouble("max_stock"));

			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setRemark1(rs.getString("remark1"));
			temp.setRemark2(rs.getString("remark2"));
			temp.setRemark3(rs.getString("remark3"));
			temp.setTax_per(rs.getDouble("tax_per"));
			temp.setDisc_per(rs.getDouble("disc_per"));
			temp.setOpen_bal(rs.getDouble("open_bal"));
			temp.setRack(rs.getString("rack"));
			temp.setWsp_disc_per(rs.getDouble("wsp_disc_per"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException, Exception {
		String sql = "select article_no,\n       article_desc,\n       article_type,\n       article_group,\n       article_sub_group,\n            article_short_desc,\n       manufacturer,\n       inventory_uom,\n       style,\n       min_stock,\n       max_stock,       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       remark1,       remark2,       remark3,       tax_per,       disc_per,       open_bal,       rack,       wsp_disc_per  from article p\n where article_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.articleNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setArticleNo(rs.getString("article_no"));
			setArticleDesc(rs.getString("article_desc"));
			setArticleDesc(rs.getString("article_desc"));
			setArticle_type(rs.getString("article_type"));
			setArticle_group(rs.getString("article_group"));
			setArticle_sub_group(rs.getString("article_sub_group"));

			setArticle_short_desc(rs.getString("article_short_desc"));
			setManufacturer(rs.getString("manufacturer"));
			setInventory_uom(rs.getString("inventory_uom"));
			setStyle(rs.getString("style"));
			setMin_stock(rs.getDouble("min_stock"));
			setMax_stock(rs.getDouble("max_stock"));

			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setRemark1(rs.getString("remark1"));
			setRemark2(rs.getString("remark2"));
			setRemark3(rs.getString("remark3"));
			setTax_per(rs.getDouble("tax_per"));
			setDisc_per(rs.getDouble("disc_per"));
			setOpen_bal(rs.getDouble("open_bal"));
			setRack(rs.getString("rack"));
			setWsp_disc_per(rs.getDouble("wsp_disc_per"));
			ArticleItems temp = new ArticleItems();
			temp.setArticle_no(getArticleNo());
			setItemsList(temp.fetchItemsForArticle(con));
			Color clr = new Color();
			selectedColorList=clr.search(con, temp.getColorList()+"''");
			ItemSize size = new ItemSize();
			selectedSizeList= size.search(con, temp.getSizeList()+"''");
			
			
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from article p\nwhere article_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.articleNo);
		ps.setDouble(2, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified By Other User.");
		}
		return "Record deleted";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.articleNo != null ? this.articleNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Article)) {
			return false;
		}
		Article other = (Article) object;
		if (((this.articleNo == null) && (other.articleNo != null))
				|| ((this.articleNo != null) && (!this.articleNo.equals(other.articleNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Article No " + this.articleNo + "";
	}

	public List<Color> getColorList() {
		return colorList;
	}

	public void setColorList(List<Color> colorList) {
		this.colorList = colorList;
	}

	public List<Color> getSelectedColorList() {
		return selectedColorList;
	}

	public void setSelectedColorList(List<Color> selectedColorList) {
		this.selectedColorList = selectedColorList;
	}

	public List<ItemSize> getSelectedSizeList() {
		return selectedSizeList;
	}

	public void setSelectedSizeList(List<ItemSize> selectedSizeList) {
		this.selectedSizeList = selectedSizeList;
	}

	public String getTransId() {
		return transId;
	}

	public void setTransId(String transId) {
		this.transId = transId;
	}

	public short getLineNo() {
		return lineNo;
	}

	public void setLineNo(short lineNo) {
		this.lineNo = lineNo;
	}

	public double getQty() {
		return qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public String getUom() {
		return uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getSelsPers() {
		return selsPers;
	}

	public void setSelsPers(String selsPers) {
		this.selsPers = selsPers;
	}

	public double getDiscPer() {
		return discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public double getDiscAmt() {
		return discAmt;
	}

	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}

	public String getTaxCode() {
		return taxCode;
	}

	public void setTaxCode(String taxCode) {
		this.taxCode = taxCode;
	}

	public double getTaxPer() {
		return taxPer;
	}

	public void setTaxPer(double taxPer) {
		this.taxPer = taxPer;
	}

	public double getTaxAmt() {
		return taxAmt;
	}

	public void setTaxAmt(double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public double getNetAmt() {
		return netAmt;
	}

	public void setNetAmt(double netAmt) {
		this.netAmt = netAmt;
	}

	public String getFabric() {
		return fabric;
	}

	public void setFabric(String fabric) {
		this.fabric = fabric;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAttr6() {
		return attr6;
	}

	public void setAttr6(String attr6) {
		this.attr6 = attr6;
	}

	public String getSel() {
		return sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpDate() {
		return expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String getSubSection() {
		return subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public String getSection() {
		return section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getItemSize() {
		return itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getBrand() {
		return brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public double getSalesPrice() {
		return salesPrice;
	}

	public void setSalesPrice(double salesPrice) {
		this.salesPrice = salesPrice;
	}

	public double getQtyOnHand() {
		return qtyOnHand;
	}

	public void setQtyOnHand(double qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public String getBarCode() {
		return barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public double getPurPrice() {
		return purPrice;
	}

	public void setPurPrice(double purPrice) {
		this.purPrice = purPrice;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getNoteText() {
		return noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getPurchaseOrder() {
		return purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public Date getReceiptDate() {
		return receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public boolean isZeroStcokAllowed() {
		return zeroStcokAllowed;
	}

	public void setZeroStcokAllowed(boolean zeroStcokAllowed) {
		this.zeroStcokAllowed = zeroStcokAllowed;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getMpPer() {
		return mpPer;
	}

	public void setMpPer(double mpPer) {
		this.mpPer = mpPer;
	}

	public double getWspPer() {
		return wspPer;
	}

	public void setWspPer(double wspPer) {
		this.wspPer = wspPer;
	}

	public boolean isItemCodeInStock() {
		return isItemCodeInStock;
	}

	public void setItemCodeInStock(boolean isItemCodeInStock) {
		this.isItemCodeInStock = isItemCodeInStock;
	}

	public double getGrossPp() {
		return grossPp;
	}

	public void setGrossPp(double grossPp) {
		this.grossPp = grossPp;
	}

	public double getDiscPerc() {
		return discPerc;
	}

	public void setDiscPerc(double discPerc) {
		this.discPerc = discPerc;
	}

	public double getTaxPerc() {
		return taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	
	
}
