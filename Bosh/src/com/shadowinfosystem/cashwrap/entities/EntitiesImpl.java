/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

public class EntitiesImpl {
	private String userId;
	private String screen;
	private boolean addAddowed;
	private boolean modifyAddowed;
	private boolean deleteAddowed;
	private boolean viewAddowed;
	private boolean postAddowed;
	private boolean printAddowed;
	private boolean cancelAllowed;

	public EntitiesImpl() {
	}

	public EntitiesImpl(String screen, boolean addAddowed, boolean modifyAddowed, boolean deleteAddowed,
			boolean viewAddowed, boolean postAddowed, boolean printAddowed, boolean cancelAllowed) {
		this.screen = screen;
		this.addAddowed = addAddowed;
		this.modifyAddowed = modifyAddowed;
		this.deleteAddowed = deleteAddowed;
		this.viewAddowed = viewAddowed;
		this.postAddowed = postAddowed;
		this.printAddowed = printAddowed;
		this.cancelAllowed = cancelAllowed;
	}

	public String getScreen() {
		return this.screen;
	}

	public String getUserId() {
		return this.userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public boolean isAddAddowed() {
		return this.addAddowed;
	}

	public void setAddAddowed(boolean addAddowed) {
		this.addAddowed = addAddowed;
	}

	public boolean isModifyAddowed() {
		return this.modifyAddowed;
	}

	public void setModifyAddowed(boolean modifyAddowed) {
		this.modifyAddowed = modifyAddowed;
	}

	public boolean isDeleteAddowed() {
		return this.deleteAddowed;
	}

	public void setDeleteAddowed(boolean deleteAddowed) {
		this.deleteAddowed = deleteAddowed;
	}

	public boolean isViewAddowed() {
		return this.viewAddowed;
	}

	public void setViewAddowed(boolean viewAddowed) {
		this.viewAddowed = viewAddowed;
	}

	public boolean isPostAddowed() {
		return this.postAddowed;
	}

	public void setPostAddowed(boolean postAddowed) {
		this.postAddowed = postAddowed;
	}

	public boolean isPrintAddowed() {
		return this.printAddowed;
	}

	public void setPrintAddowed(boolean printAddowed) {
		this.printAddowed = printAddowed;
	}

	public boolean isCancelAllowed() {
		return this.cancelAllowed;
	}

	public void setCancelAllowed(boolean cancelAllowed) {
		this.cancelAllowed = cancelAllowed;
	}
}
