/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class TransactionPrefix extends EntitiesImpl implements Serializable {
	private String prefix;
	private String transType;
	private Date validFrom;
	private Date validUpTo;
	private Long currentNo;
	private String transDesc;
	private String remark;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private String isDefault;
	private String appendPrefix;
	private short objectId;

	public TransactionPrefix() {
	}

	public TransactionPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getPrefix() {
		return this.prefix;
	}

	public void setPrefix(String prefix) {
		this.prefix = prefix;
	}

	public String getTransType() {
		return this.transType;
	}

	public void setTransType(String transType) {
		this.transType = transType;
	}

	public Date getValidFrom() {
		return this.validFrom;
	}

	public void setValidFrom(Date validFrom) {
		this.validFrom = validFrom;
	}

	public Date getValidUpTo() {
		return this.validUpTo;
	}

	public void setValidUpTo(Date validUpTo) {
		this.validUpTo = validUpTo;
	}

	public Long getCurrentNo() {
		return this.currentNo;
	}

	public void setCurrentNo(Long currentNo) {
		this.currentNo = currentNo;
	}

	public String getTransDesc() {
		return this.transDesc;
	}

	public void setTransDesc(String transDesc) {
		this.transDesc = transDesc;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String getIsDefault() {
		return this.isDefault;
	}

	public void setIsDefault(String isDefault) {
		this.isDefault = isDefault;
	}

	public String getAppendPrefix() {
		return this.appendPrefix;
	}

	public void setAppendPrefix(String appendPrefix) {
		this.appendPrefix = appendPrefix;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update transaction_prefix\n   set valid_from  = ?,\n       valid_up_to = ?,\n       current_no  = ?,\n       status      = ?,\n       created_on  = ?,\n       created_by  = ?,\n       updated_on  = ?,\n       updated_by  = ?,\n       object_id   = nvl(object_id, 1) + 1,\n       trans_desc  = ?,\n       remark      = ?,\n       is_default  = ?,\n       append_prefix = ?\n where prefix = ?\n   and trans_type = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(this.validFrom));
		ps.setDate(2, Utils.getSqlDate(this.validUpTo));
		ps.setLong(3, this.currentNo.longValue());
		ps.setString(4, this.status);
		ps.setDate(5, Utils.getSqlDate(this.createdOn));
		ps.setString(6, this.createdBy);
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getUserId());
		ps.setString(9, this.transDesc);
		ps.setString(10, this.remark);
		ps.setString(11, this.isDefault);
		ps.setString(12, this.appendPrefix);
		ps.setString(13, this.prefix);
		ps.setString(14, this.transType);
		ps.setShort(15, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Transaction Prefix record could not be Updated.");
		}
		ps.close();
		return "Record  updated.";
	}

	public String create(Connection con) throws SQLException, Exception {
		String sql = "insert into transaction_prefix\n  (prefix,\n   trans_type,\n   valid_from,\n   valid_up_to,\n   current_no,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,\n   trans_desc,\n   remark, is_default, append_prefix)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.prefix);
		ps.setString(2, this.transType);
		ps.setDate(3, Utils.getSqlDate(this.validFrom));
		ps.setDate(4, Utils.getSqlDate(this.validUpTo));
		ps.setLong(5, this.currentNo.longValue());
		ps.setString(6, this.status);
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getUserId());
		ps.setDate(9, Utils.getSqlDate(this.updatedOn));
		ps.setString(10, this.updatedBy);
		ps.setShort(11, (short) 1);
		ps.setString(12, this.transDesc);
		ps.setString(13, this.remark);
		ps.setString(14, this.isDefault);
		ps.setString(15, this.appendPrefix);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new Exception("Transaction Prefix record could not be created.");
		}
		return "Record Created";
	}

	public List<TransactionPrefix> getList(Connection con) throws SQLException {
		String sql = "select prefix,\n       trans_type,\n       valid_from,\n       valid_up_to,\n       current_no,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       trans_desc,\n       remark, is_default, append_prefix\n  from transaction_prefix\n where prefix LIKE nvl(?, prefix)\n   and trans_type LIKE nvl(?, trans_type)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.prefix);
		ps.setString(2, this.transType);

		ResultSet rs = ps.executeQuery();
		List<TransactionPrefix> list = new ArrayList();
		while (rs.next()) {
			TransactionPrefix temp = new TransactionPrefix();
			temp.setPrefix(rs.getString("prefix"));
			temp.setTransType(rs.getString("trans_type"));
			temp.setValidFrom(rs.getDate("valid_from"));
			temp.setValidUpTo(rs.getDate("valid_up_to"));
			temp.setCurrentNo(Long.valueOf(rs.getLong("current_no")));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTransDesc(rs.getString("trans_desc"));
			temp.setRemark(rs.getString("remark"));
			temp.setIsDefault(rs.getString("is_default"));
			temp.setAppendPrefix(rs.getString("append_prefix"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public List<TransactionPrefix> getListForTrans(Connection con) throws SQLException {
		String sql = "select prefix,\n       trans_type,\n       valid_from,\n       valid_up_to,\n       current_no,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       trans_desc,\n       remark, is_default, append_prefix\n  from transaction_prefix\n where prefix LIKE nvl(?, prefix)\n   and trans_type LIKE nvl(?, trans_type) and trunc(sysdate) between valid_from and valid_up_to  and nvl(status,'Active')='Active' order by prefix";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.prefix);
		ps.setString(2, this.transType);

		ResultSet rs = ps.executeQuery();
		List<TransactionPrefix> list = new ArrayList();
		while (rs.next()) {
			TransactionPrefix temp = new TransactionPrefix();
			temp.setPrefix(rs.getString("prefix"));
			temp.setTransType(rs.getString("trans_type"));
			temp.setValidFrom(rs.getDate("valid_from"));
			temp.setValidUpTo(rs.getDate("valid_up_to"));
			temp.setCurrentNo(Long.valueOf(rs.getLong("current_no")));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTransDesc(rs.getString("trans_desc"));
			temp.setRemark(rs.getString("remark"));
			temp.setIsDefault(rs.getString("is_default"));
			temp.setAppendPrefix(rs.getString("append_prefix"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException, Exception {
		String sql = "select prefix,\n       trans_type,\n       valid_from,\n       valid_up_to,\n       current_no,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       trans_desc,\n       remark, is_default, append_prefix\n  from transaction_prefix\n where prefix =?\n   and trans_type =?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.prefix);
		ps.setString(2, this.transType);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setPrefix(rs.getString("prefix"));
			setTransType(rs.getString("trans_type"));
			setValidFrom(rs.getDate("valid_from"));
			setValidUpTo(rs.getDate("valid_up_to"));
			setCurrentNo(Long.valueOf(rs.getLong("current_no")));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setTransDesc(rs.getString("trans_desc"));
			setRemark(rs.getString("remark"));
			setIsDefault(rs.getString("is_default"));
			setAppendPrefix(rs.getString("append_prefix"));
		} else {
			throw new Exception("Transaction Prefix record not Found.");
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.prefix != null ? this.prefix.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof TransactionPrefix)) {
			return false;
		}
		TransactionPrefix other = (TransactionPrefix) object;
		if (((this.prefix == null) && (other.prefix != null))
				|| ((this.prefix != null) && (!this.prefix.equals(other.prefix)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "" + this.prefix + "";
	}
}
