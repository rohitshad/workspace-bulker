/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Supplier extends EntitiesImpl implements Serializable {
	private String suppId;
	private String suppName;
	private String address;
	private String tin;
	private String pan;
	private String mail;
	private String phone;
	private String mob;
	private double discPer;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double opBalDr;
	private double opBalCr;
	private String noteText;

	public Supplier() {
	}

	public Supplier(String suppId) {
		this.suppId = suppId;
	}

	public String getSuppId() {
		return this.suppId;
	}

	public void setSuppId(String suppId) {
		this.suppId = suppId;
	}

	public String getSuppName() {
		return this.suppName;
	}

	public void setSuppName(String suppName) {
		this.suppName = suppName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTin() {
		return this.tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getPan() {
		return this.pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMob() {
		return this.mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public double getDiscPer() {
		return this.discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getOpBalDr() {
		return this.opBalDr;
	}

	public void setOpBalDr(double opBalDr) {
		this.opBalDr = opBalDr;
	}

	public double getOpBalCr() {
		return this.opBalCr;
	}

	public void setOpBalCr(double opBalCr) {
		this.opBalCr = opBalCr;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update supplier p\nset supp_name=?,address=?,tin=?,pan=?,mail=?,phone=?,mob=?,disc_per=?,op_bal_dr=?,op_bal_cr=?,note_text=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere supp_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.suppName);
		ps.setString(2, this.address);
		ps.setString(3, this.tin);
		ps.setString(4, this.pan);
		ps.setString(5, this.mail);
		ps.setString(6, this.phone);
		ps.setString(7, this.mob);
		ps.setDouble(8, this.discPer);

		ps.setDouble(9, this.opBalDr);
		ps.setDouble(10, this.opBalCr);
		ps.setString(11, this.noteText);

		ps.setString(12, this.status);
		ps.setDate(13, Utils.getSqlDate(this.createdOn));
		ps.setString(14, this.createdBy);
		ps.setDate(15, Utils.getSqlDate(new Date()));
		ps.setString(16, getUserId());
		ps.setString(17, this.suppId);
		ps.setShort(18, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into supplier\n  (supp_id,supp_name,address,tin,pan,mail,phone,mob,disc_per,op_bal_dr,op_bal_cr,note_text,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.suppId);
		ps.setString(2, this.suppName);
		ps.setString(3, this.address);
		ps.setString(4, this.tin);
		ps.setString(5, this.pan);
		ps.setString(6, this.mail);
		ps.setString(7, this.phone);
		ps.setString(8, this.mob);
		ps.setDouble(9, this.discPer);
		ps.setDouble(10, this.opBalDr);
		ps.setDouble(11, this.opBalCr);
		ps.setString(12, this.noteText);
		ps.setString(13, this.status);
		ps.setDate(14, Utils.getSqlDate(new Date()));
		ps.setString(15, getUserId());
		ps.setDate(16, Utils.getSqlDate(this.updatedOn));
		ps.setString(17, this.updatedBy);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<Supplier> getList(Connection con) throws SQLException {
		String sql = "select supp_id,supp_name,address,tin,pan,mail,phone,mob,disc_per,op_bal_cr, op_bal_dr,note_text,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from supplier p\n where 1 = 1 ";

		boolean doFetchData = false;
		if ((this.suppId != null) && (this.suppId.length() > 0)) {
			sql = sql + " and supp_id like '" + getSuppId() + "' ";
			doFetchData = true;
		}
		if ((this.suppName != null) && (this.suppName.length() > 0)) {
			sql = sql + " and upper(supp_name) like upper('" + getSuppName() + "') ";
			doFetchData = true;
		}
		if ((getAddress() != null) && (getAddress().length() > 0)) {
			sql = sql + " and upper(address) like upper('" + getAddress() + "') ";
			doFetchData = true;
		}
		if ((getTin() != null) && (getTin().length() > 0)) {
			sql = sql + " and upper(tin) like upper('" + getTin() + "') ";
			doFetchData = true;
		}
		if ((getPan() != null) && (getPan().length() > 0)) {
			sql = sql + " and upper(pan) like upper('" + getPan() + "') ";
			doFetchData = true;
		}
		if ((getPhone() != null) && (getPhone().length() > 0)) {
			sql = sql + " and upper(phone) like upper('" + getPhone() + "') ";
			doFetchData = true;
		}
		if ((getMob() != null) && (getMob().length() > 0)) {
			sql = sql + " and upper(mob) like upper('" + getMob() + "') ";
			doFetchData = true;
		}
		if ((getMail() != null) && (getMail().length() > 0)) {
			sql = sql + " and upper(mail) like upper('" + getMail() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().length() > 0)) {
			sql = sql + " and upper(nvl(status,'InActive')) like upper('" + getStatus() + "') ";
			doFetchData = true;
		}
		List<Supplier> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Supplier temp = new Supplier();
			temp.setSuppId(rs.getString("supp_id"));
			temp.setSuppName(rs.getString("supp_name"));
			temp.setAddress(rs.getString("address"));
			temp.setTin(rs.getString("tin"));
			temp.setPan(rs.getString("pan"));
			temp.setMail(rs.getString("mail"));
			temp.setPhone(rs.getString("phone"));
			temp.setMob(rs.getString("mob"));
			temp.setDiscPer(rs.getDouble("disc_Per"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setOpBalCr(rs.getDouble("op_Bal_Cr"));
			temp.setOpBalDr(rs.getDouble("op_Bal_dr"));
			temp.setNoteText(rs.getString("note_Text"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select supp_id,supp_name,address,tin,pan,mail,phone,mob,disc_per,op_bal_cr, op_bal_dr,note_text,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from supplier p\n where supp_id =? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.suppId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setSuppId(rs.getString("supp_id"));
			setSuppName(rs.getString("supp_name"));
			setAddress(rs.getString("address"));
			setTin(rs.getString("tin"));
			setPan(rs.getString("pan"));
			setMail(rs.getString("mail"));
			setPhone(rs.getString("phone"));
			setMob(rs.getString("mob"));
			setDiscPer(rs.getDouble("disc_Per"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setOpBalCr(rs.getDouble("op_Bal_Cr"));
			setOpBalDr(rs.getDouble("op_Bal_dr"));
			setNoteText(rs.getString("note_Text"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from supplier p\nwhere supp_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.suppId);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.suppId != null ? this.suppId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Supplier)) {
			return false;
		}
		Supplier other = (Supplier) object;
		if (((this.suppId == null) && (other.suppId != null))
				|| ((this.suppId != null) && (!this.suppId.equals(other.suppId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Supp Id=" + this.suppId + "";
	}
}
