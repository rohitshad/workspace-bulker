/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class InvoiceRet extends EntitiesImpl implements Serializable {
	private String invRetNo;
	private Date invRetDate;
	private String invoiceNo;
	private Date invoiceDate;
	private String invoicePrefix;
	private String customerId;
	private String customerName;
	private String custAddress;
	private double totQty;
	private double totMrp;
	private double totSalsePrice;
	private double totDiscValue;
	private double totalTax;
	private double subTotal;
	private double discPer;
	private double discAmt;
	private double otherCharges;
	private double roundOff;
	private double netValue;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double taxPerOnInv;
	private double taxAmtOnInv;
	private boolean selAll;
	private List<InvoiceRetLines> invoiceRetLines;
	private InvoiceRetLines invoiceRetLineSelected;
	private List<InvoiceRetPayDet> invRetPayLines;
	private Date fromDate;
	private Date toDate;

	public InvoiceRet() {
		this.invoiceRetLineSelected = new InvoiceRetLines();
		if (this.invoiceRetLines == null) {
			this.invoiceRetLines = new ArrayList();
		}
		if (this.invRetPayLines == null) {
			this.invRetPayLines = new ArrayList();
			InvoiceRetPayDet pd = new InvoiceRetPayDet();
			pd.setPayModeId("CASH");
			this.invRetPayLines.add(pd);
		}
	}

	public String getInvRetNo() {
		return this.invRetNo;
	}

	public void setInvRetNo(String invRetNo) {
		this.invRetNo = invRetNo;
	}

	public Date getInvRetDate() {
		return this.invRetDate;
	}

	public void setInvRetDate(Date invRetDate) {
		this.invRetDate = invRetDate;
	}

	public InvoiceRet(String invoiceNo) {
		this.invoiceNo = invoiceNo;
		this.invoiceRetLineSelected = new InvoiceRetLines();
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public InvoiceRetLines getInvoiceRetLineSelected() {
		return this.invoiceRetLineSelected;
	}

	public void setInvoiceRetLineSelected(InvoiceRetLines invoiceRetLineSelected) {
		this.invoiceRetLineSelected = invoiceRetLineSelected;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoicePrefix() {
		return this.invoicePrefix;
	}

	public void setInvoicePrefix(String invoicePrefix) {
		this.invoicePrefix = invoicePrefix;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustAddress() {
		return this.custAddress;
	}

	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}

	public double getTotQty() {
		return this.totQty;
	}

	public void setTotQty(double totQty) {
		this.totQty = totQty;
	}

	public double getTotMrp() {
		return this.totMrp;
	}

	public void setTotMrp(double totMrp) {
		this.totMrp = totMrp;
	}

	public double getTotDiscValue() {
		return this.totDiscValue;
	}

	public void setTotDiscValue(double totDiscValue) {
		this.totDiscValue = totDiscValue;
	}

	public double getTotalTax() {
		return this.totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

	public double getSubTotal() {
		return this.subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getDiscPer() {
		return this.discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public double getDiscAmt() {
		return this.discAmt;
	}

	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}

	public double getOtherCharges() {
		return this.otherCharges;
	}

	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public double getRoundOff() {
		return this.roundOff;
	}

	public void setRoundOff(double roundOff) {
		this.roundOff = roundOff;
	}

	public double getNetValue() {
		return this.netValue;
	}

	public void setNetValue(double netValue) {
		this.netValue = netValue;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public double getTotSalsePrice() {
		return this.totSalsePrice;
	}

	public void setTotSalsePrice(double totSalsePrice) {
		this.totSalsePrice = totSalsePrice;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public List<InvoiceRetLines> getInvoiceRetLines() {
		return this.invoiceRetLines;
	}

	public void setInvoiceRetLines(List<InvoiceRetLines> invoiceRetLines) {
		this.invoiceRetLines = invoiceRetLines;
	}

	public List<InvoiceRetPayDet> getInvRetPayLines() {
		return this.invRetPayLines;
	}

	public void setInvRetPayLines(List<InvoiceRetPayDet> invRetPayLines) {
		this.invRetPayLines = invRetPayLines;
	}

	public double getTaxPerOnInv() {
		return this.taxPerOnInv;
	}

	public void setTaxPerOnInv(double taxPerOnInv) {
		this.taxPerOnInv = taxPerOnInv;
	}

	public double getTaxAmtOnInv() {
		return this.taxAmtOnInv;
	}

	public void setTaxAmtOnInv(double taxAmtOnInv) {
		this.taxAmtOnInv = taxAmtOnInv;
	}

	public boolean isSelAll() {
		return this.selAll;
	}

	public void setSelAll(boolean selAll) {
		this.selAll = selAll;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update invoice_ret\n   set invoice_date   = ?,\n       invoice_prefix = ?,\n       customer_id    = ?,\n       customer_name  = ?,\n       cust_address   = ?,\n       tot_qty        = ?,\n       tot_mrp        = ?,\n       tot_disc_value = ?,\n       total_tax      = ?,\n       sub_total      = ?,\n       disc_per       = ?,\n       disc_amt       = ?,\n       other_charges  = ?,\n       round_off      = ?,\n       net_value      = ?,\n       status         = ?,\n       created_on     = ?,\n       created_by     = ?,\n       updated_on     = ?,\n       updated_by     = ?,\n       object_id      = object_id + 1,TOT_SALE_PRICE=?,invoice_no=?,inv_ret_date=?,tax_per_on_inv=?, tax_amt_on_inv=?\n where inv_ret_no = ? \n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(this.invoiceDate));
		ps.setString(2, this.invoicePrefix);
		ps.setString(3, this.customerId);
		ps.setString(4, this.customerName);
		ps.setString(5, this.custAddress);
		ps.setDouble(6, this.totQty);
		ps.setDouble(7, this.totMrp);
		ps.setDouble(8, this.totDiscValue);
		ps.setDouble(9, this.totalTax);
		ps.setDouble(10, this.subTotal);
		ps.setDouble(11, this.discPer);
		ps.setDouble(12, this.discAmt);
		ps.setDouble(13, this.otherCharges);
		ps.setDouble(14, this.roundOff);
		ps.setDouble(15, this.netValue);
		ps.setString(16, this.status);
		ps.setDate(17, Utils.getSqlDate(this.createdOn));
		ps.setString(18, this.createdBy);
		ps.setDate(19, Utils.getSqlDate(new Date()));
		ps.setString(20, getUserId());
		ps.setDouble(21, this.totSalsePrice);
		ps.setString(22, this.invoiceNo);
		ps.setDate(23, Utils.getSqlDate(this.invRetDate));
		ps.setDouble(24, getTaxPerOnInv());
		ps.setDouble(25, getTaxAmtOnInv());
		ps.setString(26, this.invRetNo);
		ps.setShort(27, this.objectId);
		int n = ps.executeUpdate();
		for (InvoiceRetLines il : this.invoiceRetLines) {
			il.setInvRetNo(this.invRetNo);
			if (il.isTempSel()) {
				if (il.getObjectId() > 0) {
					InvoiceRetLines temp = il;
					il.delete(con);
					this.invoiceRetLines.remove(temp);
				} else {
					this.invoiceRetLines.remove(il);
				}
			} else {
				il.update(con);
			}
		}
		for (InvoiceRetPayDet pd : this.invRetPayLines) {
			if (pd.getInvRetNo() == null) {
				pd.setInvRetNo(this.invRetNo);
			}
			pd.update(con);
		}
		ps.close();
		return "success";
	}

	public String create(Connection con) throws SQLException, Exception {
		if (getInvRetDate() == null) {
			setInvRetDate(new Date());
		}
		String sql = "insert into invoice_ret\n  (inv_ret_no, inv_ret_date,INVOICE_NO,\n   INVOICE_DATE,\n   INVOICE_PREFIX,\n   CUSTOMER_ID,\n   CUSTOMER_NAME,\n   CUST_ADDRESS,\n   TOT_QTY,\n   TOT_MRP,\n   TOT_DISC_VALUE,\n   TOTAL_TAX,\n   SUB_TOTAL,\n   DISC_PER,\n   DISC_AMT,\n   OTHER_CHARGES,\n   ROUND_OFF,\n   NET_VALUE,\n   STATUS,\n   CREATED_ON,\n   CREATED_BY,\n   UPDATED_ON,\n   UPDATED_BY,TOT_SALE_PRICE,tax_per_on_inv, tax_amt_on_inv)\nvalues\n  (?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ? ,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invRetNo);
		ps.setDate(2, Utils.getSqlDate(this.invRetDate));
		ps.setString(3, this.invoiceNo);
		ps.setDate(4, Utils.getSqlDate(this.invoiceDate));
		ps.setString(5, this.invoicePrefix);
		ps.setString(6, this.customerId);
		ps.setString(7, this.customerName);
		ps.setString(8, this.custAddress);
		ps.setDouble(9, this.totQty);
		ps.setDouble(10, this.totMrp);
		ps.setDouble(11, this.totDiscValue);
		ps.setDouble(12, this.totalTax);
		ps.setDouble(13, this.subTotal);
		ps.setDouble(14, this.discPer);
		ps.setDouble(15, this.discAmt);
		ps.setDouble(16, this.otherCharges);
		ps.setDouble(17, this.roundOff);
		ps.setDouble(18, this.netValue);
		ps.setString(19, this.status);
		ps.setDate(20, Utils.getSqlDate(new Date()));
		ps.setString(21, getUserId());
		ps.setDate(22, Utils.getSqlDate(this.updatedOn));
		ps.setString(23, this.updatedBy);
		ps.setDouble(24, this.totSalsePrice);
		ps.setDouble(25, getTaxPerOnInv());
		ps.setDouble(26, getTaxAmtOnInv());

		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Could Not Update Header");
		}
		for (InvoiceRetLines il : this.invoiceRetLines) {
			il.setInvRetNo(this.invRetNo);
			if (il.isTempSel()) {
				if (il.getObjectId() > 0) {
					InvoiceRetLines temp = il;
					il.delete(con);
					this.invoiceRetLines.remove(temp);
				} else {
					this.invoiceRetLines.remove(il);
				}
			} else {
				il.update(con);
			}
		}
		for (InvoiceRetPayDet pd : this.invRetPayLines) {
			pd.setInvRetNo(this.invRetNo);
			pd.update(con);
		}
		ps.close();
		return "success";
	}

	public List<InvoiceRet> getList(Connection con) throws SQLException {
		String sql = "select inv_ret_no, inv_ret_date,invoice_no,\n       invoice_date,\n       invoice_prefix,\n       customer_id,\n       customer_name,\n       cust_address,\n       tot_qty,\n       tot_mrp,\n       tot_disc_value,\n       total_tax,\n       sub_total,\n       disc_per,\n       disc_amt,\n       other_charges,\n       round_off,\n       net_value,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,TOT_SALE_PRICE,tax_per_on_inv, tax_amt_on_inv\n  from invoice_ret\n where 1=1 ";

		List<InvoiceRet> list = new ArrayList();
		boolean doFetchData = false;
		if ((getInvRetNo() != null) && (getInvRetNo().trim().length() > 0)) {
			sql = sql + " and upper(inv_ret_no) like upper('" + getInvRetNo().trim() + "') ";
			doFetchData = true;
		}
		if ((getInvoiceNo() != null) && (getInvoiceNo().trim().length() > 0)) {
			sql = sql + " and upper(invoice_no) like upper('" + getInvoiceNo().trim() + "') ";
			doFetchData = true;
		}
		if ((getCustomerId() != null) && (getCustomerId().trim().length() > 0)) {
			sql = sql + " and upper(customer_id) like upper('" + getCustomerId().trim() + "') ";
			doFetchData = true;
		}
		if ((getCustomerName() != null) && (getCustomerName().trim().length() > 0)) {
			sql = sql + " and upper(customer_name) like upper('" + getCustomerName().trim() + "') ";
			doFetchData = true;
		}
		if ((getCustAddress() != null) && (getCustAddress().trim().length() > 0)) {
			sql = sql + " and upper(cust_address) like upper('" + getCustAddress().trim() + "') ";
			doFetchData = true;
		}
		if (getFromDate() != null) {
			sql = sql + " and inv_ret_date >= to_date('" + Utils.getStrDate(this.fromDate) + "','dd/mm/yyyy') ";
			doFetchData = true;
		}
		if (getToDate() != null) {
			sql = sql + " and inv_ret_date <= to_date('" + Utils.getStrDate(this.toDate) + "','dd/mm/yyyy') ";
			doFetchData = true;
		}
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			InvoiceRet temp = new InvoiceRet();
			temp.setInvRetNo(rs.getString("inv_ret_no"));
			temp.setInvRetDate(rs.getDate("inv_ret_date"));
			temp.setInvoiceNo(rs.getString("invoice_No"));
			temp.setInvoiceDate(rs.getDate("invoice_Date"));
			temp.setInvoicePrefix(rs.getString("Invoice_Prefix"));
			temp.setCustomerId(rs.getString("customer_Id"));
			temp.setCustomerName(rs.getString("Customer_Name"));
			temp.setCustAddress(rs.getString("Cust_Address"));
			temp.setTotQty(rs.getDouble("tot_qty"));
			temp.setTotMrp(rs.getDouble("tot_mrp"));
			temp.setTotDiscValue(rs.getDouble("tot_Disc_Value"));
			temp.setTotalTax(rs.getDouble("total_tax"));
			temp.setSubTotal(rs.getDouble("sub_total"));
			temp.setDiscPer(rs.getDouble("disc_per"));
			temp.setDiscAmt(rs.getDouble("disc_Amt"));
			temp.setOtherCharges(rs.getDouble("other_Charges"));
			temp.setRoundOff(rs.getDouble("round_Off"));
			temp.setNetValue(rs.getDouble("net_Value"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTotSalsePrice(rs.getDouble("TOT_SALE_PRICE"));
			temp.setTaxPerOnInv(rs.getDouble("tax_per_on_inv"));
			temp.setTaxAmtOnInv(rs.getDouble("tax_amt_on_inv"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select inv_ret_no, inv_ret_date,invoice_no,\n       invoice_date,\n       invoice_prefix,\n       customer_id,\n       customer_name,\n       cust_address,\n       tot_qty,\n       tot_mrp,\n       tot_disc_value,\n       total_tax,\n       sub_total,\n       disc_per,\n       disc_amt,\n       other_charges,\n       round_off,\n       net_value,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,TOT_SALE_PRICE,tax_per_on_inv, tax_amt_on_inv\n  from invoice_ret \n where inv_ret_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invRetNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setInvRetNo(rs.getString("inv_ret_no"));
			setInvRetDate(rs.getDate("inv_ret_date"));
			setInvoiceNo(rs.getString("invoice_No"));
			setInvoiceDate(rs.getDate("invoice_Date"));
			setInvoicePrefix(rs.getString("Invoice_Prefix"));
			setCustomerId(rs.getString("customer_Id"));
			setCustomerName(rs.getString("Customer_Name"));
			setCustAddress(rs.getString("Cust_Address"));
			setTotQty(rs.getDouble("tot_qty"));
			setTotMrp(rs.getDouble("tot_mrp"));
			setTotDiscValue(rs.getDouble("tot_Disc_Value"));
			setTotalTax(rs.getDouble("total_tax"));
			setSubTotal(rs.getDouble("sub_total"));
			setDiscPer(rs.getDouble("disc_per"));
			setDiscAmt(rs.getDouble("disc_Amt"));
			setOtherCharges(rs.getDouble("other_Charges"));
			setRoundOff(rs.getDouble("round_Off"));
			setNetValue(rs.getDouble("net_Value"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setTotSalsePrice(rs.getDouble("TOT_SALE_PRICE"));
			setTaxPerOnInv(rs.getDouble("tax_per_on_inv"));
			setTaxAmtOnInv(rs.getDouble("tax_amt_on_inv"));
		}
		rs.close();
		ps.close();
		setInvoiceRetLines(InvoiceRetLines.load(con, this.invRetNo));
		setInvRetPayLines(InvoiceRetPayDet.load(con, this.invRetNo));
		return "success";
	}

	public String process(String isTaxInclusive) {
		processDetails(isTaxInclusive);
		processHeader(isTaxInclusive);
		processPayDetails();
		return "success";
	}

	public String processHeader(String isTaxInclusive) {
		setDiscAmt(getSubTotal() * getDiscPer() / 100.0D);
		double net_value = getSubTotal() - getDiscAmt();
		if ((isTaxInclusive != null) && (isTaxInclusive.equals("No"))) {
			setTaxAmtOnInv(net_value * getTaxPerOnInv() / 100.0D);
			net_value = net_value + getTaxAmtOnInv() + getOtherCharges();
		} else {
			double inclusiveTaxFactor = getTaxPerOnInv() / 100.0D / (1.0D + getTaxPerOnInv() / 100.0D);
			setTaxAmtOnInv((getTotSalsePrice() - getDiscAmt()) * inclusiveTaxFactor);

			net_value += getOtherCharges();
		}
		setRoundOff(net_value - (int) net_value);
		setNetValue((int) net_value);

		return "success";
	}

	public String processDetails(String isTaxInclusive) {
		double totAmt = 0.0D;
		double totQty = 0.0D;
		double totWsp = 0.0D;
		double totDisc = 0.0D;
		double totTax = 0.0D;
		double totMrp = 0.0D;
		for (InvoiceRetLines l : this.invoiceRetLines) {
			l.process(isTaxInclusive);
			totWsp += l.getSalesPrice() * l.getQty();
			totDisc += l.getDiscAmt();
			totTax += l.getTaxAmt();
			totAmt += l.getNetAmt();
			totQty += l.getQty();
			totMrp += l.getMrp() * l.getQty();
		}
		setTotQty(totQty);
		setSubTotal(totAmt);
		setNetValue((int) totAmt);
		setRoundOff(totAmt - getNetValue());
		setTotDiscValue(totDisc);
		setTotalTax(totTax);
		setTotMrp(totMrp);
		setTotSalsePrice(totWsp);
		return "success";
	}

	public String processPayDetails() {
		double payValue = getNetValue();
		InvoiceRetPayDet pdl = new InvoiceRetPayDet();
		for (InvoiceRetPayDet pd : getInvRetPayLines()) {
			payValue -= pd.getPayAmt();
			pdl = pd;
		}
		pdl.setPayAmt(payValue + pdl.getPayAmt());
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.invRetNo != null ? this.invRetNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof InvoiceRet)) {
			return false;
		}
		InvoiceRet other = (InvoiceRet) object;
		if (((this.invRetNo == null) && (other.invRetNo != null))
				|| ((this.invRetNo != null) && (!this.invRetNo.equals(other.invRetNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "invoiceNo=" + this.invoiceNo + "";
	}
}
