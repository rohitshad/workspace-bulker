/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;

public class Customer extends EntitiesImpl implements Serializable {
	private String custId;
	private String custName;
	private String address;
	private String tin;
	private String pan;
	private String mail;
	private String phone;
	private String mob;
	private String make;
	private String model;
	private String reg;
	private String next;
	private String present;
	private String tech;
	private double discPer;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private Date dob;
	private Date marrAnniv;
	private double opBalDr;
	private double opBalCr;
	private String noteText;
	private String wsDiscAllowed;
	

	public double getDiscPer() {
		return this.discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public Customer() {
	}

	public Customer(String custId) {
		this.custId = custId;
	}

	public String getCustId() {
		return this.custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getCustName() {
		return this.custName;
	}

	public void setCustName(String custName) {
		this.custName = custName;
	}

	public String getAddress() {
		return this.address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getTin() {
		return this.tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getPan() {
		return this.pan;
	}

	public void setPan(String pan) {
		this.pan = pan;
	}

	public String getMail() {
		return this.mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getMob() {
		return this.mob;
	}

	public void setMob(String mob) {
		this.mob = mob;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public Date getDob() {
		return this.dob;
	}

	public void setDob(Date dob) {
		this.dob = dob;
	}

	public Date getMarrAnniv() {
		return this.marrAnniv;
	}

	public void setMarrAnniv(Date marrAnniv) {
		this.marrAnniv = marrAnniv;
	}

	public double getOpBalDr() {
		return this.opBalDr;
	}

	public void setOpBalDr(double opBalDr) {
		this.opBalDr = opBalDr;
	}

	public double getOpBalCr() {
		return this.opBalCr;
	}

	public void setOpBalCr(double opBalCr) {
		this.opBalCr = opBalCr;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getWsDiscAllowed() {
		return this.wsDiscAllowed;
	}
 
	public void setWsDiscAllowed(String wsDiscAllowed) {
		this.wsDiscAllowed = wsDiscAllowed;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		setUpdatedOn(new Date());
		String sql = "update customer p\nset cust_name=?,address=?,tin=?,pan=?,mail=?,phone=?,mob=?,disc_per=?,\n dob=?,marr_anniv=?,op_bal_dr=?,op_bal_cr=?,note_text=?,status=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?, ws_disc_allowed=? ,make=?,reg=?,next=?,tech=?,present=?,model=? where cust_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.custName);
		ps.setString(2, this.address);
		ps.setString(3, this.tin);
		ps.setString(4, this.pan);
		ps.setString(5, this.mail);
		ps.setString(6, this.phone);
		ps.setString(7, this.mob);
		ps.setDouble(8, this.discPer);
		ps.setDate(9, Utils.getSqlDate(this.dob));
		ps.setDate(10, Utils.getSqlDate(this.marrAnniv));
		ps.setDouble(11, this.opBalDr);
		ps.setDouble(12, this.opBalCr);
		ps.setString(13, this.noteText);
		ps.setString(14, this.status);
		ps.setDate(15, Utils.getSqlDate(this.createdOn));
		ps.setString(16, this.createdBy);
		ps.setDate(17, Utils.getSqlDate(new Date()));
		ps.setString(18, this.updatedBy);
		ps.setString(19, getWsDiscAllowed());
		ps.setString(20, this.make);
		ps.setString(21, this.reg);
		ps.setString(22, this.next);
		ps.setString(23, this.tech);
		ps.setString(24, this.present);
		ps.setString(25, this.model);
		ps.setString(26, this.custId);
		ps.setShort(27, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		setCreatedOn(new Date());
		String sql = "insert into customer\n  (cust_id,cust_name,address,tin,pan,mail,phone,mob,disc_per, dob,marr_anniv,op_bal_dr,op_bal_cr,note_text,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,ws_disc_allowed,present,reg,next,tech,make,model)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.custId);
		ps.setString(2, this.custName);
		ps.setString(3, this.address);
		ps.setString(4, this.tin);
		ps.setString(5, this.pan);
		ps.setString(6, this.mail);
		ps.setString(7, this.phone);
		ps.setString(8, this.mob);
		ps.setDouble(9, this.discPer);
		ps.setDate(10, Utils.getSqlDate(this.dob));
		ps.setDate(11, Utils.getSqlDate(this.marrAnniv));
		ps.setDouble(12, this.opBalDr);
		ps.setDouble(13, this.opBalCr);
		ps.setString(14, this.noteText);
		ps.setString(15, this.status);
		ps.setDate(16, Utils.getSqlDate(new Date()));
		ps.setString(17, getUserId());
		ps.setDate(18, Utils.getSqlDate(this.updatedOn));
		ps.setString(19, this.updatedBy);
		ps.setString(20, getWsDiscAllowed());
		ps.setString(21, this.present);
		ps.setString(22, this.reg);
		ps.setString(23, this.next);
		ps.setString(24, this.tech);
		ps.setString(25, this.make);
		ps.setString(26, this.model);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<Customer> getList(Connection con) throws SQLException {
		String sql = "select cust_id,cust_name,address,tin,pan,mail,phone,mob,disc_per,marr_anniv,dob,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,op_bal_dr,op_bal_cr,ws_disc_allowed\n,present,reg,next,tech,model,make  from customer p\n where 1=1 ";

		boolean doFetchData = false;
		if ((this.custId != null) && (this.custId.length() > 0)) {
			sql = sql + " and cust_id like '" + getCustId() + "' ";
			doFetchData = true;
		}
		if ((this.custName != null) && (this.custName.length() > 0)) {
			sql = sql + " and upper(cust_name) like upper('" + getCustName() + "') ";
			doFetchData = true;
		}
		if ((getAddress() != null) && (getAddress().length() > 0)) {
			sql = sql + " and upper(address) like upper('" + getAddress() + "') ";
			doFetchData = true;
		}
		if ((getTin() != null) && (getTin().length() > 0)) {
			sql = sql + " and upper(tin) like upper('" + getTin() + "') ";
			doFetchData = true;
		}
		if ((getPan() != null) && (getPan().length() > 0)) {
			sql = sql + " and upper(pan) like upper('" + getPan() + "') ";
			doFetchData = true;
		}
		if ((getPhone() != null) && (getPhone().length() > 0)) {
			sql = sql + " and upper(phone) like upper('" + getPhone() + "') ";
			doFetchData = true;
		}
		if ((getMob() != null) && (getMob().length() > 0)) {
			sql = sql + " and upper(mob) like upper('" + getMob() + "') ";
			doFetchData = true;
		}
		if ((getMail() != null) && (getMail().length() > 0)) {
			sql = sql + " and upper(mail) like upper('" + getMail() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().length() > 0)) {
			sql = sql + " and upper(nvl(status,'InActive')) like upper('" + getStatus() + "') ";
			doFetchData = true;
		}
		if ((getWsDiscAllowed() != null) && (getWsDiscAllowed().length() > 0)) {
			sql = sql + " and upper(nvl(ws_disc_allowed,'N')) like upper('" + getWsDiscAllowed() + "') ";
			doFetchData = true;
		}
		List<Customer> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Customer temp = new Customer();
			temp.setCustId(rs.getString("cust_id"));
			temp.setCustName(rs.getString("cust_name"));
			temp.setAddress(rs.getString("address"));
			temp.setTin(rs.getString("tin"));
			temp.setPan(rs.getString("pan"));
			temp.setMail(rs.getString("mail"));
			temp.setPhone(rs.getString("phone"));
			temp.setMob(rs.getString("mob"));
			temp.setMake(rs.getString("make"));
			temp.setModel(rs.getString("model"));
			temp.setPresent(rs.getString("present"));
			temp.setNext(rs.getString("next"));
			temp.setReg(rs.getString("reg"));
			temp.setTech(rs.getString("tech"));
			temp.setDob(rs.getDate("dob"));
			temp.setMarrAnniv(rs.getDate("marr_anniv"));
			temp.setDiscPer(rs.getDouble("disc_Per"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setOpBalCr(rs.getDouble("op_bal_cr"));
			temp.setOpBalDr(rs.getDouble("op_bal_dr"));
			temp.setWsDiscAllowed(rs.getString("ws_disc_allowed"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select cust_id,cust_name,address,tin,pan,mail,phone,present,reg,next,tech,mob,make,model,disc_per,marr_anniv,dob,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,op_bal_dr,op_bal_cr,ws_disc_allowed\n  from customer p\n where cust_id =? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.custId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setCustId(rs.getString("cust_id"));
			setCustName(rs.getString("cust_name"));
			setAddress(rs.getString("address"));
			setTin(rs.getString("tin"));
			setPan(rs.getString("pan"));
			setMail(rs.getString("mail"));
			setPhone(rs.getString("phone"));
			setMob(rs.getString("mob"));
			setMake(rs.getString("make"));
			setModel(rs.getString("model"));
			setPresent(rs.getString("present"));
			setNext(rs.getString("next"));
			setReg(rs.getString("reg"));
			setTech(rs.getString("tech"));
			setDob(rs.getDate("dob"));
			setMarrAnniv(rs.getDate("marr_anniv"));
			setDiscPer(rs.getDouble("disc_Per"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setOpBalCr(rs.getDouble("op_bal_cr"));
			setOpBalDr(rs.getDouble("op_bal_dr"));
			setWsDiscAllowed(rs.getString("ws_disc_allowed"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete customer p\nwhere cust_id=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getCustId());
		ps.setShort(2, getObjectId());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified by other user.");
		}
		return "Record Deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.custId != null ? this.custId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Customer)) {
			return false;
		}
		Customer other = (Customer) object;
		return ((this.custId != null) || (other.custId == null))
				&& ((this.custId == null) || (this.custId.equals(other.custId)));
	}

	public String toString() {
		return "Cust Id=" + this.custId + "";
	}

	public String getMake() {
		return make;
	}

	public void setMake(String make) {
		this.make = make;
	}

	public String getModel() {
		return model;
	}

	public void setModel(String model) {
		this.model = model;
	}

	public String getReg() {
		return reg;
	}

	public void setReg(String reg) {
		this.reg = reg;
	}

	public String getNext() {
		return next;
	}

	public void setNext(String next) {
		this.next = next;
	}

	public String getPresent() {
		return present;
	}

	public void setPresent(String present) {
		this.present = present;
	}

	public String getTech() {
		return tech;
	}

	public void setTech(String tech) {
		this.tech = tech;
	}
}
