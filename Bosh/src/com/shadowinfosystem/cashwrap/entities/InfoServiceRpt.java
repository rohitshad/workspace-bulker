/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.util.Date;

public class InfoServiceRpt implements Serializable {
	private String reportId;
	private String reportGroup;
	private String reportDesc;
	private String jasperFile;
	private String procName;
	private Date createdOn;
	private String creaqtedBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;

	public InfoServiceRpt() {
	}

	public InfoServiceRpt(String reportId) {
		this.reportId = reportId;
	}

	public String getReportId() {
		return this.reportId;
	}

	public void setReportId(String reportId) {
		this.reportId = reportId;
	}

	public String getReportGroup() {
		return this.reportGroup;
	}

	public void setReportGroup(String reportGroup) {
		this.reportGroup = reportGroup;
	}

	public String getReportDesc() {
		return this.reportDesc;
	}

	public void setReportDesc(String reportDesc) {
		this.reportDesc = reportDesc;
	}

	public String getJasperFile() {
		return this.jasperFile;
	}

	public void setJasperFile(String jasperFile) {
		this.jasperFile = jasperFile;
	}

	public String getProcName() {
		return this.procName;
	}

	public void setProcName(String procName) {
		this.procName = procName;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreaqtedBy() {
		return this.creaqtedBy;
	}

	public void setCreaqtedBy(String creaqtedBy) {
		this.creaqtedBy = creaqtedBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.reportId != null ? this.reportId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof InfoServiceRpt)) {
			return false;
		}
		InfoServiceRpt other = (InfoServiceRpt) object;
		if (((this.reportId == null) && (other.reportId != null))
				|| ((this.reportId != null) && (!this.reportId.equals(other.reportId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "ReportId=" + this.reportId + "";
	}
}
