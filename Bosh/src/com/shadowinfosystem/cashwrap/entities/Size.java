/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Size extends EntitiesImpl implements Serializable {
	private String uom;
	private String uomDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public Size() {
	}

	public Size(String uom) {
		this.uom = uom;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public String getUomDesc() {
		return this.uomDesc;
	}

	public void setUomDesc(String uomDesc) {
		this.uomDesc = uomDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update uom p\nset uom_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere uom=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.uomDesc);
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setString(7, this.uom);
		ps.setShort(8, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified By Other User.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into uom\n  (uom,\n   uom_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by)\nvalues\n  (?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.uom);
		ps.setString(2, this.uomDesc);
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be created.");
		}
		return "Record Created.";
	}

	public List<Size> getList(Connection con) throws SQLException {
		String sql = "select uom,\n       uom_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from uom p\n where uom like nvl(?,'%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.uom);

		ResultSet rs = ps.executeQuery();
		List<Size> list = new ArrayList();
		while (rs.next()) {
			Size temp = new Size();
			temp.setUom(rs.getString("uom"));
			temp.setUomDesc(rs.getString("uom_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select uom,\n       uom_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from uom p\n where uom = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.uom);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setUom(rs.getString("uom"));
			setUomDesc(rs.getString("uom_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from uom p\nwhere uom=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.uom);
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified By Other User.");
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.uom != null ? this.uom.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Size)) {
			return false;
		}
		Size other = (Size) object;
		if (((this.uom == null) && (other.uom != null)) || ((this.uom != null) && (!this.uom.equals(other.uom)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "uom=" + this.uom + "";
	}
}
