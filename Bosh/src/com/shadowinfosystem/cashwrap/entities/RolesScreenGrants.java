/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class RolesScreenGrants extends EntitiesImpl implements Serializable {
	private String role;
	private String screenId;
	private String assign;
	private boolean canAdd;
	private boolean canModify;
	private boolean canDelete;
	private boolean canInquire;
	private boolean canPost;
	private boolean canPrint;
	private boolean canCancel;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getScreenId() {
		return this.screenId;
	}

	public void setScreenId(String screen) {
		this.screenId = screen;
	}

	public String getAssign() {
		return this.assign;
	}

	public void setAssign(String assign) {
		this.assign = assign;
	}

	public boolean isCanAdd() {
		return this.canAdd;
	}

	public void setCanAdd(boolean canAdd) {
		this.canAdd = canAdd;
	}

	public boolean isCanModify() {
		return this.canModify;
	}

	public void setCanModify(boolean canModify) {
		this.canModify = canModify;
	}

	public boolean isCanDelete() {
		return this.canDelete;
	}

	public void setCanDelete(boolean canDelete) {
		this.canDelete = canDelete;
	}

	public boolean isCanInquire() {
		return this.canInquire;
	}

	public void setCanInquire(boolean canInquire) {
		this.canInquire = canInquire;
	}

	public boolean isCanPost() {
		return this.canPost;
	}

	public void setCanPost(boolean canPost) {
		this.canPost = canPost;
	}

	public boolean isCanPrint() {
		return this.canPrint;
	}

	public void setCanPrint(boolean canPrint) {
		this.canPrint = canPrint;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public boolean isCanCancel() {
		return this.canCancel;
	}

	public void setCanCancel(boolean canCancel) {
		this.canCancel = canCancel;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update roles_screen_grants g\n   set assign = ?,\n       can_add = ?,\n       can_modify = ?,\n       can_delete = ?,\n       can_inquire = ?,\n       can_post = ?,\n       can_print = ?,\n       status = ?,\n       created_on = ?,\n       created_by = ?,\n       updated_on = ?,\n       updated_by = ?,\n       object_id=object_id +1,can_cancel=? \n where role = ?\n   and screen = ? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.assign);
		ps.setString(2, this.canAdd ? "Y" : "N");
		ps.setString(3, this.canModify ? "Y" : "N");
		ps.setString(4, this.canDelete ? "Y" : "N");
		ps.setString(5, this.canInquire ? "Y" : "N");
		ps.setString(6, this.canPost ? "Y" : "N");
		ps.setString(7, this.canPrint ? "Y" : "N");
		ps.setString(8, this.status);
		ps.setDate(9, Utils.getSqlDate(this.createdOn));
		ps.setString(10, this.createdBy);
		ps.setDate(11, Utils.getSqlDate(new Date()));
		ps.setString(12, getUserId());
		ps.setString(13, this.canCancel ? "Y" : "N");
		ps.setString(14, this.role);
		ps.setString(15, this.screenId);
		ps.setDouble(16, this.objectId);
		int n = ps.executeUpdate();
		return "Record Updated.";
	}

	public String create(Connection con) throws Exception {
		int n = 0;
		String sql = "insert into roles_screen_grants\n  (role,\n   screen,\n   assign,\n   can_add,\n   can_modify,\n   can_delete,\n   can_inquire,\n   can_post,\n   can_print,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,can_cancel)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, this.role);
			ps.setString(2, this.screenId);
			ps.setString(3, this.assign);
			ps.setString(4, this.canAdd ? "Y" : "N");
			ps.setString(5, this.canModify ? "Y" : "N");
			ps.setString(6, this.canDelete ? "Y" : "N");
			ps.setString(7, this.canInquire ? "Y" : "N");
			ps.setString(8, this.canPost ? "Y" : "N");
			ps.setString(9, this.canPrint ? "Y" : "N");
			ps.setString(10, this.status);
			ps.setDate(11, Utils.getSqlDate(new Date()));
			ps.setString(12, getUserId());
			ps.setDate(13, Utils.getSqlDate(this.updatedOn));
			ps.setString(14, this.updatedBy);
			ps.setDouble(15, 1.0D);
			ps.setString(16, this.canCancel ? "Y" : "N");
			n = ps.executeUpdate();
			ps.close();
			return "Record Created.";
		} catch (SQLException ex) {
			if (n == 0) {
				throw new Exception("Record could not be created." + ex.getMessage());
			}
		}
		return "Success";
	}

	public List<RolesScreenGrants> getList(Connection con) throws SQLException {
		String sql = "select role,\n       screen,\n       assign,\n       can_add,\n       can_modify,\n       can_delete,\n       can_inquire,\n       can_post,\n       can_print,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,can_cancel\n  from roles_screen_grants\n where role = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getRole());

		ResultSet rs = ps.executeQuery();
		List<RolesScreenGrants> list = new ArrayList();

		String tempS = "";
		while (rs.next()) {
			RolesScreenGrants temp = new RolesScreenGrants();
			temp.setRole(rs.getString("role"));
			temp.setScreenId(rs.getString("screen"));
			temp.setAssign(rs.getString("assign"));
			tempS = rs.getString("can_add");
			temp.setCanAdd((tempS != null) && (tempS.equals("Y")));
			tempS = rs.getString("can_modify");
			temp.setCanModify((tempS != null) && (tempS.equals("Y")));
			tempS = rs.getString("can_delete");
			temp.setCanDelete((tempS != null) && (tempS.equals("Y")));
			tempS = rs.getString("can_inquire");
			temp.setCanInquire((tempS != null) && (tempS.equals("Y")));
			tempS = rs.getString("can_post");
			temp.setCanPost((tempS != null) && (tempS.equals("Y")));
			tempS = rs.getString("can_print");
			temp.setCanPrint((tempS != null) && (tempS.equals("Y")));

			tempS = rs.getString("can_cancel");
			temp.setCanCancel((tempS != null) && (tempS.equals("Y")));

			temp.setStatus(rs.getString("status"));

			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.role != null ? this.role.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof RolesScreenGrants)) {
			return false;
		}
		RolesScreenGrants other = (RolesScreenGrants) object;
		if (((this.role == null) && (other.role != null)) || ((this.role != null) && (!this.role.equals(other.role)))) {
			return false;
		}
		if (((this.screenId == null) && (other.screenId != null))
				|| ((this.screenId != null) && (!this.screenId.equals(other.screenId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Roles / Sceens :" + this.role + " / " + this.screenId;
	}
}
