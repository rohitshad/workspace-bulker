/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Damages extends EntitiesImpl implements Serializable {
	private String damageNo;
	private Date damageDate;
	private double grossValue;
	private double netValue;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private Date fromDate;
	private Date toDate;
	private List<DamageLines> damageLines;
	private DamageLines damageLineSelected;

	public Damages() {
		if (this.damageLines == null) {
			this.damageLines = new ArrayList();
		}
		if (this.damageLineSelected == null) {
			this.damageLineSelected = new DamageLines();
		}
	}

	public Damages(String damageNo) {
		this.damageNo = damageNo;
	}

	public String getDamageNo() {
		return this.damageNo;
	}

	public void setDamageNo(String damageNo) {
		this.damageNo = damageNo;
	}

	public Date getDamageDate() {
		return this.damageDate;
	}

	public void setDamageDate(Date damageDate) {
		this.damageDate = damageDate;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getGrossValue() {
		return this.grossValue;
	}

	public void setGrossValue(double grossValue) {
		this.grossValue = grossValue;
	}

	public double getNetValue() {
		return this.netValue;
	}

	public void setNetValue(double netValue) {
		this.netValue = netValue;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public List<DamageLines> getDamageLines() {
		return this.damageLines;
	}

	public void setDamageLines(List<DamageLines> damageLines) {
		this.damageLines = damageLines;
	}

	public DamageLines getDamageLineSelected() {
		return this.damageLineSelected;
	}

	public void setDamageLineSelected(DamageLines damageLineSelected) {
		this.damageLineSelected = damageLineSelected;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update damages\n   set damage_date = ?,\n       gross_value = ?,\n       net_value   = ?,\n       status      = ?,\n       created_on  = ?,\n       created_by  = ?,\n       updated_on  = ?,\n       updated_by  = ? , object_id=nvl(object_id,1)+1\n where damage_no = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(getDamageDate()));
		ps.setDouble(2, this.grossValue);
		ps.setDouble(3, this.netValue);
		ps.setString(4, this.status);
		ps.setDate(5, Utils.getSqlDate(this.createdOn));
		ps.setString(6, this.createdBy);
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getUserId());
		ps.setString(9, this.damageNo);
		ps.setDouble(10, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Damages Record could not be updated.");
		}
		for (DamageLines il : this.damageLines) {
			il.setDamageNo(this.damageNo);
			il.update(con);
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException, Exception {
		String sql = "insert into damages\n  (damage_no,\n   damage_date,\n   gross_value,\n   net_value,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.damageNo);
		ps.setDate(2, Utils.getSqlDate(getDamageDate()));
		ps.setDouble(3, this.grossValue);
		ps.setDouble(4, this.netValue);
		ps.setString(5, this.status);
		ps.setDate(6, Utils.getSqlDate(new Date()));
		ps.setString(7, getUserId());
		ps.setDate(8, Utils.getSqlDate(this.updatedOn));
		ps.setString(9, this.updatedBy);
		ps.setDouble(10, 1.0D);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Record could not be created.");
		}
		for (DamageLines il : this.damageLines) {
			il.setDamageNo(this.damageNo);
			il.update(con);
		}
		return "Record Created.";
	}

	public List<Damages> getList(Connection con) throws SQLException {
		String sql = "select damage_no,\n       damage_date,\n       gross_value,\n       net_value,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from damages t\n where 1 = 1 ";

		boolean doFetchData = false;
		if ((getDamageNo() != null) && (getDamageNo().trim().length() > 0)) {
			sql = sql + " and upper(damage_no) like upper('" + getDamageNo().trim() + "') ";
			doFetchData = true;
		}
		if (this.fromDate != null) {
			sql = sql + " and trunc(damage_date)  >=  to_date('" + Utils.getStrDate(this.fromDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (this.toDate != null) {
			sql = sql + " and trunc(damage_date)  <=  to_date('" + Utils.getStrDate(this.toDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().trim().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus().trim() + "') ";
			doFetchData = true;
		}
		List<Damages> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Damages temp = new Damages();
			temp.setDamageNo(rs.getString("damage_no"));
			temp.setDamageDate(rs.getDate("damage_date"));
			temp.setGrossValue(rs.getDouble("gross_value"));
			temp.setNetValue(rs.getDouble("net_value"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select damage_no,\n       damage_date,\n       gross_value,\n       net_value,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from damages t\n where damage_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getDamageNo());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setDamageNo(rs.getString("damage_no"));
			setDamageDate(rs.getDate("damage_date"));
			setGrossValue(rs.getDouble("gross_value"));
			setNetValue(rs.getDouble("net_value"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
		}
		setDamageLines(DamageLines.load(con, getDamageNo()));
		rs.close();
		ps.close();
		return "success";
	}

	public String process(String isTaxInclusive) {
		processDetails(isTaxInclusive);
		processHeader();
		return "success";
	}

	public String processHeader() {
		return "success";
	}

	public String processDetails(String isTaxInclusive) {
		double grosssValue = 0.0D;
		double nettValue = 0.0D;
		for (DamageLines l : this.damageLines) {
			l.process(isTaxInclusive);
			grosssValue += l.getSalesPrice() * l.getQty();
			nettValue += l.getNetAmt();
		}
		setGrossValue(grosssValue);
		setNetValue(nettValue);
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.damageNo != null ? this.damageNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Damages)) {
			return false;
		}
		Damages other = (Damages) object;
		if (((this.damageNo == null) && (other.damageNo != null))
				|| ((this.damageNo != null) && (!this.damageNo.equals(other.damageNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "DamageNo=" + this.damageNo + "";
	}
}
