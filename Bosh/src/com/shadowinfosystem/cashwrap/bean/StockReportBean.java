/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "stockReport")
@SessionScoped
public class StockReportBean extends EntitiesImpl {
	private String ItemCode;
	private String articleNo;
	private String barCode;
	private String itemDesc;
	private String color;
	private String item_size;
	private String section;
	private String subSection;
	private String brand;
	private Date fromDate;
	private Date toDate;
	private String outputFormat;
	private String supplier;
	
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	@PostConstruct
	public void initPageSecurity() {
		if (getScreen() == null) {
			setScreen("StockReport");
			ScreenRights temp = this.login.getRightsForScreen(getScreen());
			setUserId(this.login.getUserid());
			setAddAddowed(temp.isAddAddowed());
			setViewAddowed(temp.isViewAddowed());
			setModifyAddowed(temp.isModifyAddowed());
			setDeleteAddowed(temp.isDeleteAddowed());
			setPostAddowed(temp.isPostAddowed());
			setPrintAddowed(temp.isPrintAddowed());
		}
	}

	public String prepareReportPage() {
		setItemCode(null);
		setFromDate(new Date());
		setToDate(new Date());
		return "StockReport";
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public String getItemCode() {
		return this.ItemCode;
	}

	public void setItemCode(String ItemCode) {
		this.ItemCode = ItemCode;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getBarCode() {
		return this.barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getItem_size() {
		return item_size;
	}

	public void setItem_size(String item_size) {
		this.item_size = item_size;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getSubSection() {
		return this.subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public String getOutputFormat() {
		if (this.outputFormat == null) {
			return "pdf";
		}
		return this.outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public String getSupplier() {
		return this.supplier;
	}

	public void setSupplier(String supplier) {
		this.supplier = supplier;
	}

	public String printItemCodeWiseStock() {
		try {
			Connection con = Data.getConnection();

			Integer ResultKey = Integer.valueOf(0);
			try {
				CallableStatement cs = con.prepareCall("{call rep_stock_item_code_wise_rpi(?,?)}");
				String parameter_ = "from_date|" + Utils.getStrDate(getFromDate());
				parameter_ = parameter_ + "^" + "to_date|" + Utils.getStrDate(getToDate());
				if ((getItemCode() != null) && (getItemCode().length() > 0)) {
					parameter_ = parameter_ + "^" + "item_code|" + getItemCode();
				}
				if ((getArticleNo() != null) && (getArticleNo().length() > 0)) {
					parameter_ = parameter_ + "^" + "article_no|" + getArticleNo();
				}
				if ((getBrand() != null) && (getBrand().length() > 0)) {
					parameter_ = parameter_ + "^" + "brand|" + getBrand();
				}
				if ((getBarCode() != null) && (getBarCode().length() > 0)) {
					parameter_ = parameter_ + "^" + "bar_code|" + getBarCode();
				}
				if ((getItemDesc() != null) && (getItemDesc().length() > 0)) {
					parameter_ = parameter_ + "^" + "item_desc|" + getItemDesc();
				}
				if ((getSection() != null) && (getSection().length() > 0)) {
					parameter_ = parameter_ + "^" + "section|" + getSection();
				}
				if ((getSubSection() != null) && (getSubSection().length() > 0)) {
					parameter_ = parameter_ + "^" + "sub_section|" + getSubSection();
				}
				if ((getSupplier() != null) && (getSupplier().length() > 0)) {
					parameter_ = parameter_ + "^" + "supplier|" + getSupplier();
				}
				cs.setString(2, parameter_);
				cs.registerOutParameter(1, 8);
				cs.executeUpdate();
				ResultKey = Integer.valueOf((int) cs.getDouble(1));
			} catch (SQLException ex) {
				Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, null, ex);
			}
			this.rep.getParameters().clear();
			this.rep.getParameters().put("Result_key", ResultKey + "");
			this.rep.setJasperFile("StockRepItemCodeWise");
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "StockReport";
	}

	public String printArticleNoWiseStock() {
		try {
			Connection con = Data.getConnection();

			Integer ResultKey = Integer.valueOf(0);
			try {
				CallableStatement cs = con.prepareCall("{call rep_stock_article_no_wise_rpi(?,?)}");
				String parameter_ = "from_date|" + Utils.getStrDate(getFromDate());
				parameter_ = parameter_ + "^" + "to_date|" + Utils.getStrDate(getToDate());
				if ((getItemCode() != null) && (getItemCode().length() > 0)) {
					parameter_ = parameter_ + "^" + "item_code|" + getItemCode();
				}
				if ((getArticleNo() != null) && (getArticleNo().length() > 0)) {
					parameter_ = parameter_ + "^" + "article_no|" + getArticleNo();
				}
				if ((getBrand() != null) && (getBrand().length() > 0)) {
					parameter_ = parameter_ + "^" + "brand|" + getBrand();
				}
				if ((getBarCode() != null) && (getBarCode().length() > 0)) {
					parameter_ = parameter_ + "^" + "bar_code|" + getBarCode();
				}
				if ((getItemDesc() != null) && (getItemDesc().length() > 0)) {
					parameter_ = parameter_ + "^" + "item_desc|" + getItemDesc();
				}
				if ((getSection() != null) && (getSection().length() > 0)) {
					parameter_ = parameter_ + "^" + "section|" + getSection();
				}
				if ((getSubSection() != null) && (getSubSection().length() > 0)) {
					parameter_ = parameter_ + "^" + "sub_section|" + getSubSection();
				}
				if ((getSupplier() != null) && (getSupplier().length() > 0)) {
					parameter_ = parameter_ + "^" + "supplier|" + getSupplier();
				}
				cs.setString(2, parameter_);
				cs.registerOutParameter(1, 8);
				cs.executeUpdate();
				ResultKey = Integer.valueOf((int) cs.getDouble(1));
			} catch (SQLException ex) {
				Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, null, ex);
			}
			this.rep.getParameters().clear();
			this.rep.getParameters().put("Result_key", ResultKey + "");
			this.rep.setJasperFile("StockRepArticleNoWise");
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "StockReport";
	}

	public String printSaleSummary() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("SaleSummary");
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "StockReport";
	}
}
