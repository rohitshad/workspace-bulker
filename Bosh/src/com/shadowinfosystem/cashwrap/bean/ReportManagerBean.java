/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowsir wo barcode priinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.awt.Desktop;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.sql.Connection;
import java.util.HashMap;
import java.util.ResourceBundle;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.print.PrintService;
import javax.print.PrintServiceLookup;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import com.shadowinfosystem.cashwrap.common.Data;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporter;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.export.JRPdfExporterParameter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporter;
import net.sf.jasperreports.engine.export.JRPrintServiceExporterParameter;
import net.sf.jasperreports.engine.export.ooxml.JRDocxExporter;
import net.sf.jasperreports.engine.export.ooxml.JRXlsxExporter;

@ManagedBean(name = "reportManager")
@SessionScoped
public class ReportManagerBean {
	private JasperPrint jasperPrint;
	private String jasperFile;
	private HashMap<String, Object> parameters;
	private String reportDirectory;
	@ManagedProperty("#{company}")
	CompanyBean company;
	@ManagedProperty("#{companyParameter}")
	CompanyParameterBean companyParameter;

	public ReportManagerBean() {
		if (this.parameters == null) {
			this.parameters = new HashMap<String, Object>();
		}
		
		this.reportDirectory = "//root//tmp//apache-tomcat-9.0.2//bin//reports//bulker";
	}

	public HashMap<String, Object> getParameters() {
		return this.parameters;
	}

	public CompanyBean getCompany() {
		return this.company;
	}

	public void setCompany(CompanyBean company) {
		this.company = company;
	}

	public void setParameters(HashMap<String, Object> parameters) {
		this.parameters = parameters;
	}

	public JasperPrint getJasperPrint() {
		return this.jasperPrint;
	}

	public void setJasperPrint(JasperPrint jasperPrint) {
		this.jasperPrint = jasperPrint;
	}

	public String getReportDirectory() {
		return this.reportDirectory;
	}

	public void setReportDirectory(String reportDirectory) {
		this.reportDirectory = reportDirectory;
	}

	public String getJasperFile() {
		return this.jasperFile;
	}

	public void setJasperFile(String jasperFile) {
		this.jasperFile = jasperFile;
	}

	public CompanyParameterBean getCompanyParameter() {
		return this.companyParameter;
	}

	public void setCompanyParameter(CompanyParameterBean companyParameter) {
		this.companyParameter = companyParameter;
	}

	public void reportBuilder() throws JRException {
		getParameters().put("BrandName", this.company.getCompany().getBrand());
		getParameters().put("CompanyName", this.company.getCompany().getCompanyName());
		getParameters().put("address1", this.company.getCompany().getAddress1());
		getParameters().put("address2", this.company.getCompany().getAddress2());
		getParameters().put("address3", this.company.getCompany().getAddress3());
		getParameters().put("Tin", this.companyParameter.getItem().getTin());
		getParameters().put("CST", this.companyParameter.getItem().getCst());
		getParameters().put("LST", this.companyParameter.getItem().getLst());
		getParameters().put("Phone", this.companyParameter.getItem().getPhone());
		getParameters().put("invoice_footer_notes", this.companyParameter.getItem().getInvoiceFooterNotes());

		Connection con = Data.getConnection();

		getParameters().put("SUBREPORT_DIR", getReportDirectory() + "//");
		String report = getReportDirectory() + "//" + getJasperFile() + ".jasper";
		this.jasperPrint = JasperFillManager.fillReport(report, getParameters(), con);
	}

	public void exportToDocx() throws JRException, IOException {
		reportBuilder();
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + getJasperFile() + ".docx");
		ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
		JRDocxExporter exporter = new JRDocxExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, this.jasperPrint);
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
		exporter.exportReport();
		servletOutputStream.flush();
		servletOutputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
	}

	public void exportToPdf() throws JRException, IOException {
		reportBuilder();
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + getJasperFile() + ".pdf");
		ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
		JasperExportManager.exportReportToPdfStream(this.jasperPrint, servletOutputStream);
		servletOutputStream.flush();
		servletOutputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
	}

	public void exportToXml() throws JRException, IOException {
		reportBuilder();
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + getJasperFile() + ".xml");
		ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
		JasperExportManager.exportReportToXmlStream(this.jasperPrint, servletOutputStream);
		servletOutputStream.flush();
		servletOutputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
	}

	public void exportToXlsx() throws JRException, IOException {
		reportBuilder();
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition", "attachment; filename=" + getJasperFile() + ".xlsx");
		ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
		JRXlsxExporter xlsxExporter = new JRXlsxExporter();
		xlsxExporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
		xlsxExporter.setParameter(JRExporterParameter.JASPER_PRINT, this.jasperPrint);

		xlsxExporter.exportReport();

		servletOutputStream.flush();
		servletOutputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
	}

	public String exportToScreen() throws JRException, IOException {
		reportBuilder();
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition", "");
		httpServletResponse.addHeader("window-target", "_blank1");
		ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
		JasperExportManager.exportReportToPdfStream(this.jasperPrint, servletOutputStream);
		servletOutputStream.flush();
		servletOutputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
		return "Report";
	}

	public String exportToPdfFile() throws JRException, IOException {
		reportBuilder();
		JRExporter je = new JRPdfExporter();
		je.setParameter(JRExporterParameter.JASPER_PRINT, this.jasperPrint);
		String fileName = "//root//tmp//bulker";
		FileOutputStream fo = new FileOutputStream(fileName);
		je.setParameter(JRExporterParameter.OUTPUT_STREAM, fo);
		je.exportReport();
		fo.close();
		Desktop desktop = Desktop.getDesktop();
		desktop.print(new File(fileName));
		return "Report";
	}

	public String exportToPrinter() throws JRException, IOException {
		reportBuilder();
		PrintService service = PrintServiceLookup.lookupDefaultPrintService();
		JRExporter exporter = new JRPrintServiceExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, this.jasperPrint);

		exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, service.getAttributes());
		exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
		exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
		exporter.exportReport();
		return "Invoice";
	}

	public String exportToPrinter(String printer_name) throws JRException, IOException {
		reportBuilder();
		PrintService service = findPrintService(printer_name);
		JRExporter exporter = new JRPrintServiceExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, this.jasperPrint);

		exporter.setParameter(JRPrintServiceExporterParameter.PRINT_SERVICE_ATTRIBUTE_SET, service.getAttributes());
		exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PAGE_DIALOG, Boolean.FALSE);
		exporter.setParameter(JRPrintServiceExporterParameter.DISPLAY_PRINT_DIALOG, Boolean.FALSE);
		exporter.exportReport();
		return "Invoice";
	}

	public String exportToPdfNprint() throws JRException, IOException {
		reportBuilder();
		HttpServletResponse httpServletResponse = (HttpServletResponse) FacesContext.getCurrentInstance()
				.getExternalContext().getResponse();
		httpServletResponse.addHeader("Content-disposition", "inline;");
		httpServletResponse.addHeader("window-target", "_blank1");
		httpServletResponse.setContentType("application/pdf");
		ServletOutputStream servletOutputStream = httpServletResponse.getOutputStream();
		JRPdfExporter exporter = new JRPdfExporter();
		exporter.setParameter(JRExporterParameter.JASPER_PRINT, getJasperPrint());
		exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, servletOutputStream);
		exporter.setParameter(JRPdfExporterParameter.PDF_JAVASCRIPT, "this.print();");
		exporter.exportReport();
		servletOutputStream.flush();
		servletOutputStream.close();
		FacesContext.getCurrentInstance().responseComplete();
		return "Report";
	}

	public static PrintService findPrintService(String printerName) {
		PrintService[] services = PrinterJob.lookupPrintServices();
		PrintService service = null;
		int count = services.length;
		for (int i = 0; i < count; i++) {
			if (services[i].getName().equalsIgnoreCase(printerName)) {
				return services[i];
			}
		}
		return service;
	}
}
