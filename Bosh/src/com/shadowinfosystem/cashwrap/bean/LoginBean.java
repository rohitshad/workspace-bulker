/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletException;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.UserInfo;
import com.shadowinfosystem.cashwrap.entities.UserRoles;

@ManagedBean(name="login")
@SessionScoped
public class LoginBean
{
  private UserInfo user;
  private String userid;
  private String password;
  private boolean isAdmin;
  private List<ScreenRights> screenGrants;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public LoginBean()
    throws ServletException, IOException
  {
    if (this.user == null) {
      this.user = new UserInfo();
    }
    if (this.screenGrants == null) {
      this.screenGrants = new ArrayList();
    }
    this.isAdmin = false;
  }
  
  public boolean isIsAdmin()
  {
    return this.isAdmin;
  }
  
  public void setIsAdmin(boolean isAdmin)
  {
    this.isAdmin = isAdmin;
  }
  
  public UserInfo getUser()
  {
    return this.user;
  }
  
  public void setUser(UserInfo user)
  {
    this.user = user;
  }
  
  public String getUserid()
  {
    return this.userid;
  }
  
  public void setUserid(String userid)
  {
    this.userid = userid;
  }
  
  public String getPassword()
  {
    return this.password;
  }
  
  public void setPassword(String password)
  {
    this.password = password;
  }
  
  public List<ScreenRights> getScreenGrants()
  {
    if ((this.screenGrants != null) && (this.screenGrants.isEmpty())) {}
    return this.screenGrants;
  }
  
  public void setScreenGrants(List<ScreenRights> screenGrants)
  {
    this.screenGrants = screenGrants;
  }
  
  public FacesContext getFacesContext()
  {
    return this.facesContext;
  }
  
  public void setFacesContext(FacesContext facesContext)
  {
    this.facesContext = facesContext;
  }
  
  public ScreenRights getRightsForScreen(String screen)
  {
    if ((getUserid().equalsIgnoreCase("Administrator")) || (getUserid().equalsIgnoreCase("admin")))
    {
      setIsAdmin(true);
      return new ScreenRights(screen, true, true, true, true, true, true, true);
    }
    if (this.user.getRolesList().contains(new UserRoles(getUserid(), "admin")))
    {
      setIsAdmin(true);
      return new ScreenRights(screen, true, true, true, true, true, true, true);
    }
    if (this.user.getRolesList().contains(new UserRoles(getUserid(), "administrator")))
    {
      setIsAdmin(true);
      return new ScreenRights(screen, true, true, true, true, true, true, true);
    }
    if (this.user.getRolesList().contains(new UserRoles(getUserid(), "Administrator")))
    {
      setIsAdmin(true);
      return new ScreenRights(screen, true, true, true, true, true, true, true);
    }
    if (this.user.getRolesList().contains(new UserRoles(getUserid(), "ADMIN")))
    {
      setIsAdmin(true);
      return new ScreenRights(screen, true, true, true, true, true, true, true);
    }
    if (this.user.getRolesList().contains(new UserRoles(getUserid(), "ADMINISTRATOR")))
    {
      setIsAdmin(true);
      return new ScreenRights(screen, true, true, true, true, true, true, true);
    }
    ScreenRights screenRights = new ScreenRights(screen, false, false, false, false, false, false, false);
    if (screen == null) {
      return screenRights;
    }
    for (ScreenRights temp : this.screenGrants) {
      if (temp.getScreen().equals(screen)) {
        return temp;
      }
    }
    return screenRights;
  }
  
  public String login()
  {
    if (this.user.isAuthentic(getUserid(), getPassword()))
    {
      setUser(new UserInfo());
      
      Connection con = null;
      try
      {
        con = Data.getConnection();
        getUser().setLoginId(getUserid());
        this.user.load(con);
        ScreenRights r = new ScreenRights();
        setScreenGrants(r.getScreenRightsForUser(getUserid(), con));
        con.close();
      }
      catch (SQLException ex)
      {
        this.facesContext = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(ex.getMessage());
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        this.facesContext.addMessage(null, fm);
        return "error";
      }
      catch (Exception ex)
      {
        Logger.getLogger(LoginBean.class.getName()).log(Level.SEVERE, null, ex);
        this.facesContext = FacesContext.getCurrentInstance();
        FacesMessage fm = new FacesMessage(ex.getMessage());
        fm.setSeverity(FacesMessage.SEVERITY_ERROR);
        this.facesContext.addMessage(null, fm);
        return "error";
      }
    }
    else
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Invalid User ID / Password.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "error";
    }
    return "success";
  }
  
  public String logout()
  {
    return "login";
  }
}
