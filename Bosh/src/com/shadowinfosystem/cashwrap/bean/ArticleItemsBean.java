/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/

package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="articleItems")
@SessionScoped
public class ArticleItemsBean
{
  ArticleItems item;
  ArticleItems filter;
  List<ArticleItems> list;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public ArticleItemsBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.filter == null) {
      this.filter = new ArticleItems();
    }
    if (this.item == null) {
      this.item = new ArticleItems();
    }
  }
  
  public String clearFilter()
  {
    setFilter(new ArticleItems());
    return "success";
  }
  
  public ArticleItems getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("ArticleItems");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(ArticleItems item)
  {
    this.item = item;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public List<ArticleItems> getList()
  {
    if ((this.list == null) || (this.list.isEmpty()))
    {
      Connection con = Data.getConnection();
      try
      {
        setList(getFilter().search(con));
        con.close();
      }
      catch (Exception ex)
      {
        Logger.getLogger(ArticleItemsBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<ArticleItems> list)
  {
    this.list = list;
  }
  
  public ArticleItems getFilter()
  {
    return this.filter;
  }
  
  public void setFilter(ArticleItems filter)
  {
    this.filter = filter;
  }
}


