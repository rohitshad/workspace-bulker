/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.TransactionPrefix;

@ManagedBean(name = "transactionPrefix")
@SessionScoped
public class TransactionPrefixBean {
	TransactionPrefix item;
	List<TransactionPrefix> list;
	List<TransactionPrefix> invPrefixlist;
	List<TransactionPrefix> invRetPrefixlist;
	List<TransactionPrefix> custPayRecPrefixlist;
	List<TransactionPrefix> custPayRecCanPrefixlist;
	List<TransactionPrefix> suppPaymentPrefixlist;
	List<TransactionPrefix> suppPaymentCanPrefixlist;
	List<TransactionPrefix> expensePrefixlist;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext;

	public TransactionPrefixBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.invPrefixlist == null) {
			this.invPrefixlist = new ArrayList();
		}
		if (this.invRetPrefixlist == null) {
			this.invRetPrefixlist = new ArrayList();
		}
		if (this.custPayRecPrefixlist == null) {
			this.custPayRecPrefixlist = new ArrayList();
		}
		if (this.custPayRecCanPrefixlist == null) {
			this.custPayRecCanPrefixlist = new ArrayList();
		}
		if (this.suppPaymentPrefixlist == null) {
			this.suppPaymentPrefixlist = new ArrayList();
		}
		if (this.suppPaymentCanPrefixlist == null) {
			this.suppPaymentCanPrefixlist = new ArrayList();
		}
		if (this.expensePrefixlist == null) {
			this.expensePrefixlist = new ArrayList();
		}
		if (this.item == null) {
			this.item = new TransactionPrefix();
		}
	}

	public TransactionPrefix getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("TransactionPrefix");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(TransactionPrefix item) {
		this.item = item;
	}

	public List<TransactionPrefix> getExpensePrefixlist() {
		if ((this.expensePrefixlist == null) || (this.expensePrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("EXPENSE");
				this.item.setPrefix(null);
				setExpensePrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.expensePrefixlist;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public void setExpensePrefixlist(List<TransactionPrefix> expensePrefixlist) {
		this.expensePrefixlist = expensePrefixlist;
	}

	public List<TransactionPrefix> getSuppPaymentCanPrefixlist() {
		if ((this.suppPaymentCanPrefixlist == null) || (this.suppPaymentCanPrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("SUPPPAYCAN");
				this.item.setPrefix(null);
				setSuppPaymentCanPrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.suppPaymentCanPrefixlist;
	}

	public void setSuppPaymentCanPrefixlist(List<TransactionPrefix> suppPaymentCanPrefixlist) {
		this.suppPaymentCanPrefixlist = suppPaymentCanPrefixlist;
	}

	public List<TransactionPrefix> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getTransactionPrefixList();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public List<TransactionPrefix> getCustPayRecPrefixlist() {
		if ((this.custPayRecPrefixlist == null) || (this.custPayRecPrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("CUSTPAYREC");
				this.item.setPrefix(null);
				setPayRecPrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.custPayRecPrefixlist;
	}

	public void setCustPayRecPrefixlist(List<TransactionPrefix> custPayRecPrefixlist) {
		this.custPayRecPrefixlist = custPayRecPrefixlist;
	}

	public void setCustPayRecCanPrefixlist(List<TransactionPrefix> custPayRecCanPrefixlist) {
		this.custPayRecCanPrefixlist = custPayRecCanPrefixlist;
	}

	public void setSuppPaymentPrefixlist(List<TransactionPrefix> suppPaymentPrefixlist) {
		this.suppPaymentPrefixlist = suppPaymentPrefixlist;
	}

	public List<TransactionPrefix> getSuppPaymentPrefixlist() {
		if ((this.suppPaymentPrefixlist == null) || (this.suppPaymentPrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("SUPPPAY");
				this.item.setPrefix(null);
				setSuppPaymentPrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.suppPaymentPrefixlist;
	}

	public List<TransactionPrefix> getCustPayRecCanPrefixlist() {
		if ((this.custPayRecCanPrefixlist == null) || (this.custPayRecCanPrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("CUSTPAYRECCAN");
				this.item.setPrefix(null);
				setCustPayRecCanPrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.custPayRecCanPrefixlist;
	}

	public void setPayRecPrefixlist(List<TransactionPrefix> custPayRecPrefixlist) {
		this.custPayRecPrefixlist = custPayRecPrefixlist;
	}

	public List<TransactionPrefix> getInvPrefixlist() {
		if ((this.invPrefixlist == null) || (this.invPrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("INV");
				this.item.setPrefix(null);
				setInvPrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.invPrefixlist;
	}

	public List<TransactionPrefix> getInvRetPrefixlist() {
		if ((this.invRetPrefixlist == null) || (this.invRetPrefixlist.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				this.item.setTransType("INVRET");
				this.item.setPrefix(null);
				setInvRetPrefixlist(this.item.getListForTrans(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.invRetPrefixlist;
	}

	public void setInvPrefixlist(List<TransactionPrefix> invPrefixlist) {
		this.invPrefixlist = invPrefixlist;
	}

	public void setInvRetPrefixlist(List<TransactionPrefix> invRetPrefixlist) {
		this.invRetPrefixlist = invRetPrefixlist;
	}

	public void setList(List<TransactionPrefix> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(e.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		} catch (Exception ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "TransactionPrefix";
	}

	public String addNew() {
		this.item = new TransactionPrefix();

		return "TransactionPrefix";
	}

	public String load(String transactionPrefix, String trans_type) throws SQLException, Exception {
		Connection con = Data.getConnection();
		this.item.setPrefix(transactionPrefix);
		this.item.setTransType(trans_type);
		this.item.load(con);
		return "TransactionPrefix";
	}

	public String getTransactionPrefixList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "TransactionPrefixList";
	}

	public String getNextOrderNo() throws SQLException {
		Connection con = Data.getConnection();
		String sql = "select decode (nvl(p.append_prefix,'Yes'),'Yes', p.prefix ||p.current_no,p.current_no) current_no , p.object_id, p.rowid rid\n  from transaction_prefix p\n where  TRUNC(SYSDATE) BETWEEN VALID_FROM AND VALID_UP_TO AND P.STATUS='Active' \n and p.prefix = nvl(?,p.prefix)  and p.trans_type = ? ORDER BY decode(nvl(P.is_default,'No'),'Yes',1,2)";

		PreparedStatement ps = null;
		String rid = "";
		try {
			con.setAutoCommit(false);
			ps = con.prepareStatement(sql);
			ps.setString(1, this.item.getPrefix());
			ps.setString(2, this.item.getTransType());
			ResultSet rs = ps.executeQuery();
			String curr_no = "";
			short objectid = 0;
			int i = 0;
			if (rs.next()) {
				curr_no = rs.getString("current_no");
				rid = rs.getString("rid");
				objectid = rs.getShort("object_id");
			} else {
				throw new Exception(
						"Please creat transaction prefix." + this.item.getPrefix() + "/" + this.item.getTransType());
			}
			rs.close();
			ps.close();
			sql = "update transaction_prefix p\n  set p.current_no = p.current_no + 1, p.object_id = p.object_id + 1\nwhere rowid = ?\n  and p.object_id = ?";

			ps = con.prepareStatement(sql);
			ps.setString(1, rid);
			ps.setShort(2, objectid);
			int nru = ps.executeUpdate();
			ps.close();
			if (nru == 1) {
				con.setAutoCommit(true);
				con.commit();
			} else {
				con.rollback();
				con.setAutoCommit(true);
				if (!ps.isClosed()) {
					ps.close();
				}
				throw new SQLException("Thransaction Order No Could not be created");
			}
			return curr_no;
		} catch (SQLException e) {
			con.rollback();
			con.setAutoCommit(true);

			throw new SQLException(e.getMessage());
		} catch (Exception ex) {
			Logger.getLogger(TransactionPrefixBean.class.getName()).log(Level.SEVERE, null, ex);
			con.rollback();
			con.setAutoCommit(true);

			throw new SQLException(ex.getMessage());
		} finally {
			if ((con != null) && (!con.isClosed())) {
				con.close();
			}
		}
	}

	public String setAsDefaultPrefix(String trans_Type, String pref_) {
		addNew();
		this.item.setTransType(trans_Type);
		getList();
		Connection con = Data.getConnection();
		for (TransactionPrefix tp : this.list) {
			if (tp.getPrefix().equals(pref_)) {
				tp.setIsDefault("Yes");
			} else {
				tp.setIsDefault("No");
			}
			try {
				tp.update(con);
			} catch (Exception ex) {
			}
		}
		try {
			addNew();
			this.item.setPrefix(pref_);
			this.item.setTransType(trans_Type);
			this.item.load(con);
			this.list.clear();
		} catch (Exception ex) {
		}
		try {
			con.close();
		} catch (SQLException ex) {
		}
		return "TransactionPrefix";
	}

	public String refreshList() {
		this.item = new TransactionPrefix();
		getList().clear();
		return "TransactionPrefixList";
	}

	public String search() {
		getList().clear();
		return "TransactionPrefixList";
	}

	public String getDefaultPrefix(String transType) {
		Connection con = Data.getConnection();
		String sql = "select p.prefix  from transaction_prefix p\n where p.trans_type = ? and nvl(p.is_default,'No')='Yes' and nvl(p.status,'Active')='Active' ";

		PreparedStatement ps = null;
		String prefixss = null;
		try {
			ps = con.prepareStatement(sql);
			ps.setString(1, this.item.getTransType());
			ResultSet rs = ps.executeQuery();
			if (rs.next()) {
				prefixss = rs.getString("prefix");
			}
			rs.close();
			ps.close();
			con.close();
		} catch (Exception e) {
		}
		return prefixss;
	}

	public String refresh() {
		getInvPrefixlist().clear();
		getInvRetPrefixlist().clear();
		getCustPayRecPrefixlist().clear();
		getSuppPaymentCanPrefixlist().clear();
		getSuppPaymentPrefixlist().clear();
		getCustPayRecCanPrefixlist().clear();

		return "TransactionPrefix";
	}
}
