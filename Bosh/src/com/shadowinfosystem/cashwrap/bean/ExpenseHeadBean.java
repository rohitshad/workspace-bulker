/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.ExpenseHead;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="expenseHead")
@SessionScoped
public class ExpenseHeadBean
{
  ExpenseHead item;
  List<ExpenseHead> list;
  List<ExpenseHead> listSelectOne;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public ExpenseHeadBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.listSelectOne == null) {
      this.listSelectOne = new ArrayList();
    }
    if (this.item == null) {
      this.item = new ExpenseHead();
    }
  }
  
  public ExpenseHead getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("ExpenseHead");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(ExpenseHead item)
  {
    this.item = item;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public List<ExpenseHead> getListSelectOne()
  {
    if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
      try
      {
        Connection con = Data.getConnection();
        addNew();
        setListSelectOne(this.item.getList(con));
        con.close();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.listSelectOne;
  }
  
  public void setListSelectOne(List<ExpenseHead> listSelectOne)
  {
    this.listSelectOne = listSelectOne;
  }
  
  public List<ExpenseHead> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        getExpenseHeadList();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ExpenseHeadBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<ExpenseHead> list)
  {
    this.list = list;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.update(con);
      this.item.load(con);
      con.close();
      this.listSelectOne.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "ExpenseHead";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.delete(con);
      addNew();
      con.close();
      this.list.clear();
      this.listSelectOne.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "ExpenseHead";
  }
  
  public String addNew()
  {
    this.item = new ExpenseHead();
    
    return "ExpenseHead";
  }
  
  public String refreshlist()
  {
    setItem(new ExpenseHead());
    this.list.clear();
    
    return "ExpenseHeadList";
  }
  
  public String load(String head)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setHead(head);
    this.item.load(con);
    return "ExpenseHead";
  }
  
  public String getExpenseHeadList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.getList(con));
    return "ExpenseHeadList";
  }
  
  public String refreshList()
  {
    addNew();
    this.list.clear();
    return "ExpenseHeadList";
  }
}
