/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "purRecRep")
@SessionScoped
public class PurchaseReceiptReportBean extends EntitiesImpl {
	private String poNo;
	private String porNo;
	private String suppId;
	private Date fromDate;
	private Date toDate;
	private String outputFormat;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public String prepareReportPage() {
		this.setPoNo((String) null);
		this.setSuppId((String) null);
		this.setFromDate(new Date());
		this.setToDate(new Date());
		return "PurchaseReceiptReport";
	}

	@PostConstruct
	public void initPageSecurity() {
		if (this.getScreen() == null) {
			this.setScreen("PurchaseReceiptReport");
			ScreenRights temp = this.login.getRightsForScreen(this.getScreen());
			this.setUserId(this.login.getUserid());
			this.setAddAddowed(temp.isAddAddowed());
			this.setViewAddowed(temp.isViewAddowed());
			this.setModifyAddowed(temp.isModifyAddowed());
			this.setDeleteAddowed(temp.isDeleteAddowed());
			this.setPostAddowed(temp.isPostAddowed());
			this.setPrintAddowed(temp.isPrintAddowed());
		}

	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public String getPoNo() {
		return this.poNo;
	}

	public void setPoNo(String poNo) {
		this.poNo = poNo;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public String getSuppId() {
		return this.suppId;
	}

	public void setSuppId(String suppId) {
		this.suppId = suppId;
	}

	public String getPorNo() {
		return this.porNo;
	}

	public void setPorNo(String porNo) {
		this.porNo = porNo;
	}

	public String getOutputFormat() {
		return this.outputFormat == null ? "pdf" : this.outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public String printPurchaseRegister() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("PurchaseReceiptRegister");
			if (this.getPoNo() != null && this.getPoNo().length() > 0) {
				this.rep.getParameters().put("PO_NO", this.getPoNo());
			}

			if (this.getPorNo() != null && this.getPorNo().length() > 0) {
				this.rep.getParameters().put("POR_NO", this.getPorNo());
			}

			if (this.getSuppId() != null && this.getSuppId().length() > 0) {
				this.rep.getParameters().put("supp_id", this.getSuppId());
			}

			if (this.getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(this.getFromDate()));
			}

			if (this.getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(this.getToDate()));
			}

			if (this.getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}

			if (this.getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}

			if (this.getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}

			if (this.getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseReceiptReport";
	}

	public String printPurchaseRegisterItemDetail() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("PurchaseReceiptRegisterItemDetail");
			if (this.getPoNo() != null && this.getPoNo().length() > 0) {
				this.rep.getParameters().put("PO_NO", this.getPoNo());
			}

			if (this.getPorNo() != null && this.getPorNo().length() > 0) {
				this.rep.getParameters().put("POR_NO", this.getPorNo());
			}

			if (this.getSuppId() != null && this.getSuppId().length() > 0) {
				this.rep.getParameters().put("supp_id", this.getSuppId());
			}

			if (this.getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(this.getFromDate()));
			}

			if (this.getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(this.getToDate()));
			}

			if (this.getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}

			if (this.getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}

			if (this.getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}

			if (this.getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseReceiptReport";
	}

	public String printPurchaseRegisterVendorWise() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("PurchaseReceiptRegisterVendorWise");
			if (this.getPoNo() != null && this.getPoNo().length() > 0) {
				this.rep.getParameters().put("po_no", this.getPoNo());
			}

			if (this.getPorNo() != null && this.getPorNo().length() > 0) {
				this.rep.getParameters().put("POR_NO", this.getPorNo());
			}

			if (this.getSuppId() != null && this.getSuppId().length() > 0) {
				this.rep.getParameters().put("supp_id", this.getSuppId());
			}

			if (this.getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(this.getFromDate()));
			}

			if (this.getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(this.getToDate()));
			}

			if (this.getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}

			if (this.getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}

			if (this.getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}

			if (this.getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "PurchaseReceiptReport";
	}

	public String printSupplierLedger() {
		Connection con = null;

		try {
			FacesMessage fm;
			try {
				con = Data.getConnection();
				Integer fm1 = Integer.valueOf(0);

				try {
					CallableStatement ex = con.prepareCall("{call rep_supp_ledger_rpi(?,?)}");
					String ex1 = "from_date|" + Utils.getStrDate(this.getFromDate());
					ex1 = ex1 + "^" + "to_date|" + Utils.getStrDate(this.getToDate());
					if (this.getSuppId() == null || this.getSuppId().length() <= 0) {
						throw new JRException("Plese select supplier.");
					}

					ex1 = ex1 + "^" + "supp_id|" + this.getSuppId();
					ex.setString(2, ex1);
					ex.registerOutParameter(1, 8);
					ex.executeUpdate();
					ex.close();
					con.close();
					fm1 = Integer.valueOf((int) ex.getDouble(1));
				} catch (SQLException arg15) {
					Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, (String) null, arg15);
				}

				this.rep.getParameters().clear();
				this.rep.getParameters().put("Result_key", fm1 + "");
				this.rep.setJasperFile("SupplierLedger");
				if (this.getOutputFormat().equalsIgnoreCase("PDF")) {
					this.rep.exportToPdf();
				}

				if (this.getOutputFormat().equalsIgnoreCase("XLSX")) {
					this.rep.exportToXlsx();
				}

				if (this.getOutputFormat().equalsIgnoreCase("DOCX")) {
					this.rep.exportToDocx();
				}

				if (this.getOutputFormat().equalsIgnoreCase("XML")) {
					this.rep.exportToXml();
				}
			} catch (JRException arg16) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg16.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			} catch (IOException arg17) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg17.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException arg14) {
				Logger.getLogger(PurchaseReceiptReportBean.class.getName()).log(Level.SEVERE, (String) null, arg14);
			}

		}

		return "PurchaseReceiptReport";
	}

	public String printSupplierWiseLedger() {
		Connection con = null;

		try {
			FacesMessage fm;
			try {
				con = Data.getConnection();
				Integer fm1 = Integer.valueOf(0);

				try {
					CallableStatement ex = con.prepareCall("{call rep_supp_wise_ledger_rpi(?,?)}");
					String ex1 = "from_date|" + Utils.getStrDate(this.getFromDate());
					ex1 = ex1 + "^" + "to_date|" + Utils.getStrDate(this.getToDate());
					if (this.getSuppId() != null && this.getSuppId().length() > 0) {
						ex1 = ex1 + "^" + "supp_id|" + this.getSuppId();
					}

					ex.setString(2, ex1);
					ex.registerOutParameter(1, 8);
					ex.executeUpdate();
					ex.close();
					con.close();
					fm1 = Integer.valueOf((int) ex.getDouble(1));
				} catch (SQLException arg15) {
					Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, (String) null, arg15);
				}

				this.rep.getParameters().clear();
				this.rep.getParameters().put("Result_key", fm1 + "");
				this.rep.setJasperFile("SupplierWiseLedger");
				if (this.getOutputFormat().equalsIgnoreCase("PDF")) {
					this.rep.exportToPdf();
				}

				if (this.getOutputFormat().equalsIgnoreCase("XLSX")) {
					this.rep.exportToXlsx();
				}

				if (this.getOutputFormat().equalsIgnoreCase("DOCX")) {
					this.rep.exportToDocx();
				}

				if (this.getOutputFormat().equalsIgnoreCase("XML")) {
					this.rep.exportToXml();
				}
			} catch (JRException arg16) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg16.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			} catch (IOException arg17) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg17.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			try {
				if (con != null && !con.isClosed()) {
					con.close();
				}
			} catch (SQLException arg14) {
				Logger.getLogger(PurchaseReceiptReportBean.class.getName()).log(Level.SEVERE, (String) null, arg14);
			}

		}

		return "PurchaseReceiptReport";
	}
}