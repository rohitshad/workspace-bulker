/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.ArticleItemsBean;
import com.shadowinfosystem.cashwrap.bean.CustomerBean;
import com.shadowinfosystem.cashwrap.bean.InventoryTransactionBean;
import com.shadowinfosystem.cashwrap.bean.InvoiceBean;
import com.shadowinfosystem.cashwrap.bean.InvoiceRefundBean;
import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.MasterParameterBean;
import com.shadowinfosystem.cashwrap.bean.SectionBean;
import com.shadowinfosystem.cashwrap.bean.StockBean;
import com.shadowinfosystem.cashwrap.bean.SubSectionBean;
import com.shadowinfosystem.cashwrap.bean.TaxBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.InvoiceLines;
import com.shadowinfosystem.cashwrap.entities.InvoiceRefund;
import com.shadowinfosystem.cashwrap.entities.InvoiceRet;
import com.shadowinfosystem.cashwrap.entities.InvoiceRetLines;
import com.shadowinfosystem.cashwrap.entities.InvoiceRetPayDet;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "invRet")
@SessionScoped
public class InvoiceRetBean {
	InvoiceRet item;
	List<InvoiceRet> list;
	@ManagedProperty("#{stock}")
	StockBean stock;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	@ManagedProperty("#{section}")
	SectionBean section;
	@ManagedProperty("#{subSection}")
	SubSectionBean subSection;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{invRefund}")
	InvoiceRefundBean invRefund;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{invoice}")
	InvoiceBean invoice;
	@ManagedProperty("#{masterParameter}")
	MasterParameterBean mastParam;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public InvoiceRetBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new InvoiceRet();
		}

	}

	public InvoiceRet getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("InvoiceRet");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public void setItem(InvoiceRet item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public MasterParameterBean getMastParam() {
		return this.mastParam;
	}

	public void setMastParam(MasterParameterBean mastParam) {
		this.mastParam = mastParam;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public SectionBean getSection() {
		return this.section;
	}

	public void setSection(SectionBean section) {
		this.section = section;
	}

	public SubSectionBean getSubSection() {
		return this.subSection;
	}

	public void setSubSection(SubSectionBean subSection) {
		this.subSection = subSection;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public InvoiceRefundBean getInvRefund() {
		return this.invRefund;
	}

	public void setInvRefund(InvoiceRefundBean invRefund) {
		this.invRefund = invRefund;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public InvoiceBean getInvoice() {
		return this.invoice;
	}

	public void setInvoice(InvoiceBean invoice) {
		this.invoice = invoice;
	}

	public List<InvoiceRet> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<InvoiceRet> list) {
		this.list = list;
	}

	public String setAndSearchItems(String itm_Code) {
		this.item.getInvoiceRetLineSelected().setItemCode(itm_Code);
		this.searchItems();
		return "success";
	}

	public String setItemCode(String itmcode) {
		this.articleItems.list.clear();
		this.item.getInvoiceRetLineSelected().setItemCode(itmcode);
		this.selectItem();
		return "InvoiceRet";
	}

	public String resetSelected() {
		this.getItem().setInvoiceRetLineSelected(new InvoiceRetLines());
		this.isSelected = false;
		this.getArticleItems().clearFilter();
		this.getArticleItems().getList().clear();
		return "InvoiceRet";
	}

	public String prepareSalesReturn(String invNo) {
		this.addNew();
		this.list.clear();
		this.stock.addNew();
		this.stock.list.clear();
		this.trans.addNew();
		this.trans.list.clear();

		try {
			this.invoice.addNew();
			this.invoice.load(invNo);
			this.addNew();
			this.getItem().setInvoiceNo(this.invoice.getItem().getInvoiceNo());
			this.getItem().setInvoiceDate(this.invoice.getItem().getInvoiceDate());
			this.getItem().setDiscPer(this.invoice.getItem().getDiscPer());
			this.getItem().setDiscAmt(this.invoice.getItem().getDiscAmt());
			this.getItem().setOtherCharges(this.invoice.getItem().getOtherCharges());
			this.getItem().setRoundOff(this.invoice.getItem().getRoundOff());
			this.getItem().setCustomerId(this.invoice.getItem().getCustomerId());
			this.getItem().setCustomerName(this.invoice.getItem().getCustomerName());
			this.getItem().setCustAddress(this.invoice.getItem().getCustAddress());
			this.getItem().setNetValue(this.invoice.getItem().getNetValue());
			this.getItem().setTaxPerOnInv(this.invoice.getItem().getTaxPerOnInv());
			this.getItem().setTaxAmtOnInv(this.invoice.getItem().getTaxAmtOnInv());
			InvoiceRetLines ex = null;
			Iterator fm1 = this.invoice.item.getInvoiceLines().iterator();

			while (fm1.hasNext()) {
				InvoiceLines il = (InvoiceLines) fm1.next();
				ex = new InvoiceRetLines();
				ex.setLineNo(this.getNextLineNo());
				ex.setItemCode(il.getItemCode());
				ex.setItemDesc(il.getItemDesc());
				ex.setQty(il.getQty());
				ex.setMrp(il.getMrp());
				ex.setUom(il.getUom());
				ex.setSalesPrice(il.getSalesPrice());
				ex.setDiscPer(il.getDiscPer());
				ex.setDiscAmt(il.getDiscAmt());
				ex.setTaxPer(il.getTaxPer());
				ex.setTaxAmt(il.getTaxAmt());
				ex.setItemSize(il.getItemSize());
				ex.setTaxCode(il.getTaxCode());
				ex.setBrand(il.getBrand());
				ex.setArticleNo(il.getArticleNo());
				ex.setSelsPers(il.getSelsPers());
				ex.setSection(il.getSection());
				ex.setSubSection(il.getSubSection());
				ex.setColor(il.getColor());
				ex.setItemSize(il.getItemSize());
				ex.setNetAmt(il.getNetAmt());
				ex.setBatch(il.getBatch());
				ex.setExpDate(il.getExpDate());
				this.getItem().getInvoiceRetLines().add(ex);
			}
		} catch (SQLException arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "InvoiceRet";
	}

	public String searchItems() {
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getInvoiceRetLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getInvoiceRetLineSelected().getItemCode());
		this.articleItems.getFilter().setBar_code(this.item.getInvoiceRetLineSelected().getBarCode());
		this.articleItems.getFilter().setArticle_no(this.item.getInvoiceRetLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getInvoiceRetLineSelected().getItemDesc());
		this.articleItems.getList();
		if (this.articleItems.getList().size() == 1) {
			this.item.getInvoiceRetLineSelected()
					.setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.selectItem();
		}

		return this.articleItems.getList().size() > 1 ? "InvoiceRet" : "InvoiceRet";
	}

	public String selectItem() {
		try {
			InvoiceRetLines ex = this.item.getInvoiceRetLineSelected();
			this.articleItems.clearFilter();
			this.articleItems.getFilter().setItem_code(ex.getItemCode());
			this.articleItems.getList();
			if (this.articleItems.getList().size() == 1) {
				this.articleItems.setItem((ArticleItems) this.articleItems.getList().get(0));
			}

			double secTax = 0.0D;
			double subSecTax = 0.0D;
			double secDisc = 0.0D;
			double subSecPerc = 0.0D;
			double tax = 0.0D;
			double disc = 0.0D;
			if (this.articleItems.item.getArticle_group() != null) {
				this.section.load(this.articleItems.item.getArticle_group());
				secTax = this.section.item.getTaxPerc();
				secDisc = this.section.item.getDiscPerc();
			}

			if (this.articleItems.item.getArticle_sub_group() != null
					&& this.articleItems.item.getArticle_group() != null) {
				this.subSection.load(this.articleItems.item.getArticle_sub_group(),
						this.articleItems.item.getArticle_group());
				subSecTax = this.subSection.item.getTaxPerc();
				subSecPerc = this.subSection.item.getDiscPerc();
			}

			if (subSecTax > 0.0D) {
				tax = subSecTax;
			} else {
				tax = secTax;
			}

			if (subSecPerc > 0.0D) {
				disc = subSecPerc;
			} else {
				disc = secDisc;
			}

			ex.setMrp(this.articleItems.item.getMrp());
			ex.setSalesPrice(this.articleItems.item.getSales_price());
			ex.setTaxPer(tax);
			ex.setTaxAmt(this.articleItems.item.getSales_price() * tax / 100.0D);
			ex.setDiscPer(disc);
			ex.setDiscAmt(this.articleItems.item.getSales_price() * disc / 100.0D);
			ex.setQty(1.0D);
			ex.setUom(this.articleItems.item.getInventory_uom());
			ex.setNetAmt(ex.getQty() * ex.getSalesPrice() - ex.getDiscAmt() + ex.getTaxAmt());
			ex.setSubSection(this.articleItems.item.getArticle_group());
			ex.setSection(this.articleItems.item.getArticle_sub_group());
			ex.setColor(this.articleItems.item.getColor());
			ex.setArticleNo(this.articleItems.item.getArticle_no());
			ex.setBarCode(this.articleItems.item.getBar_code());
			ex.setItemDesc(this.articleItems.item.getArticle_desc());
			this.stock.addNew();
			this.stock.load(ex.getItemCode());
			ex.setQtyOnHand(this.stock.item.getQtyOnHand());
			this.setIsSelected(true);
			this.reProcessSelected();
		} catch (SQLException arg13) {
			Logger.getLogger(InvoiceRetBean.class.getName()).log(Level.SEVERE, (String) null, arg13);
		}

		return "InvoiceRet";
	}

	public String setCustomer() throws SQLException {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		CustomerBean cust = (CustomerBean) exctx.getSessionMap().get("cust");
		cust.load(this.item.getCustomerId());
		this.item.setCustomerName(cust.item.getCustName());
		this.item.setCustAddress(cust.item.getAddress());
		return "InvoiceRet";
	}

	public String taxCodeSelected() throws SQLException {
		InvoiceRetLines ils = this.item.getInvoiceRetLineSelected();
		if (this.item.getInvoiceRetLineSelected().getTaxCode() != null
				&& this.item.getInvoiceRetLineSelected().getTaxCode().length() > 0) {
			ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
			TaxBean tax = (TaxBean) exctx.getSessionMap().get("tax");
			tax.load(this.item.getInvoiceRetLineSelected().getTaxCode());
			ils.setTaxPer(tax.getItem().getTaxPer());
		} else {
			ils.setTaxPer(0.0D);
		}

		return this.reProcessSelected();
	}

	public String reProcessSelected() {
		InvoiceRetLines ils = this.item.getInvoiceRetLineSelected();
		ils.process(this.mastParam.getItem().getIsTaxInclusive());
		return "InvoiceRet";
	}

	public String addItem() {
		if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage pl1 = new FacesMessage("Please select Item.");
			pl1.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, pl1);
			return "InvoiceRet";
		} else {
			InvoiceRetLines pl = this.item.getInvoiceRetLineSelected();
			pl.setLineNo(this.getNextLineNo());
			pl.setInvoiceNo(this.item.getInvoiceNo());
			this.item.getInvoiceRetLines().add(0, pl);
			this.item.setInvoiceRetLineSelected(new InvoiceRetLines());
			this.item.process(this.mastParam.getItem().getIsTaxInclusive());
			this.setIsSelected(false);
			return "InvoiceRet";
		}
	}

	public String addNewPayLine() {
		InvoiceRetPayDet pd = new InvoiceRetPayDet();
		pd.setInvRetNo(this.item.getInvRetNo());
		double amtInPay = 0.0D;

		InvoiceRetPayDet pl;
		for (Iterator i$ = this.item.getInvRetPayLines().iterator(); i$.hasNext(); amtInPay += pl.getPayAmt()) {
			pl = (InvoiceRetPayDet) i$.next();
		}

		pd.setPayAmt(this.item.getNetValue() - amtInPay);
		this.item.getInvRetPayLines().add(pd);
		return "InvoiceRet";
	}

	public String updateItem() {
		InvoiceRetLines il = this.item.getInvoiceRetLineSelected();
		this.item.setInvoiceRetLineSelected(new InvoiceRetLines());
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		return "InvoiceRet";
	}

	public String getInvRetList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "InvoiceRetList";
	}

	public String setSlectedDetail(InvoiceRetLines line) {
		this.item.setInvoiceRetLineSelected(line);
		return "InvoiceRet";
	}

	public String deletDetail(InvoiceRetLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getLineNo() > 0) {
			try {
				line.delete(con);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		con.close();
		this.item.getInvoiceRetLines().remove(line);
		return "InvoiceRet";
	}

	public String deletePayDetail(InvoiceRetPayDet line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getObjectId() == 0) {
			this.item.getInvRetPayLines().remove(line);
			return "InvoiceRet";
		} else {
			try {
				line.delete(con);
				this.item.getInvRetPayLines().remove(line);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}

			return "InvoiceRet";
		}
	}

	public String newItem() {
		this.item = new InvoiceRet();
		return "InvoiceRet";
	}

	public String load(String inv_no) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setInvRetNo(inv_no);
		this.item.load(con);
		this.getArticleItems().clearFilter();
		this.getArticleItems().getList().clear();
		return "InvoiceRet";
	}

	public short getNextLineNo() {
		boolean i = false;
		short arg3 = 0;
		Iterator i$ = this.getItem().getInvoiceRetLines().iterator();

		while (i$.hasNext()) {
			InvoiceRetLines pl = (InvoiceRetLines) i$.next();
			if (pl.getLineNo() > arg3) {
				arg3 = pl.getLineNo();
			}
		}

		++arg3;
		return arg3;
	}

	public String process() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		return "InvoiceRet";
	}

	public String selectAllClicked() {
		Iterator i$ = this.getItem().getInvoiceRetLines().iterator();

		while (i$.hasNext()) {
			InvoiceRetLines il = (InvoiceRetLines) i$.next();
			il.setTempSel(this.getItem().isSelAll());
		}

		return "InvoiceRet";
	}

	public String processHeader() {
		this.item.processHeader(this.mastParam.getItem().getIsTaxInclusive());
		return "InvoiceRet";
	}

	public String commit() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		Connection con = Data.getConnection();

		try {
			if (this.item.getInvRetNo() == null || this.item.getInvRetNo().length() == 0) {
				if (this.item.getInvoicePrefix() == null) {
					throw new Exception("Please select Invoice Return Prefix.");
				}

				this.trans.item.setPrefix(this.item.getInvoicePrefix());
				this.trans.item.setTransType("INVRET");
				this.item.setInvRetNo(this.trans.getNextOrderNo());
			}

			this.item.update(con);
			this.item.load(con);
			con.close();
			this.getArticleItems().clearFilter();
			this.getArticleItems().getList().clear();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(InvoiceRetBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "InvoiceRet";
	}

	public String addNew() {
		this.setItem(new InvoiceRet());
		this.stock.addNew();
		this.stock.list.clear();
		this.getArticleItems().clearFilter();
		this.getArticleItems().getList().clear();
		return "InvoiceRet";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		this.item.setFromDate(new Date());
		this.item.setToDate(new Date());
		return "InvoiceRetList";
	}

	public String search() {
		this.list.clear();
		return "InvoiceRetList";
	}

	public String invReturned() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				con.setAutoCommit(false);
				Iterator ex = this.item.getInvoiceRetLines().iterator();

				while (ex.hasNext()) {
					InvoiceRetLines fm1 = (InvoiceRetLines) ex.next();
					this.invTrans.addNew();
					this.invTrans.item.setTransactionId(this.invTrans.getItem().getNextTransactionId());
					this.invTrans.item.setTransactionCode("INVRET");
					this.invTrans.item.setItemCode(fm1.getItemCode());
					this.invTrans.item.setItemDesc(fm1.getItemDesc());
					this.invTrans.item.setQty(fm1.getQty());
					this.invTrans.item.setUom(fm1.getUom());
					this.invTrans.item.setDirection("+");
					this.invTrans.item.setPricePerUnit(fm1.getSalesPrice());
					this.invTrans.item.setPrice(fm1.getNetAmt());
					this.invTrans.item.setOrderNo(fm1.getInvRetNo());
					this.invTrans.item.setLineNo(fm1.getLineNo());
					this.invTrans.item.setCreatedBy(this.invTrans.getItem().getUserId());
					this.invTrans.item.setArticleNo(fm1.getArticleNo());
					this.invTrans.item.update(con);
					this.stock.addNew();
					this.stock.item.setItemCode(fm1.getItemCode());
					this.stock.item.load(con);
					if (this.stock.item.isIsItemCodeInStock()) {
						this.stock.item.setQtyOnHand(this.stock.item.getQtyOnHand() + fm1.getQty());
						this.stock.item
								.setRemark("InvRet :" + fm1.getInvRetNo() + " on :" + Utils.getStrDate(new Date()));
						this.stock.item.setUpdatedOn(new Date());
						this.stock.item.update(con);
					} else {
						this.stock.addNew();
						this.stock.item.setItemCode(fm1.getItemCode());
						this.stock.item.setItemDesc(fm1.getItemDesc());
						this.stock.item.setArticleNo(fm1.getArticleNo());
						this.stock.item.setSection(fm1.getSection());
						this.stock.item.setSubSection(fm1.getSubSection());
						this.stock.item.setUom(fm1.getUom());
						this.stock.item.setFabric(fm1.getFabric());
						this.stock.item.setRemark(fm1.getRemark());
						this.stock.item.setColor(fm1.getColor());
						this.stock.item.setStyle(fm1.getStyle());
						this.stock.item.setItemSize(fm1.getItemSize());
						this.stock.item.setBrand(fm1.getBrand());
						this.stock.item.setMrp(fm1.getMrp());
						this.stock.item.setWsp(fm1.getSalesPrice());
						this.stock.item.setTaxPerc(fm1.getTaxPer());
						this.stock.item.setTaxAmt(fm1.getTaxAmt());
						this.stock.item.setDiscPerc(fm1.getDiscPer());
						this.stock.item.setDiscAmt(fm1.getDiscAmt());
						this.stock.item.setPurchaseOrder(fm1.getInvoiceNo());
						this.stock.item.setLineNo(Short.valueOf(fm1.getLineNo()));
						this.stock.item.setQty(0.0D + fm1.getQty());
						this.stock.item.setQtyOnHand(0.0D + fm1.getQty());
						this.stock.item.setReceiptDate(new Date());
						this.stock.item.setUpdatedOn(fm1.getUpdatedOn());
						this.stock.item.setCreatedOn(fm1.getCreatedOn());
						this.stock.item.setRemark(
								"Line created on Refund of invoice Ret :" + fm1.getInvRetNo() + "/" + fm1.getLineNo());
						this.stock.item.update(con);
					}
				}

				ex = this.item.getInvRetPayLines().iterator();

				while (ex.hasNext()) {
					InvoiceRetPayDet fm2 = (InvoiceRetPayDet) ex.next();
					if (fm2.getPayModeId() != null && fm2.getPayModeId().equals("REFUND")) {
						this.invRefund.item = new InvoiceRefund();
						this.invRefund.item.setRefundVoucherNo(this.item.getInvRetNo());
						this.invRefund.item.setInvRetNo(this.item.getInvRetNo());
						this.invRefund.item.setRefundValue(fm2.getPayAmt());
						this.invRefund.item.update(con);
					}
				}

				this.item.setStatus("Returned");
				this.item.update(con);
				con.commit();
				con.setAutoCommit(true);
			} catch (SQLException arg18) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg18.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);

				try {
					con.rollback();
				} catch (SQLException arg17) {
					Logger.getLogger(InvoiceRetBean.class.getName()).log(Level.SEVERE, (String) null, arg17);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg16) {
					Logger.getLogger(InvoiceRetBean.class.getName()).log(Level.SEVERE, (String) null, arg16);
				}
			} catch (Exception arg19) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg19.getMessage() + "satyendra ");
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg15) {
					Logger.getLogger(InvoiceRetBean.class.getName()).log(Level.SEVERE, (String) null, arg15);
				}
			}

		}

		return "InvoiceRet";
	}

	public String prepareSalesReturn1() {
		this.addNew();
		this.list.clear();
		this.stock.addNew();
		this.stock.list.clear();
		this.trans.addNew();
		this.trans.list.clear();
		this.getArticleItems().clearFilter();
		this.getArticleItems().getList().clear();
		return "InvoiceRet";
	}
}