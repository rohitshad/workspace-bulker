/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIOutput;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;

import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseOrderBean;
import com.shadowinfosystem.cashwrap.bean.ReportManagerBean;
import com.shadowinfosystem.cashwrap.bean.SubSectionBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.Article;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.Color;
import com.shadowinfosystem.cashwrap.entities.ItemSize;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Size;
import com.shadowinfosystem.cashwrap.entities.TransInLines;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "article")
@SessionScoped
public class ArticleBean {
	private static final String ROOT_TMP_BULKER = "//root//tmp//bulker";
	Article item;
	List<ItemSize> itemSizeList;
	Article filter;
	List<Color> colorList;
	List<Color> selectedColorList;
	List<ItemSize> selectedSizeList;
	List<Article> list;
	String gstRate;
	


	private Set<String> hsnCode;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{subSection}")
	SubSectionBean subSection;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public ArticleBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.filter == null) {
			this.filter = new Article();
		}

		if (this.item == null) {
			this.item = new Article();
		}
		if(itemSizeList == null){
			  Connection con = Data.getConnection();
			  ItemSize itemSize = new ItemSize();
			  try {
				itemSizeList =  itemSize.getList(con);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		if(colorList == null){
			  Connection con = Data.getConnection();
			  Color color = new Color();
			  try {
				  colorList =  color.getList(con);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	
	public Article getItem() {
		
		if (this.item.getScreen() == null) {
			this.item.setScreen("Article");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public void subjectSelectionChanged(final AjaxBehaviorEvent event)  {
		String selectedValue = (String) ((UIOutput)event.getSource()).getValue();
		File file = new File(ROOT_TMP_BULKER);
		try {
			URL[]  urls={file.toURI().toURL()};
			ClassLoader loader = new URLClassLoader(urls);
			ResourceBundle bundle = ResourceBundle.getBundle("hsn", Locale.getDefault(), loader);
			gstRate = bundle.getString(selectedValue);
			item.setTax_per(Double.parseDouble(gstRate));
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		
	}
	public Set<String> getHsnCode() {
		File file = new File(ROOT_TMP_BULKER);
		Set<String> keys = new HashSet<String>();
		try {
			URL[]  urls={file.toURI().toURL()};
			ClassLoader loader = new URLClassLoader(urls);
			ResourceBundle bundle = ResourceBundle.getBundle("hsn", Locale.getDefault(), loader);
			keys = bundle.keySet();
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		return keys;
	}
	
	public String getGstRate() {
		return gstRate;
	}

	public void setGstRate(String gstRate) {
		this.gstRate = gstRate;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public SubSectionBean getSubSection() {
		return this.subSection;
	}

	public void setSubSection(SubSectionBean subSection) {
		this.subSection = subSection;
	}

	public void setItem(Article item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public List<Article> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.search();
			} catch (SQLException arg1) {
				Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, (String) null, arg1);
			}
		}

		return this.list;
	}

	public void setList(List<Article> list) {
		this.list = list;
	}

	public Article getFilter() {
		return this.filter;
	}

	public void setFilter(Article filter) {
		this.filter = filter;
	}

	public String rePreapreSubSecList() {
		this.getSubSection().getItem().setSectionNo(this.item.getArticle_group());
		this.getSubSection().setListSelectOne(this.getSubSection().getSubSecList());
		return "Article";
	}

	public String rePreapreFilterSubSecList() {
		this.getSubSection().getItem().setSectionNo(this.filter.getArticle_group());
		this.getSubSection().setListSelectOne(this.getSubSection().getSubSecList());
		return "Article";
	}

	public String commit() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			if (this.item.getInventory_uom() == null || this.item.getInventory_uom().length() <= 0) {
				throw new Exception("Please Enter Unit of measurement.");
			}

			con.setAutoCommit(false);
			if (this.item.getArticleNo() == null || this.item.getArticleNo().length() == 0) {
				this.trans.item.setPrefix((String) null);
				this.trans.item.setTransType("ARTICLE");
				this.item.setArticleNo(this.trans.getNextOrderNo());
			}
			
			List<String> colors =  new ArrayList<String>();
			Iterator itr =selectedColorList.iterator();
			 while(itr.hasNext()){
				 String clr= (String) itr.next();
				 colors.add(clr.replace("color=", ""));
			 }
			item.setColor(colors);
			
			List<String> sizes =  new ArrayList<String>();
			 itr =selectedSizeList.iterator();
			 while(itr.hasNext()){
				 String size= (String) itr.next();
				 sizes.add(size.replace("itemSize=", ""));
			 }
			item.setArticle_size(sizes);
			
			

		
			
			String ex = this.item.update(con);
			con.commit();
			con.setAutoCommit(true);
			this.item.load(con);
			this.rePreapreSubSecList();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(ex);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg6) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg6.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.rollback();
					con.close();
				} catch (SQLException arg5) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg5);
				}
			}
		} catch (Exception arg7) {
			Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, (String) null, arg7);
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg7.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.rollback();
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Article";
	}

	public String delete() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.delete(con);
			this.addNew();
			this.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Article";
	}

	public String addNew() {
		this.item = new Article();
		return "Article";
	}

	public String load(String article_no) {
		Connection con = Data.getConnection();
		this.item.setArticleNo(article_no);

		try {
			this.item.load(con);
			selectedColorList = this.item.getSelectedColorList();
			selectedSizeList  = this.item.getSelectedSizeList();
			this.rePreapreSubSecList();
		} catch (Exception arg4) {
			Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(arg4.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Article";
	}

	public String addNewItem() {
		if (this.getItem().getArticleNo() != null && this.getItem().getArticleNo().length() > 0) {
			DecimalFormat df1 = new DecimalFormat("#000");
			ArticleItems temp = new ArticleItems();
			temp.setIsEditable(true);
			temp.setArticle_no(this.getItem().getArticleNo());
			temp.setItem_code(this.getItem().getArticleNo() + "-"
					+ df1.format((long) (this.getItem().getItemsList().size() + 1)));
			temp.setStatus("Active");
			this.getItem().getItemsList().add(temp);
			return "Article";
		} else {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage df = new FacesMessage(UserMessages.getMessage("Please select article"));
			df.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, df);
			return "Article";
		}
	}

	public String deleteItemCode(ArticleItems itemToDelete) {
		Connection con = Data.getConnection();
		String msg = "";

		FacesMessage fm;
		try {
			if (itemToDelete.getObject_id() <= 0.0D) {
				this.getItem().getItemsList().remove(itemToDelete);
				msg = "Item Deleted";
			} else {
				msg = itemToDelete.delete(con);
				this.getItem().getItemsList().remove(itemToDelete);
			}

			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage ex = new FacesMessage(msg);
			ex.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, ex);
		} catch (SQLException arg7) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg7.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg6) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg6);
				}
			}
		} catch (Exception arg8) {
			Logger.getLogger(ArticleBean.class.getName()).log(Level.SEVERE, (String) null, arg8);
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg8.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Article";
	}

	public String editItemCode(ArticleItems itemToDelete) {
		itemToDelete.setIsEditable(true);
		return "Article";
	}

	public String setSPandWSP(ArticleItems itemSelected) {
		itemSelected
			.setSales_price((itemSelected.getMrp() - (itemSelected.getMrp() * this.getItem().getDisc_per() / 100.0D))/(100+this.getItem().getTax_per())*100);
		itemSelected.setWsp((itemSelected.getMrp() - (itemSelected.getMrp() * this.getItem().getDisc_per() / 100.0D))/(100+this.getItem().getTax_per())*100);
		return "Article";
	}

	public String search() throws SQLException {
		Connection con = Data.getConnection();
		this.list.clear();
		this.setList(this.getFilter().search(con));
		return "ArticleList";
	}

	public String searchOnScreen() throws SQLException {
		Connection con = Data.getConnection();
		this.list.clear();

		this.setList(this.getFilter().search(con));
		if (this.getList().size() == 1) {
			this.load(((Article) this.getList().get(0)).getArticleNo());
			return "Article";
		} else if (this.getList().isEmpty()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage("No Match Found."));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "Article";
		} else {
			return "ArticleList";
		}
	}

	public String resetFilter() throws SQLException {
		Connection con = Data.getConnection();
		this.setFilter(new Article());
		this.list.clear();
		this.setList(this.getFilter().search(con));
		return "ArticleList";
	}

	public String callBarCodePorc(String itmCode, double qty) {
		Connection con = Data.getConnection();
		CallableStatement cs = null;

		try {
			cs = con.prepareCall("{call generate_bar_code_from_article(?,?)}");
			cs.setString(1, itmCode);
			cs.setDouble(2, qty);
			cs.executeUpdate();
			cs.close();
			con.commit();
		} catch (Exception arg15) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg15.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg14) {
					;
				}
			}

		}

		return "Article";
	}
	

	public List<ItemSize> getItemSizeList() {
		return itemSizeList;
	}


	public void setItemSizeList(List<ItemSize> itemSizeList) {
		this.itemSizeList = itemSizeList;
	}

	public List<Color> getColorList() {
		return colorList;
	}


	public void setColorList(List<Color> colorList) {
		this.colorList = colorList;
	}


	public String printBarCode(String itmCode, double qty) {
		FacesMessage fm;
		try {
			this.callBarCodePorc(itmCode, qty);
			this.rep.setJasperFile("BarCode5025");
			this.rep.setParameters(new HashMap());
			this.rep.exportToPdf();
		} catch (JRException arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg6) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg6.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Article";
	}


	public List<Color> getSelectedColorList() {
		return selectedColorList;
	}


	public void setSelectedColorList(List<Color> selectedColorList) {
		this.selectedColorList = selectedColorList;
	}


	public List<ItemSize> getSelectedSizeList() {
		return selectedSizeList;
	}


	public void setSelectedSizeList(List<ItemSize> selectedSizeList) {
		this.selectedSizeList = selectedSizeList;
	}



	
	

}