/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.ItemInStock;
import com.shadowinfosystem.cashwrap.entities.MasterParameters;
import com.shadowinfosystem.cashwrap.entities.PurchaseOrderRetLines;
import com.shadowinfosystem.cashwrap.entities.PurchaseOrderReturn;
import com.shadowinfosystem.cashwrap.entities.PurchaseReceipt;
import com.shadowinfosystem.cashwrap.entities.PurchaseReceiptLines;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.Section;
import com.shadowinfosystem.cashwrap.entities.SubSection;
import com.shadowinfosystem.cashwrap.entities.Supplier;
import com.shadowinfosystem.cashwrap.entities.TransactionPrefix;

@ManagedBean(name="por")
@SessionScoped
public class PurchaseOrderReturnBean
{
  PurchaseOrderReturn item;
  List<PurchaseOrderReturn> list;
  @ManagedProperty("#{stock}")
  StockBean stock;
  @ManagedProperty("#{articleItems}")
  ArticleItemsBean articleItems;
  @ManagedProperty("#{section}")
  SectionBean section;
  @ManagedProperty("#{masterParameter}")
  MasterParameterBean mastParam;
  @ManagedProperty("#{subSection}")
  SubSectionBean subSection;
  @ManagedProperty("#{trans}")
  InventoryTransactionBean invTrans;
  @ManagedProperty("#{transactionPrefix}")
  TransactionPrefixBean trans;
  @ManagedProperty("#{login}")
  LoginBean login;
  @ManagedProperty("#{pr}")
  PurchaseReceiptBean pr;
  boolean isSelected = false;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public PurchaseOrderReturnBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.item == null) {
      this.item = new PurchaseOrderReturn();
    }
  }
  
  public PurchaseOrderReturn getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("PurchaseOrderReturn");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public MasterParameterBean getMastParam()
  {
    return this.mastParam;
  }
  
  public void setMastParam(MasterParameterBean mastParam)
  {
    this.mastParam = mastParam;
  }
  
  public ArticleItemsBean getArticleItems()
  {
    return this.articleItems;
  }
  
  public void setArticleItems(ArticleItemsBean articleItems)
  {
    this.articleItems = articleItems;
  }
  
  public PurchaseReceiptBean getPr()
  {
    return this.pr;
  }
  
  public void setPr(PurchaseReceiptBean pr)
  {
    this.pr = pr;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public TransactionPrefixBean getTrans()
  {
    return this.trans;
  }
  
  public boolean isIsSelected()
  {
    return this.isSelected;
  }
  
  public void setIsSelected(boolean isSelected)
  {
    this.isSelected = isSelected;
  }
  
  public void setTrans(TransactionPrefixBean trans)
  {
    this.trans = trans;
  }
  
  public void setItem(PurchaseOrderReturn item)
  {
    this.item = item;
  }
  
  public StockBean getStock()
  {
    return this.stock;
  }
  
  public void setStock(StockBean stock)
  {
    this.stock = stock;
  }
  
  public SectionBean getSection()
  {
    return this.section;
  }
  
  public void setSection(SectionBean section)
  {
    this.section = section;
  }
  
  public SubSectionBean getSubSection()
  {
    return this.subSection;
  }
  
  public void setSubSection(SubSectionBean subSection)
  {
    this.subSection = subSection;
  }
  
  public FacesContext getFacesContext()
  {
    return this.facesContext;
  }
  
  public void setFacesContext(FacesContext facesContext)
  {
    this.facesContext = facesContext;
  }
  
  public List<PurchaseOrderReturn> getList()
    throws SQLException
  {
    if ((this.list == null) || (this.list.isEmpty()))
    {
      Connection con = Data.getConnection();
      setList(this.item.getList(con));
      con.close();
    }
    return this.list;
  }
  
  public void setList(List<PurchaseOrderReturn> list)
  {
    this.list = list;
  }
  
  public InventoryTransactionBean getInvTrans()
  {
    return this.invTrans;
  }
  
  public void setInvTrans(InventoryTransactionBean invTrans)
  {
    this.invTrans = invTrans;
  }
  
  public String reProcessSelected()
  {
    PurchaseOrderRetLines line = this.item.getPoRetLineSelected();
    line.setDiscAmt(Double.valueOf(line.getGrossPp().doubleValue() * line.getQty().doubleValue() * line.getDiscPerc().doubleValue() / 100.0D));
    line.setTaxAmt(line.getGrossPp().doubleValue() * line.getQty().doubleValue() * line.getTaxPerc() / 100.0D);
    line.setPurPrice(Double.valueOf(line.getGrossPp().doubleValue() - line.getDiscPerc().doubleValue() / 100.0D + line.getTaxPerc() / 100.0D));
    line.setAmount(Double.valueOf(line.getQty().doubleValue() * line.getGrossPp().doubleValue() - line.getDiscAmt().doubleValue() + line.getTaxAmt()));
    if (line.getPurPrice().doubleValue() > 0.0D)
    {
      line.setMpPer(Double.valueOf((line.getMrp().doubleValue() - line.getPurPrice().doubleValue()) * 100.0D / line.getPurPrice().doubleValue()));
      line.setWspPer(Double.valueOf((line.getWsp().doubleValue() - line.getPurPrice().doubleValue()) * 100.0D / line.getPurPrice().doubleValue()));
    }
    return "success";
  }
  
  public String updateItem()
  {
    PurchaseOrderRetLines pl = this.item.getPoRetLineSelected();
    if (pl.getQty().doubleValue() <= 0.0D)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Qty Must be > 0.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "PurchaseOrderReturn";
    }
    this.item.setPoRetLineSelected(new PurchaseOrderRetLines());
    this.item.getPoRetLineSelected().setPoRetNo(this.item.getPoRetNo());
    return "PurchaseOrderReturn";
  }
  
  public String getPoRetList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.getList(con));
    
    return "PurchaseOrderRetList";
  }
  
  public String setSlectedDetail(PurchaseOrderRetLines line)
  {
    this.item.setPoRetLineSelected(line);
    return "PurchaseOrderReturn";
  }
  
  public String DeletDetail(PurchaseOrderRetLines line)
    throws SQLException
  {
    Connection con = Data.getConnection();
    if ((line.getItemCode() != null) && (line.getItemCode().length() > 0)) {
      line.delete(con);
    }
    con.close();
    this.item.getPoRetLines().remove(line);
    
    return "PurchaseOrderReturn";
  }
  
  public String setSupplier()
    throws SQLException
  {
    ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
    SupplierBean sup = (SupplierBean)exctx.getSessionMap().get("supp");
    sup.load(this.item.getSupplierId());
    
    this.item.setSupplierName(sup.item.getSuppName());
    this.item.setSupplierAddress(sup.item.getAddress());
    return "success";
  }
  
  public String getSubSecList()
  {
    ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
    SubSectionBean ss = (SubSectionBean)exctx.getSessionMap().get("subSection");
    ss.getItem().setSectionNo(this.item.getPoRetLineSelected().getSection());
    ss.getSubSecList();
    return "success";
  }
  
  public String newItem()
  {
    this.item = new PurchaseOrderReturn();
    return "PurchaseOrderReturn";
  }
  
  public String load(String poretno)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setPoRetNo(poretno);
    this.item.load(con);
    return "PurchaseOrderReturn";
  }
  
  public String resetSelected()
  {
    this.item.setPoRetLineSelected(new PurchaseOrderRetLines());
    this.articleItems.clearFilter();
    this.articleItems.getList().clear();
    this.isSelected = false;
    return "PurchaseOrderReturn";
  }
  
  public String setAndSearchItems(String itm_Code)
  {
    this.item.getPoRetLineSelected().setItemCode(itm_Code);
    searchItems();
    return "success";
  }
  
  public String setItemCode(String itmcode)
  {
    this.articleItems.list.clear();
    this.item.getPoRetLineSelected().setItemCode(itmcode);
    selectItem();
    return "PurchaseOrderReturn";
  }
  
  public String searchItems()
  {
    this.articleItems.clearFilter();
    this.articleItems.getList().clear();
    setIsSelected(false);
    this.articleItems.getFilter().setSearch_text(this.item.getPoRetLineSelected().getSearch_text());
    this.articleItems.getFilter().setItem_code(this.item.getPoRetLineSelected().getItemCode());
    this.articleItems.getFilter().setArticle_no(this.item.getPoRetLineSelected().getArticleNo());
    this.articleItems.getFilter().setArticle_desc(this.item.getPoRetLineSelected().getItemDesc());
    this.articleItems.getFilter().setBar_code(this.item.getPoRetLineSelected().getBarCode());
    
    this.articleItems.getList();
    if (this.articleItems.getList().size() == 1)
    {
      this.item.getPoRetLineSelected().setItemCode(((ArticleItems)this.articleItems.getList().get(0)).getItem_code());
      selectItem();
    }
    if (this.articleItems.getList().size() > 1) {
      return "PurchaseOrderReturn";
    }
    return "PurchaseOrderReturn";
  }
  
  public String selectItem()
  {
    try
    {
      PurchaseOrderRetLines ils = this.item.getPoRetLineSelected();
      this.articleItems.clearFilter();
      this.articleItems.getFilter().setItem_code(ils.getItemCode());
      this.articleItems.getList();
      if (this.articleItems.getList().size() == 1) {
        this.articleItems.setItem((ArticleItems)this.articleItems.getList().get(0));
      }
      this.stock.addNew();
      this.stock.load(ils.getItemCode());
      if ((this.mastParam.item.getIsBillOnZeroBal() != null) && (this.mastParam.item.getIsBillOnZeroBal().equals("No")))
      {
        if (this.stock.getItem().getQtyOnHand() <= ils.getQty().doubleValue())
        {
          this.facesContext = FacesContext.getCurrentInstance();
          FacesMessage fm = new FacesMessage("Sufficient Stock not available.");
          fm.setSeverity(FacesMessage.SEVERITY_ERROR);
          this.facesContext.addMessage(null, fm);
          return "PurchaseOrderReturn";
        }
        ils.setBatch(this.stock.getItem().getBatch());
        ils.setExpDate(this.stock.getItem().getExpDate());
      }
      double secTax = 0.0D;
      double subSecTax = 0.0D;
      double secDisc = 0.0D;
      double subSecPerc = 0.0D;
      
      double tax = 0.0D;
      double disc = 0.0D;
      if (this.articleItems.item.getArticle_group() != null)
      {
        this.section.load(this.articleItems.item.getArticle_group());
        secTax = this.section.item.getTaxPerc();
        secDisc = this.section.item.getDiscPerc();
      }
      if ((this.articleItems.item.getArticle_sub_group() != null) && (this.articleItems.item.getArticle_group() != null))
      {
        this.subSection.load(this.articleItems.item.getArticle_group(), this.articleItems.item.getArticle_group());
        subSecTax = this.subSection.item.getTaxPerc();
        subSecPerc = this.subSection.item.getDiscPerc();
      }
      if (this.articleItems.item.getTax_per() > 0.0D) {
        tax = this.articleItems.item.getTax_per();
      } else if (subSecTax > 0.0D) {
        tax = subSecTax;
      } else {
        tax = secTax;
      }
      if (subSecPerc > 0.0D) {
        disc = subSecPerc;
      } else {
        disc = secDisc;
      }
      ils.setMrp(Double.valueOf(this.articleItems.item.getMrp()));
      ils.setWsp(Double.valueOf(this.articleItems.item.getSales_price()));
      ils.setGrossPp(Double.valueOf(this.articleItems.item.getPur_price()));
      ils.setPurPrice(Double.valueOf(this.articleItems.item.getPur_price()));
      ils.setTaxPerc(tax);
      ils.setTaxAmt(this.articleItems.item.getSales_price() * tax / 100.0D);
      ils.setDiscPerc(Double.valueOf(disc));
      ils.setDiscAmt(Double.valueOf(this.articleItems.item.getSales_price() * disc / 100.0D));
      ils.setQtyOnHand(this.stock.getItem().getQtyOnHand());
      ils.setQty(Double.valueOf(1.0D));
      ils.setUom(this.articleItems.item.getInventory_uom());
      ils.setAmount(Double.valueOf(ils.getQty().doubleValue() * ils.getPurPrice().doubleValue() - ils.getDiscAmt().doubleValue() + ils.getTaxAmt()));
      
      ils.setSubSection(this.articleItems.item.getArticle_sub_group());
      ils.setSection(this.articleItems.item.getArticle_group());
      
      ils.setColor(this.articleItems.item.getColor());
      ils.setBarCode(this.articleItems.item.getBar_code());
      ils.setBrand(this.articleItems.item.getManufacturer());
      ils.setArticleNo(this.articleItems.item.getArticle_no());
      ils.setItemSize(this.articleItems.item.getArticle_size());
      ils.setItemDesc(this.articleItems.item.getArticle_desc());
      setIsSelected(true);
      
      reProcessSelected();
    }
    catch (SQLException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
    }
    return "success";
  }
  
  public String addItem()
  {
    PurchaseOrderRetLines pl = this.item.getPoRetLineSelected();
    if (pl.getQty().doubleValue() <= 0.0D)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Qty Must be > 0.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "PurchaseOrderReturn";
    }
    if (!isIsSelected())
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Please select Item.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "PurchaseOrderReturn";
    }
    pl.setLineNo(getNextLineNo());
    pl.setPoRetNo(this.item.getPoRetNo());
    this.item.getPoRetLines().add(pl);
    this.item.setPoRetLineSelected(new PurchaseOrderRetLines());
    this.item.getPoRetLineSelected().setPoRetNo(this.item.getPoRetNo());
    setIsSelected(false);
    return "PurchaseOrderReturn";
  }
  
  public String returnPO()
    throws SQLException, IOException
  {
    if ((getItem().getPoRetNo() == null) || (getItem().getPoRetNo().length() <= 0))
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Please select POR to Return.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "PurchaseOrderReturn";
    }
    if ((getItem().getStatus() != null) && (getItem().getStatus().equals("Returned")))
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("This POR is already returned.");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      return "PurchaseOrderReturn";
    }
    Connection con = Data.getConnection();
    CallableStatement cs = null;
    try
    {
      cs = con.prepareCall("{call por_return(?)}");
      cs.setString(1, getItem().getPoRetNo());
      cs.executeUpdate();
      cs.close();
      this.item.load(con);
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("POR Returnd.");
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage() + " ");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      Logger.getLogger(InvoiceBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "PurchaseOrderReturn";
  }
  
  public short getNextLineNo()
  {
    short i = 0;
    i = 0;
    for (PurchaseOrderRetLines pl : getItem().getPoRetLines()) {
      if (pl.getLineNo() > i) {
        i = pl.getLineNo();
      }
    }
    i = (short)(i + 1);
    return i;
  }
  
  public String process()
  {
    this.item.process();
    return "PurchaseOrderReturn";
  }
  
  public String commit()
  {
    process();
    Connection con = Data.getConnection();
    try
    {
      if ((this.item.getPoRetNo() == null) || (this.item.getPoRetNo().trim().length() == 0))
      {
        this.trans.item.setPrefix(null);
        this.trans.item.setTransType("PORET");
        this.item.setPoRetNo(this.trans.getNextOrderNo());
      }
      this.item.process();
      String msg = this.item.update(con);
      this.item.load(con);
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(e.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderReturnBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "PurchaseOrderReturn";
  }
  
  public String addNew()
  {
    setItem(new PurchaseOrderReturn());
    return "PurchaseOrderReturn";
  }
  
  public String search()
  {
    this.list.clear();
    return "PurchaseOrderRetList";
  }
  
  public String refreshList()
  {
    addNew();
    this.list.clear();
    return "PurchaseOrderRetList";
  }
  
  public String preparePurchaseReturn(String pr_no)
  {
    addNew();
    this.list.clear();
    this.articleItems.clearFilter();
    this.articleItems.list.clear();
    this.trans.addNew();
    this.trans.list.clear();
    try
    {
      this.pr.addNew();
      this.pr.load(pr_no);
      addNew();
      
      getItem().setPrNo(this.pr.getItem().getPorNo());
      getItem().setPrDate(this.pr.getItem().getPorDate());
      getItem().setOtherCharges(this.pr.getItem().getOtherCharges());
      getItem().setRoundOff(this.pr.getItem().getRoundOff());
      getItem().setSupplierId(this.pr.getItem().getSupplierId());
      getItem().setSupplierName(this.pr.getItem().getSupplierName());
      getItem().setSupplierAddress(this.pr.getItem().getSupplierAddress());
      getItem().setNetAmt(this.pr.getItem().getNetAmt());
      getItem().setTaxPer(this.pr.getItem().getTaxPer());
      getItem().setTaxAmt(this.pr.getItem().getTaxAmt());
      getItem().setDiscPer(this.pr.getItem().getDiscPer());
      getItem().setDiscAmt(this.pr.getItem().getDiscAmt());
      getItem().setSubTotal(this.pr.getItem().getSubTotal());
      
      PurchaseOrderRetLines porl = null;
      for (PurchaseReceiptLines il : this.pr.item.getPorLines())
      {
        porl = new PurchaseOrderRetLines();
        porl.setLineNo(getNextLineNo());
        porl.setItemCode(il.getItemCode());
        porl.setItemDesc(il.getItemDesc());
        porl.setQty(il.getQty());
        porl.setMrp(il.getMrp());
        porl.setUom(il.getUom());
        porl.setGrossPp(il.getGrossPp());
        porl.setDiscPerc(il.getDiscPerc());
        porl.setDiscAmt(il.getDiscAmt());
        porl.setTaxPerc(il.getTaxPerc());
        porl.setTaxAmt(il.getTaxAmt());
        porl.setItemSize(il.getItemSize());
        porl.setBrand(il.getBrand());
        porl.setArticleNo(il.getArticleNo());
        porl.setSection(il.getSection());
        porl.setSubSection(il.getSubSection());
        porl.setColor(il.getColor());
        porl.setItemSize(il.getItemSize());
        porl.setAmount(il.getAmount());
        getItem().getPoRetLines().add(porl);
      }
    }
    catch (SQLException ex)
    {
      PurchaseOrderRetLines porl;
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex.getMessage());
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
    }
    return "PurchaseOrderReturn";
  }
  
  public String preparePurchaseReturnScreen()
  {
    addNew();
    this.list.clear();
    this.articleItems.clearFilter();
    this.articleItems.list.clear();
    this.trans.addNew();
    this.trans.list.clear();
    return "PurchaseOrderReturn";
  }
}
