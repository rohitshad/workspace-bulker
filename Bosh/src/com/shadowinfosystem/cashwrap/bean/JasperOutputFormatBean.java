/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.util.ArrayList;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import com.shadowinfosystem.cashwrap.entities.JasperFormat;

@ManagedBean(name="jasperOutputFormat")
@SessionScoped
public class JasperOutputFormatBean
{
  List<JasperFormat> list;
  
  public JasperOutputFormatBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
  }
  
  public List<JasperFormat> getList()
  {
    if (this.list.isEmpty())
    {
      JasperFormat temp = new JasperFormat("pdf", "pdf");
      this.list.add(temp);
      temp = new JasperFormat("xlsx", "xlsx");
      this.list.add(temp);
      temp = new JasperFormat("xml", "xml");
      this.list.add(temp);
      temp = new JasperFormat("docx", "docx");
      this.list.add(temp);
    }
    return this.list;
  }
  
  public void setList(List<JasperFormat> list)
  {
    this.list = list;
  }
}
