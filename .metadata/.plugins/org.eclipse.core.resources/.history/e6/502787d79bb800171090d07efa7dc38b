/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Invoice extends EntitiesImpl implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private String invoiceNo;
	private Date invoiceDate;
	private String invoicePrefix;
	private String customerId;
	private String customerName;
	private String custAddress;
	private String custRemark;
	private double totQty;
	private double totMrp;
	private double totSalsePrice;
	private double totDiscValue;
	private double totalTax;
	private double subTotal;
	private double discPer;
	private double discAmt;
	private double otherCharges;
	private double roundOff;
	private double netValue;
	private String status;
	private double taxPerOnInv;
	private double taxAmtOnInv;
	private Timestamp createdOn;
	private String createdBy;
	private Timestamp updatedOn;
	private String updatedBy;
	private short objectId;
	private String custMob;
	private String custTin;
	private String custMake;
	private String custModel;
	private String custReg;
	private String custNext;
	private String custPresent;
	private String custTech;
	private String custService;
	private List<InvoiceLines> invoiceLines;
	private InvoiceLines invoiceLineSelected;
	private List<InvoicePayDet> invPayLines;
	private List<InvoiceCreditReceiptDet> invCreditRecDet;
	private Date fromDate;
	private Date toDate;
	private double creditAmount;
	private String notes1;
	private String notes2;
	private String notes3;
	private String taxInclusiveExclusive;

	public Invoice() {
		this.invoiceLineSelected = new InvoiceLines();
		if (this.invoiceLines == null) {
			this.invoiceLines = new ArrayList();
		}
		if (this.invPayLines == null) {
			this.invPayLines = new ArrayList();
			InvoicePayDet pd = new InvoicePayDet();
			pd.setPayModeId("CASH");
			this.invPayLines.add(pd);
		}
		if (this.invCreditRecDet == null) {
			this.invCreditRecDet = new ArrayList();
		}
	}

	public Invoice(String invoiceNo) {
		this.invoiceNo = invoiceNo;
		this.invoiceLineSelected = new InvoiceLines();
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public InvoiceLines getInvoiceLineSelected() {
		return this.invoiceLineSelected;
	}

	public void setInvoiceLineSelected(InvoiceLines invoiceLineSelected) {
		this.invoiceLineSelected = invoiceLineSelected;
	}

	public List<InvoiceCreditReceiptDet> getInvCreditRecDet() {
		return this.invCreditRecDet;
	}

	public void setInvCreditRecDet(List<InvoiceCreditReceiptDet> invCreditRecDet) {
		this.invCreditRecDet = invCreditRecDet;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getInvoiceDate() {
		return this.invoiceDate;
	}

	public void setInvoiceDate(Date invoiceDate) {
		this.invoiceDate = invoiceDate;
	}

	public String getInvoicePrefix() {
		return this.invoicePrefix;
	}

	public void setInvoicePrefix(String invoicePrefix) {
		this.invoicePrefix = invoicePrefix;
	}

	public String getCustomerId() {
		return this.customerId;
	}

	public void setCustomerId(String customerId) {
		this.customerId = customerId;
	}

	public String getCustomerName() {
		return this.customerName;
	}

	public void setCustomerName(String customerName) {
		this.customerName = customerName;
	}

	public String getCustAddress() {
		return this.custAddress;
	}

	public void setCustAddress(String custAddress) {
		this.custAddress = custAddress;
	}

	public double getTotQty() {
		return this.totQty;
	}

	public void setTotQty(double totQty) {
		this.totQty = totQty;
	}

	public double getTotMrp() {
		return this.totMrp;
	}

	public void setTotMrp(double totMrp) {
		this.totMrp = totMrp;
	}

	public double getTotDiscValue() {
		return this.totDiscValue;
	}

	public void setTotDiscValue(double totDiscValue) {
		this.totDiscValue = totDiscValue;
	}

	public double getTotalTax() {
		return this.totalTax;
	}

	public void setTotalTax(double totalTax) {
		this.totalTax = totalTax;
	}

	public double getSubTotal() {
		return this.subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}

	public double getDiscPer() {
		return this.discPer;
	}

	public void setDiscPer(double discPer) {
		this.discPer = discPer;
	}

	public double getDiscAmt() {
		return this.discAmt;
	}

	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}

	public double getOtherCharges() {
		return this.otherCharges;
	}

	public void setOtherCharges(double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public double getRoundOff() {
		return this.roundOff;
	}

	public void setRoundOff(double roundOff) {
		this.roundOff = roundOff;
	}

	public double getNetValue() {
		return this.netValue;
	}

	public void setNetValue(double netValue) {
		this.netValue = netValue;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Timestamp getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public double getTotSalsePrice() {
		return this.totSalsePrice;
	}

	public void setTotSalsePrice(double totSalsePrice) {
		this.totSalsePrice = totSalsePrice;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public double getTaxPerOnInv() {
		return this.taxPerOnInv;
	}

	public void setTaxPerOnInv(double taxPerOnInv) {
		this.taxPerOnInv = taxPerOnInv;
	}

	public double getTaxAmtOnInv() {
		return this.taxAmtOnInv;
	}

	public void setTaxAmtOnInv(double taxAmtOnInv) {
		this.taxAmtOnInv = taxAmtOnInv;
	}

	public String getNotes1() {
		return this.notes1;
	}

	public void setNotes1(String notes1) {
		this.notes1 = notes1;
	}

	public String getNotes2() {
		return this.notes2;
	}

	public void setNotes2(String notes2) {
		this.notes2 = notes2;
	}

	public String getNotes3() {
		return this.notes3;
	}

	public void setNotes3(String notes3) {
		this.notes3 = notes3;
	}

	public List<InvoiceLines> getInvoiceLines() {
		return this.invoiceLines;
	}

	public void setInvoiceLines(List<InvoiceLines> invoiceLines) {
		this.invoiceLines = invoiceLines;
	}

	public List<InvoicePayDet> getInvPayLines() {
		return this.invPayLines;
	}

	public void setInvPayLines(List<InvoicePayDet> invPayLines) {
		this.invPayLines = invPayLines;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getCustMob() {
		return this.custMob;
	}

	public String getCustMake() {
		return custMake;
	}

	public void setCustMake(String custMake) {
		this.custMake = custMake;
	}

	public void setCustMob(String custMob) {
		this.custMob = custMob;
	}

	public double getCreditAmount() {
		return this.creditAmount;
	}

	public void setCreditAmount(double creditAmount) {
		this.creditAmount = creditAmount;
	}

	public String getTaxInclusiveExclusive() {
		return this.taxInclusiveExclusive;
	}

	public void setTaxInclusiveExclusive(String taxInclusiveExclusive) {
		this.taxInclusiveExclusive = taxInclusiveExclusive;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update invoice\n   set  \n  invoice_date   = ?,\n      invoice_prefix = ?,\n       customer_id    = ?,\n       customer_name  = ?,\n       cust_address   = ?,\n       tot_qty        = ?,\n       tot_mrp        = ?,\n       tot_disc_value = ?,\n       total_tax      = ?,\n       sub_total      = ?,\n       disc_per       = ?,\n       disc_amt       = ?,\n       other_charges  = ?,\n       round_off      = ?,\n       net_value      = ?,\n       status         = ?,\n       created_on     = ?,\n       created_by     = ?,\n       updated_on     = ?,\n       updated_by     = ?,\n       object_id      = object_id + 1,TOT_SALE_PRICE=?,tax_per_on_inv=?,tax_amt_on_inv=?,CUST_MOB=?, \n notes1=?, notes2=?, notes3=?,tax_inclusive_exclusive=?,\n next   = ?,\n present   = ?,   \n tech   = ?, service   = ?,\n  tin   = ?, remark=?,reg=?, where invoice_no = ? \n   and object_id = ? ";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(this.invoiceDate));
		ps.setString(2, this.invoicePrefix);
		ps.setString(3, this.customerId);
		ps.setString(4, this.customerName);
		ps.setString(5, this.custAddress);
		ps.setDouble(6, this.totQty);
		ps.setDouble(7, this.totMrp);
		ps.setDouble(8, this.totDiscValue);
		ps.setDouble(9, this.totalTax);
		ps.setDouble(10, this.subTotal);
		ps.setDouble(11, this.discPer);
		ps.setDouble(12, this.discAmt);
		ps.setDouble(13, this.otherCharges);
		ps.setDouble(14, this.roundOff);
		ps.setDouble(15, this.netValue);
		ps.setString(16, this.status);
		ps.setTimestamp(17, Utils.getTimestamp(this.createdOn));
		ps.setString(18, this.createdBy);
		ps.setTimestamp(19, Utils.getTimestamp(new Date()));
		ps.setString(20, getUserId());
		ps.setDouble(21, this.totSalsePrice);
		ps.setDouble(22, this.taxPerOnInv);
		ps.setDouble(23, this.taxAmtOnInv);
		ps.setString(24, getCustMob());
		ps.setString(25, getNotes1());
		ps.setString(26, getNotes2());
		ps.setString(27, getNotes3());
		ps.setString(28, getTaxInclusiveExclusive());
		ps.setString(29, getCustNext());
		ps.setString(30, getCustPresent());
		ps.setString(31, getCustTech());
		ps.setString(32, getCustService());
		ps.setString(33, getCustTin());
		ps.setString(34, this.custRemark);
		ps.setString(35, this.custReg);
		ps.setString(36, this.invoiceNo);
		ps.setShort(37, this.objectId);
		int n = ps.executeUpdate();
		for (InvoiceLines il : this.invoiceLines) {
			il.setInvoiceNo(this.invoiceNo);
			il.setUpdatedBy(getUpdatedBy());
			il.update(con);
		}
		int count=0;
		double amount = 0.0;
		for (InvoicePayDet pd : this.invPayLines) {
			if (pd.getInvoiceNo() == null) {
				pd.setInvoiceNo(this.invoiceNo);
			}
			amount += pd.getCashTAmt();
			pd.update(con);
		}
		ps.close();
		
		if(amount >= this.netValue){
		this.status = "Delivered";
		}else{
			this.status = "Pending";
		}
		
		 sql = "update invoice\n   set  \n      status         = ? where invoice_no = ? \n   and object_id = ? ";

		 ps = con.prepareStatement(sql);

		ps.setString(1, this.status);
		ps.setString(2, this.invoiceNo);
		ps.setShort(3, this.objectId);
		ps.executeUpdate();
		ps.close();
		return "Record Updated";
	}

	public String create(Connection con) throws SQLException, Exception {
		if (getInvoiceDate() == null) {
			setInvoiceDate(new Date());
		}
		String sql = "insert into invoice\n  (INVOICE_NO,\n   INVOICE_DATE,\n  INVOICE_PREFIX,\n   CUSTOMER_ID,\n   CUSTOMER_NAME,\n   CUST_ADDRESS,\n   TOT_QTY,\n   TOT_MRP,\n   TOT_DISC_VALUE,\n   TOTAL_TAX,\n   SUB_TOTAL,\n   DISC_PER,\n   DISC_AMT,\n   OTHER_CHARGES,\n   ROUND_OFF,\n   NET_VALUE,\n   STATUS,\n   CREATED_ON,\n   CREATED_BY,\n   UPDATED_ON,\n   UPDATED_BY,TOT_SALE_PRICE,TAX_PER_ON_INV,\n TAX_AMT_ON_INV,cust_mob,notes1, notes2, notes3,tax_inclusive_exclusive,NEXT,\n PRESENT,\n SERVICE,\n TECH,\n TIN,\n REMARK,\n REG )\nvalues\n  (?,?,?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?,?)";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invoiceNo);
		ps.setDate(2, Utils.getSqlDate(this.invoiceDate));
		ps.setString(3, this.invoicePrefix);
		ps.setString(4, this.customerId);
		ps.setString(5, this.customerName);
		ps.setString(6, this.custAddress);
		ps.setDouble(7, this.totQty);
		ps.setDouble(8, this.totMrp);
		ps.setDouble(9, this.totDiscValue);
		ps.setDouble(10, this.totalTax);
		ps.setDouble(11, this.subTotal);
		ps.setDouble(12, this.discPer);
		ps.setDouble(13, this.discAmt);
		ps.setDouble(14, this.otherCharges);
		ps.setDouble(15, this.roundOff);
		ps.setDouble(16, this.netValue);
		ps.setString(17, this.status);
		ps.setTimestamp(18, Utils.getTimestamp(new Date()));
		ps.setString(19, getUserId());
		ps.setTimestamp(20, Utils.getTimestamp(this.updatedOn));
		ps.setString(21, this.updatedBy);
		ps.setDouble(22, this.totSalsePrice);
		ps.setDouble(23, this.taxPerOnInv);
		ps.setDouble(24, this.taxAmtOnInv);
		ps.setString(25, getCustMob());
		ps.setString(26, getNotes1());
		ps.setString(27, getNotes2());
		ps.setString(28, getNotes3());
		ps.setString(29, getTaxInclusiveExclusive());
		ps.setString(30, getCustNext());
		ps.setString(31, getCustPresent());
		ps.setString(32, getCustService());
		ps.setString(33, getCustTech());
		ps.setString(34, getCustTin());
		ps.setString(35, this.custRemark);
		ps.setString(36, this.custReg);
	
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Could Not Update Header");
		}
		for (InvoiceLines il : this.invoiceLines) {
			il.setInvoiceNo(this.invoiceNo);
			il.update(con);
		}
		for (InvoicePayDet pd : this.invPayLines) {
			pd.setInvoiceNo(this.invoiceNo);
			pd.update(con);
		}
		ps.close();
		return "Record Created.";
	}

	public List<Invoice> getList(Connection con) throws SQLException {
		String sql = "select invoice_no,\n  invoice_date,\n   next, \n REG,\n  tin, \n  present,\n  service,\n  tech,\n REMARK,   invoice_prefix,\n       customer_id,\n       customer_name,\n       cust_address,\n       tot_qty,\n       tot_mrp,\n       tot_disc_value,\n       total_tax,\n       sub_total,\n       disc_per,\n       disc_amt,\n       other_charges,\n       round_off,\n       net_value,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,TOT_SALE_PRICE,tax_per_on_inv,tax_amt_on_inv,cust_mob,notes1,notes2, notes3,tax_inclusive_exclusive\n  from invoice where 1=1 ";

		boolean doFetchData = false;
		if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
			sql = sql + " and upper(invoice_no) like upper('" + getInvoiceNo() + "') ";
			doFetchData = true;
		}
		if ((getCustReg() != null) && (getCustReg().length() > 0)) {
			sql = sql + " and upper(REG) like upper('" + getCustReg() + "') ";
			doFetchData = true;
		}
		if ((getCustomerName() != null) && (getCustomerName().length() > 0)) {
			sql = sql + " and upper(customer_name) like upper('" + getCustomerName() + "') ";
			doFetchData = true;
		}
		if ((getCustAddress() != null) && (getCustAddress().length() > 0)) {
			sql = sql + " and upper(cust_address) like upper('" + getCustAddress() + "') ";
			doFetchData = true;
		}
		if ((getCustMob() != null) && (getCustMob().length() > 0)) {
			sql = sql + " and upper(cust_mob) like upper('" + getCustMob() + "') ";
			doFetchData = true;
		}
		if ((getStatus() != null) && (getStatus().length() > 0)) {
			sql = sql + " and upper(status) like upper('" + getStatus() + "') ";
			doFetchData = true;
		}
		if ((getNotes1() != null) && (getNotes1().length() > 0)) {
			sql = sql + " and upper(notes1) like upper('" + getNotes1() + "') ";
			doFetchData = true;
		}
		if ((getNotes2() != null) && (getNotes2().length() > 0)) {
			sql = sql + " and upper(notes2) like upper('" + getNotes2() + "') ";
			doFetchData = true;
		}
		if ((getNotes3() != null) && (getNotes3().length() > 0)) {
			sql = sql + " and upper(notes3) like upper('" + getNotes3() + "') ";
			doFetchData = true;
		}
		if (this.fromDate != null) {
			sql = sql + " and trunc(invoice_date)  >=  to_date('" + Utils.getStrDate(this.fromDate)
					+ "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		if (this.toDate != null) {
			sql = sql + " and trunc(invoice_date)  <=  to_date('" + Utils.getStrDate(this.toDate) + "','dd/MM/yyyy') ";
			doFetchData = true;
		}
		List<Invoice> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Invoice temp = new Invoice();
			temp.setInvoiceNo(rs.getString("invoice_No"));
			temp.setInvoiceDate(rs.getDate("invoice_Date"));
			temp.setInvoicePrefix(rs.getString("Invoice_Prefix"));
			temp.setCustomerId(rs.getString("customer_Id"));
			temp.setCustomerName(rs.getString("Customer_Name"));
			temp.setCustAddress(rs.getString("Cust_Address"));
			temp.setCustRemark(rs.getString("REMARK"));
			temp.setCustRemark(rs.getString("REG"));
			temp.setTotQty(rs.getDouble("tot_qty"));
			temp.setTotMrp(rs.getDouble("tot_mrp"));
			temp.setTotDiscValue(rs.getDouble("tot_Disc_Value"));
			temp.setTotalTax(rs.getDouble("total_tax"));
			temp.setSubTotal(rs.getDouble("sub_total"));
			temp.setDiscPer(rs.getDouble("disc_per"));
			temp.setDiscAmt(rs.getDouble("disc_Amt"));
			temp.setOtherCharges(rs.getDouble("other_Charges"));
			temp.setRoundOff(rs.getDouble("round_Off"));
			temp.setNetValue(rs.getDouble("net_Value"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getTimestamp("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getTimestamp("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setTotSalsePrice(rs.getDouble("TOT_SALE_PRICE"));
			temp.setTaxAmtOnInv(rs.getDouble("tax_amt_on_inv"));
			temp.setTaxPerOnInv(rs.getDouble("tax_per_on_inv"));
			temp.setCustMob(rs.getString("cust_mob"));
			temp.setCustNext(rs.getString("NEXT"));
			temp.setCustPresent(rs.getString("PRESENT"));
			temp.setCustTech(rs.getString("TECH"));
			temp.setCustService(rs.getString("SERVICE"));
			temp.setCustTin(rs.getString("TIN"));
			temp.setNotes1(rs.getString("notes1"));
			temp.setNotes2(rs.getString("notes2"));
			temp.setNotes3(rs.getString("notes3"));
			temp.setTaxInclusiveExclusive(rs.getString("tax_inclusive_exclusive"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select invoice_no,\n       invoice_date,\n tin,\n  REMARK,\n REG,\n   next,\n  present,\n  service,\n  tech,\n  invoice_prefix,\n       customer_id,\n       customer_name,\n       cust_address,\n       tot_qty,\n       tot_mrp,\n       tot_disc_value,\n       total_tax,\n       sub_total,\n       disc_per,\n       disc_amt,\n       other_charges,\n       round_off,\n       net_value,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,TOT_SALE_PRICE,tax_per_on_inv,tax_amt_on_inv, cust_mob,notes1, notes2, notes3,tax_inclusive_exclusive\n  from invoice \n where invoice_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invoiceNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setInvoiceNo(rs.getString("invoice_No"));
			setInvoiceDate(rs.getDate("invoice_Date"));
			setInvoicePrefix(rs.getString("Invoice_Prefix"));
			setCustomerId(rs.getString("customer_Id"));
			setCustomerName(rs.getString("Customer_Name"));
			setCustAddress(rs.getString("Cust_Address"));
			setCustRemark(rs.getString("REMARK"));
			setCustRemark(rs.getString("REG"));
			setTotQty(rs.getDouble("tot_qty"));
			setTotMrp(rs.getDouble("tot_mrp"));
			setTotDiscValue(rs.getDouble("tot_Disc_Value"));
			setTotalTax(rs.getDouble("total_tax"));
			setSubTotal(rs.getDouble("sub_total"));
			setDiscPer(rs.getDouble("disc_per"));
			setDiscAmt(rs.getDouble("disc_Amt"));
			setOtherCharges(rs.getDouble("other_Charges"));
			setRoundOff(rs.getDouble("round_Off"));
			setNetValue(rs.getDouble("net_Value"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getTimestamp("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getTimestamp("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setTotSalsePrice(rs.getDouble("TOT_SALE_PRICE"));
			setTaxAmtOnInv(rs.getDouble("tax_amt_on_inv"));
			setTaxPerOnInv(rs.getDouble("tax_per_on_inv"));
			setCustMob(rs.getString("cust_mob"));
			setCustNext(rs.getString("NEXT"));
			setCustPresent(rs.getString("PRESENT"));
			setCustTech(rs.getString("TECH"));
			setCustTin(rs.getString("TIN"));
			setCustService(rs.getString("SERVICE"));
			setNotes1(rs.getString("notes1"));
			setNotes2(rs.getString("notes2"));
			setNotes3(rs.getString("notes3"));
			setTaxInclusiveExclusive(rs.getString("tax_inclusive_exclusive"));
		}
		rs.close();
		ps.close();
		setInvoiceLines(InvoiceLines.load(con, this.invoiceNo));
		setInvPayLines(InvoicePayDet.load(con, this.invoiceNo));
		setInvCreditRecDet(InvoiceCreditReceiptDet.load(con, this.invoiceNo));
		return "success";
	}

	public String process(String isTaxInclusive, double extracharge) {
		processDetails(isTaxInclusive, extracharge);
		processPayDetails();
		processHeader(isTaxInclusive);
		return "success";
	}

	public String processHeader(String isTaxInclusive) {
		setDiscAmt(getSubTotal() * getDiscPer() / 100.0D);
		double net_value = getSubTotal() - getDiscAmt()+getTotalTax(); 
		if ((isTaxInclusive != null) && (isTaxInclusive.equals("No"))) {
			setTaxAmtOnInv(net_value * getTaxPerOnInv() / 100.0D);
			net_value = net_value + getTaxAmtOnInv() + getOtherCharges();
		} else {
			double inclusiveTaxFactor = getTaxPerOnInv() / 100.0D / (1.0D + getTaxPerOnInv() / 100.0D);
			setTaxAmtOnInv((getTotSalsePrice() - getDiscAmt()) * inclusiveTaxFactor);

			net_value += getOtherCharges();
		}
		setRoundOff(net_value - (int) net_value);
		setNetValue((int) net_value);

		return "success";
	}

	public String processDetails(String isTaxInclusive, double extracharge) {
		double totAmt = 0.0D;
		double totQty = 0.0D;
		double totWsp = 0.0D;
		double totDisc = 0.0D;
		double totTax = 0.0D;
		double totMrp = 0.0D;
		for (InvoiceLines l : this.invoiceLines) {
			l.process(isTaxInclusive, extracharge);
			totWsp += l.getSalesPrice() * l.getQty();
			totDisc += l.getDiscAmt();
			totTax += l.getTaxAmt();
			totAmt += l.getNetAmt();
			totQty += l.getQty();
			totMrp += l.getMrp() * l.getQty();
		}
		setTotQty(totQty);
		setSubTotal(totAmt);
		setTotDiscValue(totDisc);
		setTotalTax(totTax);
		setTotMrp(totMrp);
		setTotSalsePrice(totWsp);
		return "success";
	}

	public String processPayDetails() {
		double payValue = getNetValue();
		InvoicePayDet pdl = new InvoicePayDet();
		for (Iterator<InvoicePayDet> it = getInvPayLines().iterator(); it.hasNext();) {
			InvoicePayDet pd = (InvoicePayDet) it.next();
			payValue -= pd.getCashTAmt();
			pdl = pd;
		}
		pdl.setPayAmt(payValue);
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.invoiceNo != null ? this.invoiceNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Invoice)) {
			return false;
		}
		Invoice other = (Invoice) object;
		if (((this.invoiceNo == null) && (other.invoiceNo != null))
				|| ((this.invoiceNo != null) && (!this.invoiceNo.equals(other.invoiceNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "invoiceNo=" + this.invoiceNo + "";
	}

	public String getCustModel() {
		return custModel;
	}

	public void setCustModel(String custModel) {
		this.custModel = custModel;
	}

	public String getCustReg() {
		return custReg;
	}

	public void setCustReg(String custReg) {
		this.custReg = custReg;
	}

	public String getCustNext() {
		return custNext;
	}

	public void setCustNext(String custNext) {
		this.custNext = custNext;
	}

	public String getCustPresent() {
		return custPresent;
	}

	public void setCustPresent(String custPresent) {
		this.custPresent = custPresent;
	}

	public String getCustTech() {
		return custTech;
	}

	public void setCustTech(String custTech) {
		this.custTech = custTech;
	}

	public String getCustService() {
		return custService;
	}

	public void setCustService(String custService) {
		this.custService = custService;
	}

	public String getCustTin() {
		return custTin;
	}

	public void setCustTin(String custTin) {
		this.custTin = custTin;
	}

	public String getCustRemark() {
		return custRemark;
	}

	public void setCustRemark(String custRemark) {
		this.custRemark = custRemark;
	}

	
}
