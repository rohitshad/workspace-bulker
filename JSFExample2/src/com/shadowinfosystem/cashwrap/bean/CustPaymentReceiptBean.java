/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.AccountTransactionBean;
import com.shadowinfosystem.cashwrap.bean.CustomerBean;
import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseOrderBean;
import com.shadowinfosystem.cashwrap.bean.TransactionPrefixBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.CustPaymentReceipt;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "custPayRec")
@SessionScoped
public class CustPaymentReceiptBean {
	CustPaymentReceipt item;
	List<CustPaymentReceipt> list;
	@ManagedProperty("#{accTrans}")
	AccountTransactionBean accTrans;
	FacesContext facesContext = FacesContext.getCurrentInstance();
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{login}")
	LoginBean login;

	public CustPaymentReceiptBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new CustPaymentReceipt();
		}

	}

	public CustPaymentReceipt getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("CustPaymentReceipt");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public AccountTransactionBean getAccTrans() {
		return this.accTrans;
	}

	public void setAccTrans(AccountTransactionBean accTrans) {
		this.accTrans = accTrans;
	}

	public void setItem(CustPaymentReceipt item) {
		this.item = item;
	}

	public List<CustPaymentReceipt> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.getPaymentReceiptList();
			} catch (SQLException arg2) {
				Logger.getLogger(CustPaymentReceiptBean.class.getName()).log(Level.SEVERE, (String) null, arg2);
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg2.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		return this.list;
	}

	public void setList(List<CustPaymentReceipt> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			if (this.item.getVoucherNo() == null || this.item.getVoucherNo().length() == 0) {
				if (this.item.getPrefix() == null) {
					throw new Exception("Please select Prefix.");
				}

				this.trans.addNew();
				this.trans.item.setPrefix(this.item.getPrefix());
				this.trans.item.setTransType("CUSTPAYREC");
				this.item.setVoucherNo(this.trans.getNextOrderNo());
			}

			if (this.item.getPayModeId() == null) {
				throw new Exception("Please select Payment Mode");
			}

			String e = this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (SQLException arg6) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg6.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg5) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg5);
				}
			}
		} catch (Exception arg7) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg7.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "CustPaymentReceipt";
	}

	public String addNew() {
		this.item = new CustPaymentReceipt();
		return "CustPaymentReceipt";
	}

	public String load(String voucherNo) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setVoucherNo(voucherNo);
		this.item.load(con);
		return "CustPaymentReceipt";
	}

	public String getPaymentReceiptList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "CustPaymentReceiptList";
	}

	public String search() {
		this.list.clear();
		return "CustPaymentReceiptList";
	}

	public String refresh() {
		this.setItem(new CustPaymentReceipt());
		this.list.clear();
		return "CustPaymentReceiptList";
	}

	public String setCustomer() throws SQLException {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		CustomerBean cust = (CustomerBean) exctx.getSessionMap().get("cust");
		cust.load(this.item.getCustId());
		this.item.setCustName(cust.item.getCustName());
		return "success";
	}

	public String approve() {
		Connection con = Data.getConnection();

		try {
			con.setAutoCommit(false);
			if (this.item.getAccountId() != null && this.item.getAccountId().length() > 0) {
				this.accTrans.addNew();
				this.accTrans.item.setTransCode("CustPayRec");
				this.accTrans.item.setAccountId(this.item.getAccountId());
				this.accTrans.item.setTransMode(this.item.getPayModeId());
				this.accTrans.item.setAmount(this.item.getAmount());
				this.accTrans.item.setDrCr("Cr");
				this.accTrans.item.setRefId(this.item.getVoucherNo());
				this.accTrans.item.setRefDate(Utils.getSqlDate(this.item.getVoucherDate()));
				this.accTrans.item.setBankName(this.item.getBankName());
				this.accTrans.item.setChequeNo(this.item.getChequeNo());
				this.accTrans.item.setChequeDt(this.item.getChequeDt());
				this.accTrans.item.setTransId(this.accTrans.item.getNextTransactionId());
				this.accTrans.item.update(con);
			}

			this.item.setStatus("Approved");
			this.item.update(con);
			con.setAutoCommit(true);
			con.commit();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage ex = new FacesMessage("Receipt Approved.");
			ex.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, ex);
		} catch (Exception arg16) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg16.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);

			try {
				con.rollback();
			} catch (SQLException arg15) {
				Logger.getLogger(CustPaymentReceiptBean.class.getName()).log(Level.SEVERE, (String) null, arg15);
			}

			try {
				con.setAutoCommit(true);
			} catch (SQLException arg14) {
				Logger.getLogger(CustPaymentReceiptBean.class.getName()).log(Level.SEVERE, (String) null, arg14);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg13) {
					Logger.getLogger(CustPaymentReceiptBean.class.getName()).log(Level.SEVERE, (String) null,
							arg13);
				}
			}

		}

		return "CustPaymentReceipt";
	}
}