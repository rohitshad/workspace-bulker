/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.Brand;

@ManagedBean(name = "report")
@SessionScoped
public class ReportPrinterBean {
	Brand item;
	List<Brand> list;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public ReportPrinterBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.item == null) {
			this.item = new Brand();
		}
	}

	public Brand getItem() {
		return this.item;
	}

	public void setItem(Brand item) {
		this.item = item;
	}

	public List<Brand> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getBrandList();
			} catch (SQLException ex) {
				Logger.getLogger(ReportPrinterBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public void setList(List<Brand> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
			con.close();
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(e.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "Brand";
	}

	public String addNew() {
		this.item = new Brand();

		return "Brand";
	}

	public String load(String brand) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setBrand(brand);
		this.item.load(con);
		return "Brand";
	}

	public String getBrandList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "BrandList";
	}
}
