/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.io.PrintStream;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.common.Xls;
import com.shadowinfosystem.cashwrap.entities.Color;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="color")
@SessionScoped
public class ColorBean
{
  Color item;
  List<Color> list;
  List<Color> listSlecteOne;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public ColorBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.listSlecteOne == null) {
      this.listSlecteOne = new ArrayList();
    }
    if (this.item == null) {
      this.item = new Color();
    }
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public Color getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("Color");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(Color item)
  {
    this.item = item;
  }
  
  public String search()
  {
    try
    {
      Connection con = Data.getConnection();
      setList(this.item.search(con));
      con.close();
    }
    catch (SQLException ex)
    {
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "ColorList";
  }
  
  public List<Color> getListSlecteOne()
  {
    if ((this.listSlecteOne == null) || (this.listSlecteOne.isEmpty())) {
      try
      {
        Connection con = Data.getConnection();
        addNew();
        getItem().setColor("%");
        setListSlecteOne(this.item.search(con));
        con.close();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.listSlecteOne;
  }
  
  public void setListSlecteOne(List<Color> listSlecteOne)
  {
    this.listSlecteOne = listSlecteOne;
  }
  
  public List<Color> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      try
      {
        getColorList();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.list;
  }
  
  public void setList(List<Color> list)
  {
    this.list = list;
  }
  
  public String generateXls()
  {
    Xls xls = new Xls();
    try
    {
      xls.getXls(getList());
    }
    catch (IOException ex)
    {
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalArgumentException ex)
    {
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    catch (IllegalAccessException ex)
    {
      Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
    }
    return "ColorList";
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.update(con);
      this.item.load(con);
      this.listSlecteOne.clear();
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Color";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.delete(con);
      addNew();
      this.list.clear();
      this.listSlecteOne.clear();
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Color";
  }
  
  public String addNew()
  {
    this.item = new Color();
    
    return "Color";
  }
  
  public String load(String color)
    throws SQLException
  {
    Connection con = Data.getConnection();
    this.item.setColor(color);
    this.item.load(con);
    return "Color";
  }
  
  public String getColorList()
    throws SQLException
  {
    Connection con = Data.getConnection();
    setList(this.item.search(con));
    return "ColorList";
  }
  
  public String refreshList()
  {
    addNew();
    this.list.clear();
    return "ColorList";
  }
}
