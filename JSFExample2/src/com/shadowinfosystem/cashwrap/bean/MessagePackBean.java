/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.MessagePack;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="messagePack")
@SessionScoped
public class MessagePackBean
{
  MessagePack item;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public MessagePackBean()
  {
    if (this.item == null) {
      this.item = new MessagePack();
    }
  }
  
  @PostConstruct
  public void loadData()
  {
    try
    {
      Connection con = Data.getConnection();
      this.item.load(con);
      con.close();
    }
    catch (SQLException ex)
    {
      Logger.getLogger(MessagePackBean.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
  
  public MessagePack getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("MessagePack");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public void setItem(MessagePack item)
  {
    this.item = item;
  }
  
  public String preparePage()
  {
    return "MessagePack";
  }
  
  public String sendMessage()
  {
    String resp = this.item.sendMessage();
    this.facesContext = FacesContext.getCurrentInstance();
    FacesMessage fm = new FacesMessage("Message response :" + resp);
    fm.setSeverity(FacesMessage.SEVERITY_INFO);
    this.facesContext.addMessage(null, fm);
    return "MessagePack";
  }
  
  public String sendDirectMessage()
  {
    String resp = this.item.sendMessage();
    return resp;
  }
  
  public void commit()
  {
    try
    {
      Connection con = Data.getConnection();
      this.item.update(con);
      con.close();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage("Record Updated.");
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
    }
    catch (SQLException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(ex + "");
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      Logger.getLogger(MessagePackBean.class.getName()).log(Level.SEVERE, null, ex);
    }
  }
}
