/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseOrderBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.Employee;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "emp")
@SessionScoped
public class EmployeeBean {
	Employee item;
	List<Employee> list;
	List<Employee> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public EmployeeBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}

		if (this.item == null) {
			this.item = new Employee();
		}

	}

	public Employee getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("Employee");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public void setItem(Employee item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<Employee> getListSelectOne() {
		if (this.listSelectOne.isEmpty()) {
			Connection con = Data.getConnection();

			try {
				Employee ex = new Employee();
				ex.setEmpId("%");
				this.setListSelectOne(ex.getList(con));
			} catch (SQLException arg10) {
				Logger.getLogger(EmployeeBean.class.getName()).log(Level.SEVERE, (String) null, arg10);
			} finally {
				if (con != null) {
					try {
						con.close();
					} catch (SQLException arg9) {
						;
					}
				}

			}
		}

		return this.listSelectOne;
	}

	public void setListSelectOne(List<Employee> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public List<Employee> getList() {
		if (this.list == null || this.list.isEmpty()) {
			try {
				this.getEmployeeList();
			} catch (SQLException arg1) {
				Logger.getLogger(EmployeeBean.class.getName()).log(Level.SEVERE, (String) null, arg1);
			}
		}

		return this.list;
	}

	public void setList(List<Employee> list) {
		this.list = list;
	}

	public String commit() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			if (this.item.getEmpId().equals(this.item.getSupervisor())) {
				throw new Exception("Supervise must be different employee.");
			}

			String e = this.item.update(con);
			this.item.load(con);
			con.close();
			this.listSelectOne.clear();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg5);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(EmployeeBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Employee";
	}

	public String delete() {
		Connection con = Data.getConnection();

		FacesMessage fm;
		try {
			String e = this.item.delete(con);
			this.addNew();
			con.close();
			this.list.clear();
			this.listSelectOne.clear();
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(e);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, fm);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(UserMessages.getMessage(arg5.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg5);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "Employee";
	}

	public String addNew() {
		this.item = new Employee();
		return "Employee";
	}

	public String load(String emp_id) throws SQLException {
		Connection con = Data.getConnection();
		this.addNew();
		this.item.setEmpId(emp_id);
		this.item.load(con);
		return "Employee";
	}

	public String getEmployeeList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "EmployeeList";
	}

	public String search() {
		this.list.clear();
		return "EmployeeList";
	}

	public String refresh() {
		this.getListSelectOne().clear();
		this.setItem(new Employee());
		this.list.clear();
		return "EmployeeList";
	}
}