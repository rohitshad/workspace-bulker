/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "salesReport")
@SessionScoped
public class SalesReportBean extends EntitiesImpl {
	private String invoiceNo;
	private Date fromDate;
	private Date toDate;
	private String outputFormat;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	@PostConstruct
	public void initPageSecurity() {
		if (getScreen() == null) {
			setScreen("SalesReport");
			ScreenRights temp = this.login.getRightsForScreen(getScreen());
			setUserId(this.login.getUserid());
			setAddAddowed(temp.isAddAddowed());
			setViewAddowed(temp.isViewAddowed());
			setModifyAddowed(temp.isModifyAddowed());
			setDeleteAddowed(temp.isDeleteAddowed());
			setPostAddowed(temp.isPostAddowed());
			setPrintAddowed(temp.isPrintAddowed());
		}
	}

	public String prepareReportPage() {
		setInvoiceNo(null);
		setFromDate(new Date());
		setToDate(new Date());
		return "SalesReport";
	}

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public String getOutputFormat() {
		if (this.outputFormat == null) {
			return "pdf";
		}
		return this.outputFormat;
	}

	public void setOutputFormat(String outputFormat) {
		this.outputFormat = outputFormat;
	}

	public String printInvoiceWiseSales() {
		try {
			this.rep.getParameters().clear();
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.setJasperFile("CustomerWiseSales");
			}
			else{
				this.rep.setJasperFile("InvoiceWiseSales");
			}
			
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.getParameters().put("invoice_no", getInvoiceNo());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}

	public String printInvoiceWiseSalesTax() {
		try {
			Connection con = Data.getConnection();

			Integer ResultKey = Integer.valueOf(0);
			try {
				CallableStatement cs = con.prepareCall("{call rep_sales_inv_wise_tax_rpi(?,?)}");
				String parameter_ = "from_date|" + Utils.getStrDate(getFromDate());
				parameter_ = parameter_ + "^" + "to_date|" + Utils.getStrDate(getToDate());

				cs.setString(2, parameter_);
				cs.registerOutParameter(1, 8);
				cs.executeUpdate();
				ResultKey = Integer.valueOf((int) cs.getDouble(1));
			} catch (SQLException ex) {
				Logger.getLogger(StockReportBean.class.getName()).log(Level.SEVERE, null, ex);
			}
			this.rep.getParameters().clear();
			this.rep.getParameters().put("Result_key", ResultKey + "");
			this.rep.setJasperFile("InvoiceWiseSalesTax");
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}

	public String printArticleWiseSales() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("ArticleWiseSalesReport");
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.getParameters().put("invoice_no", getInvoiceNo());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}

	public String printSaleSummary() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("SaleSummary");
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.getParameters().put("invoice_no", getInvoiceNo());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}

	public String printSaleCollectionSummary() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("SaleCollectionSummary");
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.getParameters().put("invoice_no", getInvoiceNo());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}

	public String printSaleTaxSummary() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("TaxSaleSummary");
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.getParameters().put("invoice_no", getInvoiceNo());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}

	public String printSaleTaxInvSummary() {
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("SaleTaxOnInvSummary");
			if ((getInvoiceNo() != null) && (getInvoiceNo().length() > 0)) {
				this.rep.getParameters().put("invoice_no", getInvoiceNo());
			}
			if (getFromDate() != null) {
				this.rep.getParameters().put("FROM_DATE", Utils.getStrDate(getFromDate()));
			}
			if (getToDate() != null) {
				this.rep.getParameters().put("TO_DATE", Utils.getStrDate(getToDate()));
			}
			if (getOutputFormat().equalsIgnoreCase("PDF")) {
				this.rep.exportToPdf();
			}
			if (getOutputFormat().equalsIgnoreCase("XLSX")) {
				this.rep.exportToXlsx();
			}
			if (getOutputFormat().equalsIgnoreCase("DOCX")) {
				this.rep.exportToDocx();
			}
			if (getOutputFormat().equalsIgnoreCase("XML")) {
				this.rep.exportToXml();
			}
		} catch (JRException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		} catch (IOException ex) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(ex.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
		}
		return "SalesReport";
	}
}
