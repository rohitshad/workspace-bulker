/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.bean.LoginBean;
import com.shadowinfosystem.cashwrap.bean.PurchaseOrderBean;
import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.entities.MasterParameters;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "masterParameter")
@SessionScoped
public class MasterParameterBean {
	MasterParameters item;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public MasterParameters getItem() {
		if (this.item == null) {
			this.item = new MasterParameters();
			this.item.setParameterId("MASTER_PARAM");
			Connection temp = Data.getConnection();

			try {
				this.item.load(temp);
			} catch (SQLException arg10) {
				Logger.getLogger(MasterParameterBean.class.getName()).log(Level.SEVERE, (String) null, arg10);
			} finally {
				if (temp != null) {
					try {
						temp.close();
					} catch (SQLException arg9) {
						;
					}
				}

			}
		}

		if (this.item.getScreen() == null) {
			this.item.setScreen("MasterParameters");
			ScreenRights temp1 = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp1.isAddAddowed());
			this.item.setViewAddowed(temp1.isViewAddowed());
			this.item.setModifyAddowed(temp1.isModifyAddowed());
			this.item.setDeleteAddowed(temp1.isDeleteAddowed());
			this.item.setPostAddowed(temp1.isPostAddowed());
			this.item.setPrintAddowed(temp1.isPrintAddowed());
		}

		return this.item;
	}

	public void setItem(MasterParameters item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public String preparePage() {
		return "MasterParameters";
	}

	public String commit() {
		Connection con = Data.getConnection();

		try {
			this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "MasterParameters";
	}
}