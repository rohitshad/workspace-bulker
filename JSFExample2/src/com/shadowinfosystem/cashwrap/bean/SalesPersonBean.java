/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.SalesPerson;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name = "salesPerson")
@SessionScoped
public class SalesPersonBean {
	SalesPerson item;
	List<SalesPerson> list;
	List<SalesPerson> listSelectOne;
	@ManagedProperty("#{login}")
	LoginBean login;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public SalesPersonBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}
		if (this.listSelectOne == null) {
			this.listSelectOne = new ArrayList();
		}
		if (this.item == null) {
			this.item = new SalesPerson();
		}
	}

	public SalesPerson getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("SalesPerson");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}
		return this.item;
	}

	public void setItem(SalesPerson item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public List<SalesPerson> getList() {
		if ((this.list == null) || (this.list.isEmpty())) {
			try {
				getSalesPersonList();
			} catch (SQLException ex) {
				Logger.getLogger(SalesPersonBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.list;
	}

	public List<SalesPerson> getListSelectOne() {
		if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
			try {
				Connection con = Data.getConnection();
				setListSelectOne(this.item.getList(con));
				con.close();
			} catch (SQLException ex) {
				Logger.getLogger(SalesPersonBean.class.getName()).log(Level.SEVERE, null, ex);
			}
		}
		return this.listSelectOne;
	}

	public void setListSelectOne(List<SalesPerson> listSelectOne) {
		this.listSelectOne = listSelectOne;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public void setList(List<SalesPerson> list) {
		this.list = list;
	}

	public String refreshList() {
		addNew();
		this.list.clear();
		return "SalesPersonList";
	}

	public String commit() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.update(con);
			this.item.load(con);
			con.close();
			this.list.clear();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "SalesPerson";
	}

	public String delete() {
		Connection con = Data.getConnection();
		try {
			String msg = this.item.delete(con);
			addNew();
			this.list.clear();
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(msg);
			fm.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage(null, fm);
		} catch (SQLException e) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage(null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException ex) {
					Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
				}
			}
		}
		return "SalesPerson";
	}

	public String addNew() {
		this.item = new SalesPerson();

		return "SalesPerson";
	}

	public String load(String person_code) throws SQLException {
		Connection con = Data.getConnection();
		this.item.setPersonCode(person_code);
		this.item.load(con);
		con.close();
		return "SalesPerson";
	}

	public String getSalesPersonList() throws SQLException {
		Connection con = Data.getConnection();
		setList(this.item.getList(con));
		return "SalesPersonList";
	}
}
