/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.Utils;
import com.shadowinfosystem.cashwrap.entities.ArticleItems;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;
import com.shadowinfosystem.cashwrap.entities.TransOut;
import com.shadowinfosystem.cashwrap.entities.TransOutLines;

import net.sf.jasperreports.engine.JRException;

@ManagedBean(name = "transOut")
@SessionScoped
public class TransOutBean {
	TransOut item;
	List<TransOut> list;
	@ManagedProperty("#{stock}")
	StockBean stock;
	@ManagedProperty("#{articleItems}")
	ArticleItemsBean articleItems;
	@ManagedProperty("#{section}")
	SectionBean section;
	@ManagedProperty("#{subSection}")
	SubSectionBean subSection;
	@ManagedProperty("#{transactionPrefix}")
	TransactionPrefixBean trans;
	@ManagedProperty("#{trans}")
	InventoryTransactionBean invTrans;
	@ManagedProperty("#{masterParameter}")
	MasterParameterBean mastParam;
	@ManagedProperty("#{reportManager}")
	ReportManagerBean rep;
	@ManagedProperty("#{login}")
	LoginBean login;
	@ManagedProperty("#{branch}")
	BranchBean branch;
	@ManagedProperty("#{brand}")
	BrandBean brand;
	boolean isSelected = false;
	FacesContext facesContext = FacesContext.getCurrentInstance();

	public TransOutBean() {
		if (this.list == null) {
			this.list = new ArrayList();
		}

		if (this.item == null) {
			this.item = new TransOut();
		}

	}

	public boolean isIsSelected() {
		return this.isSelected;
	}

	public BranchBean getBranch() {
		return this.branch;
	}

	public void setBranch(BranchBean branch) {
		this.branch = branch;
	}

	public void setIsSelected(boolean isSelected) {
		this.isSelected = isSelected;
	}

	public TransOut getItem() {
		if (this.item.getScreen() == null) {
			this.item.setScreen("TransOut");
			ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
			this.item.setUserId(this.login.getUserid());
			this.item.setAddAddowed(temp.isAddAddowed());
			this.item.setViewAddowed(temp.isViewAddowed());
			this.item.setModifyAddowed(temp.isModifyAddowed());
			this.item.setDeleteAddowed(temp.isDeleteAddowed());
			this.item.setPostAddowed(temp.isPostAddowed());
			this.item.setPrintAddowed(temp.isPrintAddowed());
		}

		return this.item;
	}

	public ArticleItemsBean getArticleItems() {
		return this.articleItems;
	}

	public void setArticleItems(ArticleItemsBean articleItems) {
		this.articleItems = articleItems;
	}

	public BrandBean getBrand() {
		return this.brand;
	}

	public void setBrand(BrandBean brand) {
		this.brand = brand;
	}

	public void setItem(TransOut item) {
		this.item = item;
	}

	public LoginBean getLogin() {
		return this.login;
	}

	public void setLogin(LoginBean login) {
		this.login = login;
	}

	public StockBean getStock() {
		return this.stock;
	}

	public void setStock(StockBean stock) {
		this.stock = stock;
	}

	public TransactionPrefixBean getTrans() {
		return this.trans;
	}

	public SectionBean getSection() {
		return this.section;
	}

	public void setSection(SectionBean section) {
		this.section = section;
	}

	public SubSectionBean getSubSection() {
		return this.subSection;
	}

	public ReportManagerBean getRep() {
		return this.rep;
	}

	public void setRep(ReportManagerBean rep) {
		this.rep = rep;
	}

	public void setSubSection(SubSectionBean subSection) {
		this.subSection = subSection;
	}

	public InventoryTransactionBean getInvTrans() {
		return this.invTrans;
	}

	public void setInvTrans(InventoryTransactionBean invTrans) {
		this.invTrans = invTrans;
	}

	public void setTrans(TransactionPrefixBean trans) {
		this.trans = trans;
	}

	public MasterParameterBean getMastParam() {
		return this.mastParam;
	}

	public void setMastParam(MasterParameterBean mastParam) {
		this.mastParam = mastParam;
	}

	public FacesContext getFacesContext() {
		return this.facesContext;
	}

	public void setFacesContext(FacesContext facesContext) {
		this.facesContext = facesContext;
	}

	public List<TransOut> getList() throws SQLException {
		if (this.list == null || this.list.isEmpty()) {
			Connection con = Data.getConnection();
			this.setList(this.item.getList(con));
			con.close();
		}

		return this.list;
	}

	public void setList(List<TransOut> list) {
		this.list = list;
	}

	public String setAndSearchItems(String itm_Code) {
		this.item.getLineSelected().setItemCode(itm_Code);
		this.searchItems();
		return "success";
	}

	public String setItemCode(String itmcode) {
		this.articleItems.list.clear();
		this.item.getLineSelected().setItemCode(itmcode);
		this.selectItem();
		return "TransOut";
	}

	public String setBranchName(String branchId) {
		this.getBranch().addNew();
		this.getBranch().load(branchId);
		this.getItem().setBranchName(this.getBranch().getItem().getBranchName());
		return "TransIn";
	}

	public String searchItems() {
		this.setIsSelected(false);
		this.articleItems.clearFilter();
		this.articleItems.getList().clear();
		this.setIsSelected(false);
		this.articleItems.getFilter().setSearch_text(this.item.getLineSelected().getSearch_text());
		this.articleItems.getFilter().setItem_code(this.item.getLineSelected().getItemCode());
		this.articleItems.getFilter().setArticle_no(this.item.getLineSelected().getArticleNo());
		this.articleItems.getFilter().setArticle_desc(this.item.getLineSelected().getItemDesc());
		this.articleItems.getFilter().setBar_code(this.item.getLineSelected().getBarCode());
		this.articleItems.getList();
		if (this.articleItems.getList().size() == 1) {
			this.item.getLineSelected().setItemCode(((ArticleItems) this.articleItems.getList().get(0)).getItem_code());
			this.selectItem();
		}

		return this.articleItems.getList().size() > 1 ? "TransOut" : "TransOut";
	}

	public String selectItem() {
		FacesMessage fm;
		try {
			TransOutLines ex = this.item.getLineSelected();
			this.articleItems.clearFilter();
			this.articleItems.getFilter().setItem_code(ex.getItemCode());
			this.articleItems.getList();
			if (this.articleItems.getList().size() == 1) {
				this.articleItems.setItem((ArticleItems) this.articleItems.getList().get(0));
			}

			this.stock.addNew();
			this.stock.load(ex.getItemCode());
			if (this.mastParam.item.getIsBillOnZeroBal() != null
					&& this.mastParam.item.getIsBillOnZeroBal().equals("No")) {
				if (this.stock.getItem().getQtyOnHand() <= ex.getQty()) {
					this.facesContext = FacesContext.getCurrentInstance();
					fm = new FacesMessage("Sufficient Stock not available.");
					fm.setSeverity(FacesMessage.SEVERITY_ERROR);
					this.facesContext.addMessage((String) null, fm);
					return "Invoice";
				}

				ex.setBatch(this.stock.getItem().getBatch());
				ex.setExpDate(this.stock.getItem().getExpDate());
			}

			double fm1 = 0.0D;
			double subSecTax = 0.0D;
			double secDisc = 0.0D;
			double subSecPerc = 0.0D;
			double tax = 0.0D;
			double disc = 0.0D;
			if (this.articleItems.item.getArticle_group() != null) {
				this.section.load(this.articleItems.item.getArticle_group());
				fm1 = this.section.item.getTaxPerc();
				secDisc = this.section.item.getDiscPerc();
			}

			if (this.articleItems.item.getArticle_sub_group() != null
					&& this.articleItems.item.getArticle_group() != null) {
				this.subSection.load(this.articleItems.item.getArticle_group(),
						this.articleItems.item.getArticle_group());
				subSecTax = this.subSection.item.getTaxPerc();
				subSecPerc = this.subSection.item.getDiscPerc();
			}

			if (this.articleItems.item.getTax_per() > 0.0D) {
				tax = this.articleItems.item.getTax_per();
			} else if (subSecTax > 0.0D) {
				tax = subSecTax;
			} else {
				tax = fm1;
			}

			if (subSecPerc > 0.0D) {
				disc = subSecPerc;
			} else {
				disc = secDisc;
			}

			double brandDisc = 0.0D;
			if (this.articleItems.item.getManufacturer() != null
					&& this.articleItems.item.getManufacturer().length() > 0) {
				this.brand.load(this.articleItems.item.getManufacturer());
				brandDisc = this.brand.getItem().getDiscPerc();
			}

			if (disc < brandDisc) {
				disc = brandDisc;
			}

			ex.setMrp(this.articleItems.item.getMrp());
			ex.setSalesPrice(this.articleItems.item.getSales_price());
			ex.setPurPrice(this.articleItems.item.getPur_price());
			ex.setTaxPer(tax);
			ex.setTaxAmt(this.articleItems.item.getSales_price() * tax / 100.0D);
			ex.setDiscPer(disc);
			ex.setDiscAmt(this.articleItems.item.getSales_price() * disc / 100.0D);
			ex.setQty(1.0D);
			ex.setUom(this.articleItems.item.getInventory_uom());
			ex.setNetAmt(ex.getQty() * ex.getSalesPrice() - ex.getDiscAmt() + ex.getTaxAmt());
			ex.setSubSection(this.articleItems.item.getArticle_sub_group());
			ex.setSection(this.articleItems.item.getArticle_group());
			ex.setColor(this.articleItems.item.getColor());
			ex.setBarCode(this.articleItems.item.getBar_code());
			ex.setArticleNo(this.articleItems.item.getArticle_no());
			ex.setItemDesc(this.articleItems.item.getArticle_desc());
			if (this.stock.item.getItemCode() != null && this.articleItems.getItem().getItem_code() != null
					&& this.stock.item.getItemCode().equals(this.articleItems.getItem().getItem_code())) {
				ex.setQtyOnHand(this.stock.item.getQtyOnHand());
			}

			this.setIsSelected(true);
			this.reProcessSelected();
		} catch (SQLException arg15) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg15.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "success";
	}

	public String taxCodeSelected() throws SQLException {
		TransOutLines ils = this.item.getLineSelected();
		if (this.item.getLineSelected().getTaxCode() != null && this.item.getLineSelected().getTaxCode().length() > 0) {
			ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
			TaxBean tax = (TaxBean) exctx.getSessionMap().get("tax");
			tax.load(this.item.getLineSelected().getTaxCode());
			ils.setTaxPer(tax.getItem().getTaxPer());
		} else {
			ils.setTaxPer(0.0D);
		}

		return this.reProcessSelected();
	}

	public String reProcessSelected() {
		TransOutLines ils = this.item.getLineSelected();
		if (ils.getDiscPer() > this.getMastParam().item.getMaxDiscOnItem()) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(
					"Max Disc Allowed is " + this.getMastParam().item.getMaxDiscOnItem() + " %.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		ils.process(this.mastParam.item.getIsTaxInclusive());
		return "success";
	}

	public String addItem() {
		TransOutLines pl = this.item.getLineSelected();
		FacesMessage fm;
		if (pl.getQty() <= 0.0D) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Qty must be > 0.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "TransOut";
		} else if (!this.isIsSelected()) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage("Please select Item.");
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			return "TransOut";
		} else {
			pl.setLineNo(this.getNextLineNo());
			pl.setTransId(this.item.getTransId());
			this.item.getLines().add(pl);
			this.item.setLineSelected(new TransOutLines());
			this.item.process(this.mastParam.item.getIsTaxInclusive());
			this.setIsSelected(false);
			return "TransOut";
		}
	}

	public String updateItem() {
		TransOutLines il = this.item.getLineSelected();
		this.item.setLineSelected(new TransOutLines());
		this.item.process(this.mastParam.item.getIsTaxInclusive());
		return "TransOut";
	}

	public String getTransOutList() throws SQLException {
		Connection con = Data.getConnection();
		this.setList(this.item.getList(con));
		return "TransOutList";
	}

	public String setSlectedDetail(TransOutLines line) {
		this.setIsSelected(true);
		this.item.setLineSelected(line);
		return "TransOut";
	}

	public String deletDetail(TransOutLines line) throws SQLException {
		Connection con = Data.getConnection();
		if (line.getObjectId() > 0) {
			try {
				line.delete(con);
			} catch (Exception arg4) {
				this.facesContext = FacesContext.getCurrentInstance();
				FacesMessage fm = new FacesMessage(arg4.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		}

		con.close();
		this.item.getLines().remove(line);
		return "TransOut";
	}

	public String getSubSecList() {
		ExternalContext exctx = FacesContext.getCurrentInstance().getExternalContext();
		SubSectionBean ss = (SubSectionBean) exctx.getSessionMap().get("subSection");
		ss.getItem().setSectionNo(this.item.getLineSelected().getSection());
		ss.getSubSecList();
		return "success";
	}

	public String newItem() {
		this.item = new TransOut();
		return "TransOut";
	}

	public String load(String trans_id) {
		Connection con = Data.getConnection();
		this.item.setTransId(trans_id);

		try {
			this.item.load(con);
		} catch (Exception arg4) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg4.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "TransOut";
	}

	public String printThermalReport() {
		FacesMessage fm;
		try {
			this.rep.getParameters().clear();
			this.rep.setJasperFile("TransOut");
			this.rep.getParameters().put("trans_id", this.item.getTransId());
			this.rep.exportToPdf();
		} catch (JRException arg2) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg2.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		} catch (IOException arg3) {
			this.facesContext = FacesContext.getCurrentInstance();
			fm = new FacesMessage(arg3.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
		}

		return "Report";
	}

	public short getNextLineNo() {
		boolean i = false;
		short arg3 = 0;
		Iterator i$ = this.getItem().getLines().iterator();

		while (i$.hasNext()) {
			TransOutLines pl = (TransOutLines) i$.next();
			if (pl.getLineNo() > arg3) {
				arg3 = pl.getLineNo();
			}
		}

		++arg3;
		return arg3;
	}

	public String process() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		return "TransOut";
	}

	public String commit() {
		this.process();
		Connection con = Data.getConnection();

		try {
			if (this.item.getTransId() == null || this.item.getTransId().length() == 0) {
				this.trans.addNew();
				this.trans.item.setPrefix((String) null);
				this.trans.item.setTransType("TRANSOUT");
				this.item.setTransId(this.trans.getNextOrderNo());
				this.item.setTransDate(Utils.getSqlDate(new Date()));
			}

			this.item.update(con);
			this.item.load(con);
			con.close();
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage e = new FacesMessage("Updated.");
			e.setSeverity(FacesMessage.SEVERITY_INFO);
			this.facesContext.addMessage((String) null, e);
		} catch (Exception arg5) {
			this.facesContext = FacesContext.getCurrentInstance();
			FacesMessage fm = new FacesMessage(arg5.getMessage());
			fm.setSeverity(FacesMessage.SEVERITY_ERROR);
			this.facesContext.addMessage((String) null, fm);
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg4) {
					Logger.getLogger(TransOutBean.class.getName()).log(Level.SEVERE, (String) null, arg4);
				}
			}
		}

		return "TransOut";
	}

	public String addNew() {
		this.setItem(new TransOut());
		this.stock.addNew();
		this.stock.list.clear();
		return "TransOut";
	}

	public String refreshList() {
		this.addNew();
		this.list.clear();
		this.item.setFromDate(new Date());
		this.item.setToDate(new Date());
		this.getArticleItems().clearFilter();
		this.getArticleItems().getList().clear();
		return "TransOutList";
	}

	public String resetSelected() {
		this.item.setLineSelected(new TransOutLines());
		this.articleItems.list.clear();
		this.articleItems.clearFilter();
		return "TransOut";
	}

	public String search() {
		this.list.clear();
		return "TransOutList";
	}

	public String approveReceipt() {
		this.item.process(this.mastParam.getItem().getIsTaxInclusive());
		Connection con = Data.getConnection();

		try {
			FacesMessage fm;
			try {
				con.setAutoCommit(false);
				Iterator ex = this.item.getLines().iterator();

				while (ex.hasNext()) {
					TransOutLines fm1 = (TransOutLines) ex.next();
					this.invTrans.addNew();
					this.invTrans.item.setTransactionId(this.invTrans.getItem().getNextTransactionId());
					this.invTrans.item.setTransactionCode("TRANSOUT");
					this.invTrans.item.setItemCode(fm1.getItemCode());
					this.invTrans.item.setItemDesc(fm1.getItemDesc());
					this.invTrans.item.setQty(fm1.getQty());
					this.invTrans.item.setUom(fm1.getUom());
					this.invTrans.item.setDirection("-");
					this.invTrans.item.setPricePerUnit(fm1.getSalesPrice());
					this.invTrans.item.setPrice(fm1.getNetAmt());
					this.invTrans.item.setOrderNo(fm1.getTransId());
					this.invTrans.item.setLineNo(fm1.getLineNo());
					this.invTrans.item.setCreatedBy(this.invTrans.item.getUserId());
					this.invTrans.item.setArticleNo(fm1.getArticleNo());
					this.invTrans.item.update(con);
					this.stock.addNew();
					this.stock.item.setItemCode(fm1.getItemCode());
					this.stock.item.load(con);
					if (this.mastParam.item.getIsBillOnZeroBal() != null
							&& this.mastParam.item.getIsBillOnZeroBal().equals("No")
							&& !this.stock.item.isIsItemCodeInStock()) {
						throw new Exception("Item is not found in Stock.");
					}

					if (this.stock.item.isIsItemCodeInStock()) {
						if (this.mastParam.item.getIsBillOnZeroBal() != null
								&& this.mastParam.item.getIsBillOnZeroBal().equals("No")
								&& this.stock.item.getQtyOnHand() - fm1.getQty() < 0.0D) {
							throw new Exception("Item " + fm1.getItemCode() + " don\'t have sufficient stock.");
						}

						this.stock.item.setQtyOnHand(this.stock.item.getQtyOnHand() - fm1.getQty());
						this.stock.item
								.setRemark("TROUT :" + fm1.getTransId() + " on :" + Utils.getStrDate(new Date()));
						this.stock.item.setUpdatedOn(new Date());
						this.stock.item.update(con);
					} else {
						this.stock.addNew();
						this.stock.item.setItemCode(fm1.getItemCode());
						this.stock.item.setItemDesc(fm1.getItemDesc());
						this.stock.item.setArticleNo(fm1.getArticleNo());
						this.stock.item.setSection(fm1.getSection());
						this.stock.item.setSubSection(fm1.getSubSection());
						this.stock.item.setUom(fm1.getUom());
						this.stock.item.setFabric(fm1.getFabric());
						this.stock.item.setRemark(fm1.getRemark());
						this.stock.item.setColor(fm1.getColor());
						this.stock.item.setStyle(fm1.getStyle());
						this.stock.item.setItemSize(fm1.getItemSize());
						this.stock.item.setBrand(fm1.getBrand());
						this.stock.item.setBarCode(fm1.getBarCode());
						this.stock.item.setMrp(fm1.getMrp());
						this.stock.item.setWsp(fm1.getSalesPrice());
						this.stock.item.setTaxPerc(fm1.getTaxPer());
						this.stock.item.setTaxAmt(fm1.getTaxAmt());
						this.stock.item.setDiscPerc(fm1.getDiscPer());
						this.stock.item.setDiscAmt(fm1.getDiscAmt());
						this.stock.item.setPurchaseOrder(fm1.getTransId());
						this.stock.item.setLineNo(Short.valueOf(fm1.getLineNo()));
						this.stock.item.setQty(0.0D - fm1.getQty());
						this.stock.item.setQtyOnHand(0.0D - fm1.getQty());
						this.stock.item.setReceiptDate(new Date());
						this.stock.item.setUpdatedOn(fm1.getUpdatedOn());
						this.stock.item.setCreatedOn(fm1.getCreatedOn());
						this.stock.item.setRemark(
								"Line created on delivery of Transout :" + fm1.getTransId() + "/" + fm1.getLineNo());
						this.stock.item.update(con);
					}
				}

				this.item.setStatus("Approved");
				this.item.update(con);
				con.commit();
				con.setAutoCommit(true);
			} catch (SQLException arg22) {
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg22.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);

				try {
					con.rollback();
				} catch (SQLException arg21) {
					Logger.getLogger(TransOutBean.class.getName()).log(Level.SEVERE, (String) null, arg21);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg20) {
					Logger.getLogger(TransOutBean.class.getName()).log(Level.SEVERE, (String) null, arg20);
				}
			} catch (Exception arg23) {
				try {
					con.rollback();
				} catch (SQLException arg19) {
					Logger.getLogger(TransOutBean.class.getName()).log(Level.SEVERE, (String) null, arg19);
				}

				try {
					con.setAutoCommit(true);
				} catch (SQLException arg18) {
					Logger.getLogger(TransOutBean.class.getName()).log(Level.SEVERE, (String) null, arg18);
				}

				arg23.printStackTrace();
				this.facesContext = FacesContext.getCurrentInstance();
				fm = new FacesMessage(arg23.getMessage());
				fm.setSeverity(FacesMessage.SEVERITY_ERROR);
				this.facesContext.addMessage((String) null, fm);
			}
		} finally {
			if (con != null) {
				try {
					con.close();
				} catch (SQLException arg17) {
					Logger.getLogger(TransOutBean.class.getName()).log(Level.SEVERE, (String) null, arg17);
				}
			}

		}

		return "TransOut";
	}
}