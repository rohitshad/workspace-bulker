/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.bean;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import com.shadowinfosystem.cashwrap.common.Data;
import com.shadowinfosystem.cashwrap.common.UserMessages;
import com.shadowinfosystem.cashwrap.entities.Branch;
import com.shadowinfosystem.cashwrap.entities.ScreenRights;

@ManagedBean(name="branch")
@SessionScoped
public class BranchBean
{
  Branch item;
  List<Branch> list;
  List<Branch> listSelectOne;
  @ManagedProperty("#{login}")
  LoginBean login;
  FacesContext facesContext = FacesContext.getCurrentInstance();
  
  public BranchBean()
  {
    if (this.list == null) {
      this.list = new ArrayList();
    }
    if (this.listSelectOne == null) {
      this.listSelectOne = new ArrayList();
    }
    if (this.item == null) {
      this.item = new Branch();
    }
  }
  
  public Branch getItem()
  {
    if (this.item.getScreen() == null)
    {
      this.item.setScreen("Branch");
      ScreenRights temp = this.login.getRightsForScreen(this.item.getScreen());
      this.item.setUserId(this.login.getUserid());
      this.item.setAddAddowed(temp.isAddAddowed());
      this.item.setViewAddowed(temp.isViewAddowed());
      this.item.setModifyAddowed(temp.isModifyAddowed());
      this.item.setDeleteAddowed(temp.isDeleteAddowed());
      this.item.setPostAddowed(temp.isPostAddowed());
      this.item.setPrintAddowed(temp.isPrintAddowed());
    }
    return this.item;
  }
  
  public void setItem(Branch item)
  {
    this.item = item;
  }
  
  public List<Branch> getListSelectOne()
  {
    if ((this.listSelectOne == null) || (this.listSelectOne.isEmpty())) {
      try
      {
        Connection con = Data.getConnection();
        addNew();
        this.item.setStatus("Active");
        setListSelectOne(this.item.getList(con));
        con.close();
      }
      catch (SQLException ex)
      {
        Logger.getLogger(ColorBean.class.getName()).log(Level.SEVERE, null, ex);
      }
    }
    return this.listSelectOne;
  }
  
  public LoginBean getLogin()
  {
    return this.login;
  }
  
  public void setLogin(LoginBean login)
  {
    this.login = login;
  }
  
  public void setListSelectOne(List<Branch> listSelectOne)
  {
    this.listSelectOne = listSelectOne;
  }
  
  public List<Branch> getList()
  {
    if ((this.list == null) || (this.list.isEmpty())) {
      getBranchList();
    }
    return this.list;
  }
  
  public void setList(List<Branch> list)
  {
    this.list = list;
  }
  
  public String commit()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.update(con);
      this.item.load(con);
      con.close();
      this.listSelectOne.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
      con.close();
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Branch";
  }
  
  public String delete()
  {
    Connection con = Data.getConnection();
    try
    {
      String msg = this.item.delete(con);
      addNew();
      this.list.clear();
      con.close();
      this.listSelectOne.clear();
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(msg);
      fm.setSeverity(FacesMessage.SEVERITY_INFO);
      this.facesContext.addMessage(null, fm);
      con.close();
    }
    catch (SQLException e)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(e.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Branch";
  }
  
  public String addNew()
  {
    this.item = new Branch();
    getItem();
    return "Branch";
  }
  
  public String load(String branch_id)
  {
    Connection con = null;
    try
    {
      con = Data.getConnection();
      this.item.setBranchId(branch_id);
      this.item.load(con);
    }
    catch (SQLException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(ex.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex1)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "Branch";
  }
  
  public String getBranchList()
  {
    Connection con = null;
    try
    {
      con = Data.getConnection();
      setList(this.item.getList(con));
      con.close();
    }
    catch (SQLException ex)
    {
      this.facesContext = FacesContext.getCurrentInstance();
      FacesMessage fm = new FacesMessage(UserMessages.getMessage(ex.getMessage()));
      fm.setSeverity(FacesMessage.SEVERITY_ERROR);
      this.facesContext.addMessage(null, fm);
      if (con != null) {
        try
        {
          con.close();
        }
        catch (SQLException ex1)
        {
          Logger.getLogger(PurchaseOrderBean.class.getName()).log(Level.SEVERE, null, ex);
        }
      }
    }
    return "BranchList";
  }
  
  public String refreshBranchList()
  {
    setItem(new Branch());
    this.list.clear();
    return "BranchList";
  }
  
  public String refreshBrand()
  {
    refreshBranchList();
    return "Branch";
  }
  
  public String search()
  {
    this.list.clear();
    return "BranchList";
  }
}
