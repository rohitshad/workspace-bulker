/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.common;

import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.shadowinfosystem.cashwrap.entities.EntitiesImpl;

public class MessagePack extends EntitiesImpl implements Serializable {
	private String userName;
	private String userNameFieldName;
	private String password;
	private String passwordFieldName;
	private String senderId;
	private String senderIdFieldName;
	private String mobiles;
	private String mobilesFieldName;
	private String sms;
	private String smsFieldName;
	private String additionalParameters;
	private String baseUrl;

	public String sendMessage() {
		String response = "Message could not be send.";
		try {
			String requestUrl = getBaseUrl();
			requestUrl = requestUrl + "?" + getUserNameFieldName() + "=" + URLEncoder.encode(getUserName(), "UTF-8");
			requestUrl = requestUrl + "&" + getPasswordFieldName() + "=" + URLEncoder.encode(getPassword(), "UTF-8");
			requestUrl = requestUrl + "&" + getSenderIdFieldName() + "=" + URLEncoder.encode(getSenderId(), "UTF-8");
			requestUrl = requestUrl + "&" + getMobilesFieldName() + "=" + URLEncoder.encode(getMobiles(), "UTF-8");
			requestUrl = requestUrl + "&" + getSmsFieldName() + "=" + URLEncoder.encode(getSms(), "UTF-8");
			requestUrl = requestUrl + "&" + getAdditionalParameters();

			URL url = new URL(requestUrl);
			HttpURLConnection uc = (HttpURLConnection) url.openConnection();

			response = uc.getResponseCode() + " ( " + uc.getResponseMessage() + ")";
			uc.disconnect();
		} catch (Exception ex) {
		}
		return response;
	}

	public String getUserName() {
		return this.userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserNameFieldName() {
		return this.userNameFieldName;
	}

	public void setUserNameFieldName(String userNameFieldName) {
		this.userNameFieldName = userNameFieldName;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPasswordFieldName() {
		return this.passwordFieldName;
	}

	public void setPasswordFieldName(String passwordFieldName) {
		this.passwordFieldName = passwordFieldName;
	}

	public String getSenderId() {
		return this.senderId;
	}

	public void setSenderId(String senderId) {
		this.senderId = senderId;
	}

	public String getSenderIdFieldName() {
		return this.senderIdFieldName;
	}

	public void setSenderIdFieldName(String senderIdFieldName) {
		this.senderIdFieldName = senderIdFieldName;
	}

	public String getMobiles() {
		return this.mobiles;
	}

	public void setMobiles(String mobiles) {
		this.mobiles = mobiles;
	}

	public String getMobilesFieldName() {
		return this.mobilesFieldName;
	}

	public void setMobilesFieldName(String mobilesFieldName) {
		this.mobilesFieldName = mobilesFieldName;
	}

	public String getSms() {
		return this.sms;
	}

	public void setSms(String sms) {
		this.sms = sms;
	}

	public String getSmsFieldName() {
		return this.smsFieldName;
	}

	public void setSmsFieldName(String smsFieldName) {
		this.smsFieldName = smsFieldName;
	}

	public String getAdditionalParameters() {
		return this.additionalParameters;
	}

	public void setAdditionalParameters(String additionalParameters) {
		this.additionalParameters = additionalParameters;
	}

	public String getBaseUrl() {
		return this.baseUrl;
	}

	public void setBaseUrl(String baseUrl) {
		this.baseUrl = baseUrl;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select username,\n       userfieldname,\n       password,\n       passwordfieldname,\n       senderid,\n       senderidfieldname,\n       mobiles,\n       mobilesfieldname,\n       sms,\n       smsfieldname,\n       additionalparameters,\n       baseurl\n  from message_pack t";

		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setBaseUrl(rs.getString("baseurl"));
			setUserName(rs.getString("userName"));
			setUserNameFieldName(rs.getString("userfieldname"));
			setPassword(rs.getString("password"));
			setPasswordFieldName(rs.getString("passwordFieldName"));
			setSenderId(rs.getString("senderId"));
			setSenderIdFieldName(rs.getString("senderIdFieldName"));
			setSms(rs.getString("sms"));
			setSmsFieldName(rs.getString("smsFieldName"));
			setMobiles(rs.getString("mobiles"));
			setMobilesFieldName(rs.getString("mobilesFieldName"));
			setAdditionalParameters(rs.getString("additionalParameters"));
		}
		return "success";
	}

	public String update(Connection con) throws SQLException {
		String sql = "update message_pack p\n   set username             = ?,\n       userfieldname        = ?,\n       password             = ?,\n       passwordfieldname    = ?,\n       senderid             = ?,\n       senderidfieldname    = ?,\n       mobiles              = ?,\n       mobilesfieldname     = ?,\n       sms                  = ?,\n       smsfieldname         = ?,\n       additionalparameters = ?,\n       baseurl              = ?\n where 1 = 1";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getUserName());
		ps.setString(2, getUserNameFieldName());
		ps.setString(3, getPassword());
		ps.setString(4, getPasswordFieldName());
		ps.setString(5, getSenderId());
		ps.setString(6, getSenderIdFieldName());
		ps.setString(7, getMobiles());
		ps.setString(8, getMobilesFieldName());
		ps.setString(9, getSms());
		ps.setString(10, getSmsFieldName());
		ps.setString(11, getAdditionalParameters());
		ps.setString(12, getBaseUrl());
		int n = 0;
		n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could not be Updated.");
		}
		return "success";
	}
}
