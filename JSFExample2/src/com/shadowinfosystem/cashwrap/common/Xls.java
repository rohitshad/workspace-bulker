/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.common;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFDataFormat;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;

public class Xls {
	private String file;

	public String getFile() {
		return this.file;
	}

	public void setFile(String file) {
		this.file = file;
	}

	public String getXls(List list)
			throws FileNotFoundException, IOException, IllegalArgumentException, IllegalAccessException {
		HSSFWorkbook wb = new HSSFWorkbook();
		String objName = "";
		for (Object o : list) {
			objName = o.getClass().getSimpleName();
		}
		SimpleDateFormat ldf = new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss");
		String fileName = objName + "_" + ldf.format(new Date()) + ".xls";
		String file = "F:\\SoftRetail\\xls\\" + fileName;
		FileOutputStream fout = new FileOutputStream(file);
		setFile(file);
		HSSFSheet ws = wb.createSheet(objName);
		HSSFRow wr = null;
		HSSFCellStyle cs = wb.createCellStyle();

		HSSFFont font = wb.createFont();
		font.setColor((short) 12);
		font.setBoldweight((short) 700);
		font.setFontHeightInPoints((short) 11);

		cs.setFont(font);
		for (Object o : list) {
			String colLabel = "";
			wr = ws.createRow(0);
			int c = 0;
			for (Field f : o.getClass().getDeclaredFields()) {
				colLabel = f.getName();
				HSSFCell wc = wr.createCell(c);
				wc.setCellValue(colLabel);
				wc.setCellStyle(cs);
				c++;
			}
		}
		HSSFDataFormat hsdf = wb.createDataFormat();
		HSSFCellStyle decimalCell = wb.createCellStyle();
		decimalCell.setDataFormat(hsdf.getFormat("#0.00"));

		HSSFCellStyle dateCell = wb.createCellStyle();
		dateCell.setDataFormat(hsdf.getFormat("dd/MM/yyyy"));

		HSSFCellStyle intCell = wb.createCellStyle();
		dateCell.setDataFormat(hsdf.getFormat("#0"));

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		DecimalFormat df = new DecimalFormat("#0.00");

		String colDataType = "";
		int r = 1;
		for (Object o : list) {
			int c = 0;
			wr = ws.createRow((short) r);
			for (Field f : o.getClass().getDeclaredFields()) {
				HSSFCell wc = wr.createCell(c);
				colDataType = f.getType().toString();
				f.setAccessible(true);
				if (colDataType.equals("class java.util.Date")) {
					if (f.get(o) != null) {
						wc.setCellValue(sdf.format(f.get(o)));
					} else {
						wc.setCellValue("");
					}
					wc.setCellStyle(dateCell);
				}
				if (colDataType.equals("class java.lang.String")) {
					if (f.get(o) != null) {
						wc.setCellValue(f.get(o).toString());
					} else {
						wc.setCellValue("");
					}
				}
				if ((colDataType.equals("class java.lang.Double")) || (colDataType.equals("double"))) {
					if (f.get(o) != null) {
						wc.setCellValue(df.format((Double) f.get(o)));
					} else {
						wc.setCellValue("");
					}
					wc.setCellStyle(decimalCell);
				}
				if ((colDataType.equals("class java.lang.Short")) || (colDataType.equals("short"))) {
					if (f.get(o) != null) {
						wc.setCellValue(((Short) f.get(o)).shortValue());
					} else {
						wc.setCellValue("");
					}
					wc.setCellStyle(intCell);
				}
				if ((colDataType.equals("class java.lang.Integer")) || (colDataType.equals("int"))) {
					if (f.get(o) != null) {
						wc.setCellValue(((Integer) f.get(o)).intValue());
					} else {
						wc.setCellValue("");
					}
					wc.setCellStyle(intCell);
				}
				c++;
			}
			r++;
		}
		wb.write(fout);
		fout.close();

		return "success";
	}
}
