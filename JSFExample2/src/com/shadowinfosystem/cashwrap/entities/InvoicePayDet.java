/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class InvoicePayDet implements Serializable {
	private String invoiceNo;
	private String payModeId;
	private String branchId;
	private double payAmt;
	private double cashTAmt;
	private double refAmt;
	private String ccCode;
	private String ccNo;
	private String coupId;
	private String custId;
	private String bankName;
	private String chequeNo;
	private Date chequeDt;
	private String gvNo;
	private double gvAmt;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private String refundVouchernNo;
	private String dbRowid;

	public String getInvoiceNo() {
		return this.invoiceNo;
	}

	public void setInvoiceNo(String invoiceNo) {
		this.invoiceNo = invoiceNo;
	}

	public String getPayModeId() {
		return this.payModeId;
	}

	public void setPayModeId(String payModeId) {
		this.payModeId = payModeId;
	}

	public String getBranchId() {
		return this.branchId;
	}

	public void setBranchId(String branchId) {
		this.branchId = branchId;
	}

	public double getPayAmt() {
		return this.payAmt;
	}

	public void setPayAmt(double payAmt) {
		this.payAmt = payAmt;
	}

	public double getCashTAmt() {
		return this.cashTAmt;
	}

	public void setCashTAmt(double cashTAmt) {
		this.cashTAmt = cashTAmt;
	}

	public double getRefAmt() {
		return this.refAmt;
	}

	public void setRefAmt(double refAmt) {
		this.refAmt = refAmt;
	}

	public String getCcCode() {
		return this.ccCode;
	}

	public void setCcCode(String ccCode) {
		this.ccCode = ccCode;
	}

	public String getCcNo() {
		return this.ccNo;
	}

	public void setCcNo(String ccNo) {
		this.ccNo = ccNo;
	}

	public String getCoupId() {
		return this.coupId;
	}

	public void setCoupId(String coupId) {
		this.coupId = coupId;
	}

	public String getCustId() {
		return this.custId;
	}

	public void setCustId(String custId) {
		this.custId = custId;
	}

	public String getBankName() {
		return this.bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getChequeNo() {
		return this.chequeNo;
	}

	public void setChequeNo(String chequeNo) {
		this.chequeNo = chequeNo;
	}

	public Date getChequeDt() {
		return this.chequeDt;
	}

	public void setChequeDt(Date chequeDt) {
		this.chequeDt = chequeDt;
	}

	public String getGvNo() {
		return this.gvNo;
	}

	public void setGvNo(String gvNo) {
		this.gvNo = gvNo;
	}

	public double getGvAmt() {
		return this.gvAmt;
	}

	public void setGvAmt(double gvAmt) {
		this.gvAmt = gvAmt;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String getRefundVouchernNo() {
		return this.refundVouchernNo;
	}

	public void setRefundVouchernNo(String refundVouchernNo) {
		this.refundVouchernNo = refundVouchernNo;
	}

	public String getDbRowid() {
		return this.dbRowid;
	}

	public void setDbRowid(String dbRowid) {
		this.dbRowid = dbRowid;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		this.updatedOn = new Date();
		String sql = "update invoice_pay_det\n   set branch_id  = ?,\n       pay_amt    = ?,\n       cash_t_amt = ?,\n       ref_amt    = ?,\n       cc_code    = ?,\n       cc_no      = ?,\n       coup_id    = ?,\n       cust_id    = ?,\n       bank_name  = ?,\n       cheque_no  = ?,\n       cheque_dt  = ?,\n       gv_no      = ?,\n       gv_amt     = ?,\n       status     = ?,\n       created_on = ?,\n       created_by = ?,\n       updated_on = ?,\n       updated_by = ?,\n       object_id  = nvl(object_id, 1) + 1,refund_voucher_no=?,pay_mode_id=?\n where invoice_no = ?\n   and rowid = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, "N/A");
		ps.setDouble(2, this.payAmt);
		ps.setDouble(3, this.cashTAmt);
		ps.setDouble(4, this.refAmt);
		ps.setString(5, this.ccCode);
		ps.setString(6, this.ccNo);
		ps.setString(7, this.coupId);
		ps.setString(8, this.coupId);
		ps.setString(9, this.bankName);
		ps.setString(10, this.chequeNo);
		ps.setDate(11, Utils.getSqlDate(this.chequeDt));
		ps.setString(12, this.gvNo);
		ps.setDouble(13, this.gvAmt);
		ps.setString(14, this.status);
		ps.setDate(15, Utils.getSqlDate(this.createdOn));
		ps.setString(16, this.createdBy);
		ps.setDate(17, Utils.getSqlDate(this.updatedOn));
		ps.setString(18, this.updatedBy);
		ps.setString(19, this.refundVouchernNo);
		ps.setString(20, this.payModeId);
		ps.setString(21, this.invoiceNo);
		ps.setString(22, getDbRowid());
		ps.setShort(23, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public String create(Connection con) throws SQLException {
		setObjectId((short) 1);
		this.createdOn = new Date();
		String sql = "insert into invoice_pay_det\n  (invoice_no,\n   pay_mode_id,\n   branch_id,\n   pay_amt,\n   cash_t_amt,\n   ref_amt,\n   cc_code,\n   cc_no,\n   coup_id,\n   cust_id,\n   bank_name,\n   cheque_no,\n   cheque_dt,\n   gv_no,\n   gv_amt,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,refund_voucher_no)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invoiceNo);
		ps.setString(2, this.payModeId);
		ps.setString(3, "N/A");
		ps.setDouble(4, this.payAmt);
		ps.setDouble(5, this.cashTAmt);
		ps.setDouble(6, this.refAmt);
		ps.setString(7, this.ccCode);
		ps.setString(8, this.ccNo);
		ps.setString(9, this.coupId);
		ps.setString(10, this.coupId);
		ps.setString(11, this.bankName);
		ps.setString(12, this.chequeNo);
		ps.setDate(13, Utils.getSqlDate(this.chequeDt));
		ps.setString(14, this.gvNo);
		ps.setDouble(15, this.gvAmt);
		ps.setString(16, this.status);
		ps.setDate(17, Utils.getSqlDate(this.createdOn));
		ps.setString(18, this.createdBy);
		ps.setDate(19, Utils.getSqlDate(this.updatedOn));
		ps.setString(20, this.updatedBy);
		ps.setShort(21, this.objectId);
		ps.setString(22, this.refundVouchernNo);
		int n = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public static List<InvoicePayDet> load(Connection con, String inv) throws SQLException {
		List<InvoicePayDet> list = new ArrayList();
		InvoicePayDet inp = null;
		String sql = "select invoice_no,\n       pay_mode_id,\n       branch_id,\n       pay_amt,\n       cash_t_amt,\n       ref_amt,\n       cc_code,\n       cc_no,\n       coup_id,\n       cust_id,\n       bank_name,\n       cheque_no,\n       cheque_dt,\n       gv_no,\n       gv_amt,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,refund_voucher_no,rowid rid\n  from invoice_pay_det  where invoice_no = ?\n order by pay_mode_id";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, inv);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			inp = new InvoicePayDet();
			inp.setInvoiceNo(rs.getString("invoice_no"));
			inp.setPayModeId(rs.getString("pay_mode_id"));
			inp.setBranchId(rs.getString("branch_id"));
			inp.setPayAmt(rs.getDouble("pay_amt"));
			inp.setCashTAmt(rs.getDouble("cash_t_amt"));
			inp.setRefAmt(rs.getDouble("ref_amt"));
			inp.setCcCode(rs.getString("cc_code"));
			inp.setCcNo(rs.getString("cc_no"));
			inp.setCoupId(rs.getString("coup_id"));
			inp.setCustId(rs.getString("cust_id"));
			inp.setBankName(rs.getString("bank_name"));
			inp.setChequeNo(rs.getString("cheque_no"));
			inp.setChequeDt(rs.getDate("cheque_dt"));
			inp.setGvNo(rs.getString("gv_no"));
			inp.setGvAmt(rs.getDouble("gv_amt"));
			inp.setStatus(rs.getString("status"));
			inp.setCreatedOn(rs.getDate("created_On"));
			inp.setCreatedBy(rs.getString("Created_By"));
			inp.setUpdatedOn(rs.getDate("updated_On"));
			inp.setUpdatedBy(rs.getString("updated_By"));
			inp.setObjectId((short) (int) rs.getDouble("object_Id"));
			inp.setRefundVouchernNo(rs.getString("refund_voucher_no"));
			inp.setDbRowid(rs.getString("rid"));
			list.add(inp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from invoice_pay_det where invoice_no=? and pay_mode_id=? and object_Id=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.invoiceNo);
		ps.setString(2, this.payModeId);
		ps.setShort(3, this.objectId);
		int nrd = 0;
		nrd = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (((this.invoiceNo != null ? 1 : 0) & (this.payModeId != null ? 1 : 0)) != 0
				? (this.invoiceNo + this.payModeId).hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof InvoicePayDet)) {
			return false;
		}
		InvoicePayDet other = (InvoicePayDet) object;
		if (this.invoiceNo == null) {
			return false;
		}
		if (other.invoiceNo == null) {
			return false;
		}
		if (this.payModeId == null) {
			return false;
		}
		if (other.payModeId == null) {
			return false;
		}
		return (this.invoiceNo.equals(other.invoiceNo)) && (this.payModeId.equals(other.payModeId));
	}

	public String toString() {
		return "Inv No and Pay Mode ID =" + this.invoiceNo + " + " + this.payModeId;
	}
}
