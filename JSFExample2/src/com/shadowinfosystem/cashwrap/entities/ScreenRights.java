/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ScreenRights {
	private String screen;
	private boolean addAddowed;
	private boolean modifyAddowed;
	private boolean deleteAddowed;
	private boolean viewAddowed;
	private boolean postAddowed;
	private boolean printAddowed;
	private boolean cancelAllowed;

	public ScreenRights() {
	}

	public ScreenRights(String screen, boolean addAddowed, boolean modifyAddowed, boolean deleteAddowed,
			boolean viewAddowed, boolean postAddowed, boolean printAddowed, boolean cancelAllowed) {
		this.screen = screen;
		this.addAddowed = addAddowed;
		this.modifyAddowed = modifyAddowed;
		this.deleteAddowed = deleteAddowed;
		this.viewAddowed = viewAddowed;
		this.postAddowed = postAddowed;
		this.printAddowed = printAddowed;
		this.cancelAllowed = cancelAllowed;
	}

	public String getScreen() {
		return this.screen;
	}

	public void setScreen(String screen) {
		this.screen = screen;
	}

	public boolean isAddAddowed() {
		return this.addAddowed;
	}

	public void setAddAddowed(boolean addAddowed) {
		this.addAddowed = addAddowed;
	}

	public boolean isModifyAddowed() {
		return this.modifyAddowed;
	}

	public void setModifyAddowed(boolean modifyAddowed) {
		this.modifyAddowed = modifyAddowed;
	}

	public boolean isDeleteAddowed() {
		return this.deleteAddowed;
	}

	public void setDeleteAddowed(boolean deleteAddowed) {
		this.deleteAddowed = deleteAddowed;
	}

	public boolean isViewAddowed() {
		return this.viewAddowed;
	}

	public void setViewAddowed(boolean viewAddowed) {
		this.viewAddowed = viewAddowed;
	}

	public boolean isPostAddowed() {
		return this.postAddowed;
	}

	public void setPostAddowed(boolean postAddowed) {
		this.postAddowed = postAddowed;
	}

	public boolean isPrintAddowed() {
		return this.printAddowed;
	}

	public void setPrintAddowed(boolean printAddowed) {
		this.printAddowed = printAddowed;
	}

	public boolean isCancelAllowed() {
		return this.cancelAllowed;
	}

	public void setCancelAllowed(boolean cancelAllowed) {
		this.cancelAllowed = cancelAllowed;
	}

	public List<ScreenRights> getScreenRightsForUser(String user_id_, Connection con) throws Exception {
		List<ScreenRights> list = new ArrayList();

		String sql = "select p.screen,\n       case when sum(decode(p.can_add,'Y',1,0)) > 0 then 'Y' else 'N' end can_add,\n       case when sum(decode(p.can_modify ,'Y',1,0)) > 0 then 'Y' else 'N' end can_modify,\n       case when sum(decode(p.can_delete  ,'Y',1,0)) > 0 then 'Y' else 'N' end can_delete ,\n       case when sum(decode(p.can_inquire  ,'Y',1,0)) > 0 then 'Y' else 'N' end can_inquire ,\n       case when sum(decode(p.can_post  ,'Y',1,0)) > 0 then 'Y' else 'N' end can_post ,\n       case when sum(decode(p.can_print  ,'Y',1,0)) > 0 then 'Y' else 'N' end can_print,\n       case when sum(decode(p.can_cancel  ,'Y',1,0)) > 0 then 'Y' else 'N' end can_cancel\n  from user_roles u, roles_screen_grants p\n where u.user_id = ?\n   and u.role = p.role\n group by p.screen";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, user_id_);
			ResultSet rs = ps.executeQuery();
			while (rs.next()) {
				ScreenRights temp = new ScreenRights();
				temp.setScreen(rs.getString("screen"));
				String temps = rs.getString("can_add");
				temp.setAddAddowed((temps != null) && (temps.equals("Y")));

				temps = rs.getString("can_modify");
				temp.setModifyAddowed((temps != null) && (temps.equals("Y")));

				temps = rs.getString("can_delete");
				temp.setDeleteAddowed((temps != null) && (temps.equals("Y")));

				temps = rs.getString("can_inquire");
				temp.setViewAddowed((temps != null) && (temps.equals("Y")));

				temps = rs.getString("can_post");
				temp.setPostAddowed((temps != null) && (temps.equals("Y")));

				temps = rs.getString("can_print");
				temp.setPrintAddowed((temps != null) && (temps.equals("Y")));

				temps = rs.getString("can_cancel");
				temp.setCancelAllowed((temps != null) && (temps.equals("Y")));

				list.add(temp);
			}
			rs.close();
			ps.close();
		} catch (SQLException ex) {
			Logger.getLogger(ScreenRights.class.getName()).log(Level.SEVERE, null, ex);
			throw new Exception(ex + "");
		}
		return list;
	}
}
