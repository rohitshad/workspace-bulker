/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.ResourceBundle;
import java.util.Set;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Article extends EntitiesImpl implements Serializable {
	private String articleNo;
	private String articleDesc;
	private String article_type;
	private String article_group;
	private String article_sub_group;
	private String article_size;
	private String color;
	private String article_short_desc;
	private String manufacturer;
	private String inventory_uom;
	private String style;
	private double min_stock;
	private double max_stock;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private String remark1;
	private String remark2;
	private String remark3;
	private double tax_per;
	private double disc_per;
	private double open_bal;
	private String rack;
	private double wsp_disc_per;
	private String bar_code;
	
	private String search_text;
	private List<ArticleItems> itemsList;

	public Article() {
		if (this.itemsList == null) {
			this.itemsList = new ArrayList();
		}
	}

	public Article(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getArticleDesc() {
		return this.articleDesc;
	}

	public void setArticleDesc(String articleDesc) {
		this.articleDesc = articleDesc;
	}

	public String getArticle_type() {
		return this.article_type;
	}

	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}

	public String getArticle_group() {
		return this.article_group;
	}

	public void setArticle_group(String article_group) {
		this.article_group = article_group;
	}

	public String getArticle_sub_group() {
		return this.article_sub_group;
	}

	public void setArticle_sub_group(String article_sub_group) {
		this.article_sub_group = article_sub_group;
	}

	public String getArticle_size() {
		return this.article_size;
	}

	public void setArticle_size(String article_size) {
		this.article_size = article_size;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getArticle_short_desc() {
		return this.article_short_desc;
	}

	public void setArticle_short_desc(String article_short_desc) {
		this.article_short_desc = article_short_desc;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public String getInventory_uom() {
		return this.inventory_uom;
	}

	public void setInventory_uom(String inventory_uom) {
		this.inventory_uom = inventory_uom;
	}

	public String getStyle() {
		return this.style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public double getMin_stock() {
		return this.min_stock;
	}

	public void setMin_stock(double min_stock) {
		this.min_stock = min_stock;
	}

	public double getMax_stock() {
		return this.max_stock;
	}

	public void setMax_stock(double max_stock) {
		this.max_stock = max_stock;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String getRemark1() {
		return this.remark1;
	}

	public void setRemark1(String remark1) {
		this.remark1 = remark1;
	}

	public String getRemark2() {
		return this.remark2;
	}

	public void setRemark2(String remark2) {
		this.remark2 = remark2;
	}

	public String getRemark3() {
		return this.remark3;
	}

	public void setRemark3(String remark3) {
		this.remark3 = remark3;
	}

	public double getTax_per() {
		return this.tax_per;
	}

	public void setTax_per(double tax_per) {
		this.tax_per = tax_per;
	}

	public double getDisc_per() {
		return this.disc_per;
	}

	public void setDisc_per(double disc_per) {
		this.disc_per = disc_per;
	}

	public double getOpen_bal() {
		return this.open_bal;
	}

	public void setOpen_bal(double open_bal) {
		this.open_bal = open_bal;
	}

	public String getRack() {
		return this.rack;
	}

	public void setRack(String rack) {
		this.rack = rack;
	}

	public List<ArticleItems> getItemsList() {
		return this.itemsList;
	}

	public void setItemsList(List<ArticleItems> itemsList) {
		this.itemsList = itemsList;
	}

	public double getWsp_disc_per() {
		return this.wsp_disc_per;
	}

	public void setWsp_disc_per(double wsp_disc_per) {
		this.wsp_disc_per = wsp_disc_per;
	}

	public String getBar_code() {
		return this.bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	public String getSearch_text() {
		return this.search_text;
	}

	public void setSearch_text(String search_text) {
		this.search_text = search_text;
	}
	

	


	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update article p\nset article_desc=?,\narticle_type=?,\narticle_group=?,\narticle_sub_group=?,\narticle_size=?,\ncolor=?,\narticle_short_desc=?,\nmanufacturer=?,\ninventory_uom=?,\nstyle=?,\nmin_stock=?,\nmax_stock=?,status=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\n,remark1 =? ,remark2 =? ,remark3 =? ,tax_per =? ,disc_per =? ,open_bal =? ,rack =? ,wsp_disc_per =? where article_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, this.articleDesc);
		ps.setString(2, getArticle_type());
		ps.setString(3, getArticle_group());
		ps.setString(4, getArticle_sub_group());
		ps.setString(5, getArticle_size());
		ps.setString(6, getColor());
		ps.setString(7, getArticle_short_desc());
		ps.setString(8, getManufacturer());
		ps.setString(9, getInventory_uom());
		ps.setString(10, getStatus());
		ps.setDouble(11, getMin_stock());
		ps.setDouble(12, getMax_stock());
		ps.setString(13, this.status);
		ps.setDate(14, Utils.getSqlDate(getCreatedOn()));
		ps.setString(15, this.createdBy);
		ps.setDate(16, Utils.getSqlDate(new Date()));
		ps.setString(17, getUserId());
		ps.setString(18, getRemark1());
		ps.setString(19, getRemark2());
		ps.setString(20, getRemark3());
		ps.setDouble(21, getTax_per());
		ps.setDouble(22, getDisc_per());
		ps.setDouble(23, getOpen_bal());
		ps.setString(24, getRack());
		ps.setDouble(25, getWsp_disc_per());
		ps.setString(26, this.articleNo);
		ps.setDouble(27, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record could not be modified.");
		}
		for (ArticleItems ai : getItemsList()) {
			ai.setItemDesc(getArticleDesc());
			ai.update(con);
		}
		return "Record Updated";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into article\n  (article_no,\n   article_desc,\n   article_type,\n   article_group,\n   article_sub_group,\n   article_size,\n   color,\n   article_short_desc,\n   manufacturer,\n   inventory_uom,\n   style,\n   min_stock,\n   max_stock,   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,   object_id,remark1,remark2,remark3, tax_per,disc_per, open_bal,rack,wsp_disc_per    )\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.articleNo);
		ps.setString(2, this.articleDesc);
		ps.setString(3, getArticle_type());
		ps.setString(4, getArticle_group());
		ps.setString(5, getArticle_short_desc());
		ps.setString(6, getArticle_size());
		ps.setString(7, getColor());
		ps.setString(8, getArticle_short_desc());
		ps.setString(9, getManufacturer());
		ps.setString(10, getInventory_uom());
		ps.setString(11, getStatus());
		ps.setDouble(12, getMin_stock());
		ps.setDouble(13, getMax_stock());
		ps.setString(14, this.status);
		ps.setDate(15, Utils.getSqlDate(new Date()));
		ps.setString(16, getUserId());
		ps.setDate(17, Utils.getSqlDate(this.updatedOn));
		ps.setString(18, getUpdatedBy());
		ps.setDouble(19, 1.0D);
		ps.setString(20, getRemark1());
		ps.setString(21, getRemark2());
		ps.setString(22, getRemark3());
		ps.setDouble(23, getTax_per());
		ps.setDouble(24, getDisc_per());
		ps.setDouble(25, getOpen_bal());
		ps.setString(26, getRack());
		ps.setDouble(27, getWsp_disc_per());
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Could Not Be created.");
		}
		return "Record Created";
	}

	public List<Article> search(Connection con) throws SQLException {
		String sql = "select article_no,\n       article_desc,\n       article_type,\n       article_group,\n       article_sub_group,\n       article_size,\n       color,\n       article_short_desc,\n       manufacturer,\n       inventory_uom,\n       style,\n       min_stock,\n       max_stock,       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       remark1,       remark2,       remark3,       tax_per,       disc_per,       open_bal,       rack       ,wsp_disc_per  from article p\n where 1=1 ";

		boolean doFetchData = false;
		if ((getArticleNo() != null) && (getArticleNo().length() > 0)) {
			sql = sql + " and upper(article_no) like upper('" + getArticleNo().trim() + "')";
			doFetchData = true;
		}
		if ((getArticleDesc() != null) && (getArticleDesc().length() > 0)) {
			sql = sql + " and upper(article_desc) like upper('%" + getArticleDesc().trim() + "%')";
			doFetchData = true;
		}
		if ((getArticle_type() != null) && (getArticle_type().length() > 0)) {
			sql = sql + " and upper(article_type) like upper('" + getArticle_type().trim() + "')";
			doFetchData = true;
		}
		if ((getArticle_group() != null) && (getArticle_group().length() > 0)) {
			sql = sql + " and upper(article_group) like upper('" + getArticle_group().trim() + "')";
			doFetchData = true;
		}
		if ((getArticle_sub_group() != null) && (getArticle_sub_group().length() > 0)) {
			sql = sql + " and upper(article_sub_group) like upper('" + getArticle_sub_group().trim() + "')";
			doFetchData = true;
		}
		if ((getManufacturer() != null) && (getManufacturer().length() > 0)) {
			sql = sql + " and upper(manufacturer) like upper('" + getManufacturer().trim() + "')";
			doFetchData = true;
		}
		if ((getInventory_uom() != null) && (getInventory_uom().length() > 0)) {
			sql = sql + " and upper(inventory_uom) like upper('" + getInventory_uom().trim() + "')";
			doFetchData = true;
		}
		if ((getRemark1() != null) && (getRemark1().length() > 0)) {
			sql = sql + " and upper(remark1) like upper('" + getRemark1().trim() + "')";
			doFetchData = true;
		}
		if ((getRemark2() != null) && (getRemark2().length() > 0)) {
			sql = sql + " and upper(remark2) like upper('" + getRemark2().trim() + "')";
			doFetchData = true;
		}
		if ((getRemark3() != null) && (getRemark3().length() > 0)) {
			sql = sql + " and upper(remark3) like upper('" + getRemark3().trim() + "')";
			doFetchData = true;
		}
		if ((getRack() != null) && (getRack().length() > 0)) {
			sql = sql + " and upper(rack) like upper('" + getRack().trim() + "')";
			doFetchData = true;
		}
		if ((getBar_code() != null) && (getBar_code().length() > 0)) {
			sql = sql
					+ " and exists(SELECT 1 FROM article_items A WHERE P.ARTICLE_NO= A.ARTICLE_NO AND upper(BAR_CODE) like upper('"
					+ getBar_code().trim() + "'))";
			doFetchData = true;
		}
		if ((getSearch_text() != null) && (getSearch_text().length() > 0)) {
			sql = sql
					+ " and exists(SELECT 1 FROM article_items A WHERE P.ARTICLE_NO= A.ARTICLE_NO AND upper(nvl(BAR_CODE,'')||nvl(article_no,'')||nvl(item_code,'')||nvl(item_desc,'')) like upper('%"
					+ getSearch_text().trim() + "%'))";
			doFetchData = true;
		}
		List<Article> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			Article temp = new Article();

			temp.setArticleNo(rs.getString("article_no"));
			temp.setArticleDesc(rs.getString("article_desc"));
			temp.setArticle_type(rs.getString("article_type"));
			temp.setArticle_group(rs.getString("article_group"));
			temp.setArticle_sub_group(rs.getString("article_sub_group"));
			temp.setArticle_size(rs.getString("article_size"));
			temp.setColor(rs.getString("color"));

			temp.setArticle_short_desc(rs.getString("article_short_desc"));
			temp.setManufacturer(rs.getString("manufacturer"));
			temp.setInventory_uom(rs.getString("inventory_uom"));
			temp.setStyle(rs.getString("style"));
			temp.setMin_stock(rs.getDouble("min_stock"));
			temp.setMax_stock(rs.getDouble("max_stock"));

			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setRemark1(rs.getString("remark1"));
			temp.setRemark2(rs.getString("remark2"));
			temp.setRemark3(rs.getString("remark3"));
			temp.setTax_per(rs.getDouble("tax_per"));
			temp.setDisc_per(rs.getDouble("disc_per"));
			temp.setOpen_bal(rs.getDouble("open_bal"));
			temp.setRack(rs.getString("rack"));
			temp.setWsp_disc_per(rs.getDouble("wsp_disc_per"));

			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException, Exception {
		String sql = "select article_no,\n       article_desc,\n       article_type,\n       article_group,\n       article_sub_group,\n       article_size,\n       color,\n       article_short_desc,\n       manufacturer,\n       inventory_uom,\n       style,\n       min_stock,\n       max_stock,       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       remark1,       remark2,       remark3,       tax_per,       disc_per,       open_bal,       rack,       wsp_disc_per  from article p\n where article_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.articleNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setArticleNo(rs.getString("article_no"));
			setArticleDesc(rs.getString("article_desc"));
			setArticleDesc(rs.getString("article_desc"));
			setArticle_type(rs.getString("article_type"));
			setArticle_group(rs.getString("article_group"));
			setArticle_sub_group(rs.getString("article_sub_group"));
			setArticle_size(rs.getString("article_size"));
			setColor(rs.getString("color"));

			setArticle_short_desc(rs.getString("article_short_desc"));
			setManufacturer(rs.getString("manufacturer"));
			setInventory_uom(rs.getString("inventory_uom"));
			setStyle(rs.getString("style"));
			setMin_stock(rs.getDouble("min_stock"));
			setMax_stock(rs.getDouble("max_stock"));

			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setRemark1(rs.getString("remark1"));
			setRemark2(rs.getString("remark2"));
			setRemark3(rs.getString("remark3"));
			setTax_per(rs.getDouble("tax_per"));
			setDisc_per(rs.getDouble("disc_per"));
			setOpen_bal(rs.getDouble("open_bal"));
			setRack(rs.getString("rack"));
			setWsp_disc_per(rs.getDouble("wsp_disc_per"));
			ArticleItems temp = new ArticleItems();
			temp.setArticle_no(getArticleNo());
			setItemsList(temp.fetchItemsForArticle(con));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from article p\nwhere article_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.articleNo);
		ps.setDouble(2, this.objectId);
		int n = ps.executeUpdate();
		ps.close();
		if (n == 0) {
			throw new SQLException("Record Modified By Other User.");
		}
		return "Record deleted";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.articleNo != null ? this.articleNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Article)) {
			return false;
		}
		Article other = (Article) object;
		if (((this.articleNo == null) && (other.articleNo != null))
				|| ((this.articleNo != null) && (!this.articleNo.equals(other.articleNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Article No " + this.articleNo + "";
	}
}
