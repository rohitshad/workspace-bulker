/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class PurchaseOrderLines implements Serializable {
	private String purchaseOrder;
	private short lineNo;
	private String articleNo;
	private String itemCode;
	private String barCode;
	private String subSection;
	private String section;
	private String color;
	private String itemSize;
	private String brand;
	private String style;
	private String fabric;
	private String attr6;
	private Double qty;
	private String uom;
	private Double grossPp;
	private Double discPerc;
	private Double discAmt;
	private Double purPrice;
	private Double mrp;
	private Double wsp;
	private Double amount;
	private Double mpPer;
	private Double wspPer;
	private String sel;
	private String remark;
	private String noteText;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private double taxPerc;
	private double taxAmt;
	private String itemDesc;
	private String search_text;

	public PurchaseOrderLines() {
	}

	public PurchaseOrderLines(String itemCode) {
		this.itemCode = itemCode;
	}

	public PurchaseOrderLines(String itemCode, short lineNo) {
		this.itemCode = itemCode;
		this.lineNo = lineNo;
	}

	public String getBarCode() {
		return this.barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getPurchaseOrder() {
		return this.purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public short getLineNo() {
		return this.lineNo;
	}

	public void setLineNo(short lineNo) {
		this.lineNo = lineNo;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSubSection() {
		return this.subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getItemSize() {
		return this.itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getStyle() {
		return this.style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getFabric() {
		return this.fabric;
	}

	public void setFabric(String fabric) {
		this.fabric = fabric;
	}

	public String getAttr6() {
		return this.attr6;
	}

	public void setAttr6(String attr6) {
		this.attr6 = attr6;
	}

	public Double getQty() {
		if (this.qty == null) {
			return Double.valueOf(0.0D);
		}
		return this.qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public Double getGrossPp() {
		if (this.grossPp == null) {
			return Double.valueOf(0.0D);
		}
		return this.grossPp;
	}

	public void setGrossPp(Double grossPp) {
		this.grossPp = grossPp;
	}

	public Double getDiscPerc() {
		if (this.discPerc == null) {
			return Double.valueOf(0.0D);
		}
		return this.discPerc;
	}

	public void setDiscPerc(Double discPrec) {
		this.discPerc = discPrec;
	}

	public Double getDiscAmt() {
		if (this.discAmt == null) {
			return Double.valueOf(0.0D);
		}
		return this.discAmt;
	}

	public void setDiscAmt(Double discAmt) {
		this.discAmt = discAmt;
	}

	public Double getPurPrice() {
		if (this.purPrice == null) {
			return Double.valueOf(0.0D);
		}
		return this.purPrice;
	}

	public void setPurPrice(Double purPrice) {
		this.purPrice = purPrice;
	}

	public Double getMrp() {
		if (this.mrp == null) {
			return Double.valueOf(0.0D);
		}
		return this.mrp;
	}

	public void setMrp(Double mrp) {
		this.mrp = mrp;
	}

	public Double getWsp() {
		if (this.wsp == null) {
			return Double.valueOf(0.0D);
		}
		return this.wsp;
	}

	public void setWsp(Double wsp) {
		this.wsp = wsp;
	}

	public Double getAmount() {
		if (this.amount == null) {
			return Double.valueOf(0.0D);
		}
		return this.amount;
	}

	public void setAmount(Double amount) {
		this.amount = amount;
	}

	public Double getMpPer() {
		if (this.mpPer == null) {
			return Double.valueOf(0.0D);
		}
		return this.mpPer;
	}

	public void setMpPer(Double mpPer) {
		this.mpPer = mpPer;
	}

	public Double getWspPer() {
		if (this.wspPer == null) {
			return Double.valueOf(0.0D);
		}
		return this.wspPer;
	}

	public void setWspPer(Double wspPer) {
		this.wspPer = wspPer;
	}

	public String getSel() {
		return this.sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getTaxPerc() {
		return this.taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	public double getTaxAmt() {
		return this.taxAmt;
	}

	public void setTaxAmt(double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String getSearch_text() {
		return this.search_text;
	}

	public void setSearch_text(String search_text) {
		this.search_text = search_text;
	}

	public PurchaseOrderLines getDoulicate() {
		PurchaseOrderLines pod = new PurchaseOrderLines();
		pod.setPurchaseOrder(this.purchaseOrder);
		pod.setArticleNo(this.articleNo);
		pod.setBrand(this.brand);
		pod.setColor(this.color);
		pod.setDiscPerc(this.discPerc);
		pod.setFabric(this.fabric);
		pod.setItemSize(this.itemSize);
		pod.setMrp(this.mrp);
		pod.setWsp(this.wsp);
		pod.setAttr6(this.attr6);
		pod.setRemark(this.remark);
		pod.setNoteText(this.noteText);
		pod.setPurPrice(this.purPrice);
		pod.setSection(this.section);
		pod.setSubSection(this.subSection);
		pod.setStyle(this.style);
		pod.setGrossPp(this.grossPp);
		pod.setUom(this.uom);
		pod.setQty(this.qty);
		pod.setItemDesc(this.itemDesc);
		pod.setMpPer(this.mpPer);
		pod.setWspPer(this.wspPer);
		pod.setDiscAmt(this.discAmt);
		pod.setTaxPerc(this.taxPerc);
		pod.setTaxAmt(this.taxAmt);
		pod.setAmount(this.amount);
		return pod;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update purchase_order_lines\n   set item_code=?,ARTICLE_NO     = ?,\n       SUB_SECTION    = ?,\n       SECTION        = ?,\n       COLOR          = ?,\n       ITEM_SIZE      = ?,\n       BRAND          = ?,\n       STYLE          = ?,\n       FABRIC         = ?,\n       ATTR6          = ?,\n       QTY            = ?,\n       UOM            = ?,\n       GROSS_PP       = ?,\n       DISC_PErC      = ?,\n       DISC_AMT       = ?,\n       PUR_PRICE      = ?,\n       MRP            = ?,\n       WSP            = ?,\n       AMOUNT         = ?,\n       MP_PER         = ?,\n       WSP_PER        = ?,\n       SEL            = ?,\n       REMARK         = ?,\n       NOTE_TEXT      = ?,\n       CREATED_ON     = ?,\n       CREATED_BY     = ?,\n       UPDATED_ON     = ?,\n       UPDATED_BY     = ?,\n       OBJECT_ID      = nvl(OBJECT_ID, 1) + 1,\n       BAR_CODE       = ?,tax_perc=?, tax_amt=?, item_desc=?\n where PURCHASE_ORDER = ?\n  and  LINE_NO        = ?\n   and OBJECT_ID = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.itemCode);
		ps.setString(2, this.articleNo);
		ps.setString(3, this.subSection);
		ps.setString(4, this.section);
		ps.setString(5, this.color);
		ps.setString(6, this.itemSize);
		ps.setString(7, this.brand);
		ps.setString(8, this.style);
		ps.setString(9, this.fabric);
		ps.setString(10, this.attr6);
		ps.setDouble(11, Utils.getDouble(this.qty));
		ps.setString(12, this.uom);
		ps.setDouble(13, Utils.getDouble(this.grossPp));
		ps.setDouble(14, Utils.getDouble(this.discPerc));
		ps.setDouble(15, Utils.getDouble(this.discAmt));
		ps.setDouble(16, Utils.getDouble(this.purPrice));
		ps.setDouble(17, Utils.getDouble(this.mrp));
		ps.setDouble(18, Utils.getDouble(this.wsp));
		ps.setDouble(19, Utils.getDouble(this.amount));
		ps.setDouble(20, Utils.getDouble(this.mpPer));
		ps.setDouble(21, Utils.getDouble(this.wspPer));
		ps.setString(22, this.sel);
		ps.setString(23, this.remark);
		ps.setString(24, this.noteText);
		ps.setDate(25, Utils.getSqlDate(this.createdOn));
		ps.setString(26, this.createdBy);
		ps.setDate(27, Utils.getSqlDate(this.updatedOn));
		ps.setString(28, this.updatedBy);
		ps.setString(29, this.barCode);
		ps.setDouble(30, this.taxPerc);
		ps.setDouble(31, this.taxAmt);
		ps.setString(32, this.itemDesc);
		ps.setString(33, this.purchaseOrder);
		ps.setShort(34, this.lineNo);
		ps.setShort(35, this.objectId);
		int n = ps.executeUpdate();

		return "success";
	}

	private String getNextItemCode(Connection con) throws SQLException {
		String sql = "select  nvl((select c.item_code_prefix from company_parameters c where c.parameter_id='COMPANY_PARAM'),'')|| item_code_seq.nextval from dual";
		PreparedStatement ps = con.prepareStatement(sql);
		ResultSet rs = ps.executeQuery();
		int i = 0;
		if (rs.next()) {
			i = (int) rs.getDouble(1);
		}
		rs.close();
		ps.close();
		return i + "";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into purchase_order_lines\n  (purchase_order,\n   line_no,\n   article_no,\n   item_code,\n   sub_section,\n   section,\n   color,\n   item_size,\n   brand,\n   style,\n   fabric,\n   attr6,\n   qty,\n   uom,\n   gross_pp,\n   disc_perc,\n   disc_amt,\n   pur_price,\n   mrp,\n   wsp,\n   amount,\n   mp_per,\n   wsp_per,\n   sel,\n   remark,\n   note_text,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,\n   bar_code,tax_perc, tax_amt, item_desc)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.purchaseOrder);
		ps.setShort(2, this.lineNo);
		ps.setString(3, this.articleNo);
		ps.setString(4, this.itemCode);
		ps.setString(5, this.subSection);
		ps.setString(6, this.section);
		ps.setString(7, this.color);
		ps.setString(8, this.itemSize);
		ps.setString(9, this.brand);
		ps.setString(10, this.style);
		ps.setString(11, this.fabric);
		ps.setString(12, this.attr6);
		ps.setDouble(13, Utils.getDouble(this.qty));
		ps.setString(14, this.uom);
		ps.setDouble(15, Utils.getDouble(this.grossPp));
		ps.setDouble(16, Utils.getDouble(this.discPerc));
		ps.setDouble(17, Utils.getDouble(this.discAmt));
		ps.setDouble(18, Utils.getDouble(this.purPrice));
		ps.setDouble(19, Utils.getDouble(this.mrp));
		ps.setDouble(20, Utils.getDouble(this.wsp));
		ps.setDouble(21, Utils.getDouble(this.amount));
		ps.setDouble(22, Utils.getDouble(this.mpPer));
		ps.setDouble(23, Utils.getDouble(this.wspPer));
		ps.setString(24, this.sel);
		ps.setString(25, this.remark);
		ps.setString(26, this.noteText);
		ps.setDate(27, Utils.getSqlDate(this.createdOn));
		ps.setString(28, this.createdBy);
		ps.setDate(29, Utils.getSqlDate(this.updatedOn));
		ps.setString(30, this.updatedBy);
		ps.setShort(31, (short) 1);
		ps.setString(32, this.barCode);
		ps.setDouble(33, this.taxPerc);
		ps.setDouble(34, this.taxAmt);
		ps.setString(35, this.itemDesc);
		int n = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public static List<PurchaseOrderLines> load(Connection con, String po) throws SQLException {
		List<PurchaseOrderLines> list = new ArrayList();
		PurchaseOrderLines pod = null;
		String sql = "select purchase_order,\n       line_no,\n       article_no,\n       item_code,\n       sub_section,\n       section,\n       color,\n       item_size,\n       brand,\n       style,\n       fabric,\n       attr6,\n       qty,\n       uom,\n       gross_pp,\n       disc_perc,\n       disc_amt,\n       pur_price,\n       mrp,\n       wsp,\n       amount,\n       mp_per,\n       wsp_per,\n       sel,\n       remark,\n       note_text,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       bar_code,tax_perc, tax_amt, item_desc\n  from purchase_order_lines\n where purchase_order = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, po);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			pod = new PurchaseOrderLines();
			pod.setPurchaseOrder(rs.getString("purchase_Order"));
			pod.setLineNo((short) (int) rs.getDouble("line_No"));
			pod.setArticleNo(rs.getString("article_No"));
			pod.setItemCode(rs.getString("item_code"));
			pod.setBrand(rs.getString("brand"));
			pod.setColor(rs.getString("color"));
			pod.setDiscPerc(Double.valueOf(rs.getDouble("disc_Perc")));
			pod.setDiscAmt(Double.valueOf(rs.getDouble("disc_amt")));
			pod.setAmount(Double.valueOf(rs.getDouble("amount")));
			pod.setFabric(rs.getString("fabric"));
			pod.setItemSize(rs.getString("item_Size"));
			pod.setMrp(Double.valueOf(rs.getDouble("mrp")));
			pod.setWsp(Double.valueOf(rs.getDouble("wsp")));
			pod.setAttr6(rs.getString("attr6"));
			pod.setRemark(rs.getString("remark"));
			pod.setNoteText(rs.getString("note_Text"));
			pod.setPurPrice(Double.valueOf(rs.getDouble("pur_Price")));
			pod.setSection(rs.getString("section"));
			pod.setSubSection(rs.getString("sub_Section"));
			pod.setStyle(rs.getString("style"));
			pod.setGrossPp(Double.valueOf(rs.getDouble("gross_Pp")));
			pod.setUom(rs.getString("uom"));
			pod.setQty(Double.valueOf(rs.getDouble("qty")));
			pod.setCreatedOn(rs.getDate("created_On"));
			pod.setCreatedBy(rs.getString("Created_By"));
			pod.setUpdatedOn(rs.getDate("updated_On"));
			pod.setUpdatedBy(rs.getString("updated_By"));
			pod.setObjectId((short) (int) rs.getDouble("object_Id"));
			pod.setBarCode(rs.getString("bar_Code"));
			pod.setMpPer(Double.valueOf(rs.getDouble("mp_Per")));
			pod.setWspPer(Double.valueOf(rs.getDouble("wsp_Per")));
			pod.setTaxPerc(rs.getDouble("tax_perc"));
			pod.setTaxAmt(rs.getDouble("tax_amt"));
			pod.setItemDesc(rs.getString("item_desc"));
			list.add(pod);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String processLine() {
		double tot_price = getQty().doubleValue() * getGrossPp().doubleValue();

		setDiscAmt(Double.valueOf(tot_price * getDiscPerc().doubleValue() / 100.0D));

		tot_price -= getDiscAmt().doubleValue();

		setTaxAmt(tot_price * getTaxPerc() / 100.0D);

		tot_price += getTaxAmt();
		if (getQty().doubleValue() > 0.0D) {
			setPurPrice(Double.valueOf(tot_price / getQty().doubleValue()));
		} else {
			setPurPrice(Double.valueOf(tot_price));
		}
		setAmount(Double.valueOf(tot_price));
		if (getPurPrice().doubleValue() > 0.0D) {
			setMpPer(Double.valueOf(
					(getMrp().doubleValue() - getPurPrice().doubleValue()) / getPurPrice().doubleValue() * 100.0D));
			setWspPer(Double.valueOf(
					(getWsp().doubleValue() - getPurPrice().doubleValue()) / getPurPrice().doubleValue() * 100.0D));
		}
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from purchase_order_lines where purchase_Order=? and line_No=? and object_Id=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getPurchaseOrder());
		ps.setDouble(2, getLineNo());
		ps.setDouble(3, getObjectId());
		int nrd = 0;
		nrd = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.itemCode != null ? this.itemCode.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof PurchaseOrderLines)) {
			return false;
		}
		PurchaseOrderLines other = (PurchaseOrderLines) object;
		if (((this.itemCode == null) && (other.itemCode != null))
				|| ((this.itemCode != null) && (!this.itemCode.equals(other.itemCode)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "itemCode=" + this.itemCode + "";
	}

	public PurchaseOrderLines duplicate() {
		PurchaseOrderLines line = new PurchaseOrderLines();
		line.setLineNo(getLineNo());
		line.setArticleNo(getArticleNo());
		line.setItemCode(getItemCode());
		line.setBarCode(getBarCode());
		line.setSubSection(getSubSection());
		line.setSection(getSection());
		line.setColor(getColor());
		line.setItemSize(getItemSize());
		line.setBrand(getBrand());
		line.setStyle(getStyle());
		line.setFabric(getFabric());
		line.setAttr6(getAttr6());
		line.setQty(getQty());
		line.setUom(getUom());
		line.setGrossPp(getGrossPp());
		line.setDiscPerc(getDiscPerc());
		line.setDiscAmt(getDiscAmt());
		line.setPurPrice(getPurPrice());
		line.setMrp(getMrp());
		line.setWsp(getWsp());
		line.setAmount(getAmount());
		line.setMpPer(getMpPer());
		line.setWspPer(getWspPer());
		line.setSel(getSel());
		line.setRemark(getRemark());
		line.setNoteText(getNoteText());
		line.setTaxPerc(getTaxPerc());
		line.setTaxAmt(getTaxAmt());
		line.setItemDesc(getItemDesc());
		return line;
	}
}
