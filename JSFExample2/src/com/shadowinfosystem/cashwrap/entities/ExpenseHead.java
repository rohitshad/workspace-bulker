/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class ExpenseHead extends EntitiesImpl implements Serializable {
	private String head;
	private String headDesc;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;

	public String getHead() {
		return this.head;
	}

	public void setHead(String head) {
		this.head = head;
	}

	public String getHeadDesc() {
		return this.headDesc;
	}

	public void setHeadDesc(String headDesc) {
		this.headDesc = headDesc;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update expense_head p\nset head_desc=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1\nwhere head=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getHeadDesc());
		ps.setString(2, this.status);
		ps.setDate(3, Utils.getSqlDate(this.createdOn));
		ps.setString(4, this.createdBy);
		ps.setDate(5, Utils.getSqlDate(new Date()));
		ps.setString(6, getUserId());
		ps.setString(7, getHead());
		ps.setShort(8, this.objectId);
		int n = ps.executeUpdate();
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into expense_head\n  (head,\n   head_desc,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by)\nvalues\n  (?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getHead());
		ps.setString(2, getHeadDesc());
		ps.setString(3, this.status);
		ps.setDate(4, Utils.getSqlDate(new Date()));
		ps.setString(5, getUserId());
		ps.setDate(6, Utils.getSqlDate(this.updatedOn));
		ps.setString(7, this.updatedBy);
		int n = ps.executeUpdate();
		return "Record Created.";
	}

	public List<ExpenseHead> getList(Connection con) throws SQLException {
		String sql = "select head,\n       head_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from expense_head p\n where head like nvl(?,'%')";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getHead());

		ResultSet rs = ps.executeQuery();
		List<ExpenseHead> list = new ArrayList();
		while (rs.next()) {
			ExpenseHead temp = new ExpenseHead();
			temp.setHead(rs.getString("head"));
			temp.setHeadDesc(rs.getString("head_desc"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select head,\n       head_desc,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from expense_head p\n where head = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getHead());

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setHead(rs.getString("head"));
			setHeadDesc(rs.getString("head_desc"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from expense_head p\nwhere head=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getHead());
		ps.setShort(2, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new SQLException("Record Modified By Other User.");
		}
		return "Record deleted.";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.head != null ? this.head.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof ExpenseHead)) {
			return false;
		}
		ExpenseHead other = (ExpenseHead) object;
		if (((this.head == null) && (other.head != null)) || ((this.head != null) && (!this.head.equals(other.head)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Expense head :" + this.head + "";
	}
}
