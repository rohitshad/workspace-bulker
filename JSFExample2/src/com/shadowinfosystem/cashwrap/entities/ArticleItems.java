/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class ArticleItems extends EntitiesImpl implements Serializable {
	private String item_code;
	private String article_no;
	private String bar_code;
	private double pur_price;
	private double mrp;
	private double sales_price;
	private String status;
	private Date created_on;
	private String created_by;
	private Date updated_on;
	private String updated_by;
	private double object_id;
	private double wsp;
	private String itemDesc;
	private double barCodeQty;
	private boolean isEditable;
	private String article_desc;
	private String article_short_desc;
	private String inventory_uom;
	private String article_type;
	private String article_group;
	private String article_sub_group;
	private String article_size;
	private String color;
	private String manufacturer;
	private double tax_per;
	private double disc_per;
	private String search_text;
	private double qty_on_hand;

	public ArticleItems() {
		setIsEditable(false);
	}

	public boolean isIsEditable() {
		return this.isEditable;
	}

	public void setIsEditable(boolean isEditable) {
		this.isEditable = isEditable;
	}

	public String getItem_code() {
		return this.item_code;
	}

	public void setItem_code(String item_code) {
		this.item_code = item_code;
	}

	public String getArticle_no() {
		return this.article_no;
	}

	public void setArticle_no(String article_no) {
		this.article_no = article_no;
	}

	public String getBar_code() {
		return this.bar_code;
	}

	public void setBar_code(String bar_code) {
		this.bar_code = bar_code;
	}

	public double getPur_price() {
		return this.pur_price;
	}

	public void setPur_price(double pur_price) {
		this.pur_price = pur_price;
	}

	public double getMrp() {
		return this.mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getSales_price() {
		return this.sales_price;
	}

	public void setSales_price(double sales_price) {
		this.sales_price = sales_price;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreated_on() {
		return this.created_on;
	}

	public void setCreated_on(Date created_on) {
		this.created_on = created_on;
	}

	public String getCreated_by() {
		return this.created_by;
	}

	public void setCreated_by(String created_by) {
		this.created_by = created_by;
	}

	public Date getUpdated_on() {
		return this.updated_on;
	}

	public void setUpdated_on(Date updated_on) {
		this.updated_on = updated_on;
	}

	public String getUpdated_by() {
		return this.updated_by;
	}

	public void setUpdated_by(String updated_by) {
		this.updated_by = updated_by;
	}

	public double getObject_id() {
		return this.object_id;
	}

	public void setObject_id(double object_id) {
		this.object_id = object_id;
	}

	public double getWsp() {
		return this.wsp;
	}

	public void setWsp(double wsp) {
		this.wsp = wsp;
	}

	public String getArticle_desc() {
		return this.article_desc;
	}

	public void setArticle_desc(String article_desc) {
		this.article_desc = article_desc;
	}

	public String getArticle_short_desc() {
		return this.article_short_desc;
	}

	public void setArticle_short_desc(String article_short_desc) {
		this.article_short_desc = article_short_desc;
	}

	public String getInventory_uom() {
		return this.inventory_uom;
	}

	public void setInventory_uom(String inventory_uom) {
		this.inventory_uom = inventory_uom;
	}

	public String getArticle_type() {
		return this.article_type;
	}

	public void setArticle_type(String article_type) {
		this.article_type = article_type;
	}

	public String getArticle_group() {
		return this.article_group;
	}

	public void setArticle_group(String article_group) {
		this.article_group = article_group;
	}

	public String getArticle_sub_group() {
		return this.article_sub_group;
	}

	public void setArticle_sub_group(String article_sub_group) {
		this.article_sub_group = article_sub_group;
	}

	public String getArticle_size() {
		return this.article_size;
	}

	public void setArticle_size(String article_size) {
		this.article_size = article_size;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getManufacturer() {
		return this.manufacturer;
	}

	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}

	public double getDisc_per() {
		return this.disc_per;
	}

	public void setDisc_per(double disc_per) {
		this.disc_per = disc_per;
	}

	public double getQty_on_hand() {
		return this.qty_on_hand;
	}

	public void setQty_on_hand(double qty_on_hand) {
		this.qty_on_hand = qty_on_hand;
	}

	public String getSearch_text() {
		return this.search_text;
	}

	public void setSearch_text(String search_text) {
		this.search_text = search_text;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public int hashCode() {
		int hash = 5;
		hash = 37 * hash + (this.item_code != null ? this.item_code.hashCode() : 0);
		return hash;
	}

	public double getTax_per() {
		return this.tax_per;
	}

	public void setTax_per(double tax_per) {
		this.tax_per = tax_per;
	}

	public double getBarCodeQty() {
		return this.barCodeQty;
	}

	public void setBarCodeQty(double barCodeQty) {
		this.barCodeQty = barCodeQty;
	}

	public boolean equals(Object obj) {
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ArticleItems other = (ArticleItems) obj;
		if (this.item_code == null ? other.item_code != null : !this.item_code.equals(other.item_code)) {
			return false;
		}
		return true;
	}

	public String update(Connection con) throws SQLException {
		if (getObject_id() <= 0.0D) {
			return create(con);
		}
		String sql = "update article_items\n   set article_no  = ?,\n       bar_code    = ?,\n       pur_price   = ?,\n       mrp         = ?,\n       sales_price = ?,\n       status      = ?,\n       created_on  = ?,\n       created_by  = ?,\n       updated_on  = ?,\n       updated_by  = ?,\n       object_id   = nvl(object_id, 1) + 1,wsp=?,item_desc=?\n where item_code = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getArticle_no());
		ps.setString(2, getBar_code());
		ps.setDouble(3, getPur_price());
		ps.setDouble(4, getMrp());
		ps.setDouble(5, getSales_price());
		ps.setString(6, getStatus());
		ps.setDate(7, Utils.getSqlDate(getCreated_on()));
		ps.setString(8, getCreated_by());
		ps.setDate(9, Utils.getSqlDate(new Date()));
		ps.setString(10, getUpdated_by());
		ps.setDouble(11, getWsp());
		ps.setString(12, getItemDesc());
		ps.setString(13, getItem_code());
		ps.setDouble(14, getObject_id());
		double nr = 0.0D;
		nr = ps.executeUpdate();
		if (nr <= 0.0D) {
			throw new SQLException("Record could not be modified.");
		}
		return "Record updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into article_items\n  (article_no,\n   bar_code,\n   pur_price,\n   mrp,\n   sales_price,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   item_code,\n   object_id,wsp,item_desc)\nvalues\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getArticle_no());
		ps.setString(2, getBar_code());
		ps.setDouble(3, getPur_price());
		ps.setDouble(4, getMrp());
		ps.setDouble(5, getSales_price());
		ps.setString(6, getStatus());
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getCreated_by());
		ps.setDate(9, Utils.getSqlDate(getUpdated_on()));
		ps.setString(10, getUpdated_by());
		ps.setString(11, getItem_code());
		ps.setDouble(12, 1.0D);
		ps.setDouble(13, getWsp());
		ps.setString(14, getItemDesc());
		double nr = 0.0D;
		nr = ps.executeUpdate();
		if (nr <= 0.0D) {
			throw new SQLException("Record could not be created.");
		}
		return "Record created.";
	}

	public List<ArticleItems> fetchItemsForArticle(Connection con) throws SQLException, Exception {
		String sql = "select item_code,\n       article_no,\n       bar_code,\n       pur_price,\n       mrp,\n       sales_price,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n      ,wsp,item_desc  from article_items i\n where i.article_no = ? ORDER BY I.item_code";
		if ((getArticle_no() == null) || (getArticle_no().length() <= 0)) {
			throw new Exception("Please enter Atricle Number.");
		}
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getArticle_no());

		ResultSet rs = ps.executeQuery();
		List<ArticleItems> list = new ArrayList();
		while (rs.next()) {
			ArticleItems temp = new ArticleItems();
			temp.setArticle_no(rs.getString("article_no"));
			temp.setItem_code(rs.getString("item_code"));
			temp.setBar_code(rs.getString("bar_code"));
			temp.setPur_price(rs.getDouble("pur_price"));
			temp.setMrp(rs.getDouble("mrp"));
			temp.setSales_price(rs.getDouble("sales_price"));
			temp.setStatus(rs.getString("status"));
			temp.setCreated_on(rs.getDate("created_on"));
			temp.setCreated_by(rs.getString("created_by"));
			temp.setUpdated_on(rs.getDate("updated_on"));
			temp.setUpdated_by(rs.getString("updated_by"));
			temp.setObject_id(rs.getDouble("object_id"));
			temp.setWsp(rs.getDouble("wsp"));
			temp.setItemDesc(rs.getString("item_desc"));

			list.add(temp);
		}
		return list;
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete FROM article_items\n where item_code = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getItem_code());
		ps.setDouble(2, getObject_id());
		double nr = 0.0D;
		nr = ps.executeUpdate();
		if (nr <= 0.0D) {
			throw new SQLException("Record could not be delete.");
		}
		return "Record deleted.";
	}

	public List<ArticleItems> search(Connection con) throws SQLException, Exception {
		String sql = "select d.item_code,\n       d.article_no,\n       d.bar_code,\n       d.pur_price,\n       d.mrp,\n       d.sales_price,\n       d.status,\n       d.created_on,\n       d.created_by,\n       d.updated_on,\n       d.updated_by,\n       d.object_id,\n       d.wsp,\n       h.article_desc,\n       h.article_type,\n       h.article_group,\n       h.article_sub_group,\n       h.article_size,\n       h.color,\n       h.article_short_desc,\n       h.manufacturer,\n       h.inventory_uom,\n       h.style,\n       h.min_stock,\n       h.max_stock,\n       h.remark1,\n       h.remark2,\n       h.remark3,\n       h.tax_per,\n       h.disc_per,\n       h.open_bal,\n       h.rack,\n       h.wsp_disc_per, d.item_desc item_desc_line\n  from article_items d, article h\n where d.article_no = h.article_no ";

		boolean doFetchData = false;
		if ((getArticle_no() != null) && (getArticle_no().length() > 0)) {
			sql = sql + " and d.article_no='" + getArticle_no() + "' ";
			doFetchData = true;
		}
		if ((getBar_code() != null) && (getBar_code().length() > 0)) {
			sql = sql + " and d.bar_code='" + getBar_code() + "' ";
			doFetchData = true;
		}
		if ((getItem_code() != null) && (getItem_code().length() > 0)) {
			sql = sql + " and d.item_code='" + getItem_code() + "' ";
			doFetchData = true;
		}
		if ((getArticle_desc() != null) && (getArticle_desc().length() > 0)) {
			sql = sql + " and upper(h.article_desc) like upper('%" + getArticle_desc() + "%') ";
			doFetchData = true;
		}
		if ((getSearch_text() != null) && (getSearch_text().length() > 0)) {
			sql = sql + " and upper(nvl(d.bar_code,'')||nvl(d.item_code,'')||nvl(d.article_no,'')) like upper('%"
					+ getSearch_text().trim() + "%') ";
			doFetchData = true;
		}
		if ((getArticle_short_desc() != null) && (getArticle_short_desc().length() > 0)) {
			sql = sql + " and upper(h.article_short_desc) like upper('%" + getArticle_short_desc() + "%') ";
			doFetchData = true;
		}
		List<ArticleItems> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			ArticleItems temp = new ArticleItems();
			temp.setArticle_no(rs.getString("article_no"));
			temp.setItem_code(rs.getString("item_code"));
			temp.setBar_code(rs.getString("bar_code"));
			temp.setPur_price(rs.getDouble("pur_price"));
			temp.setMrp(rs.getDouble("mrp"));
			temp.setSales_price(rs.getDouble("sales_price"));
			temp.setStatus(rs.getString("status"));
			temp.setCreated_on(rs.getDate("created_on"));
			temp.setCreated_by(rs.getString("created_by"));
			temp.setUpdated_on(rs.getDate("updated_on"));
			temp.setUpdated_by(rs.getString("updated_by"));
			temp.setObject_id(rs.getDouble("object_id"));
			temp.setWsp(rs.getDouble("wsp"));

			temp.setArticle_desc(rs.getString("article_desc"));
			temp.setArticle_short_desc(rs.getString("article_short_desc"));
			temp.setInventory_uom(rs.getString("inventory_uom"));
			temp.setArticle_type(rs.getString("article_type"));
			temp.setArticle_group(rs.getString("article_group"));
			temp.setArticle_sub_group(rs.getString("article_sub_group"));
			temp.setArticle_size(rs.getString("article_size"));
			temp.setColor(rs.getString("color"));
			temp.setManufacturer(rs.getString("manufacturer"));
			temp.setTax_per(rs.getDouble("tax_per"));
			temp.setDisc_per(rs.getDouble("disc_per"));
			temp.setItemDesc(rs.getString("item_desc_line"));

			list.add(temp);
		}
		return list;
	}
}
