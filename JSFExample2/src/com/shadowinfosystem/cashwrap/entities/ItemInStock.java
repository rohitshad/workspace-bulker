/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class ItemInStock extends EntitiesImpl implements Serializable {
	private String itemCode;
	private String barCode;
	private String subSection;
	private String section;
	private String color;
	private String itemSize;
	private String brand;
	private String style;
	private String fabric;
	private String attr6;
	private double qty;
	private double qtyOnHand;
	private String uom;
	private double grossPp;
	private double discPerc;
	private double discAmt;
	private double taxPerc;
	private double taxAmt;
	private double purPrice;
	private double mrp;
	private double wsp;
	private double amount;
	private double mpPer;
	private double wspPer;
	private String sel;
	private String remark;
	private String noteText;
	private String purchaseOrder;
	private Short lineNo;
	private Date receiptDate;
	private String articleNo;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private String itemDesc;
	private boolean zeroStcokAllowed;
	private String batch;
	private Date expDate;
	private boolean isItemCodeInStock;

	public ItemInStock() {
		this.zeroStcokAllowed = true;
	}

	public ItemInStock(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getItemCode() {
		return this.itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getSubSection() {
		return this.subSection;
	}

	public void setSubSection(String subSection) {
		this.subSection = subSection;
	}

	public String getSection() {
		return this.section;
	}

	public void setSection(String section) {
		this.section = section;
	}

	public String getColor() {
		return this.color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getItemSize() {
		return this.itemSize;
	}

	public void setItemSize(String itemSize) {
		this.itemSize = itemSize;
	}

	public String getBrand() {
		return this.brand;
	}

	public void setBrand(String brand) {
		this.brand = brand;
	}

	public String getStyle() {
		return this.style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getFabric() {
		return this.fabric;
	}

	public void setFabric(String fabric) {
		this.fabric = fabric;
	}

	public String getAttr6() {
		return this.attr6;
	}

	public void setAttr6(String attr6) {
		this.attr6 = attr6;
	}

	public double getQty() {
		return this.qty;
	}

	public void setQty(double qty) {
		this.qty = qty;
	}

	public double getQtyOnHand() {
		return this.qtyOnHand;
	}

	public void setQtyOnHand(double qtyOnHand) {
		this.qtyOnHand = qtyOnHand;
	}

	public String getUom() {
		return this.uom;
	}

	public void setUom(String uom) {
		this.uom = uom;
	}

	public double getGrossPp() {
		return this.grossPp;
	}

	public void setGrossPp(double grossPp) {
		this.grossPp = grossPp;
	}

	public double getDiscPerc() {
		return this.discPerc;
	}

	public void setDiscPerc(double discPerc) {
		this.discPerc = discPerc;
	}

	public double getDiscAmt() {
		return this.discAmt;
	}

	public void setDiscAmt(double discAmt) {
		this.discAmt = discAmt;
	}

	public double getTaxPerc() {
		return this.taxPerc;
	}

	public void setTaxPerc(double taxPerc) {
		this.taxPerc = taxPerc;
	}

	public double getTaxAmt() {
		return this.taxAmt;
	}

	public void setTaxAmt(double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public double getPurPrice() {
		return this.purPrice;
	}

	public void setPurPrice(double purPrice) {
		this.purPrice = purPrice;
	}

	public double getMrp() {
		return this.mrp;
	}

	public void setMrp(double mrp) {
		this.mrp = mrp;
	}

	public double getWsp() {
		return this.wsp;
	}

	public void setWsp(double wsp) {
		this.wsp = wsp;
	}

	public double getAmount() {
		return this.amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getMpPer() {
		return this.mpPer;
	}

	public void setMpPer(double mpPer) {
		this.mpPer = mpPer;
	}

	public double getWspPer() {
		return this.wspPer;
	}

	public void setWspPer(double wspPer) {
		this.wspPer = wspPer;
	}

	public String getSel() {
		return this.sel;
	}

	public void setSel(String sel) {
		this.sel = sel;
	}

	public String getRemark() {
		return this.remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getPurchaseOrder() {
		return this.purchaseOrder;
	}

	public void setPurchaseOrder(String purchaseOrder) {
		this.purchaseOrder = purchaseOrder;
	}

	public Short getLineNo() {
		return this.lineNo;
	}

	public void setLineNo(Short lineNo) {
		this.lineNo = lineNo;
	}

	public Date getReceiptDate() {
		return this.receiptDate;
	}

	public void setReceiptDate(Date receiptDate) {
		this.receiptDate = receiptDate;
	}

	public String getArticleNo() {
		return this.articleNo;
	}

	public void setArticleNo(String articleNo) {
		this.articleNo = articleNo;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public String getBarCode() {
		return this.barCode;
	}

	public void setBarCode(String barCode) {
		this.barCode = barCode;
	}

	public String getItemDesc() {
		return this.itemDesc;
	}

	public void setItemDesc(String itemDesc) {
		this.itemDesc = itemDesc;
	}

	public boolean isZeroStcokAllowed() {
		return this.zeroStcokAllowed;
	}

	public void setZeroStcokAllowed(boolean zeroStcokAllowed) {
		this.zeroStcokAllowed = zeroStcokAllowed;
	}

	public boolean isIsItemCodeInStock() {
		return this.isItemCodeInStock;
	}

	public void setIsItemCodeInStock(boolean isItemCodeInStock) {
		this.isItemCodeInStock = isItemCodeInStock;
	}

	public String getBatch() {
		return this.batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getExpDate() {
		return this.expDate;
	}

	public void setExpDate(Date expDate) {
		this.expDate = expDate;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update Item_in_stock \n   set PURCHASE_ORDER = ?,\n       LINE_NO        = ?,\n       ARTICLE_NO     = ?,\n       SUB_SECTION    = ?,\n       SECTION        = ?,\n       COLOR          = ?,\n       ITEM_SIZE      = ?,\n       BRAND          = ?,\n       STYLE          = ?,\n       FABRIC         = ?,\n       ATTR6          = ?,\n       QTY            = ?,\n       QTY_on_hand    = ?,\n       UOM            = ?,\n       GROSS_PP       = ?,\n       DISC_PErC      = ?,\n       DISC_AMT       = ?,\n       PUR_PRICE      = ?,\n       MRP            = ?,\n       WSP            = ?,\n       AMOUNT         = ?,\n       MP_PER         = ?,\n       WSP_PER        = ?,\n       SEL            = ?,\n       REMARK         = ?,\n       NOTE_TEXT      = ?,\n       CREATED_ON     = ?,\n       CREATED_BY     = ?,\n       UPDATED_ON     = ?,\n       UPDATED_BY     = ?,\n       OBJECT_ID      = nvl(OBJECT_ID, 1) + 1,\n       BAR_CODE       = ?,\n       receipt_date   = ?, item_desc=? , tax_perc=?, tax_amt=?, batch=?, exp_date=? where ITEM_CODE = ?\n   and OBJECT_ID = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.purchaseOrder);
		ps.setShort(2, this.lineNo.shortValue());
		ps.setString(3, this.articleNo);
		ps.setString(4, this.subSection);
		ps.setString(5, this.section);
		ps.setString(6, this.color);
		ps.setString(7, this.itemSize);
		ps.setString(8, this.brand);
		ps.setString(9, this.style);
		ps.setString(10, this.fabric);
		ps.setString(11, this.attr6);
		ps.setDouble(12, this.qty);
		ps.setDouble(13, this.qtyOnHand);
		ps.setString(14, this.uom);
		ps.setDouble(15, this.grossPp);
		ps.setDouble(16, this.discPerc);
		ps.setDouble(17, this.discAmt);
		ps.setDouble(18, this.purPrice);
		ps.setDouble(19, this.mrp);
		ps.setDouble(20, this.wsp);
		ps.setDouble(21, this.amount);
		ps.setDouble(22, this.mpPer);
		ps.setDouble(23, this.wspPer);
		ps.setString(24, this.sel);
		ps.setString(25, this.remark);
		ps.setString(26, this.noteText);
		ps.setDate(27, Utils.getSqlDate(this.createdOn));
		ps.setString(28, this.createdBy);
		ps.setDate(29, Utils.getSqlDate(this.updatedOn));
		ps.setString(30, this.updatedBy);
		ps.setString(31, this.barCode);
		ps.setDate(32, Utils.getSqlDate(this.receiptDate));
		ps.setString(33, this.itemDesc);
		ps.setDouble(34, Utils.getDouble(Double.valueOf(this.taxPerc)));
		ps.setDouble(35, Utils.getDouble(Double.valueOf(this.taxAmt)));
		ps.setString(36, getBatch());
		ps.setDate(37, Utils.getSqlDate(getExpDate()));
		ps.setString(38, this.itemCode);
		ps.setShort(39, this.objectId);
		int n = ps.executeUpdate();
		if (n <= 0) {
			throw new SQLException("Stock could not be updated.");
		}
		return "success";
	}

	public String create(Connection con) throws SQLException {
		setObjectId((short) 1);
		String sql = "insert into Item_in_stock\n  (purchase_order,\n   line_no,\n   article_no,\n   item_code,\n   sub_section,\n   section,\n   color,\n   item_size,\n   brand,\n   style,\n   fabric,\n   attr6,\n   qty,qty_on_hand,\n   uom,\n   gross_pp,\n   disc_perc,\n   disc_amt,\n   pur_price,\n   mrp,\n   wsp,\n   amount,\n   mp_per,\n   wsp_per,\n   sel,\n   remark,\n   note_text,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,\n   bar_code,RECEIPT_DATE,item_desc,tax_perc, tax_amt,batch,exp_date)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.purchaseOrder);
		ps.setShort(2, this.lineNo.shortValue());
		ps.setString(3, this.articleNo);
		ps.setString(4, this.itemCode);
		ps.setString(5, this.subSection);
		ps.setString(6, this.section);
		ps.setString(7, this.color);
		ps.setString(8, this.itemSize);
		ps.setString(9, this.brand);
		ps.setString(10, this.style);
		ps.setString(11, this.fabric);
		ps.setString(12, this.attr6);
		ps.setDouble(13, Utils.getDouble(Double.valueOf(this.qty)));
		ps.setDouble(14, Utils.getDouble(Double.valueOf(this.qtyOnHand)));
		ps.setString(15, this.uom);
		ps.setDouble(16, Utils.getDouble(Double.valueOf(this.grossPp)));
		ps.setDouble(17, Utils.getDouble(Double.valueOf(this.discPerc)));
		ps.setDouble(18, Utils.getDouble(Double.valueOf(this.discAmt)));
		ps.setDouble(19, Utils.getDouble(Double.valueOf(this.purPrice)));
		ps.setDouble(20, Utils.getDouble(Double.valueOf(this.mrp)));
		ps.setDouble(21, Utils.getDouble(Double.valueOf(this.wsp)));
		ps.setDouble(22, Utils.getDouble(Double.valueOf(this.amount)));
		ps.setDouble(23, Utils.getDouble(Double.valueOf(this.mpPer)));
		ps.setDouble(24, Utils.getDouble(Double.valueOf(this.wspPer)));
		ps.setString(25, this.sel);
		ps.setString(26, this.remark);
		ps.setString(27, this.noteText);
		ps.setDate(28, Utils.getSqlDate(this.createdOn));
		ps.setString(29, this.createdBy);
		ps.setDate(30, Utils.getSqlDate(this.updatedOn));
		ps.setString(31, this.updatedBy);
		ps.setShort(32, this.objectId);
		ps.setString(33, this.barCode);
		ps.setDate(34, Utils.getSqlDate(this.receiptDate));
		ps.setString(35, this.itemDesc);
		ps.setDouble(36, Utils.getDouble(Double.valueOf(this.taxPerc)));
		ps.setDouble(37, Utils.getDouble(Double.valueOf(this.taxAmt)));
		ps.setString(38, getBatch());
		ps.setDate(39, Utils.getSqlDate(getExpDate()));
		int n = ps.executeUpdate();
		return "success";
	}

	public String load(Connection con) throws SQLException {
		setIsItemCodeInStock(false);
		String sql = "select purchase_order,\n       line_no,\n       article_no,\n       item_code,\n       sub_section,\n       section,\n       color,\n       item_size,\n       brand,\n       style,\n       fabric,\n       attr6,\n       qty,qty_on_hand,receipt_date,\n       uom,\n       gross_pp,\n       disc_perc,\n       disc_amt,\n       pur_price,\n       mrp,\n       wsp,\n       amount,\n       mp_per,\n       wsp_per,\n       sel,\n       remark,\n       note_text,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       bar_code,item_desc,tax_perc, tax_amt, batch, exp_date\n  from Item_in_stock\n where item_code = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getItemCode());
		ResultSet rs = ps.executeQuery();
		boolean found = false;
		if (rs.next()) {
			setPurchaseOrder(rs.getString("purchase_Order"));
			setLineNo(Short.valueOf((short) (int) rs.getDouble("line_No")));
			setArticleNo(rs.getString("article_No"));
			setItemCode(rs.getString("item_code"));
			setBrand(rs.getString("brand"));
			setColor(rs.getString("color"));
			setDiscPerc(rs.getDouble("disc_Perc"));
			setDiscAmt(rs.getDouble("disc_amt"));
			setAmount(rs.getDouble("amount"));
			setFabric(rs.getString("fabric"));
			setItemSize(rs.getString("item_Size"));
			setMrp(rs.getDouble("mrp"));
			setWsp(rs.getDouble("wsp"));
			setAttr6(rs.getString("attr6"));
			setRemark(rs.getString("remark"));
			setNoteText(rs.getString("note_Text"));
			setPurPrice(rs.getDouble("pur_Price"));
			setSection(rs.getString("section"));
			setSubSection(rs.getString("sub_Section"));
			setStyle(rs.getString("style"));
			setGrossPp(rs.getDouble("gross_Pp"));
			setUom(rs.getString("uom"));
			setQty(rs.getDouble("qty"));
			setQtyOnHand(rs.getDouble("qty_on_hand"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setBarCode(rs.getString("bar_Code"));
			setMpPer(rs.getDouble("mp_Per"));
			setWspPer(rs.getDouble("wsp_Per"));
			setReceiptDate(rs.getDate("receipt_date"));
			setItemDesc(rs.getString("item_desc"));
			setTaxPerc(rs.getDouble("tax_perc"));
			setTaxAmt(rs.getDouble("tax_amt"));
			setIsItemCodeInStock(true);
			setBatch(rs.getString("batch"));
			setExpDate(rs.getDate("exp_date"));
			setIsItemCodeInStock(true);
			found = true;
		}
		rs.close();
		ps.close();
		if (found) {
			return "success";
		}
		return "failed";
	}

	public List<ItemInStock> getList(Connection con) throws SQLException {
		List<ItemInStock> list = new ArrayList();
		ItemInStock st = null;
		String sql = "select purchase_order,\n       line_no,\n       article_no,\n       item_code,\n       sub_section,\n       section,\n       color,\n       item_size,\n       brand,\n       style,\n       fabric,\n       attr6,\n       qty,qty_on_hand,receipt_date,\n       uom,\n       gross_pp,\n       disc_perc,\n       disc_amt,\n       pur_price,\n       mrp,\n       wsp,\n       amount,\n       mp_per,\n       wsp_per,\n       sel,\n       remark,\n       note_text,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       bar_code,item_desc,tax_perc, tax_amt,batch,exp_date\n  from Item_in_stock \n where  1 = 1 ";

		boolean needFetchData = false;
		if ((this.itemCode != null) && (this.itemCode.length() > 0)) {
			sql = sql + " and upper(item_code) like upper('" + getItemCode() + "') ";
			needFetchData = true;
		}
		if ((this.articleNo != null) && (this.articleNo.length() > 0)) {
			sql = sql + " and upper(article_no) like upper('" + getArticleNo() + "') ";
			needFetchData = true;
		}
		if ((this.barCode != null) && (this.barCode.length() > 0)) {
			sql = sql + " and upper(bar_code) like upper('" + getBarCode() + "') ";
			needFetchData = true;
		}
		if ((this.purchaseOrder != null) && (this.purchaseOrder.length() > 0)) {
			sql = sql + " and upper(purchase_order) like upper('" + getPurchaseOrder() + "') ";
			needFetchData = true;
		}
		if ((this.itemDesc != null) && (this.itemDesc.length() > 0)) {
			sql = sql + " and upper(item_desc) like upper('%" + getItemDesc() + "%') ";
			needFetchData = true;
		}
		if ((this.section != null) && (this.section.length() > 0)) {
			sql = sql + " and upper(section) like upper('" + getSection() + "') ";
			needFetchData = true;
		}
		if ((this.subSection != null) && (this.subSection.length() > 0)) {
			sql = sql + " and upper(sub_section) like upper('" + getSubSection() + "') ";
			needFetchData = true;
		}
		if ((this.itemSize != null) && (this.itemSize.length() > 0)) {
			sql = sql + " and upper(item_size) like upper('" + getItemSize() + "') ";
			needFetchData = true;
		}
		if ((this.brand != null) && (this.brand.length() > 0)) {
			sql = sql + " and upper(brand) like upper('" + getBrand() + "') ";
			needFetchData = true;
		}
		if (!isZeroStcokAllowed()) {
			sql = sql + " and qty_on_hand > 0 ";
		}
		if (!needFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setFetchSize(50);
		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			st = new ItemInStock();
			st.setPurchaseOrder(rs.getString("purchase_Order"));
			st.setLineNo(Short.valueOf((short) (int) rs.getDouble("line_No")));
			st.setArticleNo(rs.getString("article_No"));
			st.setItemCode(rs.getString("item_code"));
			st.setBrand(rs.getString("brand"));
			st.setColor(rs.getString("color"));
			st.setDiscPerc(rs.getDouble("disc_Perc"));
			st.setDiscAmt(rs.getDouble("disc_amt"));
			st.setAmount(rs.getDouble("amount"));
			st.setFabric(rs.getString("fabric"));
			st.setItemSize(rs.getString("item_Size"));
			st.setMrp(rs.getDouble("mrp"));
			st.setWsp(rs.getDouble("wsp"));
			st.setAttr6(rs.getString("attr6"));
			st.setRemark(rs.getString("remark"));
			st.setNoteText(rs.getString("note_Text"));
			st.setPurPrice(rs.getDouble("pur_Price"));
			st.setSection(rs.getString("section"));
			st.setSubSection(rs.getString("sub_Section"));
			st.setStyle(rs.getString("style"));
			st.setGrossPp(rs.getDouble("gross_Pp"));
			st.setUom(rs.getString("uom"));
			st.setQty(rs.getDouble("qty"));
			st.setQtyOnHand(rs.getDouble("qty_on_hand"));
			st.setCreatedOn(rs.getDate("created_On"));
			st.setCreatedBy(rs.getString("Created_By"));
			st.setUpdatedOn(rs.getDate("updated_On"));
			st.setUpdatedBy(rs.getString("updated_By"));
			st.setObjectId((short) (int) rs.getDouble("object_Id"));
			st.setBarCode(rs.getString("bar_Code"));
			st.setMpPer(rs.getDouble("mp_Per"));
			st.setWspPer(rs.getDouble("wsp_Per"));
			st.setReceiptDate(rs.getDate("receipt_date"));
			st.setItemDesc(rs.getString("item_desc"));
			st.setTaxPerc(rs.getDouble("tax_perc"));
			st.setTaxAmt(rs.getDouble("tax_amt"));
			st.setBatch(rs.getString("batch"));
			st.setExpDate(rs.getDate("exp_date"));
			list.add(st);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String delete(Connection con) throws SQLException {
		String sql = "delete from item_in_stock where item_code=? and object_Id=?";
		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.itemCode);
		ps.setDouble(2, this.objectId);
		int nrd = 0;
		nrd = ps.executeUpdate();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.itemCode != null ? this.itemCode.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof ItemInStock)) {
			return false;
		}
		ItemInStock other = (ItemInStock) object;
		if (((this.itemCode == null) && (other.itemCode != null))
				|| ((this.itemCode != null) && (!this.itemCode.equals(other.itemCode)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Item Code :" + this.itemCode + "";
	}
}
