/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class Roles extends EntitiesImpl implements Serializable {
	private String role;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;

	public Roles() {
	}

	public Roles(String role) {
		this.role = role;
	}

	public String getRole() {
		return this.role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		setUpdatedOn(new Date());
		String sql = "update roles t\n   set created_on = ?,\n       created_by = ?,\n       updated_on = ?,\n       updated_by = ?,\n       object_id  = object_id + 1\n where role = ?\n   and object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(this.createdOn));
		ps.setString(2, this.createdBy);
		ps.setDate(3, Utils.getSqlDate(this.updatedOn));
		ps.setString(4, this.updatedBy);
		ps.setString(5, this.role);
		ps.setDouble(6, this.objectId);
		int n = ps.executeUpdate();
		return "Record Updated.";
	}

	public String create(Connection con) throws Exception {
		setCreatedOn(new Date());
		setObjectId(1.0D);
		int n = 0;
		String sql = "insert into roles\n  (role,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id)\nvalues\n  (?, ?, ?, ?, ?, ?)";
		try {
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setString(1, this.role);
			ps.setDate(2, Utils.getSqlDate(this.createdOn));
			ps.setString(3, this.createdBy);
			ps.setDate(4, Utils.getSqlDate(this.updatedOn));
			ps.setString(5, this.updatedBy);
			ps.setDouble(6, this.objectId);
			n = ps.executeUpdate();
			ps.close();
			return "Record Created.";
		} catch (SQLException ex) {
			if (n == 0) {
				throw new Exception("Record could not be created." + ex.getMessage());
			}
		}
		return "Success";
	}

	public List<Roles> getList(Connection con) throws SQLException {
		String sql = "select role,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from roles\n where role like nvl(?,role) ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.role);

		ResultSet rs = ps.executeQuery();
		List<Roles> list = new ArrayList();
		while (rs.next()) {
			Roles temp = new Roles();
			temp.setRole(rs.getString("role"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId(rs.getDouble("object_Id"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select role,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id\n  from roles\n where role =? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.role);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setRole(rs.getString("role"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.role != null ? this.role.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof Roles)) {
			return false;
		}
		Roles other = (Roles) object;
		if (((this.role == null) && (other.role != null)) || ((this.role != null) && (!this.role.equals(other.role)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Role : " + this.role + "";
	}
}
