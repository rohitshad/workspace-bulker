/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class CompanyParameters extends EntitiesImpl implements Serializable {
	private static final long serialVersionUID = 1L;
	private String parameterId;
	private String tin;
	private String cst;
	private String lst;
	private String phone;
	private String invoiceFooterNotes;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private double objectId;
	private String iteCodePrefix;

	public String getTin() {
		return this.tin;
	}

	public void setTin(String tin) {
		this.tin = tin;
	}

	public String getCst() {
		return this.cst;
	}

	public void setCst(String cst) {
		this.cst = cst;
	}

	public String getLst() {
		return this.lst;
	}

	public void setLst(String lst) {
		this.lst = lst;
	}

	public String getPhone() {
		return this.phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public String getInvoiceFooterNotes() {
		return this.invoiceFooterNotes;
	}

	public void setInvoiceFooterNotes(String invoiceFooterNotes) {
		this.invoiceFooterNotes = invoiceFooterNotes;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public double getObjectId() {
		return this.objectId;
	}

	public void setObjectId(double objectId) {
		this.objectId = objectId;
	}

	public String getParameterId() {
		return this.parameterId;
	}

	public void setParameterId(String parameterId) {
		this.parameterId = parameterId;
	}

	public String getIteCodePrefix() {
		return this.iteCodePrefix;
	}

	public void setIteCodePrefix(String iteCodePrefix) {
		this.iteCodePrefix = iteCodePrefix;
	}

	public String update(Connection con) throws SQLException, Exception {
		if (getObjectId() < 1.0D) {
			return create(con);
		}
		String sql = "update company_parameters p\n   set tin                   = ?,\n       cst                   = ?,\n       lst                   = ?,\n       phone                 = ?,\n       invoice_footer_notes = ?,\n       created_on            = ?,\n       created_by            = ?,\n       updated_on            = ?,\n       updated_by            = ?,\n       object_id             = object_id + 1, item_code_prefix=?\n where p.parameter_id = ?\n   and p.object_id = ?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setString(1, getTin());
		ps.setString(2, getCst());
		ps.setString(3, getLst());
		ps.setString(4, getPhone());
		ps.setString(5, getInvoiceFooterNotes());
		ps.setDate(6, Utils.getSqlDate(this.createdOn));
		ps.setString(7, this.createdBy);
		ps.setDate(8, Utils.getSqlDate(new Date()));
		ps.setString(9, getUserId());
		ps.setString(10, getIteCodePrefix());
		ps.setString(11, getParameterId());
		ps.setDouble(12, this.objectId);
		int n = ps.executeUpdate();
		if (n == 0) {
			throw new Exception("Company Parameter record could not be Updated.");
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		String sql = "insert into company_parameters\n  (parameter_id,\n   tin,\n   cst,\n   lst,\n   phone,\n   invoice_footer_notes,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,item_code_prefix) values\n  (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, getParameterId());
		ps.setString(2, getTin());
		ps.setString(3, getCst());
		ps.setString(4, getLst());
		ps.setString(5, getPhone());
		ps.setString(6, getInvoiceFooterNotes());
		ps.setDate(7, Utils.getSqlDate(new Date()));
		ps.setString(8, getUserId());
		ps.setDate(9, Utils.getSqlDate(new Date()));
		ps.setString(10, this.updatedBy);
		ps.setDouble(11, 1.0D);
		ps.setString(12, getIteCodePrefix());
		int n = ps.executeUpdate();
		return "Record Created.";
	}

	public List<CompanyParameters> getList(Connection con) throws SQLException {
		String sql = "select parameter_id,\n       tin,\n       cst,\n       lst,\n       phone,\n       invoice_footer_notes,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,item_code_prefix\n  from company_parameters p\n where 1 = 1";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.parameterId);

		ResultSet rs = ps.executeQuery();
		List<CompanyParameters> list = new ArrayList();
		while (rs.next()) {
			CompanyParameters temp = new CompanyParameters();
			temp.setParameterId(rs.getString("parameter_id"));
			temp.setTin(rs.getString("tin"));
			temp.setCst(rs.getString("cst"));
			temp.setLst(rs.getString("lst"));
			temp.setPhone(rs.getString("phone"));
			temp.setInvoiceFooterNotes(rs.getString("invoice_footer_notes"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setIteCodePrefix(rs.getString("item_code_prefix"));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select parameter_id,\n       tin,\n       cst,\n       lst,\n       phone,\n       invoice_footer_notes,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,item_code_prefix\n  from company_parameters p\n where p.parameter_id = ? ";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.parameterId);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setParameterId(rs.getString("parameter_Id"));
			setTin(rs.getString("tin"));
			setCst(rs.getString("cst"));
			setLst(rs.getString("lst"));
			setPhone(rs.getString("phone"));
			setInvoiceFooterNotes(rs.getString("invoice_footer_notes"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId(rs.getDouble("object_Id"));
			setIteCodePrefix(rs.getString("item_code_prefix"));
		}
		rs.close();
		ps.close();
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.parameterId != null ? this.parameterId.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof CompanyParameters)) {
			return false;
		}
		CompanyParameters other = (CompanyParameters) object;
		if (((this.parameterId == null) && (other.parameterId != null))
				|| ((this.parameterId != null) && (!this.parameterId.equals(other.parameterId)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "Comapny Parameter :" + this.parameterId + "";
	}
}
