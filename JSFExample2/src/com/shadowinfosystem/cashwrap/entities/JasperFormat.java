/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

public class JasperFormat {
	private String formatCode;
	private String formatDesc;

	public JasperFormat(String formatCode, String formatDesc) {
		this.formatCode = formatCode;
		this.formatDesc = formatDesc;
	}

	public JasperFormat() {
	}

	public void setFormatCode(String formatCode) {
		this.formatCode = formatCode;
	}

	public void setFormatDesc(String formatDesc) {
		this.formatDesc = formatDesc;
	}

	public String getFormatCode() {
		return this.formatCode;
	}

	public String getFormatDesc() {
		return this.formatDesc;
	}
}
