/*** 
 * Shadowinfosystem, copyright (c) 2017, info@shadowinfosystem.com
 * Developer -  Ankit Vidua 
 *  
 *  ***/
package com.shadowinfosystem.cashwrap.entities;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.shadowinfosystem.cashwrap.common.Utils;

public class PurchaseOrderReturn extends EntitiesImpl implements Serializable {
	private static final long serialVersionUID = 1L;
	private String poRetNo;
	private Date poRetDate;
	private String prNo;
	private Date prDate;
	private String billNo;
	private Date billDate;
	private String chalanNo;
	private Date chalanDate;
	private String supplierId;
	private String supplierName;
	private String supplierAddress;
	private String noteText;
	private Double totalQty;
	private Double subTotal;
	private Double discPer;
	private Double discAmt;
	private Double exciseOh;
	private Double taxPer;
	private Double taxAmt;
	private Double freight;
	private Double otherCharges;
	private Double roundOff;
	private Double netAmt;
	private String status;
	private Date createdOn;
	private String createdBy;
	private Date updatedOn;
	private String updatedBy;
	private short objectId;
	private List<PurchaseOrderRetLines> poRetLines;
	private PurchaseOrderRetLines poRetLineSelected;
	private Date fromDate;
	private Date toDate;

	public PurchaseOrderReturn() {
		this.poRetLines = new ArrayList();
		this.poRetLineSelected = new PurchaseOrderRetLines();
	}

	public String getPoRetNo() {
		return this.poRetNo;
	}

	public void setPoRetNo(String poRetNo) {
		this.poRetNo = poRetNo;
	}

	public Date getPoRetDate() {
		return this.poRetDate;
	}

	public void setPoRetDate(Date poRetDate) {
		this.poRetDate = poRetDate;
	}

	public String getPrNo() {
		return this.prNo;
	}

	public void setPrNo(String prNo) {
		this.prNo = prNo;
	}

	public Date getPrDate() {
		return this.prDate;
	}

	public void setPrDate(Date poDate) {
		this.prDate = this.prDate;
	}

	public String getBillNo() {
		return this.billNo;
	}

	public void setBillNo(String billNo) {
		this.billNo = billNo;
	}

	public Date getBillDate() {
		return this.billDate;
	}

	public void setBillDate(Date billDate) {
		this.billDate = billDate;
	}

	public String getChalanNo() {
		return this.chalanNo;
	}

	public void setChalanNo(String chalanNo) {
		this.chalanNo = chalanNo;
	}

	public Date getChalanDate() {
		return this.chalanDate;
	}

	public void setChalanDate(Date chalanDate) {
		this.chalanDate = chalanDate;
	}

	public String getSupplierId() {
		return this.supplierId;
	}

	public void setSupplierId(String supplierId) {
		this.supplierId = supplierId;
	}

	public String getSupplierAddress() {
		return this.supplierAddress;
	}

	public void setSupplierAddress(String supplierAddress) {
		this.supplierAddress = supplierAddress;
	}

	public String getSupplierName() {
		return this.supplierName;
	}

	public void setSupplierName(String supplierName) {
		this.supplierName = supplierName;
	}

	public String getNoteText() {
		return this.noteText;
	}

	public void setNoteText(String noteText) {
		this.noteText = noteText;
	}

	public String getStatus() {
		return this.status;
	}

	public Double getDiscAmt() {
		if (this.discAmt == null) {
			return Double.valueOf(0.0D);
		}
		return this.discAmt;
	}

	public void setDiscAmt(Double discAmt) {
		this.discAmt = discAmt;
	}

	public Double getDiscPer() {
		if (this.discPer == null) {
			return Double.valueOf(0.0D);
		}
		return this.discPer;
	}

	public void setDiscPer(Double discPer) {
		this.discPer = discPer;
	}

	public Double getExciseOh() {
		if (this.exciseOh == null) {
			return Double.valueOf(0.0D);
		}
		return this.exciseOh;
	}

	public void setExciseOh(Double exciseOh) {
		this.exciseOh = exciseOh;
	}

	public Double getFreight() {
		if (this.freight == null) {
			return Double.valueOf(0.0D);
		}
		return this.freight;
	}

	public void setFreight(Double freight) {
		this.freight = freight;
	}

	public Double getNetAmt() {
		if (this.netAmt == null) {
			return Double.valueOf(0.0D);
		}
		return this.netAmt;
	}

	public void setNetAmt(Double netAmt) {
		this.netAmt = netAmt;
	}

	public Double getOtherCharges() {
		if (this.otherCharges == null) {
			return Double.valueOf(0.0D);
		}
		return this.otherCharges;
	}

	public void setOtherCharges(Double otherCharges) {
		this.otherCharges = otherCharges;
	}

	public Double getRoundOff() {
		if (this.roundOff == null) {
			return Double.valueOf(0.0D);
		}
		return this.roundOff;
	}

	public void setRoundOff(Double roundOff) {
		this.roundOff = roundOff;
	}

	public Double getSubTotal() {
		if (this.subTotal == null) {
			return Double.valueOf(0.0D);
		}
		return this.subTotal;
	}

	public void setSubTotal(Double subTotal) {
		this.subTotal = subTotal;
	}

	public Double getTaxAmt() {
		if (this.taxAmt == null) {
			return Double.valueOf(0.0D);
		}
		return this.taxAmt;
	}

	public void setTaxAmt(Double taxAmt) {
		this.taxAmt = taxAmt;
	}

	public Double getTaxPer() {
		if (this.taxPer == null) {
			return Double.valueOf(0.0D);
		}
		return this.taxPer;
	}

	public void setTaxPer(Double taxPer) {
		this.taxPer = taxPer;
	}

	public Double getTotalQty() {
		if (this.totalQty == null) {
			return Double.valueOf(0.0D);
		}
		return this.totalQty;
	}

	public void setTotalQty(Double totalQty) {
		this.totalQty = totalQty;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Date getCreatedOn() {
		return this.createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public String getCreatedBy() {
		return this.createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedOn() {
		return this.updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

	public String getUpdatedBy() {
		return this.updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public short getObjectId() {
		return this.objectId;
	}

	public void setObjectId(short objectId) {
		this.objectId = objectId;
	}

	public List<PurchaseOrderRetLines> getPoRetLines() {
		return this.poRetLines;
	}

	public void setPoRetLines(List<PurchaseOrderRetLines> poRetLines) {
		this.poRetLines = poRetLines;
	}

	public PurchaseOrderRetLines getPoRetLineSelected() {
		return this.poRetLineSelected;
	}

	public void setPoRetLineSelected(PurchaseOrderRetLines poRetLineSelected) {
		this.poRetLineSelected = poRetLineSelected;
	}

	public Date getFromDate() {
		return this.fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return this.toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String update(Connection con) throws SQLException {
		if (getObjectId() < 1) {
			return create(con);
		}
		String sql = "update purchase_order_ret p\nset po_ret_date=?, pr_no=?,pr_date=?,\nbill_no=?,\nbill_date=?,\nchalan_no=?,\nchalan_date=?,\nsupplier_id=?,\nnote_text=?,\nstatus=?,\ncreated_on=?,\ncreated_by=?,\nupdated_on=?,\nupdated_by=?,\nobject_id=nvl(object_id,1) +1,\nsupplier_name=?,\nsupplier_address=?,\nsub_total=?,\ndisc_perc=?,\ndisc_amt=?,\nexcise_oh=?,\ntax_per=?,\ntax_amt=?,\nfreight=?,\nother_charges=?,\nround_off=?,\nnet_amt=?,\ntotal_qty=?\nwhere po_ret_no=? and object_id=?";

		PreparedStatement ps = con.prepareStatement(sql);

		ps.setDate(1, Utils.getSqlDate(this.poRetDate));
		ps.setString(2, this.prNo);
		ps.setDate(3, Utils.getSqlDate(this.prDate));
		ps.setString(4, this.billNo);
		ps.setDate(5, Utils.getSqlDate(this.billDate));
		ps.setString(6, this.chalanNo);
		ps.setDate(7, Utils.getSqlDate(this.chalanDate));
		ps.setString(8, this.supplierId);
		ps.setString(9, this.noteText);
		ps.setString(10, this.status);
		ps.setDate(11, Utils.getSqlDate(this.createdOn));
		ps.setString(12, this.createdBy);
		ps.setDate(13, Utils.getSqlDate(new Date()));
		ps.setString(14, getUserId());
		ps.setString(15, this.supplierName);
		ps.setString(16, this.supplierAddress);
		ps.setDouble(17, Utils.getDouble(this.subTotal));
		ps.setDouble(18, Utils.getDouble(this.discPer));
		ps.setDouble(19, Utils.getDouble(this.discAmt));
		ps.setDouble(20, Utils.getDouble(this.exciseOh));
		ps.setDouble(21, Utils.getDouble(this.taxPer));
		ps.setDouble(22, Utils.getDouble(this.taxAmt));
		ps.setDouble(23, Utils.getDouble(this.freight));
		ps.setDouble(24, Utils.getDouble(this.otherCharges));
		ps.setDouble(25, Utils.getDouble(this.roundOff));
		ps.setDouble(26, Utils.getDouble(this.netAmt));
		ps.setDouble(27, Utils.getDouble(this.totalQty));
		ps.setString(28, this.poRetNo);
		ps.setShort(29, this.objectId);
		int n = ps.executeUpdate();
		for (PurchaseOrderRetLines pl : this.poRetLines) {
			pl.setPoRetNo(getPoRetNo());
			pl.update(con);
		}
		return "Record Updated.";
	}

	public String create(Connection con) throws SQLException {
		if (getPoRetDate() == null) {
			setPoRetDate(new Date());
		}
		String sql = "insert into purchase_order_ret\n  (po_ret_no, po_ret_date,pr_no,\n   pr_date,\n   bill_no,\n   bill_date,\n   chalan_no,\n   chalan_date,\n   supplier_id,\n   note_text,\n   status,\n   created_on,\n   created_by,\n   updated_on,\n   updated_by,\n   object_id,\n   supplier_name,\n   supplier_address,\n   sub_total,\n   disc_perc,\n   disc_amt,\n   excise_oh,\n   tax_per,\n   tax_amt,\n   freight,\n   other_charges,\n   round_off,\n   net_amt,\n   total_qty)\nvalues\n  (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.poRetNo);
		ps.setDate(2, Utils.getSqlDate(this.poRetDate));
		ps.setString(3, this.prNo);
		ps.setDate(4, Utils.getSqlDate(this.prDate));
		ps.setString(5, this.billNo);
		ps.setDate(6, Utils.getSqlDate(this.billDate));
		ps.setString(7, this.chalanNo);
		ps.setDate(8, Utils.getSqlDate(this.chalanDate));
		ps.setString(9, this.supplierId);
		ps.setString(10, this.noteText);
		ps.setString(11, this.status);
		ps.setDate(12, Utils.getSqlDate(new Date()));
		ps.setString(13, getUserId());
		ps.setDate(14, Utils.getSqlDate(this.updatedOn));
		ps.setString(15, this.updatedBy);
		ps.setShort(16, (short) 1);
		ps.setString(17, this.supplierName);
		ps.setString(18, this.supplierAddress);
		ps.setDouble(19, Utils.getDouble(this.subTotal));
		ps.setDouble(20, Utils.getDouble(this.discPer));
		ps.setDouble(21, Utils.getDouble(this.discAmt));
		ps.setDouble(22, Utils.getDouble(this.exciseOh));
		ps.setDouble(23, Utils.getDouble(this.taxPer));
		ps.setDouble(24, Utils.getDouble(this.taxAmt));
		ps.setDouble(25, Utils.getDouble(this.freight));
		ps.setDouble(26, Utils.getDouble(this.otherCharges));
		ps.setDouble(27, Utils.getDouble(this.roundOff));
		ps.setDouble(28, Utils.getDouble(this.netAmt));
		ps.setDouble(29, Utils.getDouble(this.totalQty));
		int n = ps.executeUpdate();
		for (PurchaseOrderRetLines pl : this.poRetLines) {
			pl.setPoRetNo(this.poRetNo);
			pl.update(con);
		}
		return "Record Created.";
	}

	public List<PurchaseOrderReturn> getList(Connection con) throws SQLException {
		String sql = "select po_ret_no, po_ret_date,pr_no,\n       pr_date,\n       bill_no,\n       bill_date,\n       chalan_no,\n       chalan_date,\n       supplier_id,\n       note_text,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       supplier_name,\n       supplier_address,\n       sub_total,\n       disc_perc,\n       disc_amt,\n       excise_oh,\n       tax_per,\n       tax_amt,\n       freight,\n       other_charges,\n       round_off,\n       net_amt,\n       total_qty\n  from purchase_order_ret p\n where 1=1 ";

		boolean doFetchData = false;
		if ((getPoRetNo() != null) && (getPoRetNo().trim().length() > 0)) {
			sql = sql + " and upper(po_ret_no) like  upper('" + getPoRetNo().trim() + "') ";
			doFetchData = true;
		}
		if ((getPrNo() != null) && (getPrNo().trim().length() > 0)) {
			sql = sql + " and upper(pr_no) like  upper('" + getPrNo().trim() + "') ";
			doFetchData = true;
		}
		if ((this.supplierId != null) && (this.supplierId.length() > 0)) {
			sql = sql + " and supplier_id like '" + this.supplierId + "' ";
		}
		if ((this.supplierName != null) && (this.supplierName.length() > 0)) {
			sql = sql + " and supplier_name like '" + this.supplierName + "' ";
		}
		if ((this.chalanNo != null) && (this.chalanNo.length() > 0)) {
			sql = sql + " and chalan_no like '" + this.chalanNo + "' ";
		}
		if ((this.billNo != null) && (this.billNo.length() > 0)) {
			sql = sql + " and bill_no like '" + this.billNo + "' ";
		}
		if (getFromDate() != null) {
			sql = sql + " and inv_ret_date >= to_date('" + Utils.getStrDate(this.fromDate) + "','dd/mm/yyyy') ";
			doFetchData = true;
		}
		if (getToDate() != null) {
			sql = sql + " and inv_ret_date <= to_date('" + Utils.getStrDate(this.toDate) + "','dd/mm/yyyy') ";
			doFetchData = true;
		}
		List<PurchaseOrderReturn> list = new ArrayList();
		if (!doFetchData) {
			return list;
		}
		PreparedStatement ps = con.prepareStatement(sql);

		ResultSet rs = ps.executeQuery();
		while (rs.next()) {
			PurchaseOrderReturn temp = new PurchaseOrderReturn();
			temp.setPoRetNo(rs.getString("po_ret_no"));
			temp.setPoRetDate(rs.getDate("po_Ret_Date"));
			temp.setPrNo(rs.getString("pr_No"));
			temp.setPrDate(rs.getDate("pr_Date"));
			temp.setBillNo(rs.getString("bill_No"));
			temp.setBillDate(rs.getDate("bill_Date"));
			temp.setChalanNo(rs.getString("chalan_No"));
			temp.setChalanDate(rs.getDate("chalan_Date"));
			temp.setSupplierId(rs.getString("supplier_Id"));
			temp.setSupplierName(rs.getString("supplier_Name"));
			temp.setSupplierAddress(rs.getString("supplier_Address"));
			temp.setNoteText(rs.getString("note_Text"));
			temp.setStatus(rs.getString("status"));
			temp.setCreatedOn(rs.getDate("created_On"));
			temp.setCreatedBy(rs.getString("Created_By"));
			temp.setUpdatedOn(rs.getDate("updated_On"));
			temp.setUpdatedBy(rs.getString("updated_By"));
			temp.setObjectId((short) (int) rs.getDouble("object_Id"));
			temp.setSubTotal(Double.valueOf(rs.getDouble("sub_Total")));
			temp.setDiscPer(Double.valueOf(rs.getDouble("disc_Perc")));
			temp.setDiscAmt(Double.valueOf(rs.getDouble("disc_Amt")));
			temp.setExciseOh(Double.valueOf(rs.getDouble("excise_Oh")));
			temp.setTaxPer(Double.valueOf(rs.getDouble("tax_Per")));
			temp.setTaxAmt(Double.valueOf(rs.getDouble("tax_Amt")));
			temp.setFreight(Double.valueOf(rs.getDouble("freight")));
			temp.setOtherCharges(Double.valueOf(rs.getDouble("other_Charges")));
			temp.setRoundOff(Double.valueOf(rs.getDouble("round_Off")));
			temp.setNetAmt(Double.valueOf(rs.getDouble("net_Amt")));
			temp.setTotalQty(Double.valueOf(rs.getDouble("total_Qty")));
			list.add(temp);
		}
		rs.close();
		ps.close();
		return list;
	}

	public String load(Connection con) throws SQLException {
		String sql = "select po_ret_no,po_ret_date,pr_no,\n       pr_date,\n       bill_no,\n       bill_date,\n       chalan_no,\n       chalan_date,\n       supplier_id,\n       note_text,\n       status,\n       created_on,\n       created_by,\n       updated_on,\n       updated_by,\n       object_id,\n       supplier_name,\n       supplier_address,\n       sub_total,\n       disc_perc,\n       disc_amt,\n       excise_oh,\n       tax_per,\n       tax_amt,\n       freight,\n       other_charges,\n       round_off,\n       net_amt,\n       total_qty\n  from purchase_order_ret p\n where p.po_ret_no = ?";

		PreparedStatement ps = con.prepareStatement(sql);
		ps.setString(1, this.poRetNo);

		ResultSet rs = ps.executeQuery();
		if (rs.next()) {
			setPoRetNo(rs.getString("po_ret_no"));
			setPoRetDate(rs.getDate("po_Ret_Date"));
			setPrNo(rs.getString("pr_no"));
			setPrDate(rs.getDate("pr_Date"));
			setBillNo(rs.getString("bill_No"));
			setBillDate(rs.getDate("bill_Date"));
			setChalanNo(rs.getString("chalan_No"));
			setChalanDate(rs.getDate("chalan_Date"));
			setSupplierId(rs.getString("supplier_Id"));
			setSupplierName(rs.getString("supplier_Name"));
			setSupplierAddress(rs.getString("supplier_Address"));
			setNoteText(rs.getString("note_Text"));
			setStatus(rs.getString("status"));
			setCreatedOn(rs.getDate("created_On"));
			setCreatedBy(rs.getString("Created_By"));
			setUpdatedOn(rs.getDate("updated_On"));
			setUpdatedBy(rs.getString("updated_By"));
			setObjectId((short) (int) rs.getDouble("object_Id"));
			setSubTotal(Double.valueOf(rs.getDouble("sub_Total")));
			setDiscPer(Double.valueOf(rs.getDouble("disc_Perc")));
			setDiscAmt(Double.valueOf(rs.getDouble("disc_Amt")));
			setExciseOh(Double.valueOf(rs.getDouble("excise_Oh")));
			setTaxPer(Double.valueOf(rs.getDouble("tax_Per")));
			setTaxAmt(Double.valueOf(rs.getDouble("tax_Amt")));
			setFreight(Double.valueOf(rs.getDouble("freight")));
			setOtherCharges(Double.valueOf(rs.getDouble("other_Charges")));
			setRoundOff(Double.valueOf(rs.getDouble("round_Off")));
			setNetAmt(Double.valueOf(rs.getDouble("net_Amt")));
			setTotalQty(Double.valueOf(rs.getDouble("total_Qty")));
		}
		rs.close();
		ps.close();
		setPoRetLines(PurchaseOrderRetLines.load(con, this.poRetNo));
		return "success";
	}

	public static long getSerialVersionUID() {
		return 1L;
	}

	public String process() {
		processDetails();

		return "success";
	}

	public String processDetails() {
		double totAmt = 0.0D;
		double totQty = 0.0D;
		for (PurchaseOrderRetLines l : this.poRetLines) {
			l.setDiscAmt(Double.valueOf(l.getGrossPp().doubleValue() * l.getDiscPerc().doubleValue() / 100.0D));
			l.setPurPrice(Double.valueOf(l.getGrossPp().doubleValue() - l.getDiscAmt().doubleValue()));
			l.setTaxAmt(l.getPurPrice().doubleValue() * l.getTaxPerc() / 100.0D);
			l.setAmount(Double
					.valueOf((l.getGrossPp().doubleValue() - l.getDiscAmt().doubleValue()) * l.getQty().doubleValue()
							+ l.getTaxAmt() * l.getQty().doubleValue()));
			l.setMpPer(Double.valueOf((l.getMrp().doubleValue() - l.getPurPrice().doubleValue())
					/ (l.getPurPrice().doubleValue() == 0.0D ? 1.0D : l.getPurPrice().doubleValue()) * 100.0D));
			l.setWspPer(Double.valueOf((l.getWsp().doubleValue() - l.getPurPrice().doubleValue())
					/ (l.getPurPrice().doubleValue() == 0.0D ? 1.0D : l.getPurPrice().doubleValue()) * 100.0D));
			totAmt += l.getAmount().doubleValue();
			totQty += l.getQty().doubleValue();
		}
		setTotalQty(Double.valueOf(totQty));
		setSubTotal(Double.valueOf(totAmt));
		setDiscAmt(Double.valueOf(getDiscPer().doubleValue() * getSubTotal().doubleValue() / 100.0D));
		setTaxAmt(Double.valueOf(getTaxPer().doubleValue() * getSubTotal().doubleValue() / 100.0D));
		setNetAmt(Double.valueOf(getSubTotal().doubleValue() - getDiscAmt().doubleValue() + getTaxAmt().doubleValue()
				+ getExciseOh().doubleValue()));
		return "success";
	}

	public int hashCode() {
		int hash = 0;
		hash += (this.poRetNo != null ? this.poRetNo.hashCode() : 0);
		return hash;
	}

	public boolean equals(Object object) {
		if (!(object instanceof PurchaseOrderReturn)) {
			return false;
		}
		PurchaseOrderReturn other = (PurchaseOrderReturn) object;
		if (((this.poRetNo == null) && (other.poRetNo != null))
				|| ((this.poRetNo != null) && (!this.poRetNo.equals(other.poRetNo)))) {
			return false;
		}
		return true;
	}

	public String toString() {
		return "PoRetNo=" + this.poRetNo;
	}
}
